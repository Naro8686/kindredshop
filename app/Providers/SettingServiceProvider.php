<?php

namespace App\Providers;

use App\Models\Order;
use Illuminate\Support\ServiceProvider;

class SettingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        config([
            'services.stripe.key' => setting(Order::STRIPE_TYPE . '.key', config('services.stripe.key')),
            'services.stripe.secret' => setting(Order::STRIPE_TYPE . '.secret', config('services.stripe.secret')),
            'services.stripe.webhook_secret' => setting(Order::STRIPE_TYPE . '.webhook_secret', config('services.stripe.webhook_secret')),

            'coinbase.apiKey' => setting(Order::COINBASE_TYPE . '.key', config('coinbase.apiKey')),
            'coinbase.webhookSecret' => setting(Order::COINBASE_TYPE . '.webhook_secret', config('coinbase.webhookSecret')),
            'coinbase.apiVersion' => setting(Order::COINBASE_TYPE . '.version', config('coinbase.apiVersion')),

            'payop.id' => setting(Order::PAYOP_TYPE . '.id', config('payop.id')),
            'payop.key' => setting(Order::PAYOP_TYPE . '.key', config('payop.key')),
            'payop.secret' => setting(Order::PAYOP_TYPE . '.secret', config('payop.secret')),
            'payop.token' => setting(Order::PAYOP_TYPE . '.token', config('payop.token')),

            'paypal.live.client_id' => setting(Order::PAYPAL_TYPE . '.client_id', config('paypal.live.client_id')),
            'paypal.live.client_secret' => setting(Order::PAYPAL_TYPE . '.client_secret', config('paypal.live.client_secret')),
            'paypal.live.app_id' => setting(Order::PAYPAL_TYPE . '.app_id', config('paypal.live.app_id')),
        ]);
    }
}
