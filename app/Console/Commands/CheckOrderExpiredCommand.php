<?php

namespace App\Console\Commands;

use App\Models\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Throwable;

class CheckOrderExpiredCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {

        try {
            $expired = now()->subDay();
            Order::where(function ($query) {
                $query->where('email', null)->orWhere('session_id', null);
            })->where('status', Order::PENDING)
                ->where('updated_at', '<=', now()->subMinutes(15))
                ->forceDelete();
            Order::whereNotNull('session_id')
                ->where('status', Order::PENDING)
                ->where('updated_at', '<=', $expired)
                ->chunk(100, function ($orders) {
                    foreach ($orders as $order) {
                        $info = "Checked:{$order->id}";
                        if ($status = $order->getOrderApiStatus()) {
                            if ($changed = Order::statusChange($order->session_id, $status, $order->type)) {
                                if ($changed->status === Order::SUCCESS) {
                                    $changed->sendEmailToBuyer();
                                }
                            }
                        } else {
                            $info = "Delete:{$order->id}";
                            $order->delete();
                        }
                        $this->info($info);
                    }
                });
        } catch (Throwable $e) {
            Log::error("Error CheckOrderExpiredCommand: " . $e->getMessage());
        }
    }
}
