<?php

namespace App\Jobs\CoinbaseWebhooks;

use App\Mail\OrderShipped;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Log;
use Shakurov\Coinbase\Models\CoinbaseWebhookCall;
use Throwable;

class HandleConfirmedCharge implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var CoinbaseWebhookCall
     */
    public $webhookCall;

    /**
     * Create a new job instance.
     *
     * @param CoinbaseWebhookCall $webhookCall
     */
    public function __construct(CoinbaseWebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            if (array_keys_exist(['payload', 'event', 'data', 'id'], $this->webhookCall->toArray())) {
                $session_id = $this->webhookCall->payload['event']['data']['id'];
                if ($order = Order::statusChange($session_id, Order::SUCCESS,Order::COINBASE_TYPE)){
                    $order->sendEmailToBuyer();
                }
            }
        } catch (Throwable $throwable) {
            Log::error('Confirmed error: ' . $throwable->getMessage());
        }
    }
}
