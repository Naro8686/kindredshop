<?php

namespace App\Http\Controllers;

use File;
use App\Models\Seo;
use App\Models\Post;
use App\Http\Requests\PostRequest;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Response;
use Throwable;

class PostController extends Controller
{
    public function blog()
    {
        $posts = Post::whereActive(true)->get();
        $seo = Seo::whereSlug(request()->path())->firstOrNew();
        return view('blog.index', compact('posts', 'seo'));
    }

    public function index()
    {
        $posts = Post::all();
        return view('admin.posts.index', compact('posts'));
    }

    public function create()
    {
        return view('admin.posts.create');
    }

    public function store(PostRequest $request)
    {
        try {
            $post = new Post();
            $post->title = $request['title'];
            $post->body = $request['body'];
            if ($request->has('image') && !is_null($request['image']) && $image = $request->file('image')) {
                $fileName = time() . '_' . $image->getClientOriginalName();
                $post->image = 'storage/' . $image->storeAs('posts/uploads', $fileName, 'public');
            }
            if ($request->has('save')) {
                $post->active = 0;
                $message = 'Post saved successfully';
            } else {
                $post->active = 1;
                $message = 'Post published successfully';
            }
            $post->save();
        } catch (Throwable $throwable) {
            return redirect()->back()->with('danger', $throwable->getMessage());
        }
        return redirect()->route('admin.posts.index')->with('success', $message);
    }

    public function show($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();
        $seo = Seo::whereSlug($slug)->firstOrNew();
        return view('blog.post', compact('post', 'seo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Post $post
     * @return Application|Factory|View|Response
     */
    public function edit(Post $post)
    {
        $seo = $post->meta();
        return view('admin.posts.edit', compact('post', 'seo'));
    }

    public function update(PostRequest $request, Post $post)
    {
        try {
            $message = 'Post updated!';
            $post->title = $request['title'];
            $post->body = $request['body'];
            if ($request->has('image') && !is_null($request['image']) && $image = $request->file('image')) {
                if ($post->image) File::delete($post->image);
                $fileName = time() . '_' . $image->getClientOriginalName();
                $post->image = 'storage/' . $image->storeAs('posts/uploads', $fileName, 'public');
            }
            if ($request->has('save')) {
                $post->active = 0;
                $message = 'Post saved successfully';
            } elseif ($request->has('publish')) {
                $post->active = 1;
                $message = 'Post published successfully';
            }
            $post->save();
        } catch (Throwable $throwable) {
            return redirect()->back()->with('danger', $throwable->getMessage());
        }
        return redirect()->route('admin.posts.index')->with('success', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Post $post
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Post $post)
    {
        try {
            if ($image = $post->image) File::delete($image);
            $post->meta()->delete();
            $post->delete();
            $status = 'success';
            $msg = 'The Post has been successfully deleted!';
        } catch (Throwable $e) {
            $status = 'danger';
            $msg = $e->getMessage();
        }
        return redirect()->route('admin.posts.index')->with($status, $msg);
    }
}
