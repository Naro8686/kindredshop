<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\Seo;
use App\Models\Server;
use App\Models\Setting;
use App\Models\Coupon;
use App\Models\Payop;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Middleware\OrderMiddleware;
use Log;
use Stevebauman\Location\Facades\Location;
use Shakurov\Coinbase\Facades\Coinbase;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderShipped;
use Stripe\Exception\ApiErrorException;
use Stripe\StripeClient;
use Srmklive\PayPal\Services\PayPal as PayPalClient;
use Throwable;

class NewsiteController extends Controller
{
    public function index()
    {
        // Получаем сервера только для LOL, пока хардкодим

        $gameId = 1;
        $servers = Server::where('game_id', $gameId)->get();

        // Проверка, установлены ли куки на предупреждение о cookies

        $closedbeware = $_COOKIE['closedbeware'] ?? false;

        $seo = Seo::whereSlug('home')->firstOrNew();

        return view('index', compact('seo', 'servers', 'closedbeware'));

    }

    public function terms()
    {

        $closedbeware = $_COOKIE['closedbeware'] ?? false;

        $seo = Seo::whereSlug(request()->path())->firstOrNew();

        return view('terms-and-conditions', compact('seo', 'closedbeware'));

    }

    public function ajax_products($server_id)
    {
        $session_id = request()->session()->getId();
        $products = Product::where('server_id', $server_id)
            ->orderBy('price', 'asc')
            ->get([
                'id',
                'description',
                'lifetime',
                'price',
                'server_id',
                'title',
                'accounts',
            ]);

        return view('blocks.products_block', compact('products', 'session_id'));

    }

    public function ajax_summary($product_id)
    {

        $product = Product::with('server')->where('id', $product_id)->first([
            'id',
            'description',
            'lifetime',
            'price',
            'title',
            'server_id',
        ]);

        return view('blocks.summary_block', compact('product'));

    }

    public function ajax_checkout_form(Request $request)
    {

        $requestData = $request->all();

        $productId = $requestData['prodinfo']['id'];

        if (isset($requestData['prodinfo']['lifetimeis']) && $requestData['prodinfo']['lifetimeis'] == 'true') {
            $lifetimeIs = true;
        } else {
            $lifetimeIs = false;
        }


        $product = Product::with('server')->where('id', $productId)->first([
            'id',
            'description',
            'lifetime',
            'price',
            'title',
            'server_id',
            'links'
        ]);

        $product['price'] = (float)$product['price'];

        if ($lifetimeIs) {
            $product['price_total'] = $product['price'] + (($product['price'] / 100) * $product['lifetime']);
            $product['lifetimeis'] = 1;
        } else {
            $product['price_total'] = $product['price'];
            $product['lifetimeis'] = 0;
        }

        $settings_received = Setting::all();

        $settings = [];

        foreach ($settings_received as $curSetting) {
            $settings[$curSetting['name']] = $curSetting['val'];
        }
        $link_type = $lifetimeIs ? 'lifetime' : 'basic';
        return view('blocks.checkout_modal', compact('product', 'settings', 'lifetimeIs', 'link_type'));

    }

    public function coupon(Request $request)
    {

        $couponCode = $request->get('code');

        $couponResult = Coupon::where('code', $couponCode)->first();

        if (!$couponResult) {
            return ('false');
        }

        return ($couponResult);

    }

    /**
     * @throws Exception
     */
    public function order(Request $request)
    {

        // Добавляем в массив request данные об ip пользователя

        $request->merge(['ip' => $request->ip(), 'lifetimeis' => false]);

        // Валидация поступивших данных
        $request->validate([
            'prodid' => 'required|exists:products,id|integer',
            'ip' => 'required|unique:black_lists,ip',
            'usermail' => in_array($request['payment'], [Order::PAYPAL_TYPE, Order::STRIPE_TYPE])
                ? 'nullable' : 'required|unique:black_lists,email',
            'lifetimeis' => 'required|boolean',
            'couponis' => 'required|boolean',
            'priceone' => 'required|numeric',
            'pricetotal' => 'required|numeric',
            'quantity' => 'required|integer',
            'payment' => ['required', function ($attribute, $value, $fail) {
                $method = setting("$value.enabled");
                if (is_null($method) || (int)$method === 0)
                    $fail("This ($value) payment method is currently not available.");
            }],
        ], [
            'ip.unique' => OrderMiddleware::MSG,
            'usermail.unique' => OrderMiddleware::MSG
        ]);


        try {
// Проверка купона

            $couponData = false;

            if ($request['couponis']) {
                $couponData = Coupon::where('code', $request['couponcode'])->first();
                if (is_null($couponData)) {
                    $couponPercent = 0;
                } else {
                    if ($couponData->used()) throw new Exception('Coupon expired!'); // Не истек ли купон
                    $couponPercent = (int)$couponData['percent'];
                }
            } else {
                $couponPercent = 0;
            }

            // Проверка наценки за пожизненную гарантию

            if ($request['lifetimeis']) {
                $curProdInfo = Product::where('id', $request['prodid'])->first();
                $lifetimePercent = (int)$curProdInfo['lifetime'];
            } else {
                $lifetimePercent = 0;
            }

            // Подсчет итоговой стоимости заказа

            $priceTotal = (int)$request['priceone'] * (int)$request['quantity'];

            $priceTotal += ($priceTotal / 100) * $lifetimePercent;

            $priceTotal -= ($priceTotal / 100) * $couponPercent;

            $priceTotal = round($priceTotal, 2);

            // Соответствует ли подсчитанная стоимость стоимости, рассчитанной на фронте?

            if (round((float)$request['pricetotal'], 2) !== $priceTotal) {
                throw new Exception('Wrong order price');
            }

            // Добавление заказа в бд
            // @todo проверить количество аккаунтов в наличии
            // @todo сделать max и min в форме количества
            // @todo отрефакторить это
            $checkoutData = DB::transaction(function () use ($priceTotal, $couponData, $request) {

                $session_id = $request->session()->getId();

                $product = Product::whereId($request['prodid'])->first();

                /** @var Order $order */
                $order = $product->orders()
                    ->where('session_id', $session_id)
                    ->where('status', Order::PENDING)
                    ->firstOrNew();

                if ($product->accCount($session_id)) {

                    $order->count = (int)$request['quantity'];

                    if (($product->accCount($session_id) - $order->count) < 0) throw new Exception('OUT OF STOCK', 422);

                    $order->ip = $request->ip();
                    $order->type = $request['payment'];
                    $order->email = $request['usermail'];
                    $location = Location::get($request->ip());
                    $country = ($location && $location->countryCode) ? $location->countryCode : 'US';
                    $order->countryCode = $country;

                    $order->lifetime = $request['lifetimeis'];

                    $order->amount = $priceTotal;

                    if (!is_null($couponData)) {
                        $order->coupon_id = $couponData['id'] ?? null;
                    }

                    $order->push();
                    $charge = self::createCharge($order);

                    $order->session_id = $charge['session_id'];

                    $order->save();

                    return ($charge);

                } else throw new Exception('OUT OF STOCK', 422);
            });

            return ($checkoutData);

        } catch (Throwable|Exception $exception) {
            return response(['message' => $exception->getMessage()], $exception->getCode());
        }

    }

    private static function createCharge(Order $order): ?array
    {
        $response = null;
        $product = $order->product;
        switch ($order->type) {
            case Order::COINBASE_TYPE:
                $charge = Coinbase::createCharge([
                    'name' => $product->title,
                    'description' => $product->description,
                    'local_price' => [
                        'amount' => $order->amount,
                        'currency' => 'USD',
                    ],
                    'pricing_type' => 'fixed_price',
                ]);
                if (!is_null($charge) && $charge['data']) $response = [
                    'session_id' => $charge['data']['id'],
                    'url' => $charge['data']['hosted_url'],
                ];
                break;
            case Order::PAYOP_TYPE:
                $charge = Payop::createCharge($order);
                if (!is_null($charge) && $charge['status'] && $charge['data']) $response = [
                    'session_id' => $charge['data'],
                    'url' => "https://checkout.payop.com/en/payment/invoice-preprocessing/{$charge['data']}",
                ];
                break;
            case Order::STRIPE_TYPE:
                try {
                    $links = $product->getPayLink(Order::STRIPE_TYPE);
                    return [
                        'session_id' => null,
                        'url' => $order->lifetime
                            ? $links->get('lifetime')
                            : $links->get('basic'),
                    ];
                    $stripe = new StripeClient(config('services.stripe.secret'));
                    $amount = $order->amount * 100;
                    $checkoutSession = $stripe->checkout->sessions->create([
                        'payment_method_types' => ['card'],
                        'customer_email' => $order->email,
                        'line_items' => [[
                            'price_data' => [
                                'product_data' => [
                                    'name' => $product->title,
                                    'metadata' => [
                                        'order_id' => $order->id
                                    ]
                                ],
                                'unit_amount' => $amount,
                                'currency' => 'usd',
                            ],
                            'quantity' => 1,
                            'description' => $product->description,
                        ]],
                        'mode' => 'payment',
                        'success_url' => route('order.success'),
                        'cancel_url' => route('order.fail'),
                    ]);
                    $response = [
                        'session_id' => $checkoutSession->id,
                        'url' => $checkoutSession->url,
                    ];
                } catch (Throwable $e) {
                    Log::error($e->getMessage());
                }
                break;
            case Order::PAYPAL_TYPE:
                try {
                    $links = $product->getPayLink(Order::PAYPAL_TYPE);
                    return [
                        'session_id' => null,
                        'url' => $order->lifetime
                            ? $links->get('lifetime')
                            : $links->get('basic'),
                    ];
                    $paypalClient = new PayPalClient;
                    $paypalClient->setApiCredentials(config('paypal'));
                    $token = $paypalClient->getAccessToken();
                    $paypalClient->setAccessToken($token);
                    $charge = $paypalClient->createOrder([
                        "intent" => "CAPTURE",
                        "purchase_units" => [[
                            "amount" => [
                                "currency_code" => "USD",
                                "value" => $order->amount
                            ],
                            'description' => "$product->title $product->description"
                        ]],
                        "application_context" => [
                            'return_url' => route('order.success'),
                            'cancel_url' => route('order.fail'),
                        ]
                    ]);
                    if (!is_null($charge) && isset($charge['id'])) $response = [
                        'session_id' => $charge['id'],
                        'url' => $charge['links'][1]['href'],
                    ];
                } catch (Throwable $e) {
                    Log::error("createCharge:paypal {$e->getMessage()}");
                }
                break;
        }
        return $response;
    }

    public function success(Request $request)
    {
        return redirect()->route('home')->with(['success' => 'SUCCESS! CHECK YOUR E-MAIL']);
    }

    public function fail(Request $request)
    {
        return redirect()->route('home')->with(['danger' => 'TRANSACTION ERROR!']);
    }

    public function testMail(Request $request)
    {
        $request->validate(['email' => ['required', 'email']]);
        try {
            $msg = 'sended1';
            if ($order = \App\Models\Order::first()) {
                // Mail::send('emails.orders.shippedtmp', [], function($m) use ($request) {
                //     $m->to($request['email'], 'kindredshop123');
                // });

                // Mail::to($request['email'])
                //     ->send(new OrderShippedTmp($order));

                Mail::to($request['email'])->later(now()->addSeconds(5),new OrderShipped($order));

                // $data = array('name'=>"Virat Gandhi");

                // Mail::send([], $data, function($message) use ($request) {
                //     $message->to($request['email'], 'Tutorials Point')->subject('Laravel Basic Testing Mail');
                //     $message->from('sales@kindredshop.net', 'Virat Gandhi');
                // });
            } else throw new Exception('order empty');
        } catch (Throwable $exception) {
            $msg = $exception->getMessage();
        }
        return $msg;
    }

}
