<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index()
    {
        $products = Product::whereHas('server.game')->get();
        return view('admin.products.index', compact('products'));
    }

    public function create()
    {
        $game_id = old('game_id');
        $games = Game::all();
        $servers = $game_id && is_numeric($game_id) ? $games->where('id', $game_id)->first()->servers()->get() : $games->first()->servers()->get();
        return view('admin.products.create', compact('games', 'servers'));
    }


    public function store(Request $request)
    {
        $accounts = null;
        if (!is_null($request['accounts'])) {
            $accounts = explode(PHP_EOL, $request['accounts']);
            $accounts = preg_replace('/[\r\n]+/', '', $accounts);
        }
        $request->validate([
            'title' => ['required', 'max:150'],
            'price' => ['required', 'integer'],
            'lifetime' => ['sometimes', 'nullable', 'integer', 'between:0,100'],
            'server_id' => ['required', 'exists:servers,id'],
            'accounts' => ['sometimes', 'nullable', function ($attribute, $value, $fail) use ($accounts) {
                if (!is_null($accounts)) {
                    $item = [];
                    foreach ($accounts as $key => $account) {
                        if (!preg_match('/^(.+):(.+)$/ui', $account)) {
                            $fail($attribute . ' error in line ' . ($key + 1));
                            break;
                        }
                        if (in_array($account, $item)) {
                            $fail($attribute . ' duplicated!');
                            break;
                        }
                        $item[] = $account;
                    }
                }

            }],
        ]);
        $request['accounts'] = $accounts;
        Product::create($request->only(['title', 'description', 'price', 'lifetime', 'accounts', 'server_id', 'links']));
        return redirect()->route('admin.products.index')->with('success', 'Added a new product!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    public function selectServers(Request $request)
    {
        $game = Game::findOrFail($request['game_id']);
        return response(['servers' => $game->servers]);
    }


    public function edit(Product $product)
    {
        return view('admin.products.edit', compact('product'));
    }

    public function update(Request $request, Product $product)
    {
        $accounts = null;
        if (!is_null($request['accounts'])) {
            $accounts = explode(PHP_EOL, $request['accounts']);
            $accounts = preg_replace('/[\r\n]+/', '', $accounts);
        }
        $request->validate([
            'title' => ['required', 'max:150'],
            'price' => ['required', 'integer'],
            'lifetime' => ['sometimes', 'nullable', 'integer', 'between:0,100'],
            'accounts' => ['sometimes', 'nullable', function ($attribute, $value, $fail) use ($accounts) {
                if (!is_null($accounts)) {
                    $item = [];
                    foreach ($accounts as $key => $account) {
                        if (!preg_match('/^(.+):(.+)$/ui', $account)) {
                            $fail($attribute . ' error in line ' . ($key + 1));
                            break;
                        }
                        if (in_array($account, $item)) {
                            $fail($attribute . ' duplicated!');
                            break;
                        }
                        $item[] = $account;
                    }
                }

            }],
        ]);
        $request['accounts'] = $accounts;
        $product->update($request->only(['title', 'description', 'price', 'lifetime', 'accounts', 'links']));
        return redirect()->route('admin.products.index')->with('success', 'Change saved!');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('admin.products.index')->with('success', 'The product has been successfully deleted!');
    }
}
