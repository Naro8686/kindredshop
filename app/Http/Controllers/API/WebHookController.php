<?php

namespace App\Http\Controllers\API;

use App\Models\Order;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Srmklive\PayPal\Services\PayPal as PayPalClient;
use Srmklive\PayPal\Traits\PayPalAPI\WebHooks;
use Throwable;

class WebHookController extends Controller
{
    use WebHooks;

    /**
     * Handle the incoming request.
     *
     * @param string $type
     * @param Request $request
     * @return void
     */
    public function __invoke(string $type, Request $request)
    {
        switch ($type) {
            case Order::STRIPE_TYPE:
                $this->stripe();
                break;
            case Order::PAYPAL_TYPE:
                $this->paypal($request);
                break;
            default:
                http_response_code(404);
                break;
        }
    }

    private function stripe()
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
// You can find your endpoint's secret in your webhook settings
        $endpoint_secret = config('services.stripe.webhook_secret');

        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];

        $event = null;

        try {
            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch (\UnexpectedValueException $e) {
            // Invalid payload
            http_response_code(400);
            exit();
        } catch (\Stripe\Exception\SignatureVerificationException $e) {
            // Invalid signature
            http_response_code(400);
            exit();
        }

        function fulfill_order($session)
        {
            $changed = Order::statusChange($session->id, Order::SUCCESS, Order::STRIPE_TYPE);
            if (!is_null($changed)) $changed->sendEmailToBuyer();
            else Log::error("Order not fount Success" . json_encode($session));
        }

        function create_order($session)
        {
            // TODO fill me in
        }

        function email_customer_about_failed_payment($session)
        {
            $changed = Order::statusChange($session->id, Order::DECLINE, Order::STRIPE_TYPE);
            if (is_null($changed)) Log::error("Order not fount Fail" . json_encode($session));
        }

// Handle the checkout.session.completed event
        if ($event->type == 'checkout.session.completed') {
            $session = $event->data->object;

            // Fulfill the purchase...
            fulfill_order($session);
        }

        switch ($event->type) {
            case 'checkout.session.completed':
                $session = $event->data->object;

                // Save an order in your database, marked as 'awaiting payment'
                create_order($session);

                // Check if the order is paid (for example, from a card payment)
                //
                // A delayed notification payment will have an `unpaid` status, as
                // you're still waiting for funds to be transferred from the customer's
                // account.
                if ($session->payment_status == 'paid') {
                    // Fulfill the purchase
                    fulfill_order($session);
                }

                break;

            case 'checkout.session.async_payment_succeeded':
                $session = $event->data->object;

                // Fulfill the purchase
                fulfill_order($session);

                break;

            case 'checkout.session.async_payment_failed':
                $session = $event->data->object;

                // Send an email to the customer asking them to retry their order
                email_customer_about_failed_payment($session);

                break;
        }

        http_response_code(200);
    }

    private function paypal(Request $request)
    {
        try {
            Log::info(json_encode($request->all()));
            $status = Order::PENDING;
            $session_id = $request->input('resource.id');
            switch ($request->get('event_type')) {
                case "CHECKOUT.ORDER.APPROVED":
                    $paypalClient = new PayPalClient;
                    $paypalClient->setApiCredentials(config('paypal'));
                    $token = $paypalClient->getAccessToken();
                    $paypalClient->setAccessToken($token);
                    $paypalClient->capturePaymentOrder($session_id);
                    break;
                case "PAYMENT.CAPTURE.COMPLETED":
                    $session_id = $request->input('resource.supplementary_data.related_ids.order_id', $session_id);
                    $status = Order::SUCCESS;
                    break;
                case "CHECKOUT.ORDER.DENIED":
                case "PAYMENT.CAPTURE.DENIED":
                case "PAYMENT.CAPTURE.REVERSED":
                case "PAYMENT.CAPTURE.REFUNDED":
                    $status = Order::DECLINE;
                    break;
            }
            if (!is_null($session_id) && $status !== Order::PENDING) {
                $changed = Order::statusChange($session_id, $status, Order::PAYPAL_TYPE);
                if (!is_null($changed) && $changed->status === Order::SUCCESS) $changed->sendEmailToBuyer();
                elseif (is_null($changed)) Log::error("Order not fount Success" . json_encode($request->all()));
            }
            http_response_code(200);
        } catch (Exception|Throwable $exception) {
            Log::error("paypal webhook err {$exception->getMessage()}");
            http_response_code(500);
        }
    }
}
