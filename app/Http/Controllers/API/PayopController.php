<?php

namespace App\Http\Controllers\API;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Throwable;

class PayopController extends Controller
{
    public function callback(Request $request)
    {
        $code = 500;
        $status = Order::PENDING;
        try {
            if ($session_id = $request->input("invoice.id")) { // wtf?
                $stat = $request->input("invoice.status");
                if (in_array($stat, [1, 5])) {
                    $status = ((int)$stat === 1)
                        ? Order::SUCCESS
                        : Order::DECLINE;
                }
                if ($order = Order::statusChange($session_id, $status, "payop")) { // wtf_2?
                    if ($order->status === Order::SUCCESS) {
                        $order->sendEmailToBuyer();
                    }
                    $code = 200;
                }
            }
        } catch (Throwable $exception) {
            $code = 500;
            Log::error("PayOp callback: " . $exception->getMessage());
        }
        http_response_code($code);
    }

    public function refund(Request $request)
    {
        Log::info("Payop refund \r\n" . json_encode($request->all()));
    }
}
