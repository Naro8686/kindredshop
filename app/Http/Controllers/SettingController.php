<?php

namespace App\Http\Controllers;

use App\Http\Requests\SettingRequest;
use App\Models\Setting;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;

class SettingController extends Controller
{

    public function index()
    {
        return view('admin.setting.index');
    }

    public function store(SettingRequest $request)
    {
        try {
            $data = $request->validated();
            foreach (Arr::dot($data) as $key => $val) {
                Setting::add($key, $val, Setting::getDataType($key));
            }
            return redirect()->back()->with('success', 'Settings has been saved.');
        } catch (ValidationException $e) {
            return redirect()->back()->with('danger', $e->getMessage());
        }
    }
}
