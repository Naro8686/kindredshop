<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Throwable;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard(Request $request)
    {
        if ($request->ajax()) {
            try {
                $format = "Y-m-d";
                $unit = $request->get('unit', 'hour');
                list($min, $max) = array_pad($request->get('dates',
                    [Carbon::now()->subDay()->format($format), Carbon::now()->format($format)]),
                    2,
                    Carbon::now()->format($format));
                $from = Carbon::createFromFormat($format, $min)->toDateString();
                $to = Carbon::createFromFormat($format, $max)->toDateString();

                if (!in_array($unit, ['hour', 'day', 'week', 'month', 'year'])) {
                    throw new Exception('Wrong unit format');
                }
                switch ($unit) {
                    case 'day':
                        $sqlFormat = '%Y-%m-%d';
                        break;
                    case 'week':
                        $sqlFormat = '%x-%v';
                        break;
                    case 'month':
                        $sqlFormat = '%Y-%m';
                        break;
                    case 'year':
                        $sqlFormat = '%Y';
                        break;
                    case 'hour':
                    default:
                        $sqlFormat = '%Y-%m-%d %H';
                        break;
                }

                $purchases = Order::select([
                    DB::raw("IFNULL(SUM(`orders`.`amount`),0) AS 'sum'"),
                    DB::raw("DATE_FORMAT(`orders`.`created_at`, '$sqlFormat') AS 'unit'")
                ])->where('status', Order::SUCCESS)->where(function ($query) use ($from, $to) {
                    return $query
                        ->whereDate('created_at', '>=', $from)
                        ->whereDate('created_at', '<=', $to);
                })->groupBy("unit")->orderBy("unit");

                $orders = Order::select([
                    DB::raw("COUNT(id) AS 'total'"),
                    DB::raw("IFNULL(SUM(CASE WHEN status = " . Order::SUCCESS . " THEN 1 ELSE 0 END),0) AS 'completed'"),
                    DB::raw("IFNULL(SUM(CASE WHEN status = " . Order::SUCCESS . " THEN `orders`.`amount` ELSE 0 END),0) AS 'sum'")
                ])
                    ->whereNotNull('email')
                    ->where(function ($query) use ($from, $to) {
                        return $query
                            ->whereDate('created_at', '>=', $from)
                            ->whereDate('created_at', '<=', $to);
                    })->limit(1)->first();

                $populars = Product::whereHas('orders', function ($query) {
                    return $query->whereNotNull('email');
                })
                    ->with(['server'])
                    ->withCount('orders')
                    ->orderByDesc('orders_count')
                    ->limit(5)
                    ->get();

                return response()->json([
                    'purchases' => OrderResource::collection($purchases->get()),
                    'populars' => $populars,
                    'orders' => $orders
                ]);
            } catch (Exception|Throwable $exception) {
                return response()->json([
                    'errors' => [
                        'msg' => $exception->getMessage(),
                        'line' => $exception->getLine(),
                        'trace' => $exception->getTrace(),
                    ],
                ], 500);
            }
        }
        return view('admin.dashboard');
    }
}
