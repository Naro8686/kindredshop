<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use Illuminate\Http\Request;
use Throwable;

class CouponController extends Controller
{

    public function index()
    {
        $coupons = Coupon::get();
        return view('admin.coupons.index', compact('coupons'));
    }

    public function create()
    {
        return view('admin.coupons.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required|string|max:20|unique:coupons',
            'percent' => 'required|integer|min:1|max:100',
        ]);
        $request['unlimited'] = $request->has('unlimited');
        Coupon::create($request->only(['code', 'percent', 'unlimited']));
        return redirect()->route('admin.coupons.index')->with('success', 'Coupon add!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Coupon $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        //
    }


    public function edit(Coupon $coupon)
    {
        return view('admin.coupons.edit', compact('coupon'));
    }

    public function update(Request $request, Coupon $coupon)
    {
        $request->validate([
            'code' => 'required|string|max:20|unique:coupons,code,' . $coupon->id,
            'percent' => 'required|integer|min:1|max:100',
        ]);
        $request['unlimited'] = $request->has('unlimited');
        $coupon->update($request->only(['code', 'percent','unlimited']));
        return redirect()->route('admin.coupons.index')->with('success', 'Changed!');
    }

    public function destroy(Coupon $coupon)
    {
        $status = 'success';
        $msg = 'The coupon has been successfully deleted!';
        try {
            $coupon->delete();
        } catch (Throwable $e) {
            $msg = 'Oops! ' . $e->getMessage();
            $status = 'danger';
        }
        return redirect()->route('admin.coupons.index')->with($status, $msg);
    }

    public function generate($length = 10): string
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";
        for ($i = 0; $i < $length; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        return $res;
    }

    public function autoGenerate(Request $request)
    {
        $request->validate([
            'percent' => 'required|integer|min:1|max:100',
        ]);
        $status = 'success';
        $msg = 'Generated random code!';
        try {
            $request['code'] = $this->generate();
            Coupon::create($request->only(['code', 'percent']));
        } catch (Throwable $e) {
            $msg = 'Oops! ' . $e->getMessage();
            $status = 'danger';
        }
        return redirect()->route('admin.coupons.index')->with($status, $msg);
    }
}
