<?php

namespace App\Http\Controllers;

use App\Models\BlackList;
use Illuminate\Http\Request;
use Throwable;

class BlackListController extends Controller
{

    public function index()
    {
        $bans = BlackList::get();
        return view('admin.blackList.index', compact('bans'));
    }

    public function create()
    {
        return view('admin.blackList.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'ip' => 'required_without:email|nullable|ip|unique:black_lists',
            'email' => 'required_without:ip|nullable|email|unique:black_lists',
        ]);
        BlackList::create($request->only(['email', 'ip']));
        return redirect()->back()->with('success', 'Blocked!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $ban = BlackList::findOrFail($id);
        $status = 'success';
        $msg = 'Ban removed!';
        try {
            $ban->delete();
        } catch (Throwable $e) {
            $msg = 'Oops! ' . $e->getMessage();
            $status = 'danger';
        }
        return redirect()->back()->with($status, $msg);
    }
}
