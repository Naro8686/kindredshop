<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\Server;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ServerController extends Controller
{
    public function index()
    {
        $servers = Server::all();
        return view('admin.servers.index', compact('servers'));
    }

    public function create()
    {
        $games = Game::all();
        return view('admin.servers.create', compact('games'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'max:10', Rule::unique('servers')->where(function ($query) use ($request) {
                return $query->where('name', $request['name'])
                    ->where('game_id', $request['game_id']);
            })],
            'game_id' => ['required', 'exists:games,id', 'integer'],
        ]);
        Server::create($request->only(['name', 'game_id', 'description']));
        return redirect()->route('admin.servers.index')->with('success', 'Added a new server!');
    }

    public function edit(Server $server)
    {
        $games = Game::all();
        return view('admin.servers.edit', compact('server', 'games'));
    }

    public function update(Request $request, Server $server)
    {
        $request->validate([
            'name' => ['required', 'max:10', Rule::unique('servers')->where(function ($query) use ($request,$server) {
                return $query->where('id','<>', $server->id)
                    ->where('name', $request['name'])
                    ->where('game_id', $request['game_id']);
            })],
            'game_id' => ['required', 'exists:games,id', 'integer'],
        ]);
        $server->update($request->only(['name', 'game_id', 'description']));
        return redirect()->route('admin.servers.index')->with('success', 'The name of the server has changed!');
    }

    public function destroy(Server $server)
    {
        $server->delete();
        return redirect()->route('admin.servers.index')->with('success', 'The server has been successfully deleted!');
    }
}
