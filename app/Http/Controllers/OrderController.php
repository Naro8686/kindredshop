<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Throwable;

class OrderController extends Controller
{

    public function index()
    {
        ini_set('memory_limit', '-1');
        if (\request()->ajax()) {
            return datatables()
                ->of(Order::with(['product', 'coupon', 'product.server'])->whereNotNull('orders.email')->select('orders.*'))
                ->editColumn('id', function ($order) {
                    return "<strong>{$order->id}</strong>";
                })
                ->editColumn('product.server.name', function ($order) {
                    return $order->product && $order->product->server ? $order->product->server->name : 'unknown';
                })
                ->editColumn('product.title', function ($order) {
                    return $order->product ? $order->product->title : 'unknown';
                })
                ->editColumn('details', function ($order) {
                    return "<pre class='mb-0 pl-2 pr-2 text-gray-700'>{$order->accountsToString()}</pre>";
                })
                ->editColumn('ip', function ($order) {
                    $ip = '';
                    if (!is_null($order->ip)):
                        $flag = $order->flag('img-fluid img-thumbnail w-100');
                        $ip = "<div class='d-flex align-items-center justify-content-between flex-row'>
                                            <span>{$order->ip}</span>
                                            <div style='width: 25px;margin-left: 10px'>$flag</div>
                                        </div>";
                    endif;
                    return $ip;
                })
                ->editColumn('status', function ($order) {
                    return $order->statusHtml();
                })
                ->editColumn('type', function ($order) {
                    return $order->type ? ucfirst($order->type) : 'undefined';
                })
                ->editColumn('amount', function ($order) {
                    $coupon = '';
                    if (!is_null($order->coupon)):
                        $coupon = "<span class='badge badge-info float-right ml-2'>
                        <i class='fa fa-ticket-alt'></i> {$order->coupon->percent}% </span>";
                    endif;
                    return "<p class='mb-0 d-flex align-items-center justify-content-center'>{$order->amount}$ $coupon</p>";
                })
                ->editColumn('created_at', function ($order) {
                    return "<p class='h6 text-nowrap mb-0'>{$order->dataTimeMsc()}</p>";
                })
                ->addColumn('action', function ($order) {
                    $linkShow = route('admin.orders.show', $order->id);
                    $linkDelete = route('admin.orders.destroy', $order->id);
                    $disabled = ((int)$order['status'] !== Order::PENDING) ? 'disabled' : '';
                    $blackListForm = '';
                    if (!is_null($order->email) || !is_null($order->ip)):
                        if ($ban = $order->isBanned()):
                            $token = csrf_token();
                            $linkBlackListDelete = route('admin.black-list.destroy', $ban->id);
                            $blackListForm = "<form action='$linkBlackListDelete' method='POST'>
                                                    <input type='hidden' name='_token' value='$token'>
                                                    <input type='hidden' name='_method' value='DELETE'>
                                                    <button title='lock' class='btn btn-sm btn-danger'><i
                                                            class='fa fa-ban'></i></button>
                                                </form>";
                        else:
                            $linkBlackListStore = route('admin.black-list.store');
                            $blackListForm = "<form action='$linkBlackListStore' method ='POST'>
                                                    <input type='hidden' name='email' value='{$order->email}'>
                                                    <input type='hidden' name='ip' value='{$order->ip}'>
                                                    <button title='unlock' class='btn btn-sm btn-success'><i
                                                            class='fa fa-check-circle'></i></button>
                                                </form>";
                        endif;
                    endif;


                    return "<div class='d-flex justify-content-between' role='group'>
                                        <button type='button' class='btn btn-sm btn-danger mr-1' data-toggle='modal'
                                                data-target='#confirmModal'
                                                data-url='$linkDelete'><i class='fa fa-trash'></i>
                                        </button>
                                        <a title='Check status' href='$linkShow' type='button' class='$disabled btn btn-sm btn-info mr-1'>
                                            <i class='fa fa-info-circle'></i>
                                        </a>
                                        $blackListForm
                                    </div>";
                })
                ->rawColumns(['id', 'details', 'ip', 'status', 'amount', 'created_at', 'action'])
                ->make(true);
        }
        return view('admin.orders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function show(Order $order)
    {
        $msg = "Status has not changed";
        $status = $order->getOrderApiStatus();
        if ($status !== $order->status && !is_null($order->session_id)) {
            if ($changed = Order::statusChange($order->session_id, $status, $order->type)) {
                if ($changed->status === Order::SUCCESS) {
                    $msg = "Order N{$order->id} approved.\r\n";
                    $changed->sendEmailToBuyer();
                    $msg .= "Product has been sent to email";
                } elseif ($changed->status === Order::DECLINE) {
                    $msg = "Order №{$order->id} declined";
                }
            }
        }
        return redirect()->back()->with('success', $msg);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Order $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Order $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    public function destroy(Order $order)
    {
        $status = 'success';
        $msg = 'The order has been successfully deleted!';
        try {
            if (!is_null($order->coupon)) $order->coupon->delete();
            $order->delete();
        } catch (Throwable $e) {
            $msg = 'Oops! ' . $e->getMessage();
            $status = 'danger';
        }
        return redirect()->route('admin.orders.index')->with($status, $msg);
    }

    public function success(Request $request)
    {
        return redirect()->route('payment.index')->with(['success' => 'SUCCESS! CHECK YOUR E-MAIL']);
    }

    public function fail(Request $request)
    {
        return redirect()->route('payment.index')->with(['danger' => 'TRANSACTION ERROR!']);
    }
}
