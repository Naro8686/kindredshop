<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\Post;

class SiteMapController extends Controller
{
    /**
     * Provision a new web server.
     *
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $games = Game::get('slug');
        $posts = Post::whereActive(true)->get('slug');
        $version = '<?xml version="1.0" encoding="UTF-8"?>';
        return response()->view('sitemap', compact('version', 'games', 'posts'))->header('Content-Type', 'text/xml');
    }
}
