<?php

namespace App\Http\Controllers;

use App\Models\Seo;
use Illuminate\Http\Request;

class MetaController extends Controller
{

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id): \Illuminate\Http\RedirectResponse
    {
        $seo = Seo::findOrFail($id);
        $request->validate([
            'seo.title' => 'required|string|max:250'
        ]);
        $seo->update($request['seo']);
        return redirect()->back()->with('success', 'Save meta tags!');
    }

    public function destroy($id)
    {
        //
    }
}
