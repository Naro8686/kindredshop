<?php

namespace App\Http\Controllers;

use App\Models\Seo;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home()
    {
        $seo = Seo::whereSlug('home')->firstOrCreate(['slug' => 'home']);
        return view('admin.pages.home', compact('seo'));
    }

    public function reviews()
    {
        $seo = Seo::whereSlug('reviews')->firstOrCreate(['slug' => 'reviews']);
        return view('admin.pages.reviews', compact('seo'));
    }

    public function blog()
    {
        $seo = Seo::whereSlug('blog')->firstOrCreate(['slug' => 'blog']);
        return view('admin.pages.blog', compact('seo'));
    }

    public function terms()
    {
        $seo = Seo::whereSlug('terms-and-conditions')->firstOrCreate(['slug' => 'terms-and-conditions']);
        return view('admin.pages.terms', compact('seo'));
    }
}
