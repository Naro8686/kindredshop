<?php

namespace App\Http\Controllers;

use App\Models\Game;
use Illuminate\Http\Request;
use Throwable;

class GameController extends Controller
{

    public function index()
    {
        $games = Game::all();
        return view('admin.games.index', compact('games'));
    }

    public function create()
    {
        return view('admin.games.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:games|max:150',
        ]);
        Game::create($request->only(['name']));
        return redirect()->route('admin.games.index')->with('success', 'Added a new game!');
    }

    public function edit(Game $game)
    {
        $seo = $game->meta();
        return view('admin.games.edit', compact('game', 'seo'));
    }

    public function update(Request $request, Game $game)
    {
        $request->validate([
            'name' => "required|unique:games,name,{$game->id}|max:150",
        ]);
        $game->update($request->only(['name']));
        return redirect()->route('admin.games.index')->with('success', 'The name of the game has changed!');
    }

    public function destroy(Game $game)
    {
        try {
            $game->meta()->delete();
            $game->delete();
        } catch (Throwable $e) {
        }
        return redirect()->route('admin.games.index')->with('success', 'The game has been successfully deleted!');
    }
}
