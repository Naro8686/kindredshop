<?php

namespace App\Http\Controllers;

use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CkeditorController extends Controller
{
    public function __invoke(Request $request)
    {
        $image = $request->file('upload');
        $success = true;
        $validator = Validator::make(['upload' => $image], [
            'upload' => ['required', 'image', 'mimes:jpeg,jpg,png,gif,svg', 'max:2048'],
        ]);
        if ($validator->fails())
            return ['uploaded' => $success = false, 'error' => ['message' => $validator->errors()->getMessages()['upload'][0]]];
                    $extension = $image->getClientOriginalExtension();
        $image_name = $image->getClientOriginalName() . '_' . time() . '.' . $extension;
        $path = 'storage/' . $request->file('upload')->storeAs('tmp', $image_name, 'public');
        $urlPath = asset($path);
        return ['uploaded' => $success, 'fileName' => $image_name, 'url' => $urlPath];
    }
}
