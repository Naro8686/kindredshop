<?php

namespace App\Http\Requests;

use App\Models\Setting;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Setting::getValidationRules();
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $stripe = $this['stripe'] ?? [];
        $stripe['enabled'] = $this->has('stripe.enabled');

        $coinbase = $this['coinbase'] ?? [];
        $coinbase['enabled'] = $this->has('coinbase.enabled');

        $payop = $this['payop'] ?? [];
        $payop['enabled'] = $this->has('payop.enabled');

        $paypal = $this['paypal'] ?? [];
        $paypal['enabled'] = $this->has('paypal.enabled');

        $this->merge([
            'stripe' => $stripe,
            'coinbase' => $coinbase,
            'payop' => $payop,
            'paypal' => $paypal,
        ]);
    }
}
