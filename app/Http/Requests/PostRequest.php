<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $unique = $this->request->has('post_id') ? "unique:posts,title,{$this->post_id}" : "unique:posts,title";
        return [
            'image' => ['mimes:jpeg,jpg,png,gif', 'sometimes', 'nullable', 'max:2048'],
            'title' => ['required', $unique, 'max:255'],
            'body' => ['required']
        ];
    }
}
