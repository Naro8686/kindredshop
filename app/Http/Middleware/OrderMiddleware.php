<?php

namespace App\Http\Middleware;

use App\Models\BlackList;
use Closure;
use Illuminate\Http\Request;

class OrderMiddleware
{
    public const MSG = 'You have been blocked by the site administration!';
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $ip = $request->ip();
        if ($request->has('email')) $ban = BlackList::where('ip', $ip)
            ->orWhere('email', $request['email']);
        else $ban = BlackList::where('ip', $ip);
        if ($ban->exists()) {
            abort('403', self::MSG);
        }
        return $next($request);
    }
}
