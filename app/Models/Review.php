<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Review
 *
 * @property int $id
 * @property string $name
 * @property int $rating
 * @property string|null $country
 * @property string|null $comment
 * @property int $game_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Game $game
 * @method static \Illuminate\Database\Eloquent\Builder|Review newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Review newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Review query()
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereGameId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Review whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $published
 * @method static \Illuminate\Database\Eloquent\Builder|Review wherePublished($value)
 */
class Review extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'rating', 'country', 'comment', 'game_id','published'];
    protected $casts = [
        'rating' => 'integer'
    ];

    public function game()
    {
        return $this->belongsTo(Game::class);
    }

    public function flag($class = ''): string
    {
        $country = is_null($this->country) ? 'US' : $this->country;
        return "<img src='https://www.countryflags.io/$country/shiny/64.png' class='$class' alt='$country'>";
    }
    public static function meta()
    {
        return Seo::where('slug', 'reviews')->firstOrCreate(['slug' => 'reviews']);
    }
}
