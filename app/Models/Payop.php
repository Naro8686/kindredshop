<?php

namespace App\Models;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Log;
use Throwable;

/**
 * App\Models\Payop
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Payop newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Payop newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Payop query()
 * @mixin \Eloquent
 */
class Payop extends Model
{
    use HasFactory;

    public static function createCharge(Order $order)
    {
        try {
            $amount = $order->amount;
            $currency = 'USD';
            $id = $order->id;
            $secretKey = config('payop.secret');
            $publicKey = config('payop.key');
            $token = config('payop.token');
            $signature = hash('sha256', implode(':', [$amount, $currency, $id, $secretKey]));
            $client = new Client();
            $response = $client->request('POST', 'https://payop.com/v1/invoices/create', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
                'body' => json_encode([
                    'publicKey' => $publicKey,
                    'resultUrl' => route('order.success'),
                    'failPath' => route('order.fail'),
                    'language' => 'en',
                    'signature' => $signature,
                    'order' => [
                        'id' => $id,
                        'amount' => $amount,
                        'currency' => $currency,
                        'items' => [
                            "id" => $order->product->id,
                            "name" => $order->product->title,
                            "price" => $order->product->price
                        ]
                    ],
                    'payer' => [
                        'email' => $order->email
                    ]
                ])
            ]);
            if ($response->getStatusCode() == 200) return json_decode($response->getBody()->getContents(), true);
        } catch (Throwable $e) {
            Log::error("createCharge " . $e->getMessage());
        }
        return null;
    }

    public static function getCharge($id)
    {
        $client = new Client();
        try {
            $response = $client->request('GET', "https://payop.com/v1/invoices/$id", [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json'
                ],
            ]);
            if ($response->getStatusCode() == 200) return json_decode($response->getBody()->getContents(), true)['data'];
        } catch (Throwable $e) {
            Log::error("getCharge " . $e->getMessage());
        }

        return null;
    }
}
