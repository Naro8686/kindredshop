<?php

namespace App\Models;

use DB;
use Exception;
use Log;
use Coinbase;
use Throwable;
use Stripe\StripeClient;
use App\Mail\OrderShipped;
use App\Mail\AdminOrderShipped;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Srmklive\PayPal\Services\PayPal as PayPalClient;

/**
 * App\Models\Order
 *
 * @property int $id
 * @property int $status
 * @property string|null $session_id
 * @property int $lifetime
 * @property string|null $email
 * @property string|null $ip
 * @property array|null $details
 * @property string $amount
 * @property string $type
 * @property int $count
 * @property int|null $product_id
 * @property int|null $coupon_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property string|null $countryCode
 * @property string|null $card_brand
 * @property string|null $card_last_four
 * @property string|null $trial_ends_at
 * @property-read \App\Models\Coupon|null $coupon
 * @property-read \App\Models\Product|null $product
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Query\Builder|Order onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCardBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCardLastFour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCouponId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereDetails($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereLifetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereSessionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereTrialEndsAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Order withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Order withoutTrashed()
 * @mixin \Eloquent
 */
class Order extends Model
{
    use HasFactory, SoftDeletes;

    const PENDING = 0;
    const SUCCESS = 1;
    const DECLINE = 2;
    const STRIPE_TYPE = "stripe";
    const COINBASE_TYPE = "coinbase";
    const PAYOP_TYPE = "payop";
    const PAYPAL_TYPE = "paypal";
    protected $fillable = ['product_id', 'session_id', 'email', 'count', 'lifetime', 'amount', 'status', 'details', 'type', 'ip', 'countryCode', 'coupon_id'];
    protected $casts = ['details' => 'array'];
    protected $dates = ['deleted_at'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class)->withTrashed();
    }

    public function amountHtml()
    {

        return preg_replace('/^(\d+)(\.\d+)$/is', '&dollar;$1<span class="text-sm">$2</span>', number_format($this->amount, 2, ".", ""));
    }

    public function checkoutData(): array
    {
        $product = $this->product;
        $accCount = $product->accCount($this->session_id);
        $info = empty($product->description)
            ? $product->title
            : "{$product->title} - {$product->description}";
        return [
            'id' => $this->id,
            'product_id' => $this->product_id,
            'info' => "{$this->product->server->name} $info",
            'min' => (int)(bool)$accCount,
            'max' => $accCount,
            'amountHtml' => $this->amountHtml(),
        ];
    }

    public function statusHtml()
    {
        $html = '<button class="order-status btn btn-warning btn-sm btn-block">Pending</button>';
        switch ($this->status) {
            case self::SUCCESS:
                $html = '<button class="order-status btn btn-success btn-sm btn-block">Complete</button>';
                break;
            case self::DECLINE:
                $html = '<button class="order-status btn btn-danger btn-sm btn-block">Decline</button>';
                break;
        }
        return $html;
    }

    public function accountsToString(): string
    {
        return is_array($this->details) && isset($this->details['accounts']) ? implode(PHP_EOL, $this->details['accounts']) : '';
    }

    public function countAccounts(): int
    {
        return is_array($this->details) && isset($this->details['accounts']) ? count($this->details['accounts']) : 0;
    }


    /**
     * @param string $session_id
     * @param int $status
     * @param string $type
     * @return Order|null
     */
    public static function statusChange(string $session_id, int $status = Order::PENDING, string $type = Order::COINBASE_TYPE): ?Order
    {
        $order = null;
        try {
            $order = DB::transaction(function () use ($session_id, $status, $type) {
                $updated = false;
                $order = Order::whereSessionId($session_id)
                    ->where('type', $type)
                    ->where('status', Order::PENDING)
                    ->first();
                if (!is_null($order)) {
                    if ($order->status !== $status) {
                        if ($status === Order::SUCCESS) {
                            $product = $order->product;
                            $order_acc = $product->getLastAccounts($order->count);
                            $product->accounts = array_diff($product->accounts, $order_acc);
                            if ($updated = $order->update([
                                'status' => $status,
                                'details' => ['accounts' => $order_acc],
                            ])) $product->save();
                        } else $updated = $order->update(['status' => $status]);
                    }
                }
                return $updated ? $order : null;
            });
        } catch (Throwable $throwable) {
            Log::error('statusChange error: ' . $throwable->getMessage());
        }
        return $order;
    }

    public function sendEmailToBuyer(): bool
    {
        $sent = false;
        $email = $this->email;
        if (!is_null($email) && $this->status === self::SUCCESS) {
            try {
                Mail::to($email)->later(now()->addSeconds(5), new OrderShipped($this));
                $sent = true;
                if ($appEmail = config('mail.from.address')) {
                    Mail::to($appEmail)->later(now()->addSeconds(10), new AdminOrderShipped($this));
                }
            } catch (Throwable $exception) {
                Log::error("ERROR Send ACC Order ID: $this->id \r\n" . $exception->getMessage());
            }
        }
        return $sent;
    }

    public function flag($class = ''): string
    {
        $country = strtolower($this->countryCode) ?? 'us';
        return "<img src='https://flagicons.lipis.dev/flags/4x3/$country.svg' class='$class' alt='$country'>";
    }

    public function isBanned()
    {
        $ip = $this->ip ?? 'NULL';
        $email = $this->email;
        if (!is_null($email)) $ban = BlackList::where('ip', $ip)
            ->orWhere('email', $email);
        else $ban = BlackList::where('ip', $ip);
        return $ban->first();
    }

    public function getOrderApiStatus(): int
    {
        $status = $this->status;
        $id = $this->session_id;
        if (is_null($id)) return $status;
        try {
            switch ($this->type) {
                case self::COINBASE_TYPE;
                    if ($coinbase = Coinbase::getCharge($id)) {
                        if (isset($coinbase['data']) && is_array($coinbase['data'])) {
                            $last = end($coinbase['data']);
                            if (is_array($last)) {
                                $stat = $last[count($last) - 1]['status'] ?? null;
                                if ($stat == "COMPLETED") {
                                    $status = self::SUCCESS;
                                } elseif (!($stat == "NEW" || $stat == "PENDING")) {
                                    $status = self::DECLINE;
                                }
                            }
                        }
                    }
                    break;
                case self::PAYOP_TYPE:
                    if ($payop = Payop::getCharge($id)) {
                        $stat = $payop['status'];
                        if (in_array($stat, [1, 5])) {
                            $status = ((int)$stat === 1)
                                ? self::SUCCESS
                                : self::DECLINE;
                        }
                    }
                    break;
                case self::STRIPE_TYPE:
                    $stripe = new StripeClient(config('services.stripe.secret'));
                    $checkout_session = $stripe->checkout->sessions->retrieve($id, []);
                    if ($checkout_session->status !== "open") {
                        $status = ($checkout_session->status === "complete")
                            ? self::SUCCESS
                            : self::DECLINE;
                    }
                    break;
                case self::PAYPAL_TYPE:
                    $paypalClient = new PayPalClient;
                    $paypalClient->setApiCredentials(config('paypal'));
                    $token = $paypalClient->getAccessToken();
                    $paypalClient->setAccessToken($token);
                    $result = $paypalClient->capturePaymentOrder($id);
                    if (isset($result['status'])) switch ($result['status']) {
                        case "COMPLETED":
                            $status = self::SUCCESS;
                            break;
                        case "DENIED":
                        case "UNCLAIMED":
                            $status = self::DECLINE;
                            break;
                    }
                    break;
            }
        } catch (Exception|Throwable $exception) {
            Log::error($exception->getMessage());
        }
        return $status;
    }

    public function dataTimeMsc()
    {
        return $this->created_at
            ->timezone("Europe/Moscow");
    }

}
