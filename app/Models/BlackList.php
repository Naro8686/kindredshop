<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Stevebauman\Location\Facades\Location;

/**
 * App\Models\BlackList
 *
 * @property int $id
 * @property string|null $ip
 * @property string|null $email
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|BlackList newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlackList newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BlackList query()
 * @method static \Illuminate\Database\Eloquent\Builder|BlackList whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlackList whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlackList whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlackList whereIp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BlackList whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BlackList extends Model
{
    use HasFactory;

    protected $fillable = ['ip', 'email'];

    public function flag($class = ''): string
    {
        $location = Location::get($this->ip);
        $country = $location && $location->countryCode ? $location->countryCode : 'US';
        return "<img src='https://www.countryflags.io/$country/shiny/64.png' class='$class' alt='$country'>";
    }
}
