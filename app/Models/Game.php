<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * App\Models\Game
 *
 * @property int $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Review[] $reviews
 * @property-read int|null $reviews_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Server[] $servers
 * @property-read int|null $servers_count
 * @method static \Illuminate\Database\Eloquent\Builder|Game newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Game newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Game query()
 * @method static \Illuminate\Database\Eloquent\Builder|Game whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Game whereName($value)
 * @mixin \Eloquent
 * @property string $slug
 * @method static \Illuminate\Database\Eloquent\Builder|Game findSimilarSlugs(string $attribute, array $config, string $slug)
 * @method static \Illuminate\Database\Eloquent\Builder|Game whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Game withUniqueSlugConstraints(\Illuminate\Database\Eloquent\Model $model, string $attribute, array $config, string $slug)
 */
class Game extends Model
{
    use HasFactory, Sluggable;

    protected $fillable = ['name', 'slug'];
    public $timestamps = false;

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function servers()
    {
        return $this->hasMany(Server::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function shortName($name = 'SMURFS', $replace = null): string
    {
        return is_null($this->nameMatch()) ? $this->name : ($replace ?? $this->nameMatch()) . ' ' . $name;
    }

    public function nameMatch()
    {
        return preg_match('/(league|valorant)/iu', $this->name, $matches) ? $matches[0] : null;
    }

    public function meta()
    {
        return Seo::where('slug', $this->slug)->firstOrCreate(['slug' => $this->slug]);
    }
}
