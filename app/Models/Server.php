<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Server
 *
 * @property int $id
 * @property string $name
 * @property int $game_id
 * @property-read \App\Models\Game $game
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|Server newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Server newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Server query()
 * @method static \Illuminate\Database\Eloquent\Builder|Server whereGameId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Server whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Server whereName($value)
 * @mixin \Eloquent
 * @property string|null $description
 * @method static \Illuminate\Database\Eloquent\Builder|Server whereDescription($value)
 */
class Server extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'game_id', 'description'];
    public $timestamps = false;

    public const server_descriptions = [
        'NA' => 'North America',
        'EUW' => 'West Europe',
        'EUNE' => 'Eastern and North Europe',
        'OCE' => 'Oceania',
        'RU' => 'Russia',
        'TR' => 'Turkey',
        'BR' => 'Brazil',
        'LAN' => 'Latin America North',
        'LAS' => 'Latin America South',
        'JP' => 'Japan',
        'PBE' => 'Public Beta Environment'
    ];

    public function game()
    {
        return $this->belongsTo(Game::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function getNameAttribute($value)
    {
        return strtoupper($value);
    }
}
