<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Seo
 *
 * @property int $id
 * @property string|null $slug
 * @property string|null $title
 * @property string|null $description
 * @property string|null $keyword
 * @method static \Illuminate\Database\Eloquent\Builder|Seo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Seo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Seo query()
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereTitle($value)
 * @mixin \Eloquent
 * @property string|null $keywords
 * @method static \Illuminate\Database\Eloquent\Builder|Seo whereKeywords($value)
 */
class Seo extends Model
{
    use HasFactory;

    protected $table = 'seo';
    protected $fillable = ['slug', 'title', 'description', 'keywords'];
    public $timestamps = false;
}
