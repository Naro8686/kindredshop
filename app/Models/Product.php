<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * App\Models\Product
 *
 * @property int $id
 * @property string $title
 * @property string $price
 * @property int $lifetime percent
 * @property string|null $description
 * @property array|null $accounts
 * @property int $server_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Server $server
 * @method static \Illuminate\Database\Eloquent\Builder|Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereAccounts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereLifetime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereServerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order[] $orders
 * @property-read int|null $orders_count
 * @property array|null $links
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereLinks($value)
 */
class Product extends Model
{
    use HasFactory;

    protected $fillable = ['price', 'lifetime', 'title', 'description', 'server_id', 'accounts', 'links'];
    protected $casts = ['accounts' => 'array', 'links' => 'array'];
    protected $attributes = ['links' => self::DEFAULT_LINKS];

    public const DEFAULT_LINKS = '{"paypal":{"basic": null, "lifetime": null},"stripe":{"basic": null, "lifetime": null}}';

    public function server()
    {
        return $this->belongsTo(Server::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function setLinksAttribute($value)
    {
        $this->attributes['links'] = is_null($value) ? self::DEFAULT_LINKS : json_encode($value);
    }

    /**
     * @param $method
     * @return Collection
     */
    public function getPayLink($method): Collection
    {
        return collect($this->links[$method] ?? []);
    }


    public function accountsToString(): string
    {
        return is_null($this->accounts) ? '' : implode(PHP_EOL, $this->accounts);
    }

    public function priceHtml($lifetime = false)
    {
        return preg_replace('/^(\d+)(\.\d+)$/is', '&dollar;$1<span class="text-sm">$2</span>', $this->price($lifetime));
    }

    public function price($lifetime = false)
    {
        return (string)$lifetime
            ? (((float)$this->price) + (((float)$this->price * $this->lifetime) / 100))
            : $this->price;
    }

    /**
     * Тестовый метод (для отладки, на практике не используется) - возвращает все заказы текущего товара, которые еще обрабатываются
     */
    public function accCountTest($session_id = null)
    {
        if (!is_null($session_id)) {
            $order_count = $this->orders()
                ->where('orders.session_id', '<>', $session_id)
                ->where('orders.status', Order::PENDING)
                ->get();
            return ($order_count);
        }
        return (false);
    }

    /**
     * Возвращает доступный остаток аккаунтов товара
     */
    public function accCount($session_id = null): int
    {
        $order_count = 0;
        if (!is_null($session_id)) $order_count = $this->orders()
            ->where('orders.session_id', '<>', $session_id)
            ->where('orders.status', Order::PENDING)
            ->sum('orders.count');
        $count = is_null($this->accounts) ? 0 : count($this->accounts);
        $count -= (int)$order_count;
        return max($count, 0);
    }

    public function getLastAccounts(int $count = 1): ?array
    {
        $accounts = is_null($this->accounts) ? [] : $this->accounts;
        ksort($accounts);
        return array_slice($accounts, 0, $count);
    }

    public function allAccCount(): string
    {
        $orders = $this->orders()->where('status', Order::SUCCESS)->get('details');
        $order_count = 0;
        foreach ($orders as $order) {
            $order_count += count($order->details['accounts']);
        }
        return "{$this->accCount()}/$order_count";
    }
}
