<?php

function array_keys_exist(array $needles, array $haystack): bool
{
    $match_count = 0;
    foreach ($needles as $needle) {
        if (in_array($needle, array_keys($haystack))) {
            $haystack = $haystack[$needle];
            $match_count++;
        } else break;
    }
    return count($needles) === $match_count;
}

if (!function_exists('setting')) {
    function setting($key, $default = null)
    {
        try {
            if (is_null($key)) return new \App\Models\Setting();
            if (is_array($key)) return \App\Models\Setting::set($key[0], $key[1]);
            $value = \App\Models\Setting::get($key);
            return is_null($value) ? value($default) : $value;
        } catch (\Throwable $exception) {
            return null;
        }
    }
}
