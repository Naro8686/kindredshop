<?php

namespace App\Mail;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AdminOrderShipped extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var Order
     */
    public $order;

    /**
     * Create a new message instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $order = $this->order;
        $amount = (int)$order->amount;
        $product = $order->product ? $order->product->title : '';
        $server = $order->product &&  $order->product->server ? $order->product->server->name : '';
        $game = $order->product && $order->product->server && $order->product->server->game ? ucfirst($order->product->server->game->nameMatch()) : '';
        return $this
            ->subject("Payment $$amount $game $product $server")
            ->view('emails.orders.admin-notify', ['order' => $this->order]);
    }
}
