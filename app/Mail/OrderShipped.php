<?php

namespace App\Mail;

use App\Models\Coupon;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderShipped extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var Order
     */
    public $order;

    /**
     * Create a new message instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $order = $this->order;
        $count = $order->countAccounts() > 1 ? 'x' . $order->countAccounts() : '';
        $product = $order->product ? $order->product->title : '';
        $server = $order->product && $order->product->server ? $order->product->server->name : '';
        $game = $order->product && $order->product->server && $order->product->server->game ? ucfirst($order->product->server->game->nameMatch()) : '';
        return $this
            ->subject("$count $game $product $server")
            ->replyTo(config('mail.from.address'), config('mail.from.name'))
            ->view('emails.orders.shipped', ['order' => $this->order, 'randomCoupon' => Coupon::whereCode('KISHOP')->first()]);
    }
}
