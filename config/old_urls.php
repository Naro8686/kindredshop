<?php

return [
    "category/rare",
    "category/rare/silver-kayle",
    "acc/skin",
    "category/rare/king-rammus",
    "category/rare/victorious-skins",
    "tutorial",
    "category/gifting-center",
    "category/rare/judgement-kayle",
    "category/euw",
    "category/rare/championship-riven",
    "category/na",
    "category/rare/black-alistar",
    "category/rare/ufo-corki",
    "category/rare/pax-skins",
    "reviews",
    "rp-eune",
    "category/rare/riot-skins",
    "category/rare/rusty",
    "acc/hextech-crafting",
    "category/eune",
    "category/rare/medieval-twitch",
    "acc/gift-mini-icon",
    "category/rare/urf-the-manatee",
    "acc/mystery-skin",
    "acc/silver-kayle-na",
    "acc/silver-kayle-euw",
    "acc/champion",
    "category/rare/hextech-sion",
    "acc/hextech-chest",
    "acc/mystery-emote",
    "category/rare/young-human-ryze",
    "acc/masterwork-bundle",
    "league-of-legends-accounts",
    "category/rare-without-email",
    "order-status",
    "category/rare/grey-warwick",
    "category/pbe",
    "category/tr",
    "acc/judgement-kayle-euw",
    "acc/gift-mystery-ward",
    "category/br",
    "acc/ufo-euw",
    "category/ru",
    "category/lan",
    "category/las",
];
