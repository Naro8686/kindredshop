<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Settings
    |--------------------------------------------------------------------------
    |
    | In here you can define all the settings used in your app, it will be
    | available as a settings page where user can update it if needed
    | create sections of settings with a type of input.
    */

    'stripe' => [
        'title' => 'Stripe',
        'desc' => '',
        'icon' => 'fa fa-cash-register',
        'elements' => [
            [
                'label' => 'enabled',
                'type' => 'checkbox',
                'data' => 'integer',
                'name' => 'stripe[enabled]',
                'rules' => 'required|boolean'
            ],
            [
                'label' => 'public key',
                'type' => 'text',
                'data' => 'string',
                'name' => 'stripe[key]',
                'rules' => 'nullable|string'
            ],
            [
                'label' => 'secret',
                'type' => 'text',
                'data' => 'string',
                'name' => 'stripe[secret]',
                'rules' => 'nullable|string'
            ],
            [
                'label' => 'webhook',
                'type' => 'text',
                'data' => 'string',
                'name' => 'stripe[webhook_secret]',
                'rules' => 'nullable|string'
            ],
        ]
    ],
    'coinbase' => [
        'title' => 'Coinbase',
        'desc' => '',
        'icon' => 'fa fa-cash-register',
        'elements' => [
            [
                'label' => 'enabled',
                'type' => 'checkbox',
                'data' => 'integer',
                'name' => 'coinbase[enabled]',
                'rules' => 'required|boolean'
            ],
            [
                'label' => 'public key',
                'type' => 'text',
                'data' => 'string',
                'name' => 'coinbase[key]',
                'rules' => 'nullable|string'
            ],
            [
                'label' => 'webhook',
                'type' => 'text',
                'data' => 'string',
                'name' => 'coinbase[webhook_secret]',
                'rules' => 'nullable|string'
            ],
            [
                'label' => 'Version',
                'type' => 'text',
                'data' => 'string',
                'name' => 'coinbase[version]',
                'rules' => 'nullable|string'
            ],
        ]
    ],
    'payop' => [
        'title' => 'PayOp',
        'desc' => '',
        'icon' => 'fa fa-cash-register',
        'elements' => [
            [
                'label' => 'enabled',
                'type' => 'checkbox',
                'data' => 'integer',
                'name' => 'payop[enabled]',
                'rules' => 'required|boolean'
            ],
            [
                'label' => 'id',
                'type' => 'text',
                'data' => 'string',
                'name' => 'payop[id]',
                'rules' => 'nullable|string'
            ],
            [
                'label' => 'public key',
                'type' => 'text',
                'data' => 'string',
                'name' => 'payop[key]',
                'rules' => 'nullable|string'
            ],
            [
                'label' => 'secret',
                'type' => 'text',
                'data' => 'string',
                'name' => 'payop[secret]',
                'rules' => 'nullable|string'
            ],
            [
                'label' => 'token',
                'type' => 'text',
                'data' => 'string',
                'name' => 'payop[token]',
                'rules' => 'nullable|string'
            ],
        ]
    ],
    'paypal' => [
        'title' => 'PayPal',
        'desc' => '',
        'icon' => 'fa fa-cash-register',
        'elements' => [
            [
                'label' => 'enabled',
                'type' => 'checkbox',
                'data' => 'integer',
                'name' => 'paypal[enabled]',
                'rules' => 'required|boolean'
            ],
            [
                'label' => 'client id',
                'type' => 'text',
                'data' => 'string',
                'name' => 'paypal[client_id]',
                'rules' => 'nullable|string'
            ],
            [
                'label' => 'client secret',
                'type' => 'text',
                'data' => 'string',
                'name' => 'paypal[client_secret]',
                'rules' => 'nullable|string'
            ],
            [
                'label' => 'app id',
                'type' => 'text',
                'data' => 'string',
                'name' => 'paypal[app_id]',
                'rules' => 'nullable|string'
            ],
        ]
    ],
];
