<?php return [
    'id' => env('PAYOP_API_ID'),
    'key' => env('PAYOP_API_KEY'),
    'secret' => env('PAYOP_API_SECRET_KEY'),
    'token' => env('PAYOP_API_TOKEN'),
];
