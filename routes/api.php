<?php

use App\Http\Controllers\API\WebHookController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::match(['GET', 'POST'], '/webhook/{type}', WebHookController::class)->name('payment.webhook');

Route::namespace('App\Http\Controllers\API')->group(function () {
    Route::match(['GET', 'POST'], 'payop/callback', 'PayopController@callback')->name('payop.callback');
    Route::post('payop/refund', 'PayopController@refund')->name('payop.refund');
    Route::apiResource('payop', 'PayopController');
});
