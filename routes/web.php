<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false, 'verify' => true]);

Route::namespace('App\Http\Controllers')->group(function () {
    Route::get('/test/mail', "NewsiteController@testMail");

    // Мои контроллеры
    Route::get('/', "NewsiteController@index")->name('home');

    Route::get('/terms-and-conditions', "NewsiteController@terms")->name('terms');
    Route::post('/ajax/products/{server_id}', "NewsiteController@ajax_products")->name('ajax_products');
    Route::post('/ajax/summary/{product_id}', "NewsiteController@ajax_summary")->name('ajax_summary');
    Route::post('/ajax/checkout_form', "NewsiteController@ajax_checkout_form")->name('ajax_checkout_form');
    Route::post('/ajax/coupon', "NewsiteController@coupon")->name('ajax_coupon');
    Route::post('/ajax/order', "NewsiteController@order")->name('ajax_order');
    Route::get('/order/success', "NewsiteController@success")->name('order.success');
    Route::get('/order/fail', "NewsiteController@fail")->name('order.fail');
    // END Мои контроллеры

    Route::get('/sitemap.xml', "SiteMapController")->name('sitemap');
    Route::get('/blog', "PostController@blog")->name('blog');
    Route::get('/post/{slug}', "PostController@show")->name('posts.show');
    Route::middleware(['auth', 'verified'])->name('admin.')->prefix('admin')->group(function () {
        Route::get('/', "AdminController@dashboard")->name('dashboard');
        Route::get('/settings', 'SettingController@index')->name('settings');
        Route::post('/settings', 'SettingController@store')->name('settings.store');
        Route::prefix('pages')->group(function () {
            Route::get('/home', "PageController@home")->name('pages.home');
            Route::get('/reviews', "PageController@reviews")->name('pages.reviews');
            Route::get('/blog', "PageController@blog")->name('pages.blog');
            Route::get('/terms', "PageController@terms")->name('pages.terms');
        });
        Route::resource('/games', "GameController");
        Route::resource('/servers', "ServerController");
        Route::get('/products/select-servers', "ProductController@selectServers")->name('products.selectServers');
        Route::resource('/products', "ProductController");
        Route::resource('/orders', "OrderController");
        Route::post('/coupons/generate', "CouponController@autoGenerate")->name('coupons.generate');
        Route::resource('/coupons', "CouponController");
        Route::resource('/black-list', "BlackListController");
        Route::resource('/meta', "MetaController");
        Route::resource('/posts', "PostController")->only(['index', 'create', 'store', 'edit', 'update', 'destroy']);
        Route::post('ckeditor/upload', 'CkeditorController')->name('ckeditor.upload');
    });
});
