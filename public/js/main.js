var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}

const modalBtn = document.getElementById('modal-btn');

const modal = document.getElementById('modal-wrap');
const overlay = document.querySelector('.overlay');
if (modalBtn) modalBtn.addEventListener('click', () => {
    modal.classList.add('active');
});

const closeModal= () =>{
    modal.classList.remove('active');
}

if (overlay) overlay.addEventListener('click', closeModal);