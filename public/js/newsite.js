 jQuery(document).ready(function(){

    $ = jQuery;

    // Закрытие окошка с предупреждением об использовании куков

    $('body').on('click','.message-cookies .after',function(){
        document.cookie = "closedbeware=true;max-age=31556926";
        $('.message-cookies').fadeOut(300);
    })

    // Функция подгружает блок с заказом продукта

    function updateSummary(product_id)
    {

        if (!Number.isInteger(product_id)) return (false);

        $.ajax({
            url: '/ajax/summary/'+product_id,
            method: 'post',
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
                $('#summary').html(data);
            }
        });

    }

    // Функция подгружает ajax'ом продукты, принадлежащие конкретному серверу

    function updateProdlist (server_id, cback)
    {

        if (!Number.isInteger(server_id)) return (false);

        $.ajax({
            url: '/ajax/products/'+server_id,
            method: 'post',
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){
                $('#prodlist').empty().append(data);
                cback();
            }
        });

    }

    // Функция получает ID текущего выделенного продукта

    function getSelectedProductID()
    {
        var selProduct = $('#prodlist .variant-card.variant-card_yellow').first().data('prodid');
        if (!Number.isInteger(selProduct))
        {
            return (false);
        }
        return (selProduct);
    }

    // Функция получает ID текущего выделенного сервера

    function getSelectedServerID()
    {
        var selectedServerId = $('#servlist .server-wrapper .server-card.server-card_yellow').first().data('servid');
        if (!Number.isInteger(selectedServerId))
        {
            return (false);
        }
        return (selectedServerId);
    }

    // Подгружаем товары для сервера по умолчанию

    updateProdlist (getSelectedServerID(),function(){
        updateSummary(getSelectedProductID());
    });

    // По клику на вервер выделяем его и подгружаем его товары

    $('body').on('click','#servlist .server-card',function(){

        var thIndex = $(this).index('#servlist .server-card');

        $('#servlist .server-card').eq(thIndex).addClass('server-card_yellow');
        $('#servlist .server-card').not(':eq('+thIndex+')').removeClass('server-card_yellow');

        updateProdlist (getSelectedServerID(),function(){
            updateSummary(getSelectedProductID());
        });

    });

    // По клику на товаре выделяем его и подгружаем его в форму оформления заказа

    $('body').on('click','#prodlist .variant-card:not(.disabled)',function(){
        var thIndex = $(this).index('#prodlist .variant-card');

        $('#prodlist .variant-card').eq(thIndex).addClass('variant-card_yellow');
        $('#prodlist .variant-card').not(':eq('+thIndex+')').removeClass('variant-card_yellow');

        updateSummary(getSelectedProductID());

    });

    // Меняем цену при выборе пожизненной гарантии

    $('body').on('change','#checkbox-id',function(){

        var isChecked = $(this).prop('checked');

        var defPrice = parseInt($(this).closest('#cursummary').data('price'));
        var lifetimePercent = parseInt($(this).closest('#cursummary').data('lifetime'));

        if (isChecked) {
            var nowPrice = defPrice + ((defPrice / 100) * lifetimePercent);
            $(this).closest('#cursummary').data('nowprice',nowPrice);
            $(this).closest('#cursummary').attr('data-nowprice',nowPrice);
            $(this).closest('#cursummary').data('lifetimeis',true);
            $(this).closest('#cursummary').attr('data-lifetimeis',true);
            $('#modal-btn span.big-btn').text(nowPrice.toFixed(2).split('.')[0]);
            $('#modal-btn span.small-btn').text(nowPrice.toFixed(2).split('.')[1]);
        } else {
            $(this).closest('#cursummary').data('nowprice',defPrice);
            $(this).closest('#cursummary').attr('data-nowprice',defPrice);
            $(this).closest('#cursummary').data('lifetimeis',false);
            $(this).closest('#cursummary').attr('data-lifetimeis',false);
            $('#modal-btn span.big-btn').text(defPrice.toFixed(2).split('.')[0]);
            $('#modal-btn span.small-btn').text(defPrice.toFixed(2).split('.')[1]);
        }

    });

    // Аккордеон FAQ

    $('.item__name').on('click', function(){
        $(this).toggleClass('open');
        $(this).next().slideToggle();
    });

    // Подгрузка и отображение формы заказа в модальном окне

    $('body').on('click','#modal-btn',function(){

        var prodInfo = $('#cursummary').data();

        $.ajax({
            url: '/ajax/checkout_form',
            method: 'post',
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'prodinfo': prodInfo
            },
            success: function(data){
                $('#befmodal').after(data);
            }
        });

    });

    // Закрываем модалочку по клику на фоне

    $('body').on('click','.overlay',function(){
        $('#modal-wrap').remove();
    });

    // Функция пересчета цен

    function recountCheckoutPrices ()
    {

        // Получаем данные из данных dom
        // То есть, сначала модифицируем данные, затем вызываем эту функцию для пересчета и отображения обновленных данных

        lifetimeIs = $('#modal-wrap').data('lifetimeis');
        lifetimePercent = $('#modal-wrap').data('lifetimepercent');
        onePrice = $('#modal-wrap').data('priceone');
        quantity = $('#modal-wrap').data('quantity');
        couponIs = $('#modal-wrap').data('couponis');
        couponPercent = $('#modal-wrap').data('couponpercent');

        // Пересчет итоговой цены

        summ = onePrice * quantity;

        if (lifetimeIs && Number.isInteger(lifetimePercent))
        {
            summ = summ + ((summ / 100) * lifetimePercent)
        }

        if (couponIs && Number.isInteger(couponPercent))
        {
            summ = summ - ((summ / 100) * couponPercent)
        }

        // Установка цены в поле модального окна

        $('span.bign').text(summ.toFixed(2).split('.')[0]);
        $('span.minn').text(summ.toFixed(2).split('.')[1]);

        // Установка data параметров и соотв. атрибутов для удобства тестирования

        // Количество

        $('#modal-wrap').data('quantity',quantity);
        $('#modal-wrap').attr('data-quantity',quantity);

        // Изначальная цена единицы

        $('#modal-wrap').data('priceone',onePrice);
        $('#modal-wrap').attr('data-priceone',onePrice);

        // Сумма текущая

        $('#modal-wrap').data('pricetotal',summ);
        $('#modal-wrap').attr('data-pricetotal',summ);

        // Есть ли пожизненная гарантия

        $('#modal-wrap').data('lifetimeis',lifetimeIs);
        $('#modal-wrap').attr('data-lifetimeis',lifetimeIs);

        // Процент пожизненной гарантии

        $('#modal-wrap').data('lifetimepercent',lifetimePercent);
        $('#modal-wrap').attr('data-lifetimepercent',lifetimePercent);

        // Есть ли купон

        $('#modal-wrap').data('couponis',couponIs);
        $('#modal-wrap').attr('data-couponis',couponIs);

        // Процент купона

        $('#modal-wrap').data('couponpercent',couponPercent);
        $('#modal-wrap').attr('data-couponpercent',couponPercent);

    }

    // Изменение заказываемого количества аккаунтов

    $('body').on('change','#quantity_input',function(){

        var quantity = parseInt($(this).val());

        // Устанавливаем количество

        $('#modal-wrap').data('quantity',quantity);
        $('#modal-wrap').attr('data-quantity',quantity);

        // Пересчет

        recountCheckoutPrices();

    });

    // Реакция на ввод купона

    $('body').on('change','#couponcode',function(){

        var couponCode = $(this).val();

        $.ajax({
            url: '/ajax/coupon',
            method: 'post',
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'code': couponCode
            },
            success: function(data){

                if (data == 'false') {

                    $('#couponcode').css({
                        'border': '1px solid #f00'
                    });

                    $('#modal-wrap').data('couponis',0);
                    $('#modal-wrap').attr('data-couponis',0);

                    $('#modal-wrap').data('couponpercent',0);
                    $('#modal-wrap').attr('data-couponpercent',0);

                    $('#modal-wrap').data('couponcode',0);
                    $('#modal-wrap').attr('data-couponcode',0);

                } else {

                    $('#couponcode').css({
                        'border': '1px solid #0f0'
                    });

                    $('#modal-wrap').data('couponis',1);
                    $('#modal-wrap').attr('data-couponis',1);

                    $('#modal-wrap').data('couponpercent',data['percent']);
                    $('#modal-wrap').attr('data-couponpercent',data['percent']);

                    $('#modal-wrap').data('couponcode',data['code']);
                    $('#modal-wrap').attr('data-couponcode',data['code']);

                }

                recountCheckoutPrices();

            }
        });

    });

    // Валидация поля email

    function validateMail (checkMail)
    {
        var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
        if (pattern.test(checkMail))
        {
            return (true);
        }
        return (false);
    }

    // Проверка корректности email при изменении поля формы

    $('body').on('change','input[name=usermail]',function(){
        if (!validateMail($('input[name=usermail]').val()))
        {
            $('.checkout-mail-error').show();
            return (false);
        }
        else
        {
            $('.checkout-mail-error').hide();
        }

    });

    // Оформление заказа

    $('body').on('click','.checkout-payment-card',function(){

        if ($(this).hasClass('checkout-payment-card_op'))
        {
            return (false);
        }

        var orderData = $('#modal-wrap').data();
        orderData.usermail = $('input[name=usermail]').val();
        orderData.payment = $(this).data('type');
        orderData._token = $('meta[name="csrf-token"]').attr('content');
        let emailRequired = !['paypal','stripe'].includes(orderData.payment);
        let email_block = $('.checkout-mail');
        if (emailRequired){
            if (email_block.is(":hidden")) {
                email_block.slideDown();
                return false;
            }
            if (!validateMail($('input[name=usermail]').val()))
            {
                $('.checkout-mail-error').show();
                return (false);
            }
            else
            {
                $('.checkout-mail-error').hide();
            }
        } else email_block.slideUp();


        $.ajax({
            url: '/ajax/order',
            method: 'post',
            headers:{
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: orderData,
            success: function(data){
                window.location.replace(data['url']);
            },
            error: function(data){
                $('#messagebox h3').text('Error!');
                $('#messagebox p').text(data['responseJSON']['message']);
                $('#messagebox').fadeIn(200);
                console.log(data);
            }
        });

    });

    $('body').on('click','#mbmessage .close',function(){

        $('#messagebox').fadeOut(200);

    })

 });
