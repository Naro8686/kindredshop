// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';
let loader = $(`<div style="
                width: 100%;
                height: 100%;
                position: absolute;
                top: 0;
                background-color: #0000001c;
                left: 0;
                "></div>`);
let now = new Date();
const MAX_DATE = formatDate(now);
const MIN_DATE = formatDate(now.setDate(now.getDate() - 1));
// canvas
var purchasesChart = document.getElementById("purchasesChart");
var popularsChart = document.getElementById("popularChart");
var ordersContainer = document.getElementById("orders");
// config
const purchasesChartConfig = {
    type: 'line',
    data: {
        labels: [],
        datasets: [{
            label: "$",
            fill: true,
            // lineTension: 0,
            backgroundColor: "rgba(78, 115, 223, 0.05)",
            borderColor: "rgba(78, 115, 223, 1)",
            pointRadius: 2,
            pointBackgroundColor: "rgba(78, 115, 223, 1)",
            pointBorderColor: "rgba(78, 115, 223, 1)",
            pointHoverRadius: 3,
            pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
            pointHoverBorderColor: "rgba(78, 115, 223, 1)",
            pointHitRadius: 10,
            pointBorderWidth: 2,
            data: [],
        }],
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {display: false},
        tooltips: {
            enabled: true,
            mode: 'index',
            displayColors: false,
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            titleMarginBottom: 10,
            titleFontColor: '#6e707e',
            titleFontSize: 14,
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 10,
            yPadding: 10,
            intersect: false,
            caretPadding: 10,
            callbacks: {
                title: function (tooltipItem, chart) {
                    let sum = 0;
                    let datasets = chart.datasets[0];
                    let datasetLabel = datasets.label || '';
                    tooltipItem.forEach(function (item, key) {
                        sum += item.yLabel;
                    });
                    sum = number_format(sum, 2);
                    return tooltipItem.length > 0 ? `SUM: ${sum}${datasetLabel}` : tooltipItem[0].xLabel;
                },
                label: function (tooltipItem, data) {
                    return tooltipItem.xLabel + ' - ' + number_format(tooltipItem.yLabel, 2) + '' + data.datasets[tooltipItem.datasetIndex].label;
                }
            }
        },
        scales: {
            xAxes: [{
                type: 'time',
                time: {
                    displayFormats: {
                        'hour': 'HH:mm',
                    },
                    tooltipFormats: {
                        'millisecond': 'MMM dd',
                        'second': 'yyyy-MM-dd HH:mm:ss',
                        'minute': 'yyyy-MM-dd HH:mm',
                        'hour': 'yyyy-MM-dd HH:mm',
                        'day': 'yyyy-MM-dd eeee',
                        'week': 'yyyy-MM-dd',
                        'month': 'yyyy MMMM',
                        'quarter': 'MMM-dd',
                        'year': 'yyyy',
                    },
                    tooltipFormat: 'yyyy-MM-dd HH:mm',
                    minUnit: 'hour',
                    unit: 'hour',
                    round: 'hour',
                },
                ticks: {
                    autoSkip: false,
                },
            }],
            yAxes: [{
                type: 'linear',
                stacked: false,
                ticks: {
                    autoSkip: false,
                    beginAtZero: true,
                    source: "auto",
                    callback: function (value, index, ticks) {
                        return `${value} $`;
                    }
                },
            }],
        }
    },
};
var popularsChartConfig = {
    type: 'doughnut',
    data: {
        labels: [],
        datasets: [{
            data: [],
            backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc', '#d3672d', '#cc3663'],
            hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf'],
            hoverBorderColor: "rgba(234, 236, 244, 1)",
        }],
    },
    options: {
        maintainAspectRatio: false,
        tooltips: {
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            borderColor: '#dddfeb',
            borderWidth: 1,
            xPadding: 15,
            yPadding: 15,
            displayColors: false,
            caretPadding: 10,
        },
        legend: {
            display: true
        },
        cutoutPercentage: 80,
    },
};
// charts
var purchases = chart(purchasesChart, purchasesChartConfig);
var populars = chart(popularsChart, popularsChartConfig);


function ajax_charts(data = {}) {
    const request = $.ajax({
        url: "/admin",
        method: "GET",
        data: data,
        cache: false,
        dataType: "json",
        beforeSend: () => $(purchasesChart).closest('.card').append(loader),
        success: (response) => {
            for (let key in response) {
                let data = response[key];
                switch (key) {
                    case "purchases":
                        purchaseChartEdit(purchases, data);
                        break;
                    case "populars":
                        popularChartEdit(populars, data);
                        break;
                    case "orders":
                        ordersEdit(ordersContainer, data);
                        break;
                }
            }
        }
    });
    request.always(() => loader.remove());
    return request;
}

function chart(elem, conf = {}) {
    return new Chart(elem, conf);
}

function purchaseChartEdit(chart, data = []) {
    chart.data.labels = [];
    chart.data.datasets[0].data = [];
    for (let i in data) {
        let obj = data[i];
        let timestamp = (obj.timestamp * 1000);
        let created_at = obj.created_at;
        let amount = number_format(obj.amount, 2, '.', '');
        chart.data.labels.push(timestamp);
        chart.data.datasets[0].data.push({
            x: timestamp,
            y: amount,
        });
    }
    chart.update();
}

function popularChartEdit(chart, data = []) {
    chart.data.labels = [];
    chart.data.datasets[0].data = [];
    for (let i in data) {
        let obj = data[i];
        chart.data.labels.push(obj.server.name + " " + obj.title + " " + obj.description);
        chart.data.datasets[0].data.push(obj.orders_count);
    }
    chart.update();
}

function ordersEdit(ordersContainer, data) {
    let parent = $(ordersContainer);
    let total = parseInt(data.total);
    let completed = parseInt(data.completed);
    let sum = number_format(data.sum, 2);
    let percent = (((completed / total) * 100));
    percent = isNaN(percent) ? 0 : number_format(percent, 2);
    parent.find('#total').text(total);
    parent.find('#completed').text(completed);
    parent.find('#rate').text(`${percent}%`);
    parent.find('#sum').text(`${sum} $`);
    let progress = parent
        .find('#rate')
        .closest('.row')
        .find('.progress-bar[role="progressbar"]');
    if (progress.length) progress.attr({
        "style": `width: ${percent}%`,
        "aria-valuenow": percent
    });
}

$(function () {
    ajax_charts();
    let dateRange = $(".flatpickr-input").flatpickr({
        mode: "range",
        dateFormat: "Y-m-d",
        defaultDate: [MIN_DATE, MAX_DATE],
        maxDate: "today",
        allowInput: true,
        onChange: function (selectedDates) {
            if (selectedDates.length === 2) {
                dateRange.selectedDates[0] = selectedDates[0];
                dateRange.selectedDates[1] = selectedDates[1];
                ajax_charts({
                    dates: [formatDate(selectedDates[0]), formatDate(selectedDates[1])],
                    unit: $('select#unit').val().split(":")[0]
                });
            }
        }
    });
    $('select#unit').on('change', function (e) {
        let unit = $(this).val().split(":")[0];
        let format = (purchases.options.scales.xAxes[0].time.tooltipFormats
            || purchases.options.scales.xAxes[0].time.displayFormats)[unit];
        ajax_charts({
            dates: [formatDate(dateRange.selectedDates[0]), formatDate(dateRange.selectedDates[1])],
            unit: unit
        }).done(function (response) {
            purchases.options.scales.xAxes[0].time.unit = unit;
            purchases.options.scales.xAxes[0].time.round = unit;
            purchases.options.scales.xAxes[0].time.tooltipFormat = format
            purchases.options.scales.xAxes[0].time.isoWeekday = (unit === 'week');
            purchases.update();
        });
    });
});