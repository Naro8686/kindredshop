<?php

namespace Database\Seeders;

use App\Models\Game;
use Illuminate\Database\Seeder;

class GamesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $games = [['name' => 'League of Legends', 'slug' => 'lol-smurfs'], ['name' => 'Valorant']];
        foreach ($games as $game) Game::create($game);
    }
}
