<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AdminSeeder::class,
            GamesSeeder::class,
            ServersSeeder::class,
            SettingsSeeder::class,
            CouponSeeder::class
        ]);
    }
}
