<?php

namespace Database\Seeders;


use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'stripe',
                'val' => 0,
                'type' => 'integer',
            ],
            [
                'name' => 'coinbase',
                'val' => 0,
                'type' => 'integer',
            ],
            [
                'name' => 'payop',
                'val' => 0,
                'type' => 'integer',
            ],
            [
                'name' => 'paypal',
                'val' => 0,
                'type' => 'integer',
            ],
        ];
        foreach ($data as $v) Setting::add($v['name'], $v['val'], $v['type']);
    }
}
