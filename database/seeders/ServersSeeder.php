<?php

namespace Database\Seeders;

use App\Models\Game;
use App\Models\Server;
use Illuminate\Database\Seeder;

class ServersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $servers = [
            'League of Legends' => [
                ['name' => 'euw'],
                ['name' => 'na'],
                ['name' => 'eune'],
                ['name' => 'oce'],
                ['name' => 'ru'],
                ['name' => 'tr'],
                ['name' => 'lan'],
                ['name' => 'br'],
                ['name' => 'las']
            ],
            'Valorant' => [
                ['name' => 'eu'],
                ['name' => 'na'],
                ['name' => 'oce'],
                ['name' => 'hongkong'],
            ]
        ];
        Game::whereIn('name', ['League of Legends', 'Valorant'])->orderBy('id')->take(2)->get()
            ->each(function (Game $game) use ($servers) {
                foreach ($servers[$game->name] as $server)
                    $game->servers()->create($server);
            });


    }
}
