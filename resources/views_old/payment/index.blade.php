@extends('layouts.app')
@section('title')
    | {{$seo->title ?? $game->name}}
@endsection
@section('meta')
    <meta name="description" content="{{$seo->description}}">
    <meta name="keywords" content="{{$seo->keywords}}">
@endsection
@section('head-bg')
    @push('styles')
        <link rel="stylesheet" href="{{asset('css/payment.css')}}">
    @endpush
    @push('scripts')
        <script src="{{asset('js/payment.js')}}"></script>
    @endpush
    <div class="registrationHero">
        <span class="dashDetail-2"></span>
        <div class="flex flex-col lg:flex-row pb-12 pt-8 container">
            <div class="max-w-xs flex flex-col justify-center mx-auto mb-8 lg:mb-0">
                <h1
                    class="uppercase font-bold text-3xl mb-4 game__name">
                    <p>HANDLEVELED</p>
                    @if(mb_strtolower($game->nameMatch()) === 'league')
                        {{$game->shortName('SMURFS','BUY')}}
                    @else
                        {{$game->shortName()}}
                    @endif
                </h1>
                <p class="text-gray-400 text-xs sm:text-sm lg:text-base game__desc">
                    @if(mb_strtolower($game->nameMatch()) === 'league')
                        Level 30 Smurf accounts for League with
                        high amounts of BE. Full access, clean ranked history. Accounts are delivered instantly
                        24/7.
                    @else
                        Unranked Valorant accounts with two character of your choice. Full access, clean ranked history.
                        Accounts are delivered instantly 24/7.
                    @endif
                </p>
            </div>
            @if($firstServer)
                <div class="flex flex-col lg:flex-row lg:justify-around _grow-no-cloak vc-l">
                    <div class="mb-8">
                        <div class="_box _box-server mb-8"><h3><span
                                    class="_circle">1</span>
                                Check Server</h3>
                            <div>
                                <div class="flex inline-block justify-center text-xs sm:text-sm lg:text-lg">
                                    <div class="rounded-l text-gray-300 font-bold flex items-center px-3"
                                         style="background: rgba(255, 255, 255, 0.2);">
                                        {{$firstServer?$firstServer->name:''}}
                                    </div>
                                    <button id="change" class="btn-unrounded btn-orange rounded-r" data-toggle="modal"
                                            data-type="servers"
                                            data-game-id="{{$firstServer->game_id}}"
                                            data-target="#modal">Change<i
                                            class="ml-2 far fa-hand-pointer"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="_box">
                            <h3>
                                <span class="_circle">2</span> Select variant</h3>
                            <div class="_variant-cont">
                                @include('includes.products',['products' => $firstServer->products])
                            </div>
                        </div>
                    </div>
                    <div class="_box">
                        <h3>
                            <span class="_circle">3</span> Review your Smurf
                        </h3>
                        <div class="bg-white rounded mb-4 step-3 @if(!$product = $firstServer->products->first()) invisible @endif">
                            <div class="py-4 px-8">
                                <div class="flex items-center justify-between">
                                    <div class="flex-grow"><p id="title"
                                                              class="font-bold text-black text-2xl">
                                            {{$product?($product->title.(is_numeric($product->title)?'+':'')):''}}</p>
                                        <p class="text-gray-600 text-sm sm:text-base"
                                           id="desc">{{$product?$product->description:''}}</p></div>
                                    <div class="h-10 w-10 flex items-center">
                                        <img src="{{asset('img/items/gem.png')}}"
                                             class="w-full" alt=""></div>
                                </div>
                                <div
                                    class="lifetime_cont inline-flex items-center justify-center cursor-pointer text-xs text-gray-600 mt-4">
                                    <div id="lifetime"
                                         class="w-5 h-5 rounded border border-grey inline-flex justify-center items-center bg-white mr-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"
                                             class="w-1/2 fill-current text-white">
                                            <path
                                                d="M504.502,75.496c-9.997-9.998-26.205-9.998-36.204,0L161.594,382.203L43.702,264.311c-9.997-9.998-26.205-9.997-36.204,0c-9.998,9.997-9.998,26.205,0,36.203l135.994,135.992c9.994,9.997,26.214,9.99,36.204,0L504.502,111.7C514.5,101.703,514.499,85.494,504.502,75.496z"></path>
                                        </svg>
                                    </div>
                                    <p>Lifetime warranty
                                        <i class="ml-2 fas fa-mouse-pointer"></i>
                                        <span class="percent"
                                              style="display: none"> (+{{$product?$product->lifetime:''}}%) </span>
                                    </p>
                                </div>
                                <p class="mb-2 mt-4 font-bold text-black text-lg text-left uppercase">
                                    Included</p>
                                <ul class="_list">
                                    @if(strtolower($game->nameMatch()) !== 'valorant')
                                        <li><span>Level 30+</span><i
                                                class="fas fa-chart-line"></i>
                                        </li>
                                    @endif
                                    <li><span>Full access</span><i
                                            class="fas fa-key"></i>
                                    </li>
                                    <li><span>Instant delivery</span><i
                                            class="fas fa-paper-plane"></i>
                                    </li>
                                    @if(strtolower($game->nameMatch()) !== 'valorant')
                                        <li><span>Honor Level 2</span><i
                                                class="fas fa-smile-beam"></i>
                                        </li>
                                    @endif
                                    <li><span class="">20 Days Warranty</span><i
                                            class="fas fa-user-shield"></i></li>
                                </ul>
                            </div>
                            <button class="w-full btn-unrounded btn-orange rounded-b text-lg"
                                    data-toggle="modal"
                                    data-type="checkout" data-target="#modal"
                                    style="padding-bottom: 0.75rem !important; padding-top: 0.75rem !important;">
                                Checkout
                            </button>

                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
@stop
@section('content')
    <div class="section-head-btn pay"></div>
    <div class="container mb-5">
        <div class="smurfs-info mb-16">
            <div>
                <div class="detailLine-1 detailLine-1P top-1j">
                    <div class="dashDetailWrapper-pp dark-2k" style="opacity: 1;"><span class="borderLine-3"></span>
                        <span class="dashDetail-33"></span></div>
                </div>
                <h2 id="info" class="font-bold mb-2 heading-01">
                    @if(mb_strtolower($game->nameMatch()) === 'league') {{$game->shortName('SMURFS','BUY')}} @else {{$game->shortName('')}} @endif
                    - WHAT'S INCLUDED
                </h2>
            </div>
            <div class="flexbox mt-16 column-3">
                <div class="box">
                    <h3>INSTANT DELIVERY</h3>
                    <p>
                        Your League account will be delivered instantly 24/7 through e-mail. All you have to do is enter
                        your email and it will be sent there in seconds.
                    </p>
                </div>
                <div class="box">
                    <h3>RANKED READY</h3>
                    <p>
                        Some Blue Essence will allow you to purchase 25-45 champions and Level 30+ allows you to start
                        playing ranked games immediately. Zero waiting time, you're all set!
                    </p>
                </div>
                <div class="box">
                    <h3>FULL ACCESS</h3>
                    <p>After purchasing you receive full access to the Smurf Account, which means that you will be able
                        to attach your email and change the password whenever you want. </p>
                </div>
                <div class="box">
                    <h3>ALWAYS UNRANKED</h3>
                    <p>
                        The League accounts have completely clear history in every season, ranked games have never been
                        played. Literally a perfect Smurf account for you!
                    </p>
                </div>
                <div class="box">
                    <h3>OUR WARRANTY</h3>
                    <p>You can select up to lifetime warranty and 20 days are free. Our extended warranties cover 100%
                        of account issues, including toxic bans.</p>
                </div>
                <div class="box">
                    <h3>NO LOW-PRIO QUEUE</h3>
                    <p>Smurfs Accounts often have low priority queues, but that's not the case with us! We guarantee
                        that you will be able to start playing without any delays.</p>
                </div>
            </div>
        </div>
    </div>
    @if(mb_strtolower($game->nameMatch()) === 'league' && \App\Models\Game::whereName('valorant')->exists())
        <section class="gameOverview valorant">
            <div class="volt topVolt top-1m">
                <img alt="" class="voltImage"
                     src="{{asset('img/bg/Unknown.png')}}"/>
            </div>
            <div class="container content-wrapper contentWrapper-2">
                <div class="sectionContent-3N">
                    <div class="videoWrapper-jH pay">
                        <div class="videoContainer-1q youtubeVideo-2LL ">
                            <div class="posterContainer-3PM" data-element="poster">
                                <img src="{{asset('img/bg/enoiz019r2t41.png')}}"
                                     alt="akali" class="posterImage-3av"/>
                            </div>
                        </div>
                    </div>
                    <div class="copyWrapper-2R">
                        <div class="accordionWrapper-jY">
                            <div class="accordion-1W accordion-2x">
                                <div class="item-2Z">
                                    <div class="header-3t" data-index="0">
                                        <div class="titleWrapper-3F">
                                        <span class="border-oj bottomLeft-XS light-3s videoBorder-w border___top pay">
                                            <span class="borderLine-24 firstSide-2P"></span>
                                            <span class="dashDetail-26 top-detail"></span>
                                        </span>
                                            <h1 class="heading-01 font-bold" style="line-height: 85px">
                                                TRY NEW <br>
                                                VALORANT
                                            </h1>
                                        </div>
                                    </div>
                                    <div class="copy-01 description-kq">
                                        <div class="decriptionCopy-AU pay">
                                            <div class="richTextContent-2">
                                                <p>VALORANT is the upcoming tactical shooter 5v5.</p>
                                                <br>
                                                <p> Blend your style and experience on a global, competitive stage. You
                                                    have
                                                    13
                                                    rounds to attack and defend your side using sharp gunplay and
                                                    tactical
                                                    abilities. And, with one life per-round, you'll need to think faster
                                                    than
                                                    your opponent if you want to survive. Take on foes across
                                                    Competitive
                                                    and
                                                    Unranked modes as well as Deathmatch and Spike Rush.</p>
                                                <br>
                                                <p>More than guns and bullets, you’ll choose an Agent armed with
                                                    adaptive,
                                                    swift, and lethal abilities that create opportunities to let your
                                                    gunplay
                                                    shine. No two Agents play alike, just as no two highlight reels will
                                                    look
                                                    the same.</p>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="d-flex mb-5">
                                        @foreach($games as $game)
                                            @if(mb_strtolower($game->nameMatch()) === 'valorant')
                                                <a class="appButton uppercase" style="margin-right: 15px"
                                                   href="{{route('payment.index',[$game->slug])}}">{{$game->shortName('SMURFS','BUY')}}</a>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="volt bottomVolt-K6psi bottom-b1">
                <img class="voltImage"
                     src="{{asset('img/bg/Unknown-2.png')}}"
                     alt=""/>
            </div>
        </section>
    @endif
    <div class="container mt-5 mb-5">
        @if($reviews->isNotEmpty())
            {{--            <div class="mb-16">--}}
            {{--                <h3 class="text-orange-600 font-bold text-2xl lg:text-3xl text-center w-full mb-4 sm:mb-6">Reviews</h3>--}}
            {{--                <div class="flex flex-wrap items-center justify-between">--}}
            {{--                    <div class="w-full mb-6 sm:mb-0 sm:w-1/2s">--}}
            {{--                        <p class="text-center text-gray-600 lg:text-lg lg:w-3/4 lg:mx-auto">League of Legends has an--}}
            {{--                            average--}}
            {{--                            rating of <b>{{$reviews->avg('rating')}}</b> based on <b>{{$reviews->count()}}</b> reviews.--}}
            {{--                            You can view them all or submit one--}}
            {{--                            yourself on our <a class="underline" href="{{route('reviews.index')}}">reviews page</a>.</p>--}}
            {{--                    </div>--}}
            {{--                    <div class="w-full sm:w-1/2s">--}}
            {{--                        @include('includes.review',['review'=>$reviews->last()])--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--            </div>--}}
        @endif
        <div>
            <div class="detailLine-1 detailLine-1P top-1j">
                <div class="dashDetailWrapper-pp dark-2k" style="opacity: 1;"><span class="borderLine-3"></span>
                    <span class="dashDetail-33"></span></div>
            </div>
            <h2 id="info" class="font-bold mb-2 heading-01">
                ALL YOU NEED TO KNOW
            </h2>
        </div>
        <div class="flexbox mt-16 column-2">
            <div class="box">
                <h3>What do you actually sell here?</h3>
                <p>
                    Level 30 League of Legends Accounts with lots of Blue Essence and clean ranked history - they are
                    literally perfect Smurf accounts. You can pick between different amounts of Blue Essence, which will
                    be enough to buy 25-45 champions.
                </p>
            </div>
            <div class="box">
                <h3>IS IT SAFE?</h3>
                <p>
                    Yes, as safe as it gets when it comes to buying LoL Smurfs. We don't believe customers should be
                    risking their money when purchasing online so we offer Lifetime Warranty as an option for every
                    account. Never worry again.
                </p>
            </div>
            <div class="box">
                <h3>account be only mine?</h3>
                <p>Yes, absolutely! You will get complete access to the account and can attach your own e-mail (they are
                    unverified). This is what differentiates us from other websites. You are the first and only owner of
                    the account!</p>
            </div>
            <div class="box">
                <h3>How can I get in touch with you?</h3>
                <p>
                    We provide excellent support for our League of Legends
                    accounts! Solve any of your questions through e-mail: sales@kindredshop.net or via Live Chat in the
                    lower right corner.
                </p>
            </div>
        </div>
    </div>
@endsection


