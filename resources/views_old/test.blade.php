<!DOCTYPE html>
<html lang="en-us" dir="ltr">
<head>
    <meta charSet="utf-8"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <title data-react-helmet="true">Welcome to League of Legends: Wild Rift</title>
    <style>
        html {
            line-height: 1.15;
            -webkit-text-size-adjust: 100%
        }

        [dir] body {
            margin: 0
        }

        main {
            display: block
        }

        h1 {
            font-size: 2em
        }

        [dir] h1 {
            margin: .67em 0
        }

        hr {
            box-sizing: content-box;
            height: 0;
            overflow: visible
        }

        pre {
            font-family: monospace, monospace;
            font-size: 1em
        }

        [dir] a {
            background-color: transparent
        }

        abbr[title] {
            text-decoration: underline;
            -webkit-text-decoration: underline dotted;
            text-decoration: underline dotted
        }

        [dir] abbr[title] {
            border-bottom: none
        }

        b, strong {
            font-weight: bolder
        }

        code, kbd, samp {
            font-family: monospace, monospace;
            font-size: 1em
        }

        small {
            font-size: 80%
        }

        sub, sup {
            font-size: 75%;
            line-height: 0;
            position: relative;
            vertical-align: baseline
        }

        sub {
            bottom: -.25em
        }

        sup {
            top: -.5em
        }

        [dir] img {
            border-style: none
        }

        button, input, optgroup, select, textarea {
            font-family: inherit;
            font-size: 100%;
            line-height: 1.15
        }

        [dir] button, [dir] input, [dir] optgroup, [dir] select, [dir] textarea {
            margin: 0
        }

        button, input {
            overflow: visible
        }

        button, select {
            text-transform: none
        }

        [type=button], [type=reset], [type=submit], button {
            -webkit-appearance: button
        }

        [dir] [type=button]::-moz-focus-inner, [dir] [type=reset]::-moz-focus-inner, [dir] [type=submit]::-moz-focus-inner, [dir] button::-moz-focus-inner {
            border-style: none;
            padding: 0
        }

        [type=button]:-moz-focusring, [type=reset]:-moz-focusring, [type=submit]:-moz-focusring, button:-moz-focusring {
            outline: 1px dotted ButtonText
        }

        [dir] fieldset {
            padding: .35em .75em .625em
        }

        legend {
            box-sizing: border-box;
            color: inherit;
            display: table;
            max-width: 100%;
            white-space: normal
        }

        [dir] legend {
            padding: 0
        }

        progress {
            vertical-align: baseline
        }

        textarea {
            overflow: auto
        }

        [type=checkbox], [type=radio] {
            box-sizing: border-box
        }

        [dir] [type=checkbox], [dir] [type=radio] {
            padding: 0
        }

        [type=number]::-webkit-inner-spin-button, [type=number]::-webkit-outer-spin-button {
            height: auto
        }

        [type=search] {
            -webkit-appearance: textfield;
            outline-offset: -2px
        }

        [type=search]::-webkit-search-decoration {
            -webkit-appearance: none
        }

        ::-webkit-file-upload-button {
            -webkit-appearance: button;
            font: inherit
        }

        details {
            display: block
        }

        summary {
            display: list-item
        }

        [hidden], template {
            display: none
        }

        :root {
            font-size: 62.5%
        }

        *, :after, :before {
            box-sizing: border-box
        }

        html {
            text-rendering: optimizeLegibility;
            -webkit-font-smoothing: antialiased;
            -webkit-tap-highlight-color: transparent
        }

        main {
            overflow: hidden
        }

        body, html {
            width: 100%;
            min-height: 100vh;
            position: relative
        }

        body {
            font-size: 1.6rem
        }

        [dir] body {
            margin-top: 0
        }

        a {
            color: currentColor;
            display: block;
            text-decoration: none
        }

        [dir] body > div:not(.osano-cm-window) button {
            background: transparent;
            border: none;
            cursor: pointer
        }

        body > div:not(.osano-cm-window) button:focus {
            outline: none
        }

        h1, h2, h3, h4, h5, h6, p {
            font-weight: 400
        }

        [dir] h1, [dir] h2, [dir] h3, [dir] h4, [dir] h5, [dir] h6, [dir] p {
            margin: 0
        }

        /*img {*/
        /*    display: block;*/
        /*    height: auto;*/
        /*    width: 100%*/
        /*}*/

        input, select {
            text-rendering: optimizeLegibility;
            -webkit-font-smoothing: antialiased
        }

        [dir] input, [dir] select {
            border: none
        }

        input:focus, select:focus {
            outline: none
        }

        ol, ul {
            list-style: none
        }

        [dir] ol, [dir] ul {
            margin: 0;
            padding: 0
        }

        [dir] .is-draggable {
            cursor: grab
        }

        [dir] .is-dragging {
            cursor: grabbing
        }

        .anchor-link {
            display: block;
            position: relative;
            top: -8rem;
            visibility: hidden;
            pointer-events: none
        }

        [dir] .riotbar-cookie-policy-v2.cookie-link.corner-button .label span {
            padding: 0 !important
        }

        [class*=heading-].font-normal, [class*=label-].font-normal {
            font-style: normal
        }

        .heading-01, .heading-08 {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-size: 4.5rem;
            line-height: .95;
            letter-spacing: 2px
        }

        html[lang=ja-jp] .heading-01, html[lang=ja-jp] .heading-08 {
            font-weight: 700;
            font-size: 3.5rem;
            line-height: 1.1
        }

        html[lang=tr-tr] .heading-01, html[lang=tr-tr] .heading-08 {
            line-height: 1.2
        }

        html[lang=ru-ru] .heading-01, html[lang=ru-ru] .heading-08 {
            font-family: BeaufortforLOL-Heavy, arial, georgia, sans-serif;
            line-height: 1
        }

        html[lang=zh-hk] .heading-01, html[lang=zh-hk] .heading-08, html[lang=zh-tw] .heading-01, html[lang=zh-tw] .heading-08 {
            line-height: 1.1
        }

        html[lang=ko-kr] .heading-01, html[lang=ko-kr] .heading-08 {
            font-size: 3.5rem;
            line-height: 1.1
        }

        html[lang=ar-ae] .heading-01, html[lang=ar-ae] .heading-08 {
            font-weight: 700;
            line-height: 1.6
        }

        html[lang=vi-vn] .heading-01, html[lang=vi-vn] .heading-08 {
            font-size: 3.5rem
        }

        html[lang=es-es] .heading-01, html[lang=es-es] .heading-08, html[lang=es-mx] .heading-01, html[lang=es-mx] .heading-08 {
            line-height: 1.05
        }

        .heading-02 {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-size: 2.6rem;
            line-height: 1.04;
            letter-spacing: 2px
        }

        html[lang=ja-jp] .heading-02 {
            font-size: 2rem;
            line-height: 1.1
        }

        html[lang=ru-ru] .heading-02 {
            font-size: 2.4rem;
            line-height: 1.2
        }

        .heading-03 {
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif;
            font-size: 1.4rem;
            line-height: 1.17
        }

        .heading-04 {
            font-family: BeaufortforLOL-Heavy, arial, georgia, sans-serif;
            font-size: 1.8rem
        }

        html[lang=ja-jp] .heading-04 {
            line-height: 1.6
        }

        .heading-05 {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-size: 1.8rem;
            letter-spacing: 1px
        }

        .heading-06, .heading-09 {
            font-family: Spiegel-SemiBold, arial, georgia, sans-serif;
            font-size: 1.3rem;
            letter-spacing: 1px
        }

        .heading-07 {
            font-size: 2.6rem;
            letter-spacing: 1px
        }

        .heading-07, .heading-10 {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif
        }

        .heading-10 {
            font-size: 1.4rem
        }

        .copy-01, .copy-03 {
            font-size: 1.3rem;
            line-height: 1.42
        }

        .copy-01, .copy-02, .copy-03, .label-03 {
            font-family: Spiegel-Regular, arial, georgia, sans-serif
        }

        .copy-02, .label-03 {
            font-size: 1rem;
            line-height: 1.5
        }

        .copy-03 {
            font-family: Spiegel-SemiBold, arial, georgia, sans-serif
        }

        .label-01 {
            text-transform: uppercase;
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-style: italic;
            font-size: 1.8rem;
            line-height: 1.27;
            letter-spacing: 1px
        }

        html[lang=ar-ae] .label-01 {
            font-weight: 700
        }

        .label-02 {
            text-transform: uppercase;
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif;
            font-size: 1.4rem;
            letter-spacing: 1px
        }

        html[lang=ar-ae] .label-02 {
            font-weight: 700
        }

        .label-04 {
            font-family: Spiegel-SemiBold, arial, georgia, sans-serif;
            font-size: 1rem
        }

        html[lang=ar-ae] .label-04 {
            font-weight: 500
        }

        .label-05 {
            text-transform: uppercase;
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif;
            font-size: 1rem
        }

        @media (min-width: 768px) {
            .heading-01, .heading-08 {
                font-size: 9rem
            }

            html[lang=ja-jp] .heading-01, html[lang=ja-jp] .heading-08 {
                font-size: 6.5rem
            }

            html[lang=ko-kr] .heading-01, html[lang=ko-kr] .heading-08, html[lang=vi-vn] .heading-01, html[lang=vi-vn] .heading-08 {
                font-size: 7rem
            }

            .heading-02 {
                font-size: 5.2rem
            }

            html[lang=ja-jp] .heading-02 {
                font-size: 4rem;
                line-height: 1.1
            }

            html[lang=ru-ru] .heading-02 {
                font-size: 4rem
            }

            .heading-03, .heading-10, .label-02 {
                font-size: 2.8rem
            }

            .heading-04, .heading-05 {
                font-size: 3.6rem
            }

            .copy-01, .copy-03, .heading-06, .heading-09 {
                font-size: 2.6rem
            }

            .heading-07 {
                font-size: 5.2rem
            }

            .copy-02, .label-03, .label-04, .label-05 {
                font-size: 2rem
            }

            .label-01 {
                font-size: 3.6rem;
                line-height: 1
            }

            html[lang=ar-ae] .label-01 {
                line-height: 1.1
            }
        }

        @media (min-width: 1024px) {
            .heading-01, .heading-08 {
                font-size: 11rem;
                line-height: .9;
                font-size: 85px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            .heading-01, .heading-08 {
                font-size: calc(-15px + 9.76563vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            .heading-01, .heading-08 {
                font-size: 110px
            }
        }

        @media (min-width: 1024px) {
            html[lang=zh-tw] .heading-01, html[lang=zh-tw] .heading-08 {
                font-size: 80px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=zh-tw] .heading-01, html[lang=zh-tw] .heading-08 {
                font-size: calc(40px + 3.90625vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=zh-tw] .heading-01, html[lang=zh-tw] .heading-08 {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=zh-hk] .heading-01, html[lang=zh-hk] .heading-08 {
                font-size: 80px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=zh-hk] .heading-01, html[lang=zh-hk] .heading-08 {
                font-size: calc(40px + 3.90625vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=zh-hk] .heading-01, html[lang=zh-hk] .heading-08 {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=pt-br] .heading-01, html[lang=pt-br] .heading-08 {
                font-size: 75px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=pt-br] .heading-01, html[lang=pt-br] .heading-08 {
                font-size: calc(-25px + 9.76563vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=pt-br] .heading-01, html[lang=pt-br] .heading-08 {
                font-size: 100px
            }
        }

        @media (min-width: 1024px) {
            html[lang=es-es] .heading-01, html[lang=es-es] .heading-08, html[lang=es-mx] .heading-01, html[lang=es-mx] .heading-08 {
                line-height: 1.05
            }

            html[lang=id-id] .heading-01, html[lang=id-id] .heading-08 {
                font-size: 50px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=id-id] .heading-01, html[lang=id-id] .heading-08 {
                font-size: calc(-90px + 13.67188vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=id-id] .heading-01, html[lang=id-id] .heading-08 {
                font-size: 85px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ms-my] .heading-01, html[lang=ms-my] .heading-08 {
                font-size: 80px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ms-my] .heading-01, html[lang=ms-my] .heading-08 {
                font-size: calc(20px + 5.85938vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ms-my] .heading-01, html[lang=ms-my] .heading-08 {
                font-size: 95px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ko-kr] .heading-01, html[lang=ko-kr] .heading-08 {
                font-size: 80px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ko-kr] .heading-01, html[lang=ko-kr] .heading-08 {
                font-size: calc(40px + 3.90625vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ko-kr] .heading-01, html[lang=ko-kr] .heading-08 {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ru-ru] .heading-01, html[lang=ru-ru] .heading-08 {
                font-size: 50px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ru-ru] .heading-01, html[lang=ru-ru] .heading-08 {
                font-size: calc(-30px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ru-ru] .heading-01, html[lang=ru-ru] .heading-08 {
                font-size: 70px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ja-jp] .heading-01, html[lang=ja-jp] .heading-08 {
                font-size: 60px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ja-jp] .heading-01, html[lang=ja-jp] .heading-08 {
                font-size: 5.85938vw
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ja-jp] .heading-01, html[lang=ja-jp] .heading-08 {
                font-size: 75px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ar-ae] .heading-01, html[lang=ar-ae] .heading-08 {
                font-size: 60px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ar-ae] .heading-01, html[lang=ar-ae] .heading-08 {
                font-size: calc(-20px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ar-ae] .heading-01, html[lang=ar-ae] .heading-08 {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            html[lang=th-th] .heading-01, html[lang=th-th] .heading-08 {
                font-size: 70px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=th-th] .heading-01, html[lang=th-th] .heading-08 {
                font-size: calc(-10px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=th-th] .heading-01, html[lang=th-th] .heading-08 {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=vi-vn] .heading-01, html[lang=vi-vn] .heading-08 {
                font-size: 60px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=vi-vn] .heading-01, html[lang=vi-vn] .heading-08 {
                font-size: calc(-20px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=vi-vn] .heading-01, html[lang=vi-vn] .heading-08 {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            .heading-02 {
                font-size: 4rem
            }

            html[lang=ja-jp] .heading-02 {
                font-size: 3rem;
                line-height: 1.1
            }

            html[lang=ru-ru] .heading-02 {
                font-size: 2.8rem
            }

            .heading-03, .heading-06, .heading-09, .label-02, .label-05 {
                font-size: 1.4rem
            }

            .heading-04 {
                font-size: 2.4rem
            }

            .heading-05, .heading-10, .label-01 {
                font-size: 1.9rem
            }

            .heading-07 {
                font-size: 3rem
            }

            .heading-08 {
                font-size: 8rem;
                font-size: 70px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            .heading-08 {
                font-size: calc(30px + 3.90625vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            .heading-08 {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ja-jp] .heading-08 {
                font-size: 55px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ja-jp] .heading-08 {
                font-size: calc(-5px + 5.85938vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ja-jp] .heading-08 {
                font-size: 70px
            }
        }

        @media (min-width: 1024px) {
            html[lang=th-th] .heading-08 {
                font-size: 60px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=th-th] .heading-08 {
                font-size: calc(-20px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=th-th] .heading-08 {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            html[lang=id-id] .heading-08 {
                font-size: 45px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=id-id] .heading-08 {
                font-size: calc(-95px + 13.67188vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=id-id] .heading-08 {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ms-my] .heading-08 {
                font-size: 75px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ms-my] .heading-08 {
                font-size: calc(15px + 5.85938vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ms-my] .heading-08 {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=vi-vn] .heading-08 {
                font-size: 55px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=vi-vn] .heading-08 {
                font-size: calc(-25px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=vi-vn] .heading-08 {
                font-size: 75px
            }
        }

        @media (min-width: 1024px) {
            .copy-01, .copy-03, .heading-09 {
                font-size: 1.6rem
            }

            .copy-02, .label-03 {
                font-size: 1rem;
                line-height: 1.6
            }

            .label-03 {
                font-size: 1.6rem
            }

            .label-04 {
                font-size: 1rem
            }
        }

        .container {
            height: auto;
            width: 100%;
            position: relative;
            overflow: hidden
        }

        [dir] .container {
            padding: 0 7.5%
        }

        .content-wrapper {
            height: auto;
            width: 100%;
            max-width: 123rem;
            position: relative;
            color: #fff
        }

        [dir] .content-wrapper {
            padding: 0;
            margin: 0 auto
        }

        .content-wrapper.light {
            color: #141e37
        }

        @media (min-width: 1024px) {
            [dir=ltr] .container, [dir=rtl] .container {
                padding-left: 7.3%;
                padding-right: 7.3%
            }
        }

        .layout-3yaYV {
            width: 100%;
            height: 100%;
            font-size: 1.6rem;
            color: #111
        }

        [dir] .layout-3yaYV {
            margin: 0 auto
        }

        .popupBackdrop-3TNh1 {
            height: 100%;
            width: 100%;
            position: absolute;
            overflow: auto;
            position: fixed;
            top: 0;
            bottom: 0;
            z-index: 5
        }

        [dir] .popupBackdrop-3TNh1 {
            background-color: rgba(17, 17, 17, .9)
        }

        [dir=ltr] .popupBackdrop-3TNh1, [dir=rtl] .popupBackdrop-3TNh1 {
            left: 0;
            right: 0
        }

        .closeButton-1ezYJ {
            height: 4rem;
            width: 4rem;
            display: flex;
            position: absolute;
            top: 0;
            z-index: 5;
            transition: background-color .2s ease-out
        }

        [dir] .closeButton-1ezYJ {
            padding: 1.2rem;
            border: 2px solid #fff;
            border-radius: 100%;
            margin: 1.5rem
        }

        [dir=ltr] .closeButton-1ezYJ {
            right: 0
        }

        [dir=rtl] .closeButton-1ezYJ {
            left: 0
        }

        .closeButton-1ezYJ path {
            transition: stroke .2s ease-out
        }

        [dir] .popupWrapper-Xl1ZX {
            padding-top: 19rem;
            padding-bottom: 8rem
        }

        .youtubePopupWrapper-3-bo7 {
            position: relative;
            width: 100%
        }

        [dir] .youtubePopupWrapper-3-bo7 {
            margin-top: 50%
        }

        [dir=ltr] .youtubePopupWrapper-3-bo7, [dir=rtl] .youtubePopupWrapper-3-bo7 {
            margin-left: auto;
            margin-right: auto
        }

        .youtubePopupWrapper-3-bo7 .popupInner-3vHu8 {
            height: 0;
            position: relative
        }

        [dir] .youtubePopupWrapper-3-bo7 .popupInner-3vHu8 {
            padding-bottom: 56.25%
        }

        .youtubePopupWrapper-3-bo7 .closeButton-1ezYJ {
            height: 3rem;
            width: 3rem
        }

        [dir] .youtubePopupWrapper-3-bo7 .closeButton-1ezYJ {
            padding: .8rem;
            margin: .5rem
        }

        @media (min-width: 1024px) {
            .youtubePopupWrapper-3-bo7 {
                width: 80%
            }

            [dir] .youtubePopupWrapper-3-bo7 {
                margin-top: 11rem
            }
        }

        .popupInner-3vHu8, .popupWrapper-Xl1ZX {
            position: relative
        }

        @media (min-width: 768px) {
            .closeButton-1ezYJ {
                height: 6.5rem;
                width: 6.5rem
            }

            [dir] .closeButton-1ezYJ {
                padding: 2.2rem;
                margin: 3rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .popupWrapper-Xl1ZX {
                padding-top: 21.2rem
            }

            .closeButton-1ezYJ {
                height: 4rem;
                width: 4rem
            }

            [dir] .closeButton-1ezYJ {
                padding: 1rem;
                margin: 2.5rem
            }

            [dir] .closeButton-1ezYJ:hover {
                background: #fff
            }

            .closeButton-1ezYJ:hover path {
                stroke: #141e37
            }
        }

        .icon-2tHD8 {
            height: 100%;
            width: 100%;
            display: inline-block;
            pointer-events: none
        }

        .icon-2tHD8 svg {
            height: 100%;
            width: 100%;
            display: block
        }

        .icon-2tHD8.currentColor-LyOgN svg {
            fill: currentColor
        }

        [dir] .secondaryButton-14xxs {
            padding: 0
        }

        .popupInnerWrapper-3x_1p {
            position: relative
        }

        [dir] .popupInnerWrapper-3x_1p {
            padding: 7rem 3.75rem 5rem
        }

        .dash-detail-top-eb2MM {
            height: .6rem;
            width: 6rem;
            position: absolute;
            top: -.6rem
        }

        [dir] .dash-detail-top-eb2MM {
            background: #fff
        }

        [dir=ltr] .dash-detail-top-eb2MM {
            left: 0
        }

        [dir=rtl] .dash-detail-top-eb2MM {
            right: 0
        }

        .dash-detail-top-eb2MM:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .dash-detail-top-eb2MM:after {
            border-bottom: .6rem solid #fff
        }

        [dir=ltr] .dash-detail-top-eb2MM:after {
            right: -.6rem;
            border-right: .6rem solid transparent
        }

        [dir=rtl] .dash-detail-top-eb2MM:after {
            left: -.6rem;
            border-left: .6rem solid transparent
        }

        .dash-detail-left-3jf4N {
            position: absolute;
            top: 7rem;
            height: 6rem;
            width: .5rem
        }

        [dir] .dash-detail-left-3jf4N {
            background: #fff
        }

        [dir=ltr] .dash-detail-left-3jf4N {
            left: 0
        }

        [dir=rtl] .dash-detail-left-3jf4N {
            right: 0
        }

        .dash-detail-left-3jf4N:after {
            content: "";
            display: block;
            position: absolute;
            bottom: -.5rem
        }

        [dir] .dash-detail-left-3jf4N:after {
            border-top: .5rem solid #fff
        }

        [dir=ltr] .dash-detail-left-3jf4N:after {
            right: 0;
            border-right: .5rem solid transparent
        }

        [dir=rtl] .dash-detail-left-3jf4N:after {
            left: 0;
            border-left: .5rem solid transparent
        }

        .croppedBackground-35JVw {
            content: "";
            display: block;
            position: absolute;
            height: 100%;
            width: 100%;
            top: 0;
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 3.25rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 3.25rem) 100%, 0 100%)
        }

        [dir] .croppedBackground-35JVw {
            background: #2858f0
        }

        [dir=ltr] .croppedBackground-35JVw {
            background: linear-gradient(30deg, #2858f0 20%, #32c8ff 60%);
            left: 0
        }

        [dir=rtl] .croppedBackground-35JVw {
            background: linear-gradient(-30deg, #2858f0 20%, #32c8ff 60%);
            right: 0;
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 3rem 100%, 0 calc(100% - 3.25rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 3rem 100%, 0 calc(100% - 3.25rem))
        }

        .registrationOptionsWrapper-dM5j2 {
            position: relative;
            display: flex;
            flex-direction: column
        }

        [dir] .registrationOptionTitle-11B2- {
            margin-bottom: 2.5rem
        }

        .registrationOption-3J87X {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            position: relative
        }

        [dir] .registrationOption-3J87X {
            text-align: center
        }

        [dir] .registrationOption-3J87X:not(:last-child) {
            margin-bottom: 7rem
        }

        .ctaWrapper-1p2xC {
            display: flex;
            flex-direction: column;
            justify-self: flex-end
        }

        [dir] .ctaWrapper-1p2xC {
            margin-top: 3.25rem
        }

        [dir] .registrationButton-1XgPW:not(:last-child) {
            margin-bottom: 2rem
        }

        @media (min-width: 768px) {
            [dir] .popupInnerWrapper-3x_1p {
                padding: 14rem 7.5rem 10rem
            }

            .popupInnerWrapper-3x_1p:before {
                width: 12rem
            }

            .dash-detail-top-eb2MM {
                height: .6rem;
                width: 12rem;
                top: -.6rem
            }

            [dir] .dash-detail-top-eb2MM {
                background: #fff
            }

            .dash-detail-top-eb2MM:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .dash-detail-top-eb2MM:after {
                border-bottom: .6rem solid #fff
            }

            [dir=ltr] .dash-detail-top-eb2MM:after {
                right: -.6rem;
                border-right: .6rem solid transparent
            }

            [dir=rtl] .dash-detail-top-eb2MM:after {
                left: -.6rem;
                border-left: .6rem solid transparent
            }

            .dashDetailLeft-1mVLy {
                top: 14rem;
                height: 12rem;
                width: .5rem
            }

            [dir] .dashDetailLeft-1mVLy {
                background: #fff
            }

            .dashDetailLeft-1mVLy:after {
                content: "";
                display: block;
                position: absolute;
                bottom: -.5rem
            }

            [dir] .dashDetailLeft-1mVLy:after {
                border-top: .5rem solid #fff
            }

            [dir=ltr] .dashDetailLeft-1mVLy:after {
                right: 0;
                border-right: .5rem solid transparent
            }

            [dir=rtl] .dashDetailLeft-1mVLy:after {
                left: 0;
                border-left: .5rem solid transparent
            }

            .croppedBackground-35JVw {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 6rem), calc(100% - 6.5rem) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 6rem), calc(100% - 6.5rem) 100%, 0 100%)
            }

            [dir=rtl] .croppedBackground-35JVw {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 6rem 100%, 0 calc(100% - 6.5rem));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 6rem 100%, 0 calc(100% - 6.5rem))
            }

            [dir] .registrationOptionTitle-11B2- {
                margin-bottom: 5rem
            }

            [dir] .ctaWrapper-1p2xC {
                margin-top: 6.5rem
            }

            [dir] .registrationButton-1XgPW:not(:last-child) {
                margin-bottom: 4rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .popupInnerWrapper-3x_1p {
                padding: 6rem 8rem 5rem;
                margin-bottom: 0
            }

            .popupInnerWrapper-3x_1p:before {
                width: 9rem
            }

            .dash-detail-top-eb2MM {
                height: .6rem;
                width: 9rem
            }

            [dir] .dash-detail-top-eb2MM {
                background: #fff
            }

            .dash-detail-top-eb2MM:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .dash-detail-top-eb2MM:after {
                border-bottom: .6rem solid #fff
            }

            [dir=ltr] .dash-detail-top-eb2MM:after {
                right: -.6rem;
                border-right: .6rem solid transparent
            }

            [dir=rtl] .dash-detail-top-eb2MM:after {
                left: -.6rem;
                border-left: .6rem solid transparent
            }

            .dashDetailLeft-1mVLy {
                display: none
            }

            .registrationOption-3J87X {
                width: 50%
            }

            [dir] .registrationOption-3J87X:not(:last-child) {
                margin-bottom: 0
            }

            [dir=ltr] .registrationOption-3J87X:first-of-type {
                padding-right: 7rem
            }

            [dir=ltr] .registrationOption-3J87X:last-of-type, [dir=rtl] .registrationOption-3J87X:first-of-type {
                padding-left: 7rem
            }

            [dir=rtl] .registrationOption-3J87X:last-of-type {
                padding-right: 7rem
            }

            .registrationOption-3J87X:nth-of-type(2):before {
                content: "";
                display: block;
                position: absolute;
                top: -.5rem;
                width: .2rem;
                height: calc(100% + 1rem)
            }

            [dir] .registrationOption-3J87X:nth-of-type(2):before {
                background: #fff
            }

            [dir=ltr] .registrationOption-3J87X:nth-of-type(2):before {
                left: -.2rem
            }

            [dir=rtl] .registrationOption-3J87X:nth-of-type(2):before {
                right: -.2rem
            }

            .lineDetail-EVBpi {
                height: 8rem;
                width: .5rem;
                position: absolute;
                top: -1rem
            }

            [dir] .lineDetail-EVBpi {
                background: #fff
            }

            [dir=ltr] .lineDetail-EVBpi {
                left: -.5rem
            }

            [dir=rtl] .lineDetail-EVBpi {
                right: -.5rem
            }

            .lineDetail-EVBpi:after {
                content: "";
                display: block;
                position: absolute;
                bottom: -.5rem
            }

            [dir] .lineDetail-EVBpi:after {
                border-top: .5rem solid #fff
            }

            [dir=ltr] .lineDetail-EVBpi:after {
                right: 0;
                border-left: .5rem solid transparent
            }

            [dir=rtl] .lineDetail-EVBpi:after {
                left: 0;
                border-right: .5rem solid transparent
            }

            .registrationOptionsWrapper-dM5j2 {
                flex-direction: row
            }

            [dir] .registrationDescription-1lwcB {
                padding: 0 2rem
            }

            .ctaWrapper-1p2xC {
                max-width: none
            }

            [dir] .registrationOptionTitle-11B2- {
                margin-bottom: 4rem
            }
        }

        @media (min-width: 1280px) {
            [dir] .registrationDescription-1lwcB {
                padding: 0 6rem
            }

            .ctaWrapper-1p2xC {
                flex-direction: row
            }

            [dir] .ctaWrapper-1p2xC {
                margin-top: 6.5rem
            }

            [dir] .registrationButton-1XgPW:not(:last-child) {
                margin-bottom: 0
            }

            [dir=ltr] .registrationButton-1XgPW:not(:last-child) {
                margin-right: 2rem
            }

            [dir=rtl] .registrationButton-1XgPW:not(:last-child) {
                margin-left: 2rem
            }
        }

        .primaryButton-2ec0w {
            transition: color .2s ease-out;
            z-index: 1;
            position: relative;
            width: 100%;
            min-width: 15rem;
            overflow: hidden;
            visibility: hidden
        }

        [dir] .primaryButton-2ec0w {
            padding: 1.5rem 2.75rem;
            text-align: center
        }

        [dir=ltr] .primaryButton-2ec0w {
            transform: skewX(-14.5deg)
        }

        [dir=rtl] .primaryButton-2ec0w {
            transform: skewX(14.5deg)
        }

        .primaryButton-2ec0w .background-u8Ka7 {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0;
            transition: background-color .2s ease-out
        }

        [dir=ltr] .primaryButton-2ec0w .background-u8Ka7 {
            left: 0
        }

        [dir=rtl] .primaryButton-2ec0w .background-u8Ka7 {
            right: 0
        }

        .primaryButton-2ec0w.disabled-12PLo {
            opacity: .7
        }

        [dir=ltr] .primaryButton-2ec0w .label-3gc3A {
            transform: skewX(14.5deg)
        }

        [dir=rtl] .primaryButton-2ec0w .label-3gc3A {
            transform: skewX(-14.5deg)
        }

        .primaryButton-2ec0w .label-3gc3A.iconWrapper-3XtC2 {
            display: flex;
            align-items: center;
            justify-content: center
        }

        .primaryButton-2ec0w .animatedBorder-1iWpQ {
            content: "";
            display: block;
            position: absolute;
            height: .2rem;
            width: .2rem;
            pointer-events: none
        }

        [dir] .primaryButton-2ec0w .animatedBorder-1iWpQ {
            background: transparent
        }

        .primaryButton-2ec0w .animatedBorder-1iWpQ.top-cn3E4 {
            top: 0
        }

        [dir] .primaryButton-2ec0w .animatedBorder-1iWpQ.top-cn3E4 {
            border-top-width: 2px
        }

        [dir=ltr] .primaryButton-2ec0w .animatedBorder-1iWpQ.top-cn3E4 {
            left: 0;
            transform-origin: top left;
            border-right-width: 2px;
            border-style: solid solid none none
        }

        [dir=rtl] .primaryButton-2ec0w .animatedBorder-1iWpQ.top-cn3E4 {
            right: 0;
            transform-origin: top right;
            border-left-width: 2px;
            border-style: solid none none solid
        }

        .primaryButton-2ec0w .animatedBorder-1iWpQ.bottom-B_Gla {
            bottom: 0
        }

        [dir] .primaryButton-2ec0w .animatedBorder-1iWpQ.bottom-B_Gla {
            border-bottom-width: 2px
        }

        [dir=ltr] .primaryButton-2ec0w .animatedBorder-1iWpQ.bottom-B_Gla {
            right: 0;
            transform-origin: bottom right;
            border-left-width: 2px;
            border-style: none none solid solid
        }

        [dir=rtl] .primaryButton-2ec0w .animatedBorder-1iWpQ.bottom-B_Gla {
            left: 0;
            transform-origin: bottom left;
            border-right-width: 2px;
            border-style: none solid solid none
        }

        .default-2alKM {
            color: #fff
        }

        [dir] .default-2alKM .background-u8Ka7 {
            background-color: #32c8ff
        }

        [dir] .default-2alKM .animatedBorder-1iWpQ {
            border-color: #32c8ff
        }

        .light-2mmpH {
            color: #141e37
        }

        [dir] .light-2mmpH .background-u8Ka7 {
            background-color: #fff
        }

        [dir] .light-2mmpH .animatedBorder-1iWpQ {
            border-color: #fff
        }

        .dark-3gXr0 {
            color: #fff
        }

        [dir] .dark-3gXr0 .background-u8Ka7 {
            background-color: #141e37
        }

        [dir] .dark-3gXr0 .animatedBorder-1iWpQ {
            border-color: #141e37
        }

        .icon-Cxc7G {
            height: 1.6rem;
            width: 1.6rem
        }

        [dir=ltr] .icon-Cxc7G {
            margin-left: 1rem
        }

        [dir=rtl] .icon-Cxc7G {
            margin-right: 1rem
        }

        @media (min-width: 768px) {
            .primaryButton-2ec0w {
                min-width: 30rem
            }

            [dir] .primaryButton-2ec0w {
                padding: 3rem 6.5rem
            }
        }

        @media (min-width: 1024px) {
            .primaryButton-2ec0w {
                min-width: 18rem;
                pointer-events: none
            }

            [dir] .primaryButton-2ec0w {
                padding: 1.8rem 2.9rem
            }

            [dir] .primaryButton-2ec0w:hover .animatedBorder-1iWpQ {
                background-color: transparent
            }

            .primaryButton-2ec0w:hover.light-2mmpH {
                color: #fff
            }

            [dir] .primaryButton-2ec0w:hover.light-2mmpH .background-u8Ka7 {
                background-color: #141e37
            }

            .primaryButton-2ec0w:hover.default-2alKM {
                color: #32c8ff
            }

            [dir] .primaryButton-2ec0w:hover.default-2alKM .background-u8Ka7 {
                background-color: #fff
            }

            .primaryButton-2ec0w:hover.dark-3gXr0 {
                color: #141e37
            }

            [dir] .primaryButton-2ec0w:hover.dark-3gXr0 .background-u8Ka7 {
                background-color: #fff
            }

            .primaryButton-2ec0w:hover.hasWhiteBackground-3bKmI.default-2alKM {
                color: #fff
            }

            [dir] .primaryButton-2ec0w:hover.hasWhiteBackground-3bKmI.default-2alKM .background-u8Ka7 {
                background-color: #141e37
            }

            .primaryButton-2ec0w:hover.hasWhiteBackground-3bKmI.dark-3gXr0 {
                color: #fff
            }

            [dir] .primaryButton-2ec0w:hover.hasWhiteBackground-3bKmI.dark-3gXr0 .background-u8Ka7 {
                background-color: #32c8ff
            }
        }

        .loader-3Z0Sy {
            height: 5rem;
            width: 5rem
        }

        .loader-3Z0Sy.relativeSize-381r7 {
            height: 40%;
            width: 40%
        }

        .loader-3Z0Sy svg path {
            fill: none;
            stroke-dasharray: 350;
            stroke-dashoffset: 350;
            stroke-width: 2px
        }

        [dir=ltr] .loader-3Z0Sy svg path, [dir=rtl] .loader-3Z0Sy svg path {
            animation: animate-stroke-3RpMd 1s linear infinite forwards
        }

        .loader-3Z0Sy.light-2B9lw svg path {
            stroke: #fff
        }

        .loader-3Z0Sy.dark-16ius svg path {
            stroke: #6e87ff
        }

        @keyframes animate-stroke-3RpMd {
            0% {
                opacity: 1;
                stroke-dashoffset: 350
            }
            50% {
                opacity: 1
            }
            90% {
                opacity: 0;
                stroke-dashoffset: 0
            }
            99% {
                opacity: 0;
                stroke-dashoffset: 0
            }
        }

        .header-1mER8 {
            position: absolute;
            top: 0;
            height: 8rem
        }

        [dir] .header-1mER8 {
            background-color: #000;
            margin-top: -8rem
        }

        [dir=ltr] .header-1mER8, [dir=rtl] .header-1mER8 {
            right: 0;
            left: 0
        }

        .video-CB3lJ, .video-CB3lJ video {
            height: 100%;
            width: 100%
        }

        .video-CB3lJ video {
            display: block;
            -o-object-fit: cover;
            object-fit: cover;
            -o-object-position: center;
            object-position: center
        }

        [dir] .video-CB3lJ video {
            border: none
        }

        .is-edge-pxol0 .video-CB3lJ video, .is-ie-1b6yW .video-CB3lJ video {
            position: absolute;
            top: 50%
        }

        [dir=ltr] .is-edge-pxol0 .video-CB3lJ video, [dir=ltr] .is-ie-1b6yW .video-CB3lJ video {
            transform: translate(-50%, -50%);
            left: 50%;
            right: 50%
        }

        [dir=rtl] .is-edge-pxol0 .video-CB3lJ video, [dir=rtl] .is-ie-1b6yW .video-CB3lJ video {
            transform: translate(50%, -50%);
            right: 50%;
            left: 50%
        }

        .video-CB3lJ video:focus {
            outline: none
        }

        .poster-3GL6m {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0
        }

        [dir] .poster-3GL6m {
            background-size: cover;
            background-position: 50%
        }

        .gameRegistration-Pg50j {
            position: relative;
            min-height: 59rem;
            z-index: 1
        }

        [dir] .gameRegistration-Pg50j {
            text-align: center
        }

        html[browser="Internet Explorer"] .gameRegistration-Pg50j {
            overflow: hidden
        }

        .storesWrapper-29oqK {
            display: flex;
            flex-wrap: wrap;
            justify-content: center
        }

        .mainContainer-2Z4DA {
            top: 50%;
            position: absolute
        }

        [dir] .mainContainer-2Z4DA {
            transform: translateY(-50%)
        }

        [dir] .registerButton-l5hcN {
            margin-bottom: 3.2rem
        }

        .gameRegistrationVideo-3HYvt {
            position: absolute;
            height: 115%
        }

        [dir] .gameRegistrationVideo-3HYvt {
            transform: translateY(-13%)
        }

        .gameRegistrationVideo-3HYvt:after {
            content: "";
            display: block;
            position: absolute;
            height: 100%;
            width: 100%;
            top: 0;
            opacity: .2
        }

        [dir] .gameRegistrationVideo-3HYvt:after {
            background-color: #111
        }

        [dir=ltr] .gameRegistrationVideo-3HYvt:after {
            left: 0
        }

        [dir=rtl] .gameRegistrationVideo-3HYvt:after {
            right: 0
        }

        .contentWrapper-34dzw {
            display: flex;
            flex-direction: column;
            align-items: center
        }

        [dir] .contentWrapper-34dzw {
            padding: 5rem 0 6.25rem
        }

        .appStoreWrapper-2vz4u {
            display: flex;
            flex-direction: column
        }

        .appButton-1oYeg {
            height: 3.5rem
        }

        [dir] .appButton-1oYeg {
            margin: 0 .75rem
        }

        .appButton-1oYeg img {
            height: 100%;
            width: auto
        }

        [dir] .appButton-1oYeg:not(:last-child) {
            margin-bottom: 1.5rem
        }

        .disabled-waBSk {
            height: 3.5rem;
            width: auto;
            position: relative
        }

        .disabled-waBSk:after {
            content: "";
            display: block;
            position: absolute;
            height: 100%;
            width: 100%;
            pointer-events: none;
            top: 0;
            opacity: .6
        }

        [dir] .disabled-waBSk:after {
            background-color: #888;
            border-radius: .6rem
        }

        [dir=ltr] .disabled-waBSk:after {
            left: 0
        }

        [dir=rtl] .disabled-waBSk:after {
            right: 0
        }

        @media (min-width: 480px) {
            [dir=ltr] .registrationHero-3H8xK .appButton-1oYeg:not(:last-child) {
                margin-right: 1.5rem
            }

            [dir=rtl] .registrationHero-3H8xK .appButton-1oYeg:not(:last-child) {
                margin-left: 1.5rem
            }
        }

        @media (min-width: 768px) {
            .gameRegistration-Pg50j {
                min-height: 119.7rem
            }

            [dir] .registerButton-l5hcN {
                margin-bottom: 6rem
            }

            .appButton-1oYeg, .disabled-waBSk {
                height: 4rem
            }

            .storesWrapper-29oqK {
                height: 6rem
            }
        }

        @media (min-width: 1024px) {
            .gameRegistration-Pg50j {
                min-height: 100.8rem
            }

            .storesWrapper-29oqK {
                height: 4rem
            }

            [dir] .registerButton-l5hcN {
                margin-bottom: 3.2rem
            }

            .appStoreWrapper-2vz4u {
                flex-direction: row;
                align-items: center
            }
        }

        @media (min-width: 1280px) {
            .gameRegistration-Pg50j {
                min-height: 81.3rem
            }
        }

        .playButton-1HE4d {
            height: 5rem;
            width: 5rem;
            top: 50%;
            pointer-events: all;
            position: absolute;
            z-index: 2
        }

        [dir] .playButton-1HE4d {
            cursor: pointer
        }

        [dir=ltr] .playButton-1HE4d {
            left: 50%;
            transform: translate(-50%, -50%)
        }

        [dir=rtl] .playButton-1HE4d {
            right: 50%;
            transform: translate(50%, -50%)
        }

        .modalContent-2ieg9 {
            width: 100%;
            height: 100%
        }

        .posterContainer-3PMhr .posterImage-3av-B {
            height: 100%;
            width: 100%;
            position: absolute;
            z-index: 1;
            display: block;
            top: 0
        }

        [dir=ltr] .posterContainer-3PMhr .posterImage-3av-B {
            left: 0
        }

        [dir=rtl] .posterContainer-3PMhr .posterImage-3av-B {
            right: 0
        }

        .posterContainer-3PMhr .youtubeVideo-2LLmZ {
            height: 100%
        }

        [dir] .posterContainer-3PMhr .youtubeVideo-2LLmZ {
            background: #111
        }

        .posterContainer-3PMhr .youtubeVideo-2LLmZ > iframe {
            height: 100%;
            width: 100%
        }

        .posterContainer-3PMhr .overlay-2VKk6 {
            content: "";
            position: absolute;
            display: initial;
            top: 0;
            bottom: 0;
            z-index: 1
        }

        [dir] .posterContainer-3PMhr .overlay-2VKk6 {
            background: rgba(0, 0, 0, .2)
        }

        [dir=ltr] .posterContainer-3PMhr .overlay-2VKk6, [dir=rtl] .posterContainer-3PMhr .overlay-2VKk6 {
            left: 0;
            right: 0
        }

        @media (min-width: 768px) {
            .playButton-1HE4d {
                height: 6rem;
                width: 6rem
            }
        }

        @media (min-width: 1024px) {
            .playButton-1HE4d {
                top: 50%
            }

            .playButton-1HE4d circle {
                transition: stroke .1s ease-out
            }

            .playButton-1HE4d path {
                transition: transform .1s ease-out
            }

            [dir] .playButton-1HE4d path {
                transform-origin: center;
                transform: scale(1) translate(0)
            }

            .playButton-1HE4d:before {
                content: "";
                display: block;
                position: absolute;
                height: 100%;
                width: 100%;
                top: 0;
                opacity: 0;
                z-index: -1;
                transition: opacity .1s ease-out
            }

            [dir] .playButton-1HE4d:before {
                border-radius: 100%;
                background: #141e37
            }

            [dir=ltr] .playButton-1HE4d:before {
                left: 0
            }

            [dir=rtl] .playButton-1HE4d:before {
                right: 0
            }

            .playButton-1HE4d:hover circle {
                stroke: #141e37
            }

            [dir] .playButton-1HE4d:hover path {
                transform: scale(1.2) translate(0)
            }

            .playButton-1HE4d:hover:before {
                opacity: 1
            }
        }

        .popupInnerWrapper-1IF9J {
            position: absolute;
            top: 0;
            bottom: 0
        }

        [dir=ltr] .popupInnerWrapper-1IF9J, [dir=rtl] .popupInnerWrapper-1IF9J {
            left: 0;
            right: 0
        }

        .gameImageData-zkvSz {
            position: relative
        }

        [dir] .gameImageData-zkvSz {
            padding-bottom: 25rem
        }

        .gameImageData-zkvSz:before {
            content: "";
            display: block;
            position: absolute;
            height: 100%;
            width: 100%;
            top: .5rem
        }

        [dir] .gameImageData-zkvSz:before {
            background-repeat: no-repeat;
            /*background-image: url(../static/game-data-image-background-mobile-d094b50430f23ffb4e1561202e3ac7c7.jpg);*/
            background-position: center 70%
        }

        [dir=ltr] .gameImageData-zkvSz:before {
            left: 0
        }

        [dir=rtl] .gameImageData-zkvSz:before {
            right: 0
        }

        .gameImageData-zkvSz:after {
            content: "";
            display: block;
            position: absolute;
            height: 100%;
            width: 100%;
            top: 0
        }

        [dir] .gameImageData-zkvSz:after {
            background: rgba(50, 200, 255, .4);
            background: linear-gradient(0deg, rgba(50, 200, 255, .4) 25%, transparent 40%)
        }

        [dir=ltr] .gameImageData-zkvSz:after {
            left: 0
        }

        [dir=rtl] .gameImageData-zkvSz:after {
            right: 0
        }

        [dir] .gameImageData-zkvSz .title-U1XU8 {
            margin-bottom: 2.25rem
        }

        .gameImageData-zkvSz .bottomVolt-2lxc1, .gameImageData-zkvSz .topVolt-pNMQz {
            display: none
        }

        .gameImageData-zkvSz .mobileTopVolt-3R9X8 {
            top: -10rem
        }

        .gameImageData-zkvSz .mobileTopVolt-3R9X8 img {
            width: auto
        }

        .gameImageData-zkvSz .mobileBottomVolt-3pvK7 {
            z-index: 1;
            bottom: 12%
        }

        .gameImageData-zkvSz .mobileBottomVolt-3pvK7 img {
            width: auto
        }

        .gameImageData-zkvSz .contentWrapper-1SCaE {
            display: flex;
            flex-direction: column
        }

        .gameImageData-zkvSz .titleCopyWrapper-3UxOk {
            position: relative
        }

        .gameImageData-zkvSz .copyWrapper-Vs74R {
            z-index: 2;
            position: relative
        }

        .gameImageData-zkvSz .ctasWrapper-2gHih {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            position: absolute;
            width: 100%;
            bottom: -20rem;
            z-index: 3
        }

        [dir=ltr] .gameImageData-zkvSz .ctasWrapper-2gHih {
            left: 0
        }

        [dir=rtl] .gameImageData-zkvSz .ctasWrapper-2gHih {
            right: 0
        }

        [dir] .gameImageData-zkvSz .findChampButton-33_qx {
            margin-bottom: 2.5rem
        }

        .gameImageData-zkvSz .leftLine-1nhET {
            height: calc(100% - 1rem);
            width: .3rem;
            display: none;
            position: absolute;
            top: 1rem
        }

        [dir=ltr] .gameImageData-zkvSz .leftLine-1nhET {
            left: -.3rem
        }

        [dir=rtl] .gameImageData-zkvSz .leftLine-1nhET {
            right: -.3rem
        }

        [dir] .gameImageData-zkvSz .leftLine-1nhET.dark-1QlD_ {
            background-color: #141e37
        }

        .gameImageData-zkvSz .championImage-1iMem {
            position: absolute;
            width: 150%;
            top: 0
        }

        [dir=ltr] .gameImageData-zkvSz .championImage-1iMem {
            left: -25%
        }

        [dir=rtl] .gameImageData-zkvSz .championImage-1iMem {
            right: -25%
        }

        .gameImageData-zkvSz .championWrapper-6JBVW {
            position: relative
        }

        [dir] .gameImageData-zkvSz .championWrapper-6JBVW {
            padding-top: 100%
        }

        @media (min-width: 768px) {
            .gameImageData-zkvSz .mobileTopVolt-3R9X8 {
                width: 160%;
                top: calc(25% + 10rem - 55vw)
            }

            .gameImageData-zkvSz .mobileTopVolt-3R9X8 img {
                width: 100%
            }

            html[lang=tr-tr] .gameImageData-zkvSz .title-U1XU8 {
                font-size: 5rem
            }

            [dir] .gameImageData-zkvSz .findChampButton-33_qx {
                margin-bottom: 5rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .gameImageData-zkvSz {
                padding-top: 20rem;
                padding-bottom: 20rem
            }

            html[dir][lang=de-de] .gameImageData-zkvSz, html[dir][lang=es-es] .gameImageData-zkvSz, html[dir][lang=fr-fr] .gameImageData-zkvSz, html[dir][lang=it-it] .gameImageData-zkvSz, html[dir][lang=pl-pl] .gameImageData-zkvSz {
                padding-bottom: 17rem
            }

            [dir] .gameImageData-zkvSz:after {
                background: rgba(50, 200, 255, .4);
                background: linear-gradient(0deg, rgba(50, 200, 255, .4), transparent 50%)
            }

            [dir] .gameImageData-zkvSz:before {
                background-image: url(../static/game-data-image-background-desktop-d0d1aa2090b43bdc9664da6564535c45.jpg);
                background-size: cover
            }

            .gameImageData-zkvSz .contentWrapper-1SCaE {
                flex-direction: row-reverse;
                align-items: flex-start;
                justify-content: space-between
            }

            .gameImageData-zkvSz .leftLine-1nhET {
                display: block
            }

            .gameImageData-zkvSz .description-eFk_k {
                width: 100%;
                -webkit-font-kerning: none;
                font-kerning: none
            }

            [dir=ltr] .gameImageData-zkvSz .description-eFk_k {
                padding-left: 3rem
            }

            [dir=rtl] .gameImageData-zkvSz .description-eFk_k {
                padding-right: 3rem
            }

            html[browser="Internet Explorer"] .gameImageData-zkvSz .description-eFk_k {
                max-width: 80%
            }

            .gameImageData-zkvSz .bottomVolt-2lxc1, .gameImageData-zkvSz .topVolt-pNMQz {
                display: block
            }

            .gameImageData-zkvSz .mobileBottomVolt-3pvK7, .gameImageData-zkvSz .mobileTopVolt-3R9X8 {
                display: none
            }

            .gameImageData-zkvSz .topVolt-pNMQz {
                width: 105%
            }

            .gameImageData-zkvSz .bottomVolt-2lxc1 {
                z-index: 1
            }

            [dir] .gameImageData-zkvSz .bottomVolt-2lxc1 {
                transform: scaleX(1) translateY(-8%)
            }

            [dir=rtl] .gameImageData-zkvSz .bottomVolt-2lxc1 {
                transform: scaleX(-1) translateY(-8%)
            }

            .gameImageData-zkvSz .titleCopyWrapper-3UxOk {
                width: calc(50% - 1.5rem)
            }

            [dir] .gameImageData-zkvSz .titleCopyWrapper-3UxOk {
                margin-bottom: 2rem
            }

            [dir] .gameImageData-zkvSz .title-U1XU8 {
                margin-bottom: 4rem
            }

            html[lang=tr-tr] .gameImageData-zkvSz .title-U1XU8 {
                font-size: 6rem
            }

            .gameImageData-zkvSz .ctasWrapper-2gHih {
                flex-direction: row;
                justify-content: flex-start;
                position: static
            }

            [dir] .gameImageData-zkvSz .ctasWrapper-2gHih {
                margin-top: 3.5rem
            }

            [dir=ltr] .gameImageData-zkvSz .ctasWrapper-2gHih {
                padding-left: 3rem
            }

            [dir=rtl] .gameImageData-zkvSz .ctasWrapper-2gHih {
                padding-right: 3rem
            }

            [dir] .gameImageData-zkvSz .findChampButton-33_qx {
                margin-bottom: 0
            }

            [dir=ltr] .gameImageData-zkvSz .findChampButton-33_qx {
                margin-right: 3rem
            }

            [dir=rtl] .gameImageData-zkvSz .findChampButton-33_qx {
                margin-left: 3rem
            }

            .gameImageData-zkvSz .championWrapper-6JBVW {
                width: calc(50% - 1.5rem)
            }

            [dir] .gameImageData-zkvSz .championWrapper-6JBVW {
                padding-top: 49%
            }

            .gameImageData-zkvSz .championImage-1iMem {
                width: 153%;
                top: -25%
            }

            [dir=ltr] .gameImageData-zkvSz .championImage-1iMem {
                left: -40%
            }

            [dir=rtl] .gameImageData-zkvSz .championImage-1iMem {
                right: -40%
            }

            html[lang=ar-ae] .gameImageData-zkvSz .championImage-1iMem, html[lang=de-de] .gameImageData-zkvSz .championImage-1iMem, html[lang=es-es] .gameImageData-zkvSz .championImage-1iMem, html[lang=es-mx] .gameImageData-zkvSz .championImage-1iMem, html[lang=fr-fr] .gameImageData-zkvSz .championImage-1iMem, html[lang=it-it] .gameImageData-zkvSz .championImage-1iMem, html[lang=pl-pl] .gameImageData-zkvSz .championImage-1iMem, html[lang=pt-br] .gameImageData-zkvSz .championImage-1iMem, html[lang=th-th] .gameImageData-zkvSz .championImage-1iMem, html[lang=vi-vn] .gameImageData-zkvSz .championImage-1iMem {
                top: -10%
            }

            html[lang=th-th] .gameImageData-zkvSz .championImage-1iMem {
                top: 5%
            }
        }

        @media (min-width: 1280px) {
            [dir] .gameImageData-zkvSz, html[dir][lang=de-de] .gameImageData-zkvSz, html[dir][lang=es-es] .gameImageData-zkvSz, html[dir][lang=fr-fr] .gameImageData-zkvSz, html[dir][lang=it-it] .gameImageData-zkvSz, html[dir][lang=pl-pl] .gameImageData-zkvSz {
                padding-bottom: 20rem
            }

            html[dir][lang=tr-tr] .gameImageData-zkvSz {
                padding-bottom: 15rem
            }

            .gameImageData-zkvSz .description-eFk_k {
                max-width: calc(80% - .6rem);
                width: 100%
            }

            html[dir][lang=de-de] .gameImageData-zkvSz .championImage-1iMem, html[dir][lang=es-es] .gameImageData-zkvSz .championImage-1iMem, html[dir][lang=fr-fr] .gameImageData-zkvSz .championImage-1iMem, html[dir][lang=it-it] .gameImageData-zkvSz .championImage-1iMem, html[dir][lang=pl-pl] .gameImageData-zkvSz .championImage-1iMem {
                padding-top: -10%
            }

            html[lang=ar-ae] .gameImageData-zkvSz .championImage-1iMem, html[lang=es-mx] .gameImageData-zkvSz .championImage-1iMem, html[lang=pt-br] .gameImageData-zkvSz .championImage-1iMem, html[lang=th-th] .gameImageData-zkvSz .championImage-1iMem, html[lang=vi-vn] .gameImageData-zkvSz .championImage-1iMem {
                top: -25%
            }
        }

        @media (min-width: 1440px) {
            [dir] .gameImageData-zkvSz {
                padding-bottom: 16rem
            }

            .gameImageData-zkvSz .title-U1XU8 {
                width: 120%
            }

            html[lang=ru-ru] .gameImageData-zkvSz .title-U1XU8 {
                width: 115%
            }

            html[lang=id-id] .gameImageData-zkvSz .title-U1XU8 {
                width: 110%
            }

            html[lang=tr-tr] .gameImageData-zkvSz .title-U1XU8 {
                font-size: 8rem
            }

            .gameImageData-zkvSz .titleCopyWrapper-3UxOk {
                width: calc(41.66667% - 1.75rem)
            }

            [dir] .gameImageData-zkvSz .titleCopyWrapper-3UxOk {
                margin-bottom: 15rem
            }

            .gameImageData-zkvSz .description-eFk_k {
                max-width: calc(80% - .6rem);
                width: 100%
            }

            .gameImageData-zkvSz .championImage-1iMem {
                width: 168%;
                top: -35%
            }
        }

        @media (min-width: 1921px) {
            html[dir][lang=de-de] .gameImageData-zkvSz, html[dir][lang=es-es] .gameImageData-zkvSz, html[dir][lang=fr-fr] .gameImageData-zkvSz, html[dir][lang=it-it] .gameImageData-zkvSz, html[dir][lang=pl-pl] .gameImageData-zkvSz {
                padding-top: 20rem
            }
        }

        .articleSection-s3byS {
            position: relative;
            z-index: 4
        }

        [dir] .articleSection-s3byS {
            background-color: #fff
        }

        [dir] .richTextBlock-1Zef0 {
            margin: 2.25rem 0
        }

        [dir] .richTextBlock-1Zef0:not(:first-child) {
            padding-top: 0
        }

        .article-2iGqo {
            display: flex;
            width: 100%;
            color: #111;
            max-width: 123rem;
            justify-content: space-between
        }

        [dir] .article-2iGqo {
            padding: 0;
            margin: 4.5rem auto 0
        }

        .articleContent-3krbP {
            max-width: 100%
        }

        .aside-2saIZ {
            display: none
        }

        @media (min-width: 768px) {
            [dir] .richTextBlock-1Zef0 {
                margin: 4.5rem 0
            }
        }

        @media (min-width: 1024px) {
            .articleContent-3krbP {
                max-width: 66rem
            }

            [dir=ltr] .article-2iGqo {
                padding: 5rem 3.5rem 0 8.5rem
            }

            [dir=rtl] .article-2iGqo {
                padding: 5rem 8.5rem 0 3.5rem
            }

            .article-2iGqo.widerArticle-5sbyt .articleContent-3krbP {
                max-width: 80rem
            }

            [dir] .richTextBlock-1Zef0 {
                margin: 4rem 0
            }

            .articleSection-s3byS {
                overflow: visible
            }

            [dir] .tableWrapper-1tl5C {
                margin-bottom: 10rem
            }
        }

        @media (min-width: 1280px) {
            .aside-2saIZ {
                display: block;
                min-width: 25rem;
                position: relative;
                align-self: flex-start
            }
        }

        @media (min-width: 1440px) {
            [dir=ltr] .article-2iGqo {
                padding: 5rem 7rem 0 17rem
            }

            [dir=rtl] .article-2iGqo {
                padding: 5rem 17rem 0 7rem
            }
        }

        [class*=heading-].font-normal-A6Khr, [class*=label-].font-normal-A6Khr {
            font-style: normal
        }

        .heading-01-BgqKT, .heading-08-2PjDr {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-size: 4.5rem;
            line-height: .95;
            letter-spacing: 2px
        }

        html[lang=ja-jp] .heading-01-BgqKT, html[lang=ja-jp] .heading-08-2PjDr {
            font-weight: 700;
            font-size: 3.5rem;
            line-height: 1.1
        }

        html[lang=tr-tr] .heading-01-BgqKT, html[lang=tr-tr] .heading-08-2PjDr {
            line-height: 1.2
        }

        html[lang=ru-ru] .heading-01-BgqKT, html[lang=ru-ru] .heading-08-2PjDr {
            font-family: BeaufortforLOL-Heavy, arial, georgia, sans-serif;
            line-height: 1
        }

        html[lang=zh-hk] .heading-01-BgqKT, html[lang=zh-hk] .heading-08-2PjDr, html[lang=zh-tw] .heading-01-BgqKT, html[lang=zh-tw] .heading-08-2PjDr {
            line-height: 1.1
        }

        html[lang=ko-kr] .heading-01-BgqKT, html[lang=ko-kr] .heading-08-2PjDr {
            font-size: 3.5rem;
            line-height: 1.1
        }

        html[lang=ar-ae] .heading-01-BgqKT, html[lang=ar-ae] .heading-08-2PjDr {
            font-weight: 700;
            line-height: 1.6
        }

        html[lang=vi-vn] .heading-01-BgqKT, html[lang=vi-vn] .heading-08-2PjDr {
            font-size: 3.5rem
        }

        html[lang=es-es] .heading-01-BgqKT, html[lang=es-es] .heading-08-2PjDr, html[lang=es-mx] .heading-01-BgqKT, html[lang=es-mx] .heading-08-2PjDr {
            line-height: 1.05
        }

        .heading-02-3jMzI {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-size: 2.6rem;
            line-height: 1.04;
            letter-spacing: 2px
        }

        html[lang=ja-jp] .heading-02-3jMzI {
            font-size: 2rem;
            line-height: 1.1
        }

        html[lang=ru-ru] .heading-02-3jMzI {
            font-size: 2.4rem;
            line-height: 1.2
        }

        .heading-03-UfvlI {
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif;
            font-size: 1.4rem;
            line-height: 1.17
        }

        .heading-04-2H_tA {
            font-family: BeaufortforLOL-Heavy, arial, georgia, sans-serif;
            font-size: 1.8rem
        }

        html[lang=ja-jp] .heading-04-2H_tA {
            line-height: 1.6
        }

        .heading-05-1BzlG {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-size: 1.8rem;
            letter-spacing: 1px
        }

        .heading-06-1dl_c, .heading-09-3bHXU {
            font-family: Spiegel-SemiBold, arial, georgia, sans-serif;
            font-size: 1.3rem;
            letter-spacing: 1px
        }

        .heading-07-10eMw {
            font-size: 2.6rem;
            letter-spacing: 1px
        }

        .heading-07-10eMw, .heading-10-7_kOF {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif
        }

        .heading-10-7_kOF {
            font-size: 1.4rem
        }

        .copy-01-2wYLK, .copy-03-3RN7V {
            font-family: Spiegel-Regular, arial, georgia, sans-serif;
            font-size: 1.3rem;
            line-height: 1.42
        }

        .copy-02-2-e6g, .label-03-uAl2S {
            font-family: Spiegel-Regular, arial, georgia, sans-serif;
            font-size: 1rem;
            line-height: 1.5
        }

        .copy-03-3RN7V {
            font-family: Spiegel-SemiBold, arial, georgia, sans-serif
        }

        .label-01-3ndM5 {
            text-transform: uppercase;
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-style: italic;
            font-size: 1.8rem;
            line-height: 1.27;
            letter-spacing: 1px
        }

        html[lang=ar-ae] .label-01-3ndM5 {
            font-weight: 700
        }

        .label-02-1XOLo {
            text-transform: uppercase;
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif;
            font-size: 1.4rem;
            letter-spacing: 1px
        }

        html[lang=ar-ae] .label-02-1XOLo {
            font-weight: 700
        }

        .label-04-Czh3u {
            font-family: Spiegel-SemiBold, arial, georgia, sans-serif;
            font-size: 1rem
        }

        html[lang=ar-ae] .label-04-Czh3u {
            font-weight: 500
        }

        .label-05-c30vc {
            text-transform: uppercase;
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif;
            font-size: 1rem
        }

        @media (min-width: 768px) {
            .heading-01-BgqKT, .heading-08-2PjDr {
                font-size: 9rem
            }

            html[lang=ja-jp] .heading-01-BgqKT, html[lang=ja-jp] .heading-08-2PjDr {
                font-size: 6.5rem
            }

            html[lang=ko-kr] .heading-01-BgqKT, html[lang=ko-kr] .heading-08-2PjDr, html[lang=vi-vn] .heading-01-BgqKT, html[lang=vi-vn] .heading-08-2PjDr {
                font-size: 7rem
            }

            .heading-02-3jMzI {
                font-size: 5.2rem
            }

            html[lang=ja-jp] .heading-02-3jMzI {
                font-size: 4rem;
                line-height: 1.1
            }

            html[lang=ru-ru] .heading-02-3jMzI {
                font-size: 4rem
            }

            .heading-03-UfvlI, .heading-10-7_kOF, .label-02-1XOLo {
                font-size: 2.8rem
            }

            .heading-04-2H_tA, .heading-05-1BzlG {
                font-size: 3.6rem
            }

            .copy-01-2wYLK, .copy-03-3RN7V, .heading-06-1dl_c, .heading-09-3bHXU {
                font-size: 2.6rem
            }

            .heading-07-10eMw {
                font-size: 5.2rem
            }

            .copy-02-2-e6g, .label-03-uAl2S, .label-04-Czh3u, .label-05-c30vc {
                font-size: 2rem
            }

            .label-01-3ndM5 {
                font-size: 3.6rem;
                line-height: 1
            }

            html[lang=ar-ae] .label-01-3ndM5 {
                line-height: 1.1
            }
        }

        @media (min-width: 1024px) {
            .heading-01-BgqKT, .heading-08-2PjDr {
                font-size: 11rem;
                line-height: .9;
                font-size: 85px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            .heading-01-BgqKT, .heading-08-2PjDr {
                font-size: calc(-15px + 9.76563vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            .heading-01-BgqKT, .heading-08-2PjDr {
                font-size: 110px
            }
        }

        @media (min-width: 1024px) {
            html[lang=zh-tw] .heading-01-BgqKT, html[lang=zh-tw] .heading-08-2PjDr {
                font-size: 80px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=zh-tw] .heading-01-BgqKT, html[lang=zh-tw] .heading-08-2PjDr {
                font-size: calc(40px + 3.90625vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=zh-tw] .heading-01-BgqKT, html[lang=zh-tw] .heading-08-2PjDr {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=zh-hk] .heading-01-BgqKT, html[lang=zh-hk] .heading-08-2PjDr {
                font-size: 80px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=zh-hk] .heading-01-BgqKT, html[lang=zh-hk] .heading-08-2PjDr {
                font-size: calc(40px + 3.90625vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=zh-hk] .heading-01-BgqKT, html[lang=zh-hk] .heading-08-2PjDr {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=pt-br] .heading-01-BgqKT, html[lang=pt-br] .heading-08-2PjDr {
                font-size: 75px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=pt-br] .heading-01-BgqKT, html[lang=pt-br] .heading-08-2PjDr {
                font-size: calc(-25px + 9.76563vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=pt-br] .heading-01-BgqKT, html[lang=pt-br] .heading-08-2PjDr {
                font-size: 100px
            }
        }

        @media (min-width: 1024px) {
            html[lang=es-es] .heading-01-BgqKT, html[lang=es-es] .heading-08-2PjDr, html[lang=es-mx] .heading-01-BgqKT, html[lang=es-mx] .heading-08-2PjDr {
                line-height: 1.05
            }

            html[lang=id-id] .heading-01-BgqKT, html[lang=id-id] .heading-08-2PjDr {
                font-size: 50px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=id-id] .heading-01-BgqKT, html[lang=id-id] .heading-08-2PjDr {
                font-size: calc(-90px + 13.67188vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=id-id] .heading-01-BgqKT, html[lang=id-id] .heading-08-2PjDr {
                font-size: 85px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ms-my] .heading-01-BgqKT, html[lang=ms-my] .heading-08-2PjDr {
                font-size: 80px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ms-my] .heading-01-BgqKT, html[lang=ms-my] .heading-08-2PjDr {
                font-size: calc(20px + 5.85938vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ms-my] .heading-01-BgqKT, html[lang=ms-my] .heading-08-2PjDr {
                font-size: 95px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ko-kr] .heading-01-BgqKT, html[lang=ko-kr] .heading-08-2PjDr {
                font-size: 80px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ko-kr] .heading-01-BgqKT, html[lang=ko-kr] .heading-08-2PjDr {
                font-size: calc(40px + 3.90625vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ko-kr] .heading-01-BgqKT, html[lang=ko-kr] .heading-08-2PjDr {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ru-ru] .heading-01-BgqKT, html[lang=ru-ru] .heading-08-2PjDr {
                font-size: 50px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ru-ru] .heading-01-BgqKT, html[lang=ru-ru] .heading-08-2PjDr {
                font-size: calc(-30px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ru-ru] .heading-01-BgqKT, html[lang=ru-ru] .heading-08-2PjDr {
                font-size: 70px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ja-jp] .heading-01-BgqKT, html[lang=ja-jp] .heading-08-2PjDr {
                font-size: 60px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ja-jp] .heading-01-BgqKT, html[lang=ja-jp] .heading-08-2PjDr {
                font-size: 5.85938vw
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ja-jp] .heading-01-BgqKT, html[lang=ja-jp] .heading-08-2PjDr {
                font-size: 75px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ar-ae] .heading-01-BgqKT, html[lang=ar-ae] .heading-08-2PjDr {
                font-size: 60px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ar-ae] .heading-01-BgqKT, html[lang=ar-ae] .heading-08-2PjDr {
                font-size: calc(-20px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ar-ae] .heading-01-BgqKT, html[lang=ar-ae] .heading-08-2PjDr {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            html[lang=th-th] .heading-01-BgqKT, html[lang=th-th] .heading-08-2PjDr {
                font-size: 70px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=th-th] .heading-01-BgqKT, html[lang=th-th] .heading-08-2PjDr {
                font-size: calc(-10px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=th-th] .heading-01-BgqKT, html[lang=th-th] .heading-08-2PjDr {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=vi-vn] .heading-01-BgqKT, html[lang=vi-vn] .heading-08-2PjDr {
                font-size: 60px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=vi-vn] .heading-01-BgqKT, html[lang=vi-vn] .heading-08-2PjDr {
                font-size: calc(-20px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=vi-vn] .heading-01-BgqKT, html[lang=vi-vn] .heading-08-2PjDr {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            .heading-02-3jMzI {
                font-size: 4rem
            }

            html[lang=ja-jp] .heading-02-3jMzI {
                font-size: 3rem;
                line-height: 1.1
            }

            html[lang=ru-ru] .heading-02-3jMzI {
                font-size: 2.8rem
            }

            .heading-03-UfvlI, .heading-06-1dl_c, .heading-09-3bHXU, .label-02-1XOLo, .label-05-c30vc {
                font-size: 1.4rem
            }

            .heading-04-2H_tA {
                font-size: 2.4rem
            }

            .heading-05-1BzlG, .heading-10-7_kOF, .label-01-3ndM5 {
                font-size: 1.9rem
            }

            .heading-07-10eMw {
                font-size: 3rem
            }

            .heading-08-2PjDr {
                font-size: 8rem;
                font-size: 70px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            .heading-08-2PjDr {
                font-size: calc(30px + 3.90625vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            .heading-08-2PjDr {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ja-jp] .heading-08-2PjDr {
                font-size: 55px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ja-jp] .heading-08-2PjDr {
                font-size: calc(-5px + 5.85938vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ja-jp] .heading-08-2PjDr {
                font-size: 70px
            }
        }

        @media (min-width: 1024px) {
            html[lang=th-th] .heading-08-2PjDr {
                font-size: 60px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=th-th] .heading-08-2PjDr {
                font-size: calc(-20px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=th-th] .heading-08-2PjDr {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            html[lang=id-id] .heading-08-2PjDr {
                font-size: 45px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=id-id] .heading-08-2PjDr {
                font-size: calc(-95px + 13.67188vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=id-id] .heading-08-2PjDr {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ms-my] .heading-08-2PjDr {
                font-size: 75px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ms-my] .heading-08-2PjDr {
                font-size: calc(15px + 5.85938vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ms-my] .heading-08-2PjDr {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=vi-vn] .heading-08-2PjDr {
                font-size: 55px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=vi-vn] .heading-08-2PjDr {
                font-size: calc(-25px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=vi-vn] .heading-08-2PjDr {
                font-size: 75px
            }
        }

        @media (min-width: 1024px) {
            .copy-01-2wYLK, .copy-03-3RN7V, .heading-09-3bHXU {
                font-size: 1.6rem
            }

            .copy-02-2-e6g, .label-03-uAl2S {
                font-size: 1rem;
                line-height: 1.6
            }

            .label-03-uAl2S {
                font-size: 1.6rem
            }

            .label-04-Czh3u {
                font-size: 1rem
            }
        }

        .contentWrapper-3tNzB {
            color: #141e37
        }

        .contentWrapper-3tNzB .image-3BtDp {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 1.5rem), calc(100% - 2rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 1.5rem), calc(100% - 2rem) 100%, 0 100%)
        }

        [dir] .contentWrapper-3tNzB .image-3BtDp {
            margin-top: 3.5rem;
            margin-bottom: 3.5rem
        }

        [dir=rtl] .contentWrapper-3tNzB .image-3BtDp {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 2rem 100%, 0 calc(100% - 1.5rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 2rem 100%, 0 calc(100% - 1.5rem))
        }

        [dir] .contentWrapper-3tNzB .richText-20OSE h2 {
            margin-bottom: 3rem
        }

        [dir] .contentWrapper-3tNzB .richText-20OSE h2:not(:first-child) {
            margin-top: 4.5rem
        }

        .contentWrapper-3tNzB .richText-20OSE h2, .contentWrapper-3tNzB .richText-20OSE h2 > span {
            font-style: italic;
            font-size: 4rem;
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            text-transform: uppercase
        }

        html[lang=th-th] .contentWrapper-3tNzB .richText-20OSE h2, html[lang=th-th] .contentWrapper-3tNzB .richText-20OSE h2 > span {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-weight: 700;
            line-height: 1.4;
            font-style: normal
        }

        html[lang=ar-ae] .contentWrapper-3tNzB .richText-20OSE h2, html[lang=ar-ae] .contentWrapper-3tNzB .richText-20OSE h2 > span {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal;
            line-height: 1.15
        }

        html[lang=ru-ru] .contentWrapper-3tNzB .richText-20OSE h2, html[lang=ru-ru] .contentWrapper-3tNzB .richText-20OSE h2 > span {
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif
        }

        html[lang=tr-tr] .contentWrapper-3tNzB .richText-20OSE h2, html[lang=tr-tr] .contentWrapper-3tNzB .richText-20OSE h2 > span {
            line-height: 1.1
        }

        html[lang=ko-kr] .contentWrapper-3tNzB .richText-20OSE h2, html[lang=ko-kr] .contentWrapper-3tNzB .richText-20OSE h2 > span {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        html[lang=vi-vn] .contentWrapper-3tNzB .richText-20OSE h2, html[lang=vi-vn] .contentWrapper-3tNzB .richText-20OSE h2 > span {
            font-family: RobotoCondensed-Bold, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            letter-spacing: 1px;
            line-height: 1.18
        }

        html[lang=zh-hk] .contentWrapper-3tNzB .richText-20OSE h2, html[lang=zh-hk] .contentWrapper-3tNzB .richText-20OSE h2 > span, html[lang=zh-tw] .contentWrapper-3tNzB .richText-20OSE h2, html[lang=zh-tw] .contentWrapper-3tNzB .richText-20OSE h2 > span {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-style: normal;
            font-weight: 500;
            line-height: 1.1
        }

        html[lang=ja-jp] .contentWrapper-3tNzB .richText-20OSE h2, html[lang=ja-jp] .contentWrapper-3tNzB .richText-20OSE h2 > span {
            font-family: BeaufortforLOLJa-Regular, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        @media (min-width: 768px) {
            [dir] .contentWrapper-3tNzB .richText-20OSE h2 {
                margin-bottom: 6rem
            }

            [dir] .contentWrapper-3tNzB .richText-20OSE h2:not(:first-child) {
                margin-top: 9rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .contentWrapper-3tNzB .richText-20OSE h2 {
                margin-bottom: 3.2rem
            }

            [dir] .contentWrapper-3tNzB .richText-20OSE h2:not(:first-child) {
                margin-top: 8rem
            }
        }

        [dir] .contentWrapper-3tNzB .richText-20OSE h3 {
            margin-top: 3rem;
            margin-bottom: 1.6rem
        }

        .contentWrapper-3tNzB .richText-20OSE h3, .contentWrapper-3tNzB .richText-20OSE h3 > span {
            font-style: italic;
            font-size: 2.4rem;
            font-family: BeaufortforLOL-Heavy, arial, georgia, sans-serif;
            text-transform: uppercase
        }

        html[lang=th-th] .contentWrapper-3tNzB .richText-20OSE h3, html[lang=th-th] .contentWrapper-3tNzB .richText-20OSE h3 > span {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif
        }

        html[lang=ar-ae] .contentWrapper-3tNzB .richText-20OSE h3, html[lang=ar-ae] .contentWrapper-3tNzB .richText-20OSE h3 > span {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal
        }

        html[lang=ru-ru] .contentWrapper-3tNzB .richText-20OSE h3, html[lang=ru-ru] .contentWrapper-3tNzB .richText-20OSE h3 > span {
            font-family: Spiegel-Regular, arial, georgia, sans-serif
        }

        html[lang=ko-kr] .contentWrapper-3tNzB .richText-20OSE h3, html[lang=ko-kr] .contentWrapper-3tNzB .richText-20OSE h3 > span {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            word-break: keep-all
        }

        html[lang=vi-vn] .contentWrapper-3tNzB .richText-20OSE h3, html[lang=vi-vn] .contentWrapper-3tNzB .richText-20OSE h3 > span {
            font-family: RobotoCondensed-Regular, Spiegel-Regular, arial, georgia, sans-serif
        }

        html[lang=zh-hk] .contentWrapper-3tNzB .richText-20OSE h3, html[lang=zh-hk] .contentWrapper-3tNzB .richText-20OSE h3 > span, html[lang=zh-tw] .contentWrapper-3tNzB .richText-20OSE h3, html[lang=zh-tw] .contentWrapper-3tNzB .richText-20OSE h3 > span {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-weight: 400
        }

        html[lang=ja-jp] .contentWrapper-3tNzB .richText-20OSE h3, html[lang=ja-jp] .contentWrapper-3tNzB .richText-20OSE h3 > span {
            font-family: SpiegelJa-Regular, Spiegel-Regular, arial, georgia, sans-serif;
            line-height: 1.3
        }

        @media (min-width: 768px) {
            [dir] .contentWrapper-3tNzB .richText-20OSE h3 {
                margin-top: 6rem;
                margin-bottom: 3.2rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .contentWrapper-3tNzB .richText-20OSE h3 {
                margin-top: 3.2rem;
                margin-bottom: 1.8rem
            }
        }

        .contentWrapper-3tNzB .richText-20OSE h4 {
            color: #32c8ff
        }

        [dir] .contentWrapper-3tNzB .richText-20OSE h4 {
            margin-top: 1.6rem;
            margin-bottom: .2rem
        }

        .contentWrapper-3tNzB .richText-20OSE h4, .contentWrapper-3tNzB .richText-20OSE h4 > span {
            font-style: normal;
            font-size: 1.9rem;
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            text-transform: uppercase
        }

        html[lang=th-th] .contentWrapper-3tNzB .richText-20OSE h4, html[lang=th-th] .contentWrapper-3tNzB .richText-20OSE h4 > span {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-weight: 700;
            line-height: 1.4;
            font-style: normal
        }

        html[lang=ar-ae] .contentWrapper-3tNzB .richText-20OSE h4, html[lang=ar-ae] .contentWrapper-3tNzB .richText-20OSE h4 > span {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal;
            line-height: 1.15
        }

        html[lang=ru-ru] .contentWrapper-3tNzB .richText-20OSE h4, html[lang=ru-ru] .contentWrapper-3tNzB .richText-20OSE h4 > span {
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif
        }

        html[lang=tr-tr] .contentWrapper-3tNzB .richText-20OSE h4, html[lang=tr-tr] .contentWrapper-3tNzB .richText-20OSE h4 > span {
            line-height: 1.1
        }

        html[lang=ko-kr] .contentWrapper-3tNzB .richText-20OSE h4, html[lang=ko-kr] .contentWrapper-3tNzB .richText-20OSE h4 > span {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        html[lang=vi-vn] .contentWrapper-3tNzB .richText-20OSE h4, html[lang=vi-vn] .contentWrapper-3tNzB .richText-20OSE h4 > span {
            font-family: RobotoCondensed-Bold, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            letter-spacing: 1px;
            line-height: 1.18
        }

        html[lang=zh-hk] .contentWrapper-3tNzB .richText-20OSE h4, html[lang=zh-hk] .contentWrapper-3tNzB .richText-20OSE h4 > span, html[lang=zh-tw] .contentWrapper-3tNzB .richText-20OSE h4, html[lang=zh-tw] .contentWrapper-3tNzB .richText-20OSE h4 > span {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-style: normal;
            font-weight: 500;
            line-height: 1.1
        }

        html[lang=ja-jp] .contentWrapper-3tNzB .richText-20OSE h4, html[lang=ja-jp] .contentWrapper-3tNzB .richText-20OSE h4 > span {
            font-family: BeaufortforLOLJa-Regular, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        .contentWrapper-3tNzB .richText-20OSE td {
            position: relative;
            vertical-align: top;
            font-size: 1.3rem
        }

        [dir] .contentWrapper-3tNzB .richText-20OSE td {
            border: 1px solid #c89b3c;
            padding: 1rem;
            margin: 2rem 0
        }

        .contentWrapper-3tNzB .richText-20OSE td p {
            min-width: 4rem;
            max-width: 100%;
            overflow-wrap: break-word;
            word-break: break-all
        }

        [dir] .contentWrapper-3tNzB .richText-20OSE td p {
            margin: 0;
            padding-bottom: 1rem
        }

        @media (min-width: 1024px) {
            [dir] .contentWrapper-3tNzB .richText-20OSE td {
                border: 2px solid #c89b3c;
                padding: 2rem
            }
        }

        .contentWrapper-3tNzB .richText-20OSE .table-scrollbar-wrapper {
            height: .5rem;
            width: 44%;
            position: relative
        }

        [dir] .contentWrapper-3tNzB .richText-20OSE .table-scrollbar-wrapper {
            background: rgba(200, 155, 60, .5);
            border-radius: 5px;
            margin: 2rem auto 0
        }

        @media (min-width: 768px) {
            .contentWrapper-3tNzB .richText-20OSE .table-scrollbar-wrapper {
                display: none
            }
        }

        .contentWrapper-3tNzB .richText-20OSE .table-scrollbar {
            position: absolute;
            top: 0;
            width: 42%;
            height: 100%;
            display: block
        }

        [dir] .contentWrapper-3tNzB .richText-20OSE .table-scrollbar {
            background: #c89b3c;
            border-radius: 5px
        }

        [dir=ltr] .contentWrapper-3tNzB .richText-20OSE .table-scrollbar {
            left: 0
        }

        [dir=rtl] .contentWrapper-3tNzB .richText-20OSE .table-scrollbar {
            right: 0
        }

        [dir] .contentWrapper-3tNzB .richText-20OSE .rich-text-table-wrapper {
            padding: 1rem 0 0;
            margin: 2rem 0
        }

        .contentWrapper-3tNzB .richText-20OSE .table-overflow {
            width: 100%;
            overflow-x: scroll;
            overflow-y: hidden;
            position: relative
        }

        .contentWrapper-3tNzB .richText-20OSE .table-overflow::-webkit-scrollbar {
            display: none
        }

        .contentWrapper-3tNzB .richText-20OSE .table-overflow table {
            width: 200%;
            position: relative
        }

        @media (min-width: 768px) {
            .contentWrapper-3tNzB .richText-20OSE .table-overflow {
                overflow-x: unset;
                overflow-y: unset
            }

            .contentWrapper-3tNzB .richText-20OSE .table-overflow table {
                width: 100%
            }
        }

        .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td {
            font-family: BeaufortforLOL-Heavy, arial, georgia, sans-serif;
            text-transform: uppercase;
            color: #c89b3c
        }

        html[lang=th-th] .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif
        }

        html[lang=ar-ae] .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal
        }

        html[lang=ru-ru] .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td {
            font-family: Spiegel-Regular, arial, georgia, sans-serif
        }

        html[lang=ko-kr] .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            word-break: keep-all
        }

        html[lang=vi-vn] .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td {
            font-family: RobotoCondensed-Regular, Spiegel-Regular, arial, georgia, sans-serif
        }

        html[lang=zh-hk] .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td, html[lang=zh-tw] .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-weight: 400
        }

        html[lang=ja-jp] .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td {
            font-family: SpiegelJa-Regular, Spiegel-Regular, arial, georgia, sans-serif;
            line-height: 1.3
        }

        .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td p {
            font-family: BeaufortforLOL-Heavy, arial, georgia, sans-serif;
            text-transform: uppercase;
            color: #c89b3c
        }

        html[lang=th-th] .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td p {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif
        }

        html[lang=ar-ae] .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td p {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal
        }

        html[lang=ru-ru] .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td p {
            font-family: Spiegel-Regular, arial, georgia, sans-serif
        }

        html[lang=ko-kr] .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td p {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            word-break: keep-all
        }

        html[lang=vi-vn] .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td p {
            font-family: RobotoCondensed-Regular, Spiegel-Regular, arial, georgia, sans-serif
        }

        html[lang=zh-hk] .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td p, html[lang=zh-tw] .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td p {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-weight: 400
        }

        html[lang=ja-jp] .contentWrapper-3tNzB .richText-20OSE tr:first-of-type td p {
            font-family: SpiegelJa-Regular, Spiegel-Regular, arial, georgia, sans-serif;
            line-height: 1.3
        }

        .contentWrapper-3tNzB .richText-20OSE tr:last-of-type td:last-of-type:after {
            content: "";
            display: block;
            position: absolute;
            width: 2.2rem;
            height: 2.2rem;
            bottom: -.3rem
        }

        [dir] .contentWrapper-3tNzB .richText-20OSE tr:last-of-type td:last-of-type:after {
            border-top: 2rem solid transparent
        }

        [dir=ltr] .contentWrapper-3tNzB .richText-20OSE tr:last-of-type td:last-of-type:after {
            right: -.2rem;
            border-right: 2rem solid #fff
        }

        [dir=rtl] .contentWrapper-3tNzB .richText-20OSE tr:last-of-type td:last-of-type:after {
            left: -.2rem;
            border-left: 2rem solid #fff
        }

        @media (min-width: 1024px) {
            .contentWrapper-3tNzB .richText-20OSE tr:last-of-type td:last-of-type:after {
                width: 2rem;
                height: 2rem;
                bottom: -.2rem
            }
        }

        .contentWrapper-3tNzB .richText-20OSE tr:last-of-type td:last-of-type:before {
            content: "";
            display: block;
            position: absolute;
            width: .2rem;
            height: 2.7rem;
            bottom: -.1rem
        }

        [dir] .contentWrapper-3tNzB .richText-20OSE tr:last-of-type td:last-of-type:before {
            transform-origin: bottom center;
            background-color: #c89b3c
        }

        [dir=ltr] .contentWrapper-3tNzB .richText-20OSE tr:last-of-type td:last-of-type:before {
            right: 1.7rem;
            transform: rotate(45deg)
        }

        [dir=rtl] .contentWrapper-3tNzB .richText-20OSE tr:last-of-type td:last-of-type:before {
            left: 1.7rem;
            transform: rotate(-45deg)
        }

        @media (min-width: 1024px) {
            .contentWrapper-3tNzB .richText-20OSE tr:last-of-type td:last-of-type:before {
                width: .3rem;
                height: 2.8rem
            }
        }

        .contentWrapper-3tNzB .richText-20OSE table {
            border-collapse: collapse;
            width: 100%;
            position: relative
        }

        .contentWrapper-3tNzB .richText-20OSE table tbody {
            position: relative
        }

        .contentWrapper-3tNzB .richText-20OSE table tbody:before {
            content: "";
            display: block;
            position: absolute;
            width: 4rem;
            height: .5rem;
            top: -.5rem
        }

        [dir] .contentWrapper-3tNzB .richText-20OSE table tbody:before {
            background: #c89b3c
        }

        [dir=ltr] .contentWrapper-3tNzB .richText-20OSE table tbody:before {
            left: 0
        }

        [dir=rtl] .contentWrapper-3tNzB .richText-20OSE table tbody:before {
            right: 0
        }

        @media (min-width: 1024px) {
            [dir=ltr] .contentWrapper-3tNzB .richText-20OSE table tbody:before {
                left: -.1rem
            }

            [dir=rtl] .contentWrapper-3tNzB .richText-20OSE table tbody:before {
                right: -.1rem
            }
        }

        .contentWrapper-3tNzB .richText-20OSE table tbody:after {
            content: "";
            display: block;
            position: absolute;
            width: .5rem;
            height: .5rem;
            top: -.5rem
        }

        [dir] .contentWrapper-3tNzB .richText-20OSE table tbody:after {
            border-bottom: .5rem solid #c89b3c
        }

        [dir=ltr] .contentWrapper-3tNzB .richText-20OSE table tbody:after {
            left: 4rem;
            border-right: .5rem solid transparent
        }

        [dir=rtl] .contentWrapper-3tNzB .richText-20OSE table tbody:after {
            right: 4rem;
            border-left: .5rem solid transparent
        }

        @media (min-width: 1024px) {
            [dir=ltr] .contentWrapper-3tNzB .richText-20OSE table tbody:after {
                left: 3.9rem
            }

            [dir=rtl] .contentWrapper-3tNzB .richText-20OSE table tbody:after {
                right: 3.9rem
            }
        }

        @media (min-width: 768px) {
            .contentWrapper-3tNzB .image-3BtDp {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 4rem) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 4rem) 100%, 0 100%)
            }

            [dir=rtl] .contentWrapper-3tNzB .image-3BtDp {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 4rem 100%, 0 calc(100% - 3rem));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 4rem 100%, 0 calc(100% - 3rem))
            }
        }

        @media (min-width: 1024px) {
            .flexWrapper-2x-ze {
                display: flex
            }

            [dir] .flexWrapper-2x-ze .image-3BtDp {
                margin-top: 0;
                margin-bottom: 0
            }

            .flexWrapper-2x-ze .sideImageWrapper-3_Wv6 {
                max-width: calc(50% - 3rem);
                flex-shrink: 0;
                display: flex;
                align-items: center
            }

            [dir=ltr] .flexWrapper-2x-ze .sideImageWrapper-3_Wv6.imageLeft-2jj3L {
                margin-right: 3rem
            }

            [dir=ltr] .flexWrapper-2x-ze .sideImageWrapper-3_Wv6.imageRight-3MtP0, [dir=rtl] .flexWrapper-2x-ze .sideImageWrapper-3_Wv6.imageLeft-2jj3L {
                margin-left: 3rem
            }

            [dir=rtl] .flexWrapper-2x-ze .sideImageWrapper-3_Wv6.imageRight-3MtP0 {
                margin-right: 3rem
            }
        }

        .table-20r7_ {
            position: relative;
            width: 24.5rem;
            -webkit-clip-path: polygon(0 -1rem, 100% -1rem, 100% calc(100% - 3.3rem), calc(100% - 3.3rem) 100%, 0 100%);
            clip-path: polygon(0 -1rem, 100% -1rem, 100% calc(100% - 3.3rem), calc(100% - 3.3rem) 100%, 0 100%)
        }

        [dir] .table-20r7_ {
            border: 2px solid #32c8ff
        }

        [dir=ltr] .table-20r7_ {
            margin-right: calc(8.33333% + .25rem);
            padding: 1rem .25rem 3rem 3rem
        }

        [dir=rtl] .table-20r7_ {
            margin-left: calc(8.33333% + .25rem);
            padding: 1rem 3rem 3rem .25rem;
            -webkit-clip-path: polygon(0 -1rem, 100% -1rem, 100% 100%, 3.3rem 100%, 0 calc(100% - 3.3rem));
            clip-path: polygon(0 -1rem, 100% -1rem, 100% 100%, 3.3rem 100%, 0 calc(100% - 3.3rem))
        }

        .table-20r7_ .title-1FDcW {
            color: #32c8ff
        }

        [dir] .table-20r7_ .title-1FDcW {
            margin-bottom: 2.5rem
        }

        .table-20r7_ .itemBullet-I3_Jn {
            height: 1.2rem;
            width: 1.2rem;
            flex-shrink: 0;
            transition: transform .4s ease-out
        }

        [dir=ltr] .table-20r7_ .itemBullet-I3_Jn {
            margin-right: .5rem
        }

        [dir=rtl] .table-20r7_ .itemBullet-I3_Jn {
            margin-left: .5rem
        }

        .table-20r7_ .itemBullet-I3_Jn path {
            transition: fill .2s ease-out
        }

        .table-20r7_ .contentWrapper-3-qt8 {
            max-height: 80vh;
            overflow-y: scroll
        }

        [dir] .table-20r7_ .contentWrapper-3-qt8 {
            margin: 2rem 0 .25rem
        }

        .table-20r7_ .contentWrapper-3-qt8::-webkit-scrollbar {
            width: .7rem
        }

        [dir] .table-20r7_ .contentWrapper-3-qt8::-webkit-scrollbar-track {
            background: #fff
        }

        [dir] .table-20r7_ .contentWrapper-3-qt8::-webkit-scrollbar-thumb {
            background: #32c8ff
        }

        .table-20r7_ .cornerDetail-1Ud1y {
            position: absolute;
            bottom: 0
        }

        [dir=ltr] .table-20r7_ .cornerDetail-1Ud1y {
            right: 3rem
        }

        [dir=rtl] .table-20r7_ .cornerDetail-1Ud1y {
            left: 3rem
        }

        .table-20r7_ .cornerDetail-1Ud1y:after {
            content: "";
            display: block;
            position: absolute;
            bottom: 0
        }

        [dir] .table-20r7_ .cornerDetail-1Ud1y:after {
            border-bottom: 3.3rem solid #32c8ff
        }

        [dir=ltr] .table-20r7_ .cornerDetail-1Ud1y:after {
            right: -3rem;
            border-left: 3.3rem solid transparent
        }

        [dir=rtl] .table-20r7_ .cornerDetail-1Ud1y:after {
            left: -3rem;
            border-right: 3.3rem solid transparent
        }

        .table-20r7_ .listWrapper-3Qww8 {
            overflow: hidden
        }

        .table-20r7_ .listItem-xcCJ_ {
            display: flex;
            align-items: center;
            font-family: Spiegel-Regular, arial, georgia, sans-serif;
            font-size: 1.4rem;
            transition: color .2s ease-out
        }

        [dir] .table-20r7_ .listItem-xcCJ_:not(:first-child) {
            margin-top: .5rem
        }

        [dir] .table-20r7_ .listItem-xcCJ_.innerListItem-2GnrO {
            margin: .5rem 0
        }

        [dir=ltr] .table-20r7_ .listItem-xcCJ_.innerListItem-2GnrO {
            padding-left: 1.75rem
        }

        [dir=rtl] .table-20r7_ .listItem-xcCJ_.innerListItem-2GnrO {
            padding-right: 1.75rem
        }

        .table-20r7_ .listItem-xcCJ_:hover {
            color: #32c8ff
        }

        [dir=ltr] .table-20r7_ .listItem-xcCJ_:hover .itemBullet-I3_Jn {
            transform: translateX(-.4rem)
        }

        [dir=rtl] .table-20r7_ .listItem-xcCJ_:hover .itemBullet-I3_Jn {
            transform: translateX(.4rem)
        }

        .table-20r7_ .listItem-xcCJ_:hover .itemBullet-I3_Jn path {
            fill: #141e37
        }

        .table-20r7_ .dashDetail-xWV12 {
            position: absolute;
            top: -.7rem;
            height: .7rem;
            width: 7rem
        }

        [dir] .table-20r7_ .dashDetail-xWV12 {
            background: #32c8ff
        }

        [dir=ltr] .table-20r7_ .dashDetail-xWV12 {
            left: -.2rem
        }

        [dir=rtl] .table-20r7_ .dashDetail-xWV12 {
            right: -.2rem
        }

        .table-20r7_ .dashDetail-xWV12:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .table-20r7_ .dashDetail-xWV12:after {
            border-bottom: .7rem solid #32c8ff
        }

        [dir=ltr] .table-20r7_ .dashDetail-xWV12:after {
            right: -.7rem;
            border-right: .7rem solid transparent
        }

        [dir=rtl] .table-20r7_ .dashDetail-xWV12:after {
            left: -.7rem;
            border-left: .7rem solid transparent
        }

        html[dir=ltr][browser=Safari] .table-20r7_ .dashDetail-xWV12:after {
            right: -.6rem
        }

        html[dir=rtl][browser=Safari] .table-20r7_ .dashDetail-xWV12:after {
            left: -.6rem
        }

        html[dir=ltr][browser=Firefox] .table-20r7_ .dashDetail-xWV12:after {
            right: -.6rem
        }

        html[dir=rtl][browser=Firefox] .table-20r7_ .dashDetail-xWV12:after {
            left: -.6rem
        }

        [class*=heading-].font-normal-3tb1I, [class*=label-].font-normal-3tb1I {
            font-style: normal
        }

        .heading-01-1GgUX, .heading-08-xNvPd {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-size: 4.5rem;
            line-height: .95;
            letter-spacing: 2px
        }

        html[lang=ja-jp] .heading-01-1GgUX, html[lang=ja-jp] .heading-08-xNvPd {
            font-weight: 700;
            font-size: 3.5rem;
            line-height: 1.1
        }

        html[lang=tr-tr] .heading-01-1GgUX, html[lang=tr-tr] .heading-08-xNvPd {
            line-height: 1.2
        }

        html[lang=ru-ru] .heading-01-1GgUX, html[lang=ru-ru] .heading-08-xNvPd {
            font-family: BeaufortforLOL-Heavy, arial, georgia, sans-serif;
            line-height: 1
        }

        html[lang=zh-hk] .heading-01-1GgUX, html[lang=zh-hk] .heading-08-xNvPd, html[lang=zh-tw] .heading-01-1GgUX, html[lang=zh-tw] .heading-08-xNvPd {
            line-height: 1.1
        }

        html[lang=ko-kr] .heading-01-1GgUX, html[lang=ko-kr] .heading-08-xNvPd {
            font-size: 3.5rem;
            line-height: 1.1
        }

        html[lang=ar-ae] .heading-01-1GgUX, html[lang=ar-ae] .heading-08-xNvPd {
            font-weight: 700;
            line-height: 1.6
        }

        html[lang=vi-vn] .heading-01-1GgUX, html[lang=vi-vn] .heading-08-xNvPd {
            font-size: 3.5rem
        }

        html[lang=es-es] .heading-01-1GgUX, html[lang=es-es] .heading-08-xNvPd, html[lang=es-mx] .heading-01-1GgUX, html[lang=es-mx] .heading-08-xNvPd {
            line-height: 1.05
        }

        .heading-02-2kXin {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-size: 2.6rem;
            line-height: 1.04;
            letter-spacing: 2px
        }

        html[lang=ja-jp] .heading-02-2kXin {
            font-size: 2rem;
            line-height: 1.1
        }

        html[lang=ru-ru] .heading-02-2kXin {
            font-size: 2.4rem;
            line-height: 1.2
        }

        .heading-03-2-U9M {
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif;
            font-size: 1.4rem;
            line-height: 1.17
        }

        .heading-04-193hu {
            font-family: BeaufortforLOL-Heavy, arial, georgia, sans-serif;
            font-size: 1.8rem
        }

        html[lang=ja-jp] .heading-04-193hu {
            line-height: 1.6
        }

        .heading-05-Jz8y2 {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-size: 1.8rem;
            letter-spacing: 1px
        }

        .heading-06-16fvl, .heading-09-2ivP1 {
            font-family: Spiegel-SemiBold, arial, georgia, sans-serif;
            font-size: 1.3rem;
            letter-spacing: 1px
        }

        .heading-07-1e7AY {
            font-size: 2.6rem;
            letter-spacing: 1px
        }

        .heading-07-1e7AY, .heading-10-1uxAr {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif
        }

        .heading-10-1uxAr {
            font-size: 1.4rem
        }

        .copy-01-2XKpW, .copy-03-1hv3L, .richTextContent-2_e96 a, .richTextContent-2_e96 ol, .richTextContent-2_e96 p, .richTextContent-2_e96 ul {
            font-family: Spiegel-Regular, arial, georgia, sans-serif;
            font-size: 1.3rem;
            line-height: 1.42
        }

        .copy-02-2MrBx, .label-03-3LQzy {
            font-family: Spiegel-Regular, arial, georgia, sans-serif;
            font-size: 1rem;
            line-height: 1.5
        }

        .copy-03-1hv3L {
            font-family: Spiegel-SemiBold, arial, georgia, sans-serif
        }

        .label-01-2ufXU {
            text-transform: uppercase;
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-style: italic;
            font-size: 1.8rem;
            line-height: 1.27;
            letter-spacing: 1px
        }

        html[lang=ar-ae] .label-01-2ufXU {
            font-weight: 700
        }

        .label-02-MTcL1 {
            text-transform: uppercase;
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif;
            font-size: 1.4rem;
            letter-spacing: 1px
        }

        html[lang=ar-ae] .label-02-MTcL1 {
            font-weight: 700
        }

        .label-04-1X1IS {
            font-family: Spiegel-SemiBold, arial, georgia, sans-serif;
            font-size: 1rem
        }

        html[lang=ar-ae] .label-04-1X1IS {
            font-weight: 500
        }

        .label-05-359xl {
            text-transform: uppercase;
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif;
            font-size: 1rem
        }

        @media (min-width: 768px) {
            .heading-01-1GgUX, .heading-08-xNvPd {
                font-size: 9rem
            }

            html[lang=ja-jp] .heading-01-1GgUX, html[lang=ja-jp] .heading-08-xNvPd {
                font-size: 6.5rem
            }

            html[lang=ko-kr] .heading-01-1GgUX, html[lang=ko-kr] .heading-08-xNvPd, html[lang=vi-vn] .heading-01-1GgUX, html[lang=vi-vn] .heading-08-xNvPd {
                font-size: 7rem
            }

            .heading-02-2kXin {
                font-size: 5.2rem
            }

            html[lang=ja-jp] .heading-02-2kXin {
                font-size: 4rem;
                line-height: 1.1
            }

            html[lang=ru-ru] .heading-02-2kXin {
                font-size: 4rem
            }

            .heading-03-2-U9M, .heading-10-1uxAr, .label-02-MTcL1 {
                font-size: 2.8rem
            }

            .heading-04-193hu, .heading-05-Jz8y2 {
                font-size: 3.6rem
            }

            .copy-01-2XKpW, .copy-03-1hv3L, .heading-06-16fvl, .heading-09-2ivP1, .richTextContent-2_e96 a, .richTextContent-2_e96 ol, .richTextContent-2_e96 p, .richTextContent-2_e96 ul {
                font-size: 2.6rem
            }

            .heading-07-1e7AY {
                font-size: 5.2rem
            }

            .copy-02-2MrBx, .label-03-3LQzy, .label-04-1X1IS, .label-05-359xl {
                font-size: 2rem
            }

            .label-01-2ufXU {
                font-size: 3.6rem;
                line-height: 1
            }

            html[lang=ar-ae] .label-01-2ufXU {
                line-height: 1.1
            }
        }

        @media (min-width: 1024px) {
            .heading-01-1GgUX, .heading-08-xNvPd {
                font-size: 11rem;
                line-height: .9;
                font-size: 85px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            .heading-01-1GgUX, .heading-08-xNvPd {
                font-size: calc(-15px + 9.76563vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            .heading-01-1GgUX, .heading-08-xNvPd {
                font-size: 110px
            }
        }

        @media (min-width: 1024px) {
            html[lang=zh-tw] .heading-01-1GgUX, html[lang=zh-tw] .heading-08-xNvPd {
                font-size: 80px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=zh-tw] .heading-01-1GgUX, html[lang=zh-tw] .heading-08-xNvPd {
                font-size: calc(40px + 3.90625vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=zh-tw] .heading-01-1GgUX, html[lang=zh-tw] .heading-08-xNvPd {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=zh-hk] .heading-01-1GgUX, html[lang=zh-hk] .heading-08-xNvPd {
                font-size: 80px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=zh-hk] .heading-01-1GgUX, html[lang=zh-hk] .heading-08-xNvPd {
                font-size: calc(40px + 3.90625vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=zh-hk] .heading-01-1GgUX, html[lang=zh-hk] .heading-08-xNvPd {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=pt-br] .heading-01-1GgUX, html[lang=pt-br] .heading-08-xNvPd {
                font-size: 75px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=pt-br] .heading-01-1GgUX, html[lang=pt-br] .heading-08-xNvPd {
                font-size: calc(-25px + 9.76563vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=pt-br] .heading-01-1GgUX, html[lang=pt-br] .heading-08-xNvPd {
                font-size: 100px
            }
        }

        @media (min-width: 1024px) {
            html[lang=es-es] .heading-01-1GgUX, html[lang=es-es] .heading-08-xNvPd, html[lang=es-mx] .heading-01-1GgUX, html[lang=es-mx] .heading-08-xNvPd {
                line-height: 1.05
            }

            html[lang=id-id] .heading-01-1GgUX, html[lang=id-id] .heading-08-xNvPd {
                font-size: 50px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=id-id] .heading-01-1GgUX, html[lang=id-id] .heading-08-xNvPd {
                font-size: calc(-90px + 13.67188vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=id-id] .heading-01-1GgUX, html[lang=id-id] .heading-08-xNvPd {
                font-size: 85px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ms-my] .heading-01-1GgUX, html[lang=ms-my] .heading-08-xNvPd {
                font-size: 80px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ms-my] .heading-01-1GgUX, html[lang=ms-my] .heading-08-xNvPd {
                font-size: calc(20px + 5.85938vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ms-my] .heading-01-1GgUX, html[lang=ms-my] .heading-08-xNvPd {
                font-size: 95px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ko-kr] .heading-01-1GgUX, html[lang=ko-kr] .heading-08-xNvPd {
                font-size: 80px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ko-kr] .heading-01-1GgUX, html[lang=ko-kr] .heading-08-xNvPd {
                font-size: calc(40px + 3.90625vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ko-kr] .heading-01-1GgUX, html[lang=ko-kr] .heading-08-xNvPd {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ru-ru] .heading-01-1GgUX, html[lang=ru-ru] .heading-08-xNvPd {
                font-size: 50px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ru-ru] .heading-01-1GgUX, html[lang=ru-ru] .heading-08-xNvPd {
                font-size: calc(-30px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ru-ru] .heading-01-1GgUX, html[lang=ru-ru] .heading-08-xNvPd {
                font-size: 70px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ja-jp] .heading-01-1GgUX, html[lang=ja-jp] .heading-08-xNvPd {
                font-size: 60px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ja-jp] .heading-01-1GgUX, html[lang=ja-jp] .heading-08-xNvPd {
                font-size: 5.85938vw
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ja-jp] .heading-01-1GgUX, html[lang=ja-jp] .heading-08-xNvPd {
                font-size: 75px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ar-ae] .heading-01-1GgUX, html[lang=ar-ae] .heading-08-xNvPd {
                font-size: 60px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ar-ae] .heading-01-1GgUX, html[lang=ar-ae] .heading-08-xNvPd {
                font-size: calc(-20px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ar-ae] .heading-01-1GgUX, html[lang=ar-ae] .heading-08-xNvPd {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            html[lang=th-th] .heading-01-1GgUX, html[lang=th-th] .heading-08-xNvPd {
                font-size: 70px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=th-th] .heading-01-1GgUX, html[lang=th-th] .heading-08-xNvPd {
                font-size: calc(-10px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=th-th] .heading-01-1GgUX, html[lang=th-th] .heading-08-xNvPd {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=vi-vn] .heading-01-1GgUX, html[lang=vi-vn] .heading-08-xNvPd {
                font-size: 60px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=vi-vn] .heading-01-1GgUX, html[lang=vi-vn] .heading-08-xNvPd {
                font-size: calc(-20px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=vi-vn] .heading-01-1GgUX, html[lang=vi-vn] .heading-08-xNvPd {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            .heading-02-2kXin {
                font-size: 4rem
            }

            html[lang=ja-jp] .heading-02-2kXin {
                font-size: 3rem;
                line-height: 1.1
            }

            html[lang=ru-ru] .heading-02-2kXin {
                font-size: 2.8rem
            }

            .heading-03-2-U9M, .heading-06-16fvl, .heading-09-2ivP1, .label-02-MTcL1, .label-05-359xl {
                font-size: 1.4rem
            }

            .heading-04-193hu {
                font-size: 2.4rem
            }

            .heading-05-Jz8y2, .heading-10-1uxAr, .label-01-2ufXU {
                font-size: 1.9rem
            }

            .heading-07-1e7AY {
                font-size: 3rem
            }

            .heading-08-xNvPd {
                font-size: 8rem;
                font-size: 70px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            .heading-08-xNvPd {
                font-size: calc(30px + 3.90625vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            .heading-08-xNvPd {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ja-jp] .heading-08-xNvPd {
                font-size: 55px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ja-jp] .heading-08-xNvPd {
                font-size: calc(-5px + 5.85938vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ja-jp] .heading-08-xNvPd {
                font-size: 70px
            }
        }

        @media (min-width: 1024px) {
            html[lang=th-th] .heading-08-xNvPd {
                font-size: 60px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=th-th] .heading-08-xNvPd {
                font-size: calc(-20px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=th-th] .heading-08-xNvPd {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            html[lang=id-id] .heading-08-xNvPd {
                font-size: 45px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=id-id] .heading-08-xNvPd {
                font-size: calc(-95px + 13.67188vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=id-id] .heading-08-xNvPd {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ms-my] .heading-08-xNvPd {
                font-size: 75px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ms-my] .heading-08-xNvPd {
                font-size: calc(15px + 5.85938vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ms-my] .heading-08-xNvPd {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=vi-vn] .heading-08-xNvPd {
                font-size: 55px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=vi-vn] .heading-08-xNvPd {
                font-size: calc(-25px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=vi-vn] .heading-08-xNvPd {
                font-size: 75px
            }
        }

        @media (min-width: 1024px) {
            .copy-01-2XKpW, .copy-03-1hv3L, .heading-09-2ivP1, .richTextContent-2_e96 a, .richTextContent-2_e96 ol, .richTextContent-2_e96 p, .richTextContent-2_e96 ul {
                font-size: 1.6rem
            }

            .copy-02-2MrBx, .label-03-3LQzy {
                font-size: 1rem;
                line-height: 1.6
            }

            .label-03-3LQzy {
                font-size: 1.6rem
            }

            .label-04-1X1IS {
                font-size: 1rem
            }
        }

        .richTextContent-2_e96 {
            width: 100%
        }

        .richTextContent-2_e96 * {
            font-family: Spiegel-Regular, arial, georgia, sans-serif
        }

        html[lang=th-th] .richTextContent-2_e96 * {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif
        }

        html[lang=ar-ae] .richTextContent-2_e96 * {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal
        }

        html[lang=ru-ru] .richTextContent-2_e96 * {
            font-family: Spiegel-Regular, arial, georgia, sans-serif
        }

        html[lang=ko-kr] .richTextContent-2_e96 * {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif
        }

        html[lang=vi-vn] .richTextContent-2_e96 * {
            font-family: RobotoCondensed-Regular, Spiegel-Regular, arial, georgia, sans-serif
        }

        html[lang=zh-hk] .richTextContent-2_e96 *, html[lang=zh-tw] .richTextContent-2_e96 * {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-weight: 400
        }

        html[lang=ja-jp] .richTextContent-2_e96 * {
            font-family: SpiegelJa-Regular, Spiegel-Regular, arial, georgia, sans-serif;
            line-height: 1.3
        }

        .richTextContent-2_e96 a {
            display: inline-block;
            position: relative;
            color: #32c8ff;
            transition: color .2s ease-out;
            max-width: 100%
        }

        html[browser="Internet Explorer"] .richTextContent-2_e96 a {
            height: auto
        }

        .richTextContent-2_e96 a:after {
            content: "";
            display: block;
            position: absolute;
            height: .2rem;
            width: 100%;
            bottom: -.1rem;
            transition: background-color .2s ease-out
        }

        [dir] .richTextContent-2_e96 a:after {
            background-color: #32c8ff
        }

        .richTextContent-2_e96 a:hover {
            color: #141e37
        }

        [dir] .richTextContent-2_e96 a:hover:after {
            background-color: #141e37
        }

        .richTextContent-2_e96 img {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 1.5rem), calc(100% - 2rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 1.5rem), calc(100% - 2rem) 100%, 0 100%);
            max-width: 100%;
            width: auto
        }

        [dir] .richTextContent-2_e96 img {
            margin: 3.5rem 0
        }

        [dir=rtl] .richTextContent-2_e96 img {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 2rem 100%, 0 calc(100% - 1.5rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 2rem 100%, 0 calc(100% - 1.5rem))
        }

        .richTextContent-2_e96 a > img {
            max-width: 100%
        }

        [dir] .richTextContent-2_e96 a > img {
            margin: 1.4rem 0
        }

        .richTextContent-2_e96 p {
            font-size: 1.3rem
        }

        [dir] .richTextContent-2_e96 p {
            margin-bottom: 1.6rem
        }

        .richTextContent-2_e96 blockquote {
            font-size: 1.3rem
        }

        .richTextContent-2_e96 figure {
            position: relative;
            display: flex;
            flex-direction: column;
            max-width: 100%
        }

        html[browser="Internet Explorer"] .richTextContent-2_e96 figure {
            height: auto
        }

        html[browser="Internet Explorer"] .richTextContent-2_e96 figure > img {
            height: 100%
        }

        .richTextContent-2_e96 figure figcaption {
            font-size: 1.2rem;
            font-style: italic;
            width: 80%
        }

        [dir] .richTextContent-2_e96 figure figcaption {
            text-align: center;
            margin: 1rem auto 0
        }

        html[browser="Internet Explorer"] .richTextContent-2_e96 figure a {
            height: 100%
        }

        .richTextContent-2_e96 figure a:after {
            display: none
        }

        [dir] .richTextContent-2_e96 figure a + figcaption {
            margin-top: .75rem
        }

        .richTextContent-2_e96 h1, .richTextContent-2_e96 h2, .richTextContent-2_e96 h3, .richTextContent-2_e96 h4, .richTextContent-2_e96 h5, .richTextContent-2_e96 h6 {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            text-transform: uppercase
        }

        html[lang=th-th] .richTextContent-2_e96 h1, html[lang=th-th] .richTextContent-2_e96 h2, html[lang=th-th] .richTextContent-2_e96 h3, html[lang=th-th] .richTextContent-2_e96 h4, html[lang=th-th] .richTextContent-2_e96 h5, html[lang=th-th] .richTextContent-2_e96 h6 {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-weight: 700;
            line-height: 1.4;
            font-style: normal
        }

        html[lang=ar-ae] .richTextContent-2_e96 h1, html[lang=ar-ae] .richTextContent-2_e96 h2, html[lang=ar-ae] .richTextContent-2_e96 h3, html[lang=ar-ae] .richTextContent-2_e96 h4, html[lang=ar-ae] .richTextContent-2_e96 h5, html[lang=ar-ae] .richTextContent-2_e96 h6 {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal;
            line-height: 1.15
        }

        html[lang=ru-ru] .richTextContent-2_e96 h1, html[lang=ru-ru] .richTextContent-2_e96 h2, html[lang=ru-ru] .richTextContent-2_e96 h3, html[lang=ru-ru] .richTextContent-2_e96 h4, html[lang=ru-ru] .richTextContent-2_e96 h5, html[lang=ru-ru] .richTextContent-2_e96 h6 {
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif
        }

        html[lang=tr-tr] .richTextContent-2_e96 h1, html[lang=tr-tr] .richTextContent-2_e96 h2, html[lang=tr-tr] .richTextContent-2_e96 h3, html[lang=tr-tr] .richTextContent-2_e96 h4, html[lang=tr-tr] .richTextContent-2_e96 h5, html[lang=tr-tr] .richTextContent-2_e96 h6 {
            line-height: 1.1
        }

        html[lang=ko-kr] .richTextContent-2_e96 h1, html[lang=ko-kr] .richTextContent-2_e96 h2, html[lang=ko-kr] .richTextContent-2_e96 h3, html[lang=ko-kr] .richTextContent-2_e96 h4, html[lang=ko-kr] .richTextContent-2_e96 h5, html[lang=ko-kr] .richTextContent-2_e96 h6 {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        html[lang=vi-vn] .richTextContent-2_e96 h1, html[lang=vi-vn] .richTextContent-2_e96 h2, html[lang=vi-vn] .richTextContent-2_e96 h3, html[lang=vi-vn] .richTextContent-2_e96 h4, html[lang=vi-vn] .richTextContent-2_e96 h5, html[lang=vi-vn] .richTextContent-2_e96 h6 {
            font-family: RobotoCondensed-Bold, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            letter-spacing: 1px;
            line-height: 1.18
        }

        html[lang=zh-hk] .richTextContent-2_e96 h1, html[lang=zh-hk] .richTextContent-2_e96 h2, html[lang=zh-hk] .richTextContent-2_e96 h3, html[lang=zh-hk] .richTextContent-2_e96 h4, html[lang=zh-hk] .richTextContent-2_e96 h5, html[lang=zh-hk] .richTextContent-2_e96 h6, html[lang=zh-tw] .richTextContent-2_e96 h1, html[lang=zh-tw] .richTextContent-2_e96 h2, html[lang=zh-tw] .richTextContent-2_e96 h3, html[lang=zh-tw] .richTextContent-2_e96 h4, html[lang=zh-tw] .richTextContent-2_e96 h5, html[lang=zh-tw] .richTextContent-2_e96 h6 {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-style: normal;
            font-weight: 500;
            line-height: 1.1
        }

        html[lang=ja-jp] .richTextContent-2_e96 h1, html[lang=ja-jp] .richTextContent-2_e96 h2, html[lang=ja-jp] .richTextContent-2_e96 h3, html[lang=ja-jp] .richTextContent-2_e96 h4, html[lang=ja-jp] .richTextContent-2_e96 h5, html[lang=ja-jp] .richTextContent-2_e96 h6 {
            font-family: BeaufortforLOLJa-Regular, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        .richTextContent-2_e96 h1 b, .richTextContent-2_e96 h1 em, .richTextContent-2_e96 h1 i, .richTextContent-2_e96 h1 strong, .richTextContent-2_e96 h1 u, .richTextContent-2_e96 h2 b, .richTextContent-2_e96 h2 em, .richTextContent-2_e96 h2 i, .richTextContent-2_e96 h2 strong, .richTextContent-2_e96 h2 u, .richTextContent-2_e96 h3 b, .richTextContent-2_e96 h3 em, .richTextContent-2_e96 h3 i, .richTextContent-2_e96 h3 strong, .richTextContent-2_e96 h3 u, .richTextContent-2_e96 h4 b, .richTextContent-2_e96 h4 em, .richTextContent-2_e96 h4 i, .richTextContent-2_e96 h4 strong, .richTextContent-2_e96 h4 u, .richTextContent-2_e96 h5 b, .richTextContent-2_e96 h5 em, .richTextContent-2_e96 h5 i, .richTextContent-2_e96 h5 strong, .richTextContent-2_e96 h5 u, .richTextContent-2_e96 h6 b, .richTextContent-2_e96 h6 em, .richTextContent-2_e96 h6 i, .richTextContent-2_e96 h6 strong, .richTextContent-2_e96 h6 u {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            text-transform: uppercase
        }

        html[lang=th-th] .richTextContent-2_e96 h1 b, html[lang=th-th] .richTextContent-2_e96 h1 em, html[lang=th-th] .richTextContent-2_e96 h1 i, html[lang=th-th] .richTextContent-2_e96 h1 strong, html[lang=th-th] .richTextContent-2_e96 h1 u, html[lang=th-th] .richTextContent-2_e96 h2 b, html[lang=th-th] .richTextContent-2_e96 h2 em, html[lang=th-th] .richTextContent-2_e96 h2 i, html[lang=th-th] .richTextContent-2_e96 h2 strong, html[lang=th-th] .richTextContent-2_e96 h2 u, html[lang=th-th] .richTextContent-2_e96 h3 b, html[lang=th-th] .richTextContent-2_e96 h3 em, html[lang=th-th] .richTextContent-2_e96 h3 i, html[lang=th-th] .richTextContent-2_e96 h3 strong, html[lang=th-th] .richTextContent-2_e96 h3 u, html[lang=th-th] .richTextContent-2_e96 h4 b, html[lang=th-th] .richTextContent-2_e96 h4 em, html[lang=th-th] .richTextContent-2_e96 h4 i, html[lang=th-th] .richTextContent-2_e96 h4 strong, html[lang=th-th] .richTextContent-2_e96 h4 u, html[lang=th-th] .richTextContent-2_e96 h5 b, html[lang=th-th] .richTextContent-2_e96 h5 em, html[lang=th-th] .richTextContent-2_e96 h5 i, html[lang=th-th] .richTextContent-2_e96 h5 strong, html[lang=th-th] .richTextContent-2_e96 h5 u, html[lang=th-th] .richTextContent-2_e96 h6 b, html[lang=th-th] .richTextContent-2_e96 h6 em, html[lang=th-th] .richTextContent-2_e96 h6 i, html[lang=th-th] .richTextContent-2_e96 h6 strong, html[lang=th-th] .richTextContent-2_e96 h6 u {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-weight: 700;
            line-height: 1.4;
            font-style: normal
        }

        html[lang=ar-ae] .richTextContent-2_e96 h1 b, html[lang=ar-ae] .richTextContent-2_e96 h1 em, html[lang=ar-ae] .richTextContent-2_e96 h1 i, html[lang=ar-ae] .richTextContent-2_e96 h1 strong, html[lang=ar-ae] .richTextContent-2_e96 h1 u, html[lang=ar-ae] .richTextContent-2_e96 h2 b, html[lang=ar-ae] .richTextContent-2_e96 h2 em, html[lang=ar-ae] .richTextContent-2_e96 h2 i, html[lang=ar-ae] .richTextContent-2_e96 h2 strong, html[lang=ar-ae] .richTextContent-2_e96 h2 u, html[lang=ar-ae] .richTextContent-2_e96 h3 b, html[lang=ar-ae] .richTextContent-2_e96 h3 em, html[lang=ar-ae] .richTextContent-2_e96 h3 i, html[lang=ar-ae] .richTextContent-2_e96 h3 strong, html[lang=ar-ae] .richTextContent-2_e96 h3 u, html[lang=ar-ae] .richTextContent-2_e96 h4 b, html[lang=ar-ae] .richTextContent-2_e96 h4 em, html[lang=ar-ae] .richTextContent-2_e96 h4 i, html[lang=ar-ae] .richTextContent-2_e96 h4 strong, html[lang=ar-ae] .richTextContent-2_e96 h4 u, html[lang=ar-ae] .richTextContent-2_e96 h5 b, html[lang=ar-ae] .richTextContent-2_e96 h5 em, html[lang=ar-ae] .richTextContent-2_e96 h5 i, html[lang=ar-ae] .richTextContent-2_e96 h5 strong, html[lang=ar-ae] .richTextContent-2_e96 h5 u, html[lang=ar-ae] .richTextContent-2_e96 h6 b, html[lang=ar-ae] .richTextContent-2_e96 h6 em, html[lang=ar-ae] .richTextContent-2_e96 h6 i, html[lang=ar-ae] .richTextContent-2_e96 h6 strong, html[lang=ar-ae] .richTextContent-2_e96 h6 u {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal;
            line-height: 1.15
        }

        html[lang=ru-ru] .richTextContent-2_e96 h1 b, html[lang=ru-ru] .richTextContent-2_e96 h1 em, html[lang=ru-ru] .richTextContent-2_e96 h1 i, html[lang=ru-ru] .richTextContent-2_e96 h1 strong, html[lang=ru-ru] .richTextContent-2_e96 h1 u, html[lang=ru-ru] .richTextContent-2_e96 h2 b, html[lang=ru-ru] .richTextContent-2_e96 h2 em, html[lang=ru-ru] .richTextContent-2_e96 h2 i, html[lang=ru-ru] .richTextContent-2_e96 h2 strong, html[lang=ru-ru] .richTextContent-2_e96 h2 u, html[lang=ru-ru] .richTextContent-2_e96 h3 b, html[lang=ru-ru] .richTextContent-2_e96 h3 em, html[lang=ru-ru] .richTextContent-2_e96 h3 i, html[lang=ru-ru] .richTextContent-2_e96 h3 strong, html[lang=ru-ru] .richTextContent-2_e96 h3 u, html[lang=ru-ru] .richTextContent-2_e96 h4 b, html[lang=ru-ru] .richTextContent-2_e96 h4 em, html[lang=ru-ru] .richTextContent-2_e96 h4 i, html[lang=ru-ru] .richTextContent-2_e96 h4 strong, html[lang=ru-ru] .richTextContent-2_e96 h4 u, html[lang=ru-ru] .richTextContent-2_e96 h5 b, html[lang=ru-ru] .richTextContent-2_e96 h5 em, html[lang=ru-ru] .richTextContent-2_e96 h5 i, html[lang=ru-ru] .richTextContent-2_e96 h5 strong, html[lang=ru-ru] .richTextContent-2_e96 h5 u, html[lang=ru-ru] .richTextContent-2_e96 h6 b, html[lang=ru-ru] .richTextContent-2_e96 h6 em, html[lang=ru-ru] .richTextContent-2_e96 h6 i, html[lang=ru-ru] .richTextContent-2_e96 h6 strong, html[lang=ru-ru] .richTextContent-2_e96 h6 u {
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif
        }

        html[lang=tr-tr] .richTextContent-2_e96 h1 b, html[lang=tr-tr] .richTextContent-2_e96 h1 em, html[lang=tr-tr] .richTextContent-2_e96 h1 i, html[lang=tr-tr] .richTextContent-2_e96 h1 strong, html[lang=tr-tr] .richTextContent-2_e96 h1 u, html[lang=tr-tr] .richTextContent-2_e96 h2 b, html[lang=tr-tr] .richTextContent-2_e96 h2 em, html[lang=tr-tr] .richTextContent-2_e96 h2 i, html[lang=tr-tr] .richTextContent-2_e96 h2 strong, html[lang=tr-tr] .richTextContent-2_e96 h2 u, html[lang=tr-tr] .richTextContent-2_e96 h3 b, html[lang=tr-tr] .richTextContent-2_e96 h3 em, html[lang=tr-tr] .richTextContent-2_e96 h3 i, html[lang=tr-tr] .richTextContent-2_e96 h3 strong, html[lang=tr-tr] .richTextContent-2_e96 h3 u, html[lang=tr-tr] .richTextContent-2_e96 h4 b, html[lang=tr-tr] .richTextContent-2_e96 h4 em, html[lang=tr-tr] .richTextContent-2_e96 h4 i, html[lang=tr-tr] .richTextContent-2_e96 h4 strong, html[lang=tr-tr] .richTextContent-2_e96 h4 u, html[lang=tr-tr] .richTextContent-2_e96 h5 b, html[lang=tr-tr] .richTextContent-2_e96 h5 em, html[lang=tr-tr] .richTextContent-2_e96 h5 i, html[lang=tr-tr] .richTextContent-2_e96 h5 strong, html[lang=tr-tr] .richTextContent-2_e96 h5 u, html[lang=tr-tr] .richTextContent-2_e96 h6 b, html[lang=tr-tr] .richTextContent-2_e96 h6 em, html[lang=tr-tr] .richTextContent-2_e96 h6 i, html[lang=tr-tr] .richTextContent-2_e96 h6 strong, html[lang=tr-tr] .richTextContent-2_e96 h6 u {
            line-height: 1.1
        }

        html[lang=ko-kr] .richTextContent-2_e96 h1 b, html[lang=ko-kr] .richTextContent-2_e96 h1 em, html[lang=ko-kr] .richTextContent-2_e96 h1 i, html[lang=ko-kr] .richTextContent-2_e96 h1 strong, html[lang=ko-kr] .richTextContent-2_e96 h1 u, html[lang=ko-kr] .richTextContent-2_e96 h2 b, html[lang=ko-kr] .richTextContent-2_e96 h2 em, html[lang=ko-kr] .richTextContent-2_e96 h2 i, html[lang=ko-kr] .richTextContent-2_e96 h2 strong, html[lang=ko-kr] .richTextContent-2_e96 h2 u, html[lang=ko-kr] .richTextContent-2_e96 h3 b, html[lang=ko-kr] .richTextContent-2_e96 h3 em, html[lang=ko-kr] .richTextContent-2_e96 h3 i, html[lang=ko-kr] .richTextContent-2_e96 h3 strong, html[lang=ko-kr] .richTextContent-2_e96 h3 u, html[lang=ko-kr] .richTextContent-2_e96 h4 b, html[lang=ko-kr] .richTextContent-2_e96 h4 em, html[lang=ko-kr] .richTextContent-2_e96 h4 i, html[lang=ko-kr] .richTextContent-2_e96 h4 strong, html[lang=ko-kr] .richTextContent-2_e96 h4 u, html[lang=ko-kr] .richTextContent-2_e96 h5 b, html[lang=ko-kr] .richTextContent-2_e96 h5 em, html[lang=ko-kr] .richTextContent-2_e96 h5 i, html[lang=ko-kr] .richTextContent-2_e96 h5 strong, html[lang=ko-kr] .richTextContent-2_e96 h5 u, html[lang=ko-kr] .richTextContent-2_e96 h6 b, html[lang=ko-kr] .richTextContent-2_e96 h6 em, html[lang=ko-kr] .richTextContent-2_e96 h6 i, html[lang=ko-kr] .richTextContent-2_e96 h6 strong, html[lang=ko-kr] .richTextContent-2_e96 h6 u {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        html[lang=vi-vn] .richTextContent-2_e96 h1 b, html[lang=vi-vn] .richTextContent-2_e96 h1 em, html[lang=vi-vn] .richTextContent-2_e96 h1 i, html[lang=vi-vn] .richTextContent-2_e96 h1 strong, html[lang=vi-vn] .richTextContent-2_e96 h1 u, html[lang=vi-vn] .richTextContent-2_e96 h2 b, html[lang=vi-vn] .richTextContent-2_e96 h2 em, html[lang=vi-vn] .richTextContent-2_e96 h2 i, html[lang=vi-vn] .richTextContent-2_e96 h2 strong, html[lang=vi-vn] .richTextContent-2_e96 h2 u, html[lang=vi-vn] .richTextContent-2_e96 h3 b, html[lang=vi-vn] .richTextContent-2_e96 h3 em, html[lang=vi-vn] .richTextContent-2_e96 h3 i, html[lang=vi-vn] .richTextContent-2_e96 h3 strong, html[lang=vi-vn] .richTextContent-2_e96 h3 u, html[lang=vi-vn] .richTextContent-2_e96 h4 b, html[lang=vi-vn] .richTextContent-2_e96 h4 em, html[lang=vi-vn] .richTextContent-2_e96 h4 i, html[lang=vi-vn] .richTextContent-2_e96 h4 strong, html[lang=vi-vn] .richTextContent-2_e96 h4 u, html[lang=vi-vn] .richTextContent-2_e96 h5 b, html[lang=vi-vn] .richTextContent-2_e96 h5 em, html[lang=vi-vn] .richTextContent-2_e96 h5 i, html[lang=vi-vn] .richTextContent-2_e96 h5 strong, html[lang=vi-vn] .richTextContent-2_e96 h5 u, html[lang=vi-vn] .richTextContent-2_e96 h6 b, html[lang=vi-vn] .richTextContent-2_e96 h6 em, html[lang=vi-vn] .richTextContent-2_e96 h6 i, html[lang=vi-vn] .richTextContent-2_e96 h6 strong, html[lang=vi-vn] .richTextContent-2_e96 h6 u {
            font-family: RobotoCondensed-Bold, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            letter-spacing: 1px;
            line-height: 1.18
        }

        html[lang=zh-hk] .richTextContent-2_e96 h1 b, html[lang=zh-hk] .richTextContent-2_e96 h1 em, html[lang=zh-hk] .richTextContent-2_e96 h1 i, html[lang=zh-hk] .richTextContent-2_e96 h1 strong, html[lang=zh-hk] .richTextContent-2_e96 h1 u, html[lang=zh-hk] .richTextContent-2_e96 h2 b, html[lang=zh-hk] .richTextContent-2_e96 h2 em, html[lang=zh-hk] .richTextContent-2_e96 h2 i, html[lang=zh-hk] .richTextContent-2_e96 h2 strong, html[lang=zh-hk] .richTextContent-2_e96 h2 u, html[lang=zh-hk] .richTextContent-2_e96 h3 b, html[lang=zh-hk] .richTextContent-2_e96 h3 em, html[lang=zh-hk] .richTextContent-2_e96 h3 i, html[lang=zh-hk] .richTextContent-2_e96 h3 strong, html[lang=zh-hk] .richTextContent-2_e96 h3 u, html[lang=zh-hk] .richTextContent-2_e96 h4 b, html[lang=zh-hk] .richTextContent-2_e96 h4 em, html[lang=zh-hk] .richTextContent-2_e96 h4 i, html[lang=zh-hk] .richTextContent-2_e96 h4 strong, html[lang=zh-hk] .richTextContent-2_e96 h4 u, html[lang=zh-hk] .richTextContent-2_e96 h5 b, html[lang=zh-hk] .richTextContent-2_e96 h5 em, html[lang=zh-hk] .richTextContent-2_e96 h5 i, html[lang=zh-hk] .richTextContent-2_e96 h5 strong, html[lang=zh-hk] .richTextContent-2_e96 h5 u, html[lang=zh-hk] .richTextContent-2_e96 h6 b, html[lang=zh-hk] .richTextContent-2_e96 h6 em, html[lang=zh-hk] .richTextContent-2_e96 h6 i, html[lang=zh-hk] .richTextContent-2_e96 h6 strong, html[lang=zh-hk] .richTextContent-2_e96 h6 u, html[lang=zh-tw] .richTextContent-2_e96 h1 b, html[lang=zh-tw] .richTextContent-2_e96 h1 em, html[lang=zh-tw] .richTextContent-2_e96 h1 i, html[lang=zh-tw] .richTextContent-2_e96 h1 strong, html[lang=zh-tw] .richTextContent-2_e96 h1 u, html[lang=zh-tw] .richTextContent-2_e96 h2 b, html[lang=zh-tw] .richTextContent-2_e96 h2 em, html[lang=zh-tw] .richTextContent-2_e96 h2 i, html[lang=zh-tw] .richTextContent-2_e96 h2 strong, html[lang=zh-tw] .richTextContent-2_e96 h2 u, html[lang=zh-tw] .richTextContent-2_e96 h3 b, html[lang=zh-tw] .richTextContent-2_e96 h3 em, html[lang=zh-tw] .richTextContent-2_e96 h3 i, html[lang=zh-tw] .richTextContent-2_e96 h3 strong, html[lang=zh-tw] .richTextContent-2_e96 h3 u, html[lang=zh-tw] .richTextContent-2_e96 h4 b, html[lang=zh-tw] .richTextContent-2_e96 h4 em, html[lang=zh-tw] .richTextContent-2_e96 h4 i, html[lang=zh-tw] .richTextContent-2_e96 h4 strong, html[lang=zh-tw] .richTextContent-2_e96 h4 u, html[lang=zh-tw] .richTextContent-2_e96 h5 b, html[lang=zh-tw] .richTextContent-2_e96 h5 em, html[lang=zh-tw] .richTextContent-2_e96 h5 i, html[lang=zh-tw] .richTextContent-2_e96 h5 strong, html[lang=zh-tw] .richTextContent-2_e96 h5 u, html[lang=zh-tw] .richTextContent-2_e96 h6 b, html[lang=zh-tw] .richTextContent-2_e96 h6 em, html[lang=zh-tw] .richTextContent-2_e96 h6 i, html[lang=zh-tw] .richTextContent-2_e96 h6 strong, html[lang=zh-tw] .richTextContent-2_e96 h6 u {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-style: normal;
            font-weight: 500;
            line-height: 1.1
        }

        html[lang=ja-jp] .richTextContent-2_e96 h1 b, html[lang=ja-jp] .richTextContent-2_e96 h1 em, html[lang=ja-jp] .richTextContent-2_e96 h1 i, html[lang=ja-jp] .richTextContent-2_e96 h1 strong, html[lang=ja-jp] .richTextContent-2_e96 h1 u, html[lang=ja-jp] .richTextContent-2_e96 h2 b, html[lang=ja-jp] .richTextContent-2_e96 h2 em, html[lang=ja-jp] .richTextContent-2_e96 h2 i, html[lang=ja-jp] .richTextContent-2_e96 h2 strong, html[lang=ja-jp] .richTextContent-2_e96 h2 u, html[lang=ja-jp] .richTextContent-2_e96 h3 b, html[lang=ja-jp] .richTextContent-2_e96 h3 em, html[lang=ja-jp] .richTextContent-2_e96 h3 i, html[lang=ja-jp] .richTextContent-2_e96 h3 strong, html[lang=ja-jp] .richTextContent-2_e96 h3 u, html[lang=ja-jp] .richTextContent-2_e96 h4 b, html[lang=ja-jp] .richTextContent-2_e96 h4 em, html[lang=ja-jp] .richTextContent-2_e96 h4 i, html[lang=ja-jp] .richTextContent-2_e96 h4 strong, html[lang=ja-jp] .richTextContent-2_e96 h4 u, html[lang=ja-jp] .richTextContent-2_e96 h5 b, html[lang=ja-jp] .richTextContent-2_e96 h5 em, html[lang=ja-jp] .richTextContent-2_e96 h5 i, html[lang=ja-jp] .richTextContent-2_e96 h5 strong, html[lang=ja-jp] .richTextContent-2_e96 h5 u, html[lang=ja-jp] .richTextContent-2_e96 h6 b, html[lang=ja-jp] .richTextContent-2_e96 h6 em, html[lang=ja-jp] .richTextContent-2_e96 h6 i, html[lang=ja-jp] .richTextContent-2_e96 h6 strong, html[lang=ja-jp] .richTextContent-2_e96 h6 u {
            font-family: BeaufortforLOLJa-Regular, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        .richTextContent-2_e96 h1 {
            font-size: 3.4rem;
            font-style: italic
        }

        .richTextContent-2_e96 h2 {
            font-size: 2.8rem;
            font-style: italic
        }

        .richTextContent-2_e96 h2 > span {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            text-transform: uppercase
        }

        html[lang=th-th] .richTextContent-2_e96 h2 > span {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-weight: 700;
            line-height: 1.4;
            font-style: normal
        }

        html[lang=ar-ae] .richTextContent-2_e96 h2 > span {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal;
            line-height: 1.15
        }

        html[lang=ru-ru] .richTextContent-2_e96 h2 > span {
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif
        }

        html[lang=tr-tr] .richTextContent-2_e96 h2 > span {
            line-height: 1.1
        }

        html[lang=ko-kr] .richTextContent-2_e96 h2 > span {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        html[lang=vi-vn] .richTextContent-2_e96 h2 > span {
            font-family: RobotoCondensed-Bold, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            letter-spacing: 1px;
            line-height: 1.18
        }

        html[lang=zh-hk] .richTextContent-2_e96 h2 > span, html[lang=zh-tw] .richTextContent-2_e96 h2 > span {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-style: normal;
            font-weight: 500;
            line-height: 1.1
        }

        html[lang=ja-jp] .richTextContent-2_e96 h2 > span {
            font-family: BeaufortforLOLJa-Regular, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        .richTextContent-2_e96 h3 {
            font-size: 2.4rem
        }

        .richTextContent-2_e96 h3 > span {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            text-transform: uppercase
        }

        html[lang=th-th] .richTextContent-2_e96 h3 > span {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-weight: 700;
            line-height: 1.4;
            font-style: normal
        }

        html[lang=ar-ae] .richTextContent-2_e96 h3 > span {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal;
            line-height: 1.15
        }

        html[lang=ru-ru] .richTextContent-2_e96 h3 > span {
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif
        }

        html[lang=tr-tr] .richTextContent-2_e96 h3 > span {
            line-height: 1.1
        }

        html[lang=ko-kr] .richTextContent-2_e96 h3 > span {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        html[lang=vi-vn] .richTextContent-2_e96 h3 > span {
            font-family: RobotoCondensed-Bold, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            letter-spacing: 1px;
            line-height: 1.18
        }

        html[lang=zh-hk] .richTextContent-2_e96 h3 > span, html[lang=zh-tw] .richTextContent-2_e96 h3 > span {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-style: normal;
            font-weight: 500;
            line-height: 1.1
        }

        html[lang=ja-jp] .richTextContent-2_e96 h3 > span {
            font-family: BeaufortforLOLJa-Regular, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        .richTextContent-2_e96 h4 {
            font-size: 2rem
        }

        .richTextContent-2_e96 h5 {
            font-size: 1.8rem
        }

        .richTextContent-2_e96 h6 {
            font-size: 1.6rem
        }

        .richTextContent-2_e96 ul {
            list-style: disc
        }

        .richTextContent-2_e96 ol {
            list-style: decimal
        }

        [dir] .richTextContent-2_e96 ol, [dir] .richTextContent-2_e96 ul {
            margin: 1rem 0
        }

        [dir=ltr] .richTextContent-2_e96 ol, [dir=ltr] .richTextContent-2_e96 ul {
            margin-left: 1.5rem
        }

        [dir=rtl] .richTextContent-2_e96 ol, [dir=rtl] .richTextContent-2_e96 ul {
            margin-right: 1.5rem
        }

        .richTextContent-2_e96 ol li, .richTextContent-2_e96 ul li {
            font-size: 1.3rem
        }

        [dir] .richTextContent-2_e96 ol li:not(:last-child), [dir] .richTextContent-2_e96 ul li:not(:last-child) {
            margin-bottom: .25rem
        }

        @media (min-width: 768px) {
            .richTextContent-2_e96 img {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 4rem) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 4rem) 100%, 0 100%)
            }

            [dir] .richTextContent-2_e96 img {
                margin: 4rem 0
            }

            [dir=rtl] .richTextContent-2_e96 img {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 4rem 100%, 0 calc(100% - 3rem));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 4rem 100%, 0 calc(100% - 3rem))
            }

            [dir] .richTextContent-2_e96 a > img {
                margin: 2.4rem 0
            }

            [dir] .richTextContent-2_e96 figure {
                margin: 4rem 0
            }

            [dir] .richTextContent-2_e96 figure img {
                margin: 0
            }

            [dir] .richTextContent-2_e96 ol, [dir] .richTextContent-2_e96 ul {
                margin: 2.5rem 0
            }

            [dir=ltr] .richTextContent-2_e96 ol, [dir=ltr] .richTextContent-2_e96 ul {
                margin-left: 2rem
            }

            [dir=rtl] .richTextContent-2_e96 ol, [dir=rtl] .richTextContent-2_e96 ul {
                margin-right: 2rem
            }

            .richTextContent-2_e96 ol li, .richTextContent-2_e96 ul li {
                font-size: 1.6rem
            }

            [dir] .richTextContent-2_e96 ol li:not(:last-child), [dir] .richTextContent-2_e96 ul li:not(:last-child) {
                margin-bottom: .5rem
            }

            .richTextContent-2_e96 a, .richTextContent-2_e96 blockquote, .richTextContent-2_e96 p {
                font-size: 1.6rem
            }

            .richTextContent-2_e96 h1 {
                font-size: 5.4rem
            }

            .richTextContent-2_e96 h2 {
                font-size: 4.8rem
            }

            .richTextContent-2_e96 h3 {
                font-size: 3.2rem
            }

            .richTextContent-2_e96 h4 {
                font-size: 2.8rem
            }

            .richTextContent-2_e96 h5 {
                font-size: 2.6rem
            }

            .richTextContent-2_e96 h6 {
                font-size: 2.4rem
            }
        }

        @media (min-width: 1024px) {
            [dir=ltr] .richTextContent-2_e96 ol, [dir=ltr] .richTextContent-2_e96 ul {
                margin-left: 1.7rem
            }

            [dir=rtl] .richTextContent-2_e96 ol, [dir=rtl] .richTextContent-2_e96 ul {
                margin-right: 1.7rem
            }
        }

        .accordion-1Wkgq .item-2ZxmI {
            overflow: hidden;
            color: #fff
        }

        [dir] .accordion-1Wkgq .item-2ZxmI:not(:last-of-type) {
            margin-bottom: 1.2rem
        }

        [dir] .accordion-1Wkgq .item-2ZxmI:hover {
            cursor: pointer
        }

        .accordion-1Wkgq .header-3t {
            display: flex;
            justify-content: space-between;
            align-items: center
        }

        .accordion-1Wkgq .icon-2kTCb {
            height: 3rem;
            width: 3rem;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden
        }

        [dir] .accordion-1Wkgq .icon-2kTCb {
            transform-origin: center
        }

        [dir=ltr] .accordion-1Wkgq .icon-2kTCb {
            margin-right: 2rem
        }

        [dir=rtl] .accordion-1Wkgq .icon-2kTCb {
            margin-left: 2rem
        }

        [dir=ltr] .accordion-1Wkgq .title-3gre_ {
            transform-origin: left
        }

        [dir=rtl] .accordion-1Wkgq .title-3gre_ {
            transform-origin: right
        }

        .accordion-1Wkgq .titleWrapper-3FHW4 {
            display: flex;
            align-items: center
        }

        .accordion-1Wkgq .titleWrapper-3FHW4 .icon-2kTCb, .accordion-1Wkgq .titleWrapper-3FHW4 .title-3gre_ {
            transition: transform .2s ease-in-out
        }

        [dir] .accordion-1Wkgq .titleWrapper-3FHW4 .icon-2kTCb {
            transform: scale(.8)
        }

        [dir] .accordion-1Wkgq .titleWrapper-3FHW4 .title-3gre_ {
            transform: scale(.6)
        }

        [dir] .accordion-1Wkgq .titleWrapper-3FHW4.active-1Ri9D .icon-2kTCb, [dir] .accordion-1Wkgq .titleWrapper-3FHW4.active-1Ri9D .title-3gre_ {
            transform: scale(1)
        }

        .accordion-1Wkgq .indicator-1FTvI {
            height: .3rem;
            width: 1.3rem;
            min-width: 1.3rem;
            min-height: .3rem;
            position: relative;
            transition: transform .5s ease-out
        }

        [dir] .accordion-1Wkgq .indicator-1FTvI {
            background-color: #fff;
            transform: rotate(0deg);
            transform-origin: center
        }

        .accordion-1Wkgq .indicator-1FTvI:before {
            content: "";
            display: block;
            position: absolute;
            height: 100%;
            width: 100%;
            transition: transform .5s ease-out
        }

        [dir] .accordion-1Wkgq .indicator-1FTvI:before {
            background-color: #fff;
            transform-origin: center
        }

        [dir=ltr] .accordion-1Wkgq .indicator-1FTvI:before {
            transform: rotate(90deg)
        }

        [dir=rtl] .accordion-1Wkgq .indicator-1FTvI:before {
            transform: rotate(-90deg)
        }

        [dir=ltr] .accordion-1Wkgq .indicator-1FTvI.active-1Ri9D {
            transform: rotate(180deg)
        }

        [dir=rtl] .accordion-1Wkgq .indicator-1FTvI.active-1Ri9D {
            transform: rotate(-180deg)
        }

        [dir=ltr] .accordion-1Wkgq .indicator-1FTvI.active-1Ri9D:before {
            transform: rotate(180deg)
        }

        [dir=rtl] .accordion-1Wkgq .indicator-1FTvI.active-1Ri9D:before {
            transform: rotate(-180deg)
        }

        .accordion-1Wkgq .description-kqOZ2 {
            height: 0;
            position: relative
        }

        .accordion-1Wkgq .description-kqOZ2 .decriptionCopy-AU {
            position: absolute
        }

        [dir=ltr] .accordion-1Wkgq .description-kqOZ2 .decriptionCopy-AU {
            padding: 2rem 4rem 3.2rem 0
        }

        [dir=rtl] .accordion-1Wkgq .description-kqOZ2 .decriptionCopy-AUOe0 {
            padding: 2rem 0 3.2rem 4rem
        }

        .accordion-1Wkgq .description-kqOZ2 .decriptionCopy-AUOe0:before {
            content: "";
            display: block;
            position: absolute;
            height: .25rem;
            width: 100%;
            bottom: .55rem;
            z-index: -1
        }

        [dir] .accordion-1Wkgq .description-kqOZ2 .decriptionCopy-AUOe0:before {
            background: hsla(0, 0%, 100%, .2)
        }

        [dir=ltr] .accordion-1Wkgq .description-kqOZ2 .decriptionCopy-AUOe0:before {
            left: 0
        }

        [dir=rtl] .accordion-1Wkgq .description-kqOZ2 .decriptionCopy-AUOe0:before {
            right: 0
        }

        .accordion-1Wkgq .dashDetail-3YdzR {
            position: absolute;
            bottom: .2rem;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            height: .35rem;
            width: 5rem
        }

        [dir] .accordion-1Wkgq .dashDetail-3YdzR {
            background: hsla(0, 0%, 100%, .2)
        }

        [dir=ltr] .accordion-1Wkgq .dashDetail-3YdzR {
            left: 0;
            transform-origin: left
        }

        [dir=rtl] .accordion-1Wkgq .dashDetail-3YdzR {
            right: 0;
            transform-origin: right
        }

        .accordion-1Wkgq .dashDetail-3YdzR:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .accordion-1Wkgq .dashDetail-3YdzR:after {
            border-top: .35rem solid hsla(0, 0%, 100%, .2)
        }

        [dir=ltr] .accordion-1Wkgq .dashDetail-3YdzR:after {
            right: -.35rem;
            border-right: .35rem solid transparent
        }

        [dir=rtl] .accordion-1Wkgq .dashDetail-3YdzR:after {
            left: -.35rem;
            border-left: .35rem solid transparent
        }

        [dir] .accordion-1Wkgq.newsArticle-3EHEx {
            margin: 1.6rem 0
        }

        .accordion-1Wkgq.newsArticle-3EHEx .item-2ZxmI {
            color: #32c8ff
        }

        [dir] .accordion-1Wkgq.newsArticle-3EHEx .indicator-1FTvI, [dir] .accordion-1Wkgq.newsArticle-3EHEx .indicator-1FTvI:before {
            background-color: #32c8ff
        }

        [dir] .accordion-1Wkgq.newsArticle-3EHEx .titleWrapper-3FHW4 .title-3gre_, [dir] .accordion-1Wkgq.newsArticle-3EHEx .titleWrapper-3FHW4.active-1Ri9D .scale-2Ebyg {
            transform: scale(.6)
        }

        .accordion-1Wkgq.newsArticle-3EHEx .dashDetail-3YdzR {
            height: .35rem;
            width: 5rem
        }

        [dir] .accordion-1Wkgq.newsArticle-3EHEx .dashDetail-3YdzR {
            background: rgba(17, 17, 17, .2)
        }

        .accordion-1Wkgq.newsArticle-3EHEx .dashDetail-3YdzR:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .accordion-1Wkgq.newsArticle-3EHEx .dashDetail-3YdzR:after {
            border-top: .35rem solid rgba(17, 17, 17, .2)
        }

        [dir=ltr] .accordion-1Wkgq.newsArticle-3EHEx .dashDetail-3YdzR:after {
            right: -.35rem;
            border-right: .35rem solid transparent
        }

        [dir=rtl] .accordion-1Wkgq.newsArticle-3EHEx .dashDetail-3YdzR:after {
            left: -.35rem;
            border-left: .35rem solid transparent
        }

        .accordion-1Wkgq.newsArticle-3EHEx .description-kqOZ2 {
            color: #111
        }

        .accordion-1Wkgq.newsArticle-3EHEx .decriptionCopy-AUOe0 {
            width: 80%
        }

        [dir] .accordion-1Wkgq.newsArticle-3EHEx .decriptionCopy-AUOe0 ol, [dir] .accordion-1Wkgq.newsArticle-3EHEx .decriptionCopy-AUOe0 ul {
            margin-top: 0
        }

        .accordion-1Wkgq.newsArticle-3EHEx .decriptionCopy-AUOe0:after {
            content: "";
            display: block;
            position: absolute;
            height: .25rem;
            width: 100%;
            bottom: .55rem;
            z-index: -1
        }

        [dir] .accordion-1Wkgq.newsArticle-3EHEx .decriptionCopy-AUOe0:after {
            background: rgba(17, 17, 17, .2)
        }

        [dir=ltr] .accordion-1Wkgq.newsArticle-3EHEx .decriptionCopy-AUOe0:after {
            left: 0
        }

        [dir=rtl] .accordion-1Wkgq.newsArticle-3EHEx .decriptionCopy-AUOe0:after {
            right: 0
        }

        [dir] .contentWrapper-13oyL {
            margin: 2.25rem 0
        }

        .title-2o8bH {
            font-size: 2.4rem;
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            text-transform: uppercase
        }

        html[lang=th-th] .title-2o8bH {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-weight: 700;
            line-height: 1.4;
            font-style: normal
        }

        html[lang=ar-ae] .title-2o8bH {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal;
            line-height: 1.15
        }

        html[lang=ru-ru] .title-2o8bH {
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif
        }

        html[lang=tr-tr] .title-2o8bH {
            line-height: 1.1
        }

        html[lang=ko-kr] .title-2o8bH {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        html[lang=vi-vn] .title-2o8bH {
            font-family: RobotoCondensed-Bold, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            letter-spacing: 1px;
            line-height: 1.18
        }

        html[lang=zh-hk] .title-2o8bH, html[lang=zh-tw] .title-2o8bH {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-style: normal;
            font-weight: 500;
            line-height: 1.1
        }

        html[lang=ja-jp] .title-2o8bH {
            font-family: BeaufortforLOLJa-Regular, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        @media (min-width: 768px) {
            [dir] .contentWrapper-13oyL {
                margin: 4.5rem 0
            }

            .title-2o8bH {
                font-size: 3.2rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .contentWrapper-13oyL {
                margin: 4rem 0
            }
        }

        .linkButton-1Ll1h {
            transition: color .2s ease-out;
            display: flex;
            z-index: 1
        }

        .linkButton-1Ll1h.disabled-2Gydq {
            opacity: .7
        }

        .icon-1djbk {
            height: 2.2rem;
            width: .8rem
        }

        [dir=ltr] .icon-1djbk {
            margin-left: 1rem
        }

        [dir=rtl] .icon-1djbk {
            margin-right: 1rem;
            transform: translateX(0) scale(-1)
        }

        .icon-1djbk path {
            stroke-width: .8rem;
            transition: stroke .2s ease-out
        }

        .default-2JTfs {
            color: #32c8ff
        }

        .default-2JTfs .icon-1djbk path {
            stroke: #32c8ff
        }

        .light-2dUxQ {
            color: #fff
        }

        .light-2dUxQ .icon-1djbk path {
            stroke: #fff
        }

        .dark-L4CHE {
            color: #141e37
        }

        .dark-L4CHE .icon-1djbk path {
            stroke: #141e37
        }

        .white-3RHqi {
            color: #fff
        }

        .white-3RHqi .icon-1djbk path {
            stroke: #fff
        }

        @media (min-width: 768px) {
            .icon-1djbk {
                height: 3.6rem;
                width: 1.2rem
            }

            [dir=ltr] .icon-1djbk {
                margin-left: 2rem
            }

            [dir=rtl] .icon-1djbk {
                margin-right: 2rem
            }

            .icon-1djbk path {
                stroke-width: .7rem
            }
        }

        @media (min-width: 1024px) {
            .linkButton-1Ll1h .icon-1djbk {
                height: 1.7rem;
                width: .7rem;
                transition: transform .3s ease-out
            }

            [dir=ltr] .linkButton-1Ll1h .icon-1djbk {
                margin-left: 1.2rem
            }

            [dir=rtl] .linkButton-1Ll1h .icon-1djbk {
                margin-right: 1.2rem
            }

            .linkButton-1Ll1h .icon-1djbk path {
                stroke-width: .7rem
            }

            [dir=ltr] .linkButton-1Ll1h:hover .icon-1djbk {
                transform: translateX(.3rem)
            }

            [dir=rtl] .linkButton-1Ll1h:hover .icon-1djbk {
                transform: translateX(-.3rem);
                transform: translateX(-.3rem) scale(-1)
            }

            .linkButton-1Ll1h:hover.default-2JTfs {
                color: #141e37
            }

            .linkButton-1Ll1h:hover.default-2JTfs .icon-1djbk path {
                stroke: #141e37
            }

            .linkButton-1Ll1h:hover.light-2dUxQ {
                color: #32c8ff
            }

            .linkButton-1Ll1h:hover.light-2dUxQ .icon-1djbk path {
                stroke: #32c8ff
            }

            .linkButton-1Ll1h:hover.dark-L4CHE {
                color: #32c8ff
            }

            .linkButton-1Ll1h:hover.dark-L4CHE .icon-1djbk path {
                stroke: #32c8ff
            }

            .linkButton-1Ll1h:hover.white-3RHqi {
                color: #141e37
            }

            .linkButton-1Ll1h:hover.white-3RHqi .icon-1djbk path {
                stroke: #141e37
            }
        }

        .buttonsWrapper-1Czcz {
            display: flex;
            flex-direction: column
        }

        [dir] .buttonsWrapper-1Czcz {
            margin-bottom: 4.5rem;
            margin-top: 4.5rem
        }

        .button-122cq {
            display: flex;
            align-items: center;
            justify-content: center;
            width: 100%
        }

        .button-122cq[data-size="2"] {
            width: 100%
        }

        [dir] .button-122cq[data-size="2"]:first-child {
            margin-bottom: 2.5rem
        }

        .buttonInline-3yrE7 {
            display: inline-block
        }

        @media (min-width: 1024px) {
            .buttonsWrapper-1Czcz {
                flex-direction: row
            }

            .button-122cq[data-size="2"] {
                width: calc(50% - 5rem)
            }

            .button-122cq[data-size="2"]:first-child {
                justify-content: flex-end
            }

            [dir] .button-122cq[data-size="2"]:first-child {
                margin-bottom: 0
            }

            [dir=ltr] .button-122cq[data-size="2"]:first-child {
                margin-right: 5rem
            }

            [dir=rtl] .button-122cq[data-size="2"]:first-child {
                margin-left: 5rem
            }

            .button-122cq[data-size="2"]:last-child {
                justify-content: flex-start
            }
        }

        .carouselBlock-qmivW {
            position: relative
        }

        [dir] .carouselBlock-qmivW {
            margin: 5rem 0
        }

        .carouselBlock-qmivW .carouselWrapper-8HwTf {
            position: relative;
            height: 100%;
            width: 100%
        }

        [dir] .carouselBlock-qmivW .carouselWrapper-8HwTf {
            padding-bottom: 3rem
        }

        .carouselBlock-qmivW .frameBorderWrapper-1A4bS {
            position: absolute;
            top: 0;
            bottom: 0;
            width: calc(100% + 15vw);
            height: 100%;
            color: #c89b3c;
            pointer-events: none
        }

        [dir] .carouselBlock-qmivW .frameBorderWrapper-1A4bS {
            margin-top: 3rem
        }

        [dir=ltr] .carouselBlock-qmivW .frameBorderWrapper-1A4bS {
            left: 0;
            right: 0;
            transform: translateX(-7.5vw)
        }

        [dir=rtl] .carouselBlock-qmivW .frameBorderWrapper-1A4bS {
            right: 0;
            left: 0;
            transform: translateX(7.5vw)
        }

        .carouselBlock-qmivW .frame-1ss9H {
            height: auto;
            width: 100%;
            position: absolute;
            top: 0;
            z-index: 2
        }

        [dir] .carouselBlock-qmivW .frame-1ss9H {
            padding: 0 1.5rem
        }

        [dir=ltr] .carouselBlock-qmivW .frame-1ss9H {
            left: 0
        }

        [dir=rtl] .carouselBlock-qmivW .frame-1ss9H {
            right: 0
        }

        .carouselBlock-qmivW .frame-1ss9H path {
            stroke: #c89b3c
        }

        .carouselBlock-qmivW .carousel-3RfuK {
            position: relative;
            height: 0;
            overflow: hidden;
            width: calc(100% + 15vw)
        }

        [dir] .carouselBlock-qmivW .carousel-3RfuK {
            margin-top: 2.5rem;
            padding-bottom: 100%
        }

        [dir=ltr] .carouselBlock-qmivW .carousel-3RfuK {
            transform: translateX(-7.5vw)
        }

        [dir=rtl] .carouselBlock-qmivW .carousel-3RfuK {
            transform: translateX(7.5vw)
        }

        .carouselBlock-qmivW .carouselContent-2wAuu {
            position: absolute;
            top: 0;
            display: flex;
            height: 100%
        }

        [dir=ltr] .carouselBlock-qmivW .carouselContent-2wAuu {
            left: 0
        }

        [dir=rtl] .carouselBlock-qmivW .carouselContent-2wAuu {
            right: 0
        }

        .carouselBlock-qmivW .slide-1EIh- {
            position: relative;
            min-width: 100%
        }

        .carouselBlock-qmivW .slide-1EIh-:after {
            content: "";
            position: absolute;
            top: 0;
            bottom: 0
        }

        [dir] .carouselBlock-qmivW .slide-1EIh-:after {
            background: #000
        }

        [dir=ltr] .carouselBlock-qmivW .slide-1EIh-:after {
            left: 0;
            right: 0;
            background: linear-gradient(90deg, rgba(0, 0, 0, .6), transparent 25%, transparent 75%, rgba(0, 0, 0, .6))
        }

        [dir=rtl] .carouselBlock-qmivW .slide-1EIh-:after {
            right: 0;
            left: 0;
            background: linear-gradient(-90deg, rgba(0, 0, 0, .6), transparent 25%, transparent 75%, rgba(0, 0, 0, .6))
        }

        .carouselBlock-qmivW .slide-1EIh- .image-2vsK9 {
            width: 100%;
            height: 100%;
            -o-object-fit: cover;
            object-fit: cover;
            -o-object-position: center;
            object-position: center
        }

        .carouselBlock-qmivW .slide-1EIh- .ieImage-3JFDp {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0
        }

        [dir] .carouselBlock-qmivW .slide-1EIh- .ieImage-3JFDp {
            background-size: cover;
            background-position: 50%;
            background-repeat: no-repeat
        }

        [dir=ltr] .carouselBlock-qmivW .slide-1EIh- .ieImage-3JFDp {
            left: 0
        }

        [dir=rtl] .carouselBlock-qmivW .slide-1EIh- .ieImage-3JFDp {
            right: 0
        }

        .carouselBlock-qmivW .thumbnailsList-L2jek {
            display: flex;
            justify-content: space-between
        }

        [dir] .carouselBlock-qmivW .thumbnailsList-L2jek {
            margin-bottom: 3.5rem
        }

        [dir] .carouselBlock-qmivW .descriptionWrapper-8JlkH {
            padding: 0;
            margin-top: 3rem
        }

        .carouselBlock-qmivW .slideTitle-2NjDh {
            color: #c89b3c
        }

        [dir] .carouselBlock-qmivW .slideTitle-2NjDh {
            margin-bottom: 2rem
        }

        .carouselBlock-qmivW .arrowsWrapper-_VpyB {
            height: 100%;
            width: 100%;
            position: absolute;
            display: flex;
            justify-content: space-between;
            align-items: center;
            top: 0;
            pointer-events: none
        }

        [dir] .carouselBlock-qmivW .arrowsWrapper-_VpyB {
            padding: 0 1rem
        }

        [dir=ltr] .carouselBlock-qmivW .arrowsWrapper-_VpyB {
            left: 0
        }

        [dir=rtl] .carouselBlock-qmivW .arrowsWrapper-_VpyB {
            right: 0
        }

        .carouselBlock-qmivW .arrowsWrapper-_VpyB .arrow-3Cpa- {
            display: flex;
            color: #c89b3c;
            transition: opacity .3s ease-out, transform .3s ease-out;
            width: 4.2rem;
            height: auto;
            pointer-events: auto
        }

        .carouselBlock-qmivW .arrowsWrapper-_VpyB .arrow-3Cpa- div {
            opacity: 1;
            visibility: visible
        }

        [dir=ltr] .carouselBlock-qmivW .arrowsWrapper-_VpyB .arrow-3Cpa-.arrowPrev-1KZNF {
            transform: rotate(-180deg)
        }

        [dir=rtl] .carouselBlock-qmivW .arrowsWrapper-_VpyB .arrow-3Cpa-.arrowPrev-1KZNF {
            transform: rotate(180deg);
            transform: rotate(0deg)
        }

        [dir] .carouselBlock-qmivW .arrowsWrapper-_VpyB .arrow-3Cpa-.arrowNext-3ZX70 {
            transform: rotate(0deg)
        }

        [dir=rtl] .carouselBlock-qmivW .arrowsWrapper-_VpyB .arrow-3Cpa-.arrowNext-3ZX70 {
            transform: rotate(-180deg)
        }

        .carouselBlock-qmivW .thumbnail-IHq6d {
            overflow: hidden;
            opacity: .35;
            width: 4.5rem;
            height: 4.5rem
        }

        [dir] .carouselBlock-qmivW .thumbnail-IHq6d {
            border-radius: 50%;
            cursor: pointer
        }

        .carouselBlock-qmivW .thumbnail-IHq6d.isActive-3dYcY {
            opacity: 1
        }

        .carouselBlock-qmivW .copy-27To5 {
            min-height: 4.6rem
        }

        .carouselBlock-qmivW .posterContainer-7dtXg .playButton-1exIg {
            height: 5rem;
            width: 5rem;
            top: 50%;
            pointer-events: all;
            position: absolute;
            z-index: 2
        }

        [dir] .carouselBlock-qmivW .posterContainer-7dtXg .playButton-1exIg {
            cursor: pointer
        }

        [dir=ltr] .carouselBlock-qmivW .posterContainer-7dtXg .playButton-1exIg {
            left: 50%;
            transform: translate(-50%, -50%)
        }

        [dir=rtl] .carouselBlock-qmivW .posterContainer-7dtXg .playButton-1exIg {
            right: 50%;
            transform: translate(50%, -50%)
        }

        .carouselBlock-qmivW .posterContainer-7dtXg .posterImage-fGAE- {
            height: 100%;
            width: 100%;
            position: absolute;
            z-index: 1;
            display: block;
            top: 0
        }

        [dir=ltr] .carouselBlock-qmivW .posterContainer-7dtXg .posterImage-fGAE- {
            left: 0
        }

        [dir=rtl] .carouselBlock-qmivW .posterContainer-7dtXg .posterImage-fGAE- {
            right: 0
        }

        .carouselBlock-qmivW .posterContainer-7dtXg .youtubeVideo-2TPAw {
            height: 100%
        }

        [dir] .carouselBlock-qmivW .posterContainer-7dtXg .youtubeVideo-2TPAw {
            background: #111
        }

        .carouselBlock-qmivW .posterContainer-7dtXg .youtubeVideo-2TPAw > iframe {
            height: 100%;
            width: 100%
        }

        .carouselBlock-qmivW .posterContainer-7dtXg .overlay-3tBYa {
            content: "";
            position: absolute;
            display: initial;
            top: 0;
            bottom: 0;
            z-index: 1
        }

        [dir] .carouselBlock-qmivW .posterContainer-7dtXg .overlay-3tBYa {
            background: rgba(0, 0, 0, .2)
        }

        [dir=ltr] .carouselBlock-qmivW .posterContainer-7dtXg .overlay-3tBYa, [dir=rtl] .carouselBlock-qmivW .posterContainer-7dtXg .overlay-3tBYa {
            left: 0;
            right: 0
        }

        @media (min-width: 768px) {
            .carouselBlock-qmivW .posterContainer-7dtXg .playButton-1exIg {
                height: 6rem;
                width: 6rem
            }
        }

        @media (min-width: 1024px) {
            .carouselBlock-qmivW .posterContainer-7dtXg .playButton-1exIg {
                top: 50%
            }

            .carouselBlock-qmivW .posterContainer-7dtXg .playButton-1exIg circle {
                transition: stroke .1s ease-out
            }

            .carouselBlock-qmivW .posterContainer-7dtXg .playButton-1exIg path {
                transition: transform .1s ease-out
            }

            [dir] .carouselBlock-qmivW .posterContainer-7dtXg .playButton-1exIg path {
                transform-origin: center;
                transform: scale(1) translate(0)
            }

            .carouselBlock-qmivW .posterContainer-7dtXg .playButton-1exIg:before {
                content: "";
                display: block;
                position: absolute;
                height: 100%;
                width: 100%;
                top: 0;
                opacity: 0;
                z-index: -1;
                transition: opacity .1s ease-out
            }

            [dir] .carouselBlock-qmivW .posterContainer-7dtXg .playButton-1exIg:before {
                border-radius: 100%;
                background: #141e37
            }

            [dir=ltr] .carouselBlock-qmivW .posterContainer-7dtXg .playButton-1exIg:before {
                left: 0
            }

            [dir=rtl] .carouselBlock-qmivW .posterContainer-7dtXg .playButton-1exIg:before {
                right: 0
            }

            .carouselBlock-qmivW .posterContainer-7dtXg .playButton-1exIg:hover circle {
                stroke: #141e37
            }

            [dir] .carouselBlock-qmivW .posterContainer-7dtXg .playButton-1exIg:hover path {
                transform: scale(1.2) translate(0)
            }

            .carouselBlock-qmivW .posterContainer-7dtXg .playButton-1exIg:hover:before {
                opacity: 1
            }
        }

        @media (min-width: 768px) {
            .carouselBlock-qmivW .frameBorderWrapper-1A4bS {
                display: none
            }

            [dir] .carouselBlock-qmivW .arrowsWrapper-_VpyB {
                padding: 0 3rem
            }

            .carouselBlock-qmivW .arrowsWrapper-_VpyB .arrow-3Cpa- {
                width: 6.8rem;
                height: 6.8rem
            }
        }

        @media (min-width: 1024px) {
            .carouselBlock-qmivW .frameBorderWrapper-1A4bS {
                display: block;
                width: 100%;
                height: 100%
            }

            [dir] .carouselBlock-qmivW .frameBorderWrapper-1A4bS {
                transform: translateX(0)
            }

            .carouselBlock-qmivW .frame-1ss9H {
                height: 100%
            }

            [dir] .carouselBlock-qmivW .frame-1ss9H {
                padding: 0 1rem
            }

            .carouselBlock-qmivW .carousel-3RfuK {
                position: relative;
                height: 0;
                overflow: hidden;
                width: 100%
            }

            [dir] .carouselBlock-qmivW .carousel-3RfuK {
                margin-top: 3rem;
                padding-bottom: 56.25%;
                transform: translateX(0)
            }

            [dir] .carouselBlock-qmivW .arrowsWrapper-_VpyB {
                padding: 0 1rem
            }

            .carouselBlock-qmivW .arrowsWrapper-_VpyB .arrow-3Cpa- {
                width: 4.8rem;
                height: 100%
            }

            .carouselBlock-qmivW .arrowsWrapper-_VpyB .arrow-3Cpa- div {
                opacity: 0;
                visibility: hidden
            }

            [dir] .carouselBlock-qmivW .descriptionWrapper-8JlkH {
                padding: 0 7rem;
                margin-top: 2.5rem
            }

            .carouselBlock-qmivW .thumbnail-IHq6d {
                width: 6.6rem;
                height: 6.6rem
            }
        }

        .galleryBlock-17sGz {
            position: relative
        }

        [dir] .galleryBlock-17sGz {
            padding: 2.25rem 0
        }

        [dir] .title-1CN8F {
            margin-bottom: 2.4rem
        }

        .sliderWrapper-1u3-z {
            position: relative;
            width: calc(100% + 15vw);
            overflow: hidden
        }

        [dir=ltr] .sliderWrapper-1u3-z {
            transform: translateX(-7.5vw)
        }

        [dir=rtl] .sliderWrapper-1u3-z {
            transform: translateX(7.5vw)
        }

        .controlsWrapper-20kbA {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 50%;
            pointer-events: none
        }

        [dir=ltr] .controlsWrapper-20kbA {
            left: 50%;
            transform: translate(-50%, -50%)
        }

        [dir=rtl] .controlsWrapper-20kbA {
            right: 50%;
            transform: translate(50%, -50%)
        }

        .frame-2PcDi {
            height: 100%;
            width: 91%;
            position: absolute;
            top: 50%;
            z-index: 2
        }

        [dir=ltr] .frame-2PcDi {
            left: 50%;
            transform: translate(-50%, -50%)
        }

        [dir=rtl] .frame-2PcDi {
            right: 50%;
            transform: translate(50%, -50%)
        }

        .frame-2PcDi path {
            stroke: #fff
        }

        .sliderContent-1E2-V {
            display: flex
        }

        .arrowsWrapper-1dWg5 {
            height: 100%;
            width: 100%;
            position: abolute;
            display: flex;
            justify-content: space-between;
            align-items: center;
            top: 50%
        }

        [dir] .arrowsWrapper-1dWg5 {
            padding: 0 2.7%
        }

        .sliderArrow-2semQ {
            display: flex;
            color: #fff;
            transition: opacity .3s ease-out, transform .3s ease-out;
            width: 9.3%;
            pointer-events: all
        }

        [dir=ltr] .sliderArrow-2semQ.arrowPrev-MmKV8 {
            transform: rotate(-180deg)
        }

        [dir=rtl] .sliderArrow-2semQ.arrowPrev-MmKV8 {
            transform: rotate(180deg);
            transform: rotate(0deg)
        }

        [dir] .sliderArrow-2semQ.arrowNext-1W19x {
            transform: rotate(0deg)
        }

        [dir=rtl] .sliderArrow-2semQ.arrowNext-1W19x {
            transform: rotate(-180deg)
        }

        .slide-cvCMk {
            position: relative;
            min-width: 100%
        }

        .slide-cvCMk:before {
            content: "";
            display: block
        }

        [dir] .slide-cvCMk:before {
            padding-top: 100%
        }

        .slide-cvCMk:after {
            content: "";
            position: absolute;
            top: 0;
            bottom: 0
        }

        [dir] .slide-cvCMk:after {
            background: #000
        }

        [dir=ltr] .slide-cvCMk:after {
            left: 0;
            right: 0;
            background: linear-gradient(90deg, rgba(0, 0, 0, .6), transparent 25%, transparent 75%, rgba(0, 0, 0, .6))
        }

        [dir=rtl] .slide-cvCMk:after {
            right: 0;
            left: 0;
            background: linear-gradient(-90deg, rgba(0, 0, 0, .6), transparent 25%, transparent 75%, rgba(0, 0, 0, .6))
        }

        .captions-2gB_1 {
            font-family: Spiegel-Regular, arial, georgia, sans-serif;
            min-height: 3.3rem;
            font-size: 1.2rem;
            font-style: italic
        }

        [dir] .captions-2gB_1 {
            padding: 1rem 0 0;
            text-align: center
        }

        .ieImage-J80qC {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0
        }

        [dir] .ieImage-J80qC {
            background-size: cover;
            background-position: 50%;
            background-repeat: no-repeat
        }

        [dir=ltr] .ieImage-J80qC {
            left: 0
        }

        [dir=rtl] .ieImage-J80qC {
            right: 0
        }

        .image-2mw7J {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0;
            -o-object-fit: cover;
            object-fit: cover;
            -o-object-position: center;
            object-position: center
        }

        [dir=ltr] .image-2mw7J {
            left: 0
        }

        [dir=rtl] .image-2mw7J {
            right: 0
        }

        .bullets-1mX3a {
            display: flex;
            justify-content: center
        }

        [dir] .bullets-1mX3a {
            margin-top: 2.5rem
        }

        .bulletWrapper-3gEhM {
            height: .75rem;
            width: .75rem;
            opacity: .4;
            transition: opacity .2s ease-out
        }

        [dir=ltr] .bulletWrapper-3gEhM {
            margin-right: 1rem
        }

        [dir=rtl] .bulletWrapper-3gEhM {
            margin-left: 1rem
        }

        [dir=ltr] .bulletWrapper-3gEhM:last-child {
            margin-right: 0
        }

        [dir=rtl] .bulletWrapper-3gEhM:last-child {
            margin-left: 0
        }

        .bulletWrapper-3gEhM.isActive-1jbwL {
            opacity: 1
        }

        .bulletIcon-20Ymn path {
            fill: #32c8ff
        }

        @media (min-width: 768px) {
            [dir] .galleryBlock-17sGz {
                padding: 4.5rem 0
            }

            [dir] .title-1CN8F {
                margin-bottom: 5rem
            }

            [dir] .bullets-1mX3a {
                margin-top: 2rem
            }

            .bulletWrapper-3gEhM {
                height: 1.5rem;
                width: 1.5rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .title-1CN8F {
                margin-bottom: 2.4rem
            }

            .sliderWrapper-1u3-z {
                width: 100%
            }

            [dir] .sliderWrapper-1u3-z {
                transform: none
            }

            .slide-cvCMk {
                position: relative
            }

            .slide-cvCMk:before {
                content: "";
                display: block
            }

            [dir] .slide-cvCMk:before {
                padding-top: 56.25%
            }

            .sliderArrow-2semQ {
                width: 7.6%
            }

            [dir] .arrowsWrapper-1dWg5 {
                padding: 0 2.3%
            }

            .bulletWrapper-3gEhM {
                height: 1rem;
                width: 1rem
            }
        }

        .separator-fh8RT {
            position: relative;
            width: 100%;
            height: .2rem
        }

        [dir] .separator-fh8RT {
            margin: 4rem 0
        }

        .separator-fh8RT .lineWrapper-3R7lS {
            position: relative;
            display: block;
            width: 100%;
            height: 100%
        }

        .separator-fh8RT.Short-2IQ-9 {
            width: 100%
        }

        [dir] .separator-fh8RT.Short-2IQ-9 {
            background-color: #e1e1e1
        }

        .separator-fh8RT.Short-2IQ-9:before {
            content: "";
            position: absolute;
            bottom: -.3rem;
            height: .5rem;
            width: 10rem;
            -webkit-clip-path: polygon(0 0, .5rem 100%, 100% 100%, 100% 0);
            clip-path: polygon(0 0, .5rem 100%, 100% 100%, 100% 0)
        }

        [dir] .separator-fh8RT.Short-2IQ-9:before {
            background-color: #e1e1e1
        }

        [dir=ltr] .separator-fh8RT.Short-2IQ-9:before {
            right: 0
        }

        [dir=rtl] .separator-fh8RT.Short-2IQ-9:before {
            left: 0
        }

        .separator-fh8RT.Long-1VoFy {
            display: inline-block;
            width: 100vw
        }

        [dir=ltr] .separator-fh8RT.Long-1VoFy {
            left: 0;
            transform: translate(-7.5%)
        }

        [dir=rtl] .separator-fh8RT.Long-1VoFy {
            right: 0;
            transform: translate(7.5%)
        }

        .separator-fh8RT.Long-1VoFy .lineWrapper-3R7lS {
            width: calc(100vw - 2.5rem)
        }

        [dir=ltr] .separator-fh8RT.Long-1VoFy .lineWrapper-3R7lS {
            margin-left: auto;
            margin-right: 0
        }

        [dir=rtl] .separator-fh8RT.Long-1VoFy .lineWrapper-3R7lS {
            margin-right: auto;
            margin-left: 0
        }

        .separator-fh8RT.Long-1VoFy .line-2BIT9 {
            position: absolute;
            top: 0;
            bottom: 0;
            display: inline-block;
            width: 100%
        }

        [dir] .separator-fh8RT.Long-1VoFy .line-2BIT9 {
            background: #141e37
        }

        [dir=ltr] .separator-fh8RT.Long-1VoFy .line-2BIT9 {
            left: 0
        }

        [dir=rtl] .separator-fh8RT.Long-1VoFy .line-2BIT9 {
            right: 0
        }

        .separator-fh8RT.Long-1VoFy .line-2BIT9:after {
            content: "";
            position: absolute;
            top: -5rem;
            width: .2rem;
            height: 5rem
        }

        [dir] .separator-fh8RT.Long-1VoFy .line-2BIT9:after {
            background-color: #141e37
        }

        [dir=ltr] .separator-fh8RT.Long-1VoFy .line-2BIT9:after {
            left: -.1rem;
            transform-origin: 100% 100%;
            transform: rotate(-45deg)
        }

        [dir=rtl] .separator-fh8RT.Long-1VoFy .line-2BIT9:after {
            right: -.1rem;
            transform-origin: 0 100%;
            transform: rotate(45deg)
        }

        .separator-fh8RT.Long-1VoFy .line-2BIT9:before {
            content: "";
            position: absolute;
            bottom: -.2rem;
            width: 5rem;
            height: .2rem
        }

        [dir] .separator-fh8RT.Long-1VoFy .line-2BIT9:before {
            background-color: #141e37
        }

        [dir=ltr] .separator-fh8RT.Long-1VoFy .line-2BIT9:before {
            right: 0
        }

        [dir=rtl] .separator-fh8RT.Long-1VoFy .line-2BIT9:before {
            left: 0
        }

        @media (min-width: 768px) {
            .separator-fh8RT.Long-1VoFy .lineWrapper-3R7lS {
                width: calc(100vw - 5rem)
            }
        }

        @media (min-width: 1024px) {
            [dir=ltr] .separator-fh8RT.Long-1VoFy {
                transform: translate(-7.3%) translate(-8.5rem)
            }

            [dir=rtl] .separator-fh8RT.Long-1VoFy {
                transform: translate(7.3%) translate(8.5rem)
            }
        }

        @media (min-width: 1440px) {
            [dir=ltr] .separator-fh8RT.Long-1VoFy {
                left: -50%;
                transform: translate(-50%) translate(77.5rem)
            }

            [dir=rtl] .separator-fh8RT.Long-1VoFy {
                right: -50%;
                transform: translate(50%) translate(-77.5rem)
            }

            .separator-fh8RT.Long-1VoFy .line-2BIT9:after {
                top: -10rem;
                height: 10rem
            }

            .separator-fh8RT.Long-1VoFy .line-2BIT9:before {
                width: 10rem
            }
        }

        .twitchWrapper-2-Muk {
            position: relative;
            width: 100%;
            height: 0;
            overflow: hidden
        }

        [dir] .twitchWrapper-2-Muk {
            padding-bottom: 56.25%;
            margin-top: 3rem 0
        }

        .twitchWrapper-2-Muk iframe {
            position: absolute;
            top: 0
        }

        [dir=ltr] .twitchWrapper-2-Muk iframe {
            left: 0
        }

        [dir=rtl] .twitchWrapper-2-Muk iframe {
            right: 0
        }

        .loaderWrapper-3opZd {
            min-height: 50rem;
            width: 100%;
            opacity: .3;
            display: flex;
            align-items: center;
            justify-content: center
        }

        html[browser="Internet Explorer"] .blockWrapper-33_bR {
            display: none
        }

        .videoBlock-1o9D7 {
            position: relative
        }

        [dir] .videoBlock-1o9D7 {
            margin-bottom: 1.75rem;
            margin-top: 4.75rem
        }

        [dir] .videoBlock-1o9D7 .videoTitle-ZaYAw {
            margin-bottom: 2.5rem
        }

        .videoBlock-1o9D7 .videoWrapper-37ZaE {
            height: 100%;
            width: 100%;
            position: relative
        }

        .videoBlock-1o9D7 .videoWrapper-37ZaE:before {
            content: "";
            display: block
        }

        [dir] .videoBlock-1o9D7 .videoWrapper-37ZaE:before {
            padding-top: 56.25%
        }

        .videoBlock-1o9D7 .video-1ORHj {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0
        }

        [dir=ltr] .videoBlock-1o9D7 .video-1ORHj {
            left: 0
        }

        [dir=rtl] .videoBlock-1o9D7 .video-1ORHj {
            right: 0
        }

        .videoBlock-1o9D7 .videoBorder-1MqHZ {
            position: absolute;
            bottom: -.6rem
        }

        [dir=ltr] .videoBlock-1o9D7 .videoBorder-1MqHZ {
            right: -.6rem
        }

        [dir=rtl] .videoBlock-1o9D7 .videoBorder-1MqHZ {
            left: -.6rem
        }

        @media (min-width: 768px) {
            [dir] .videoBlock-1o9D7 {
                margin-bottom: 3.5rem
            }

            [dir] .videoBlock-1o9D7 .videoTitle-ZaYAw {
                margin-bottom: 5rem
            }

            .videoBlock-1o9D7 .videoBorder-1MqHZ {
                bottom: -1.2rem
            }

            [dir=ltr] .videoBlock-1o9D7 .videoBorder-1MqHZ {
                right: -1.2rem
            }

            [dir=rtl] .videoBlock-1o9D7 .videoBorder-1MqHZ {
                left: -1.2rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .videoBlock-1o9D7 {
                margin-bottom: 6rem
            }

            [dir] .videoBlock-1o9D7 .videoTitle-ZaYAw {
                margin-bottom: 2.3rem
            }

            .videoBlock-1o9D7 .videoBorder-1MqHZ {
                bottom: -2rem
            }

            [dir=ltr] .videoBlock-1o9D7 .videoBorder-1MqHZ {
                right: -2rem
            }

            [dir=rtl] .videoBlock-1o9D7 .videoBorder-1MqHZ {
                left: -2rem
            }
        }

        .firstSide-2PgwT {
            width: .25rem;
            height: 100%
        }

        .secondSide-2g7B7 {
            width: 100%;
            height: .25rem
        }

        .topRight-2gfP1 {
            top: 0
        }

        [dir=ltr] .topRight-2gfP1 {
            right: 0
        }

        [dir=rtl] .topRight-2gfP1 {
            left: 0
        }

        .topRight-2gfP1 .firstSide-2PgwT {
            bottom: 0
        }

        [dir] .topRight-2gfP1 .firstSide-2PgwT {
            transform-origin: top
        }

        [dir=ltr] .topRight-2gfP1 .firstSide-2PgwT {
            right: 0
        }

        [dir=rtl] .topRight-2gfP1 .firstSide-2PgwT {
            left: 0
        }

        .topRight-2gfP1 .dashDetail-26mBt {
            top: -.375rem
        }

        [dir=ltr] .topRight-2gfP1 .dashDetail-26mBt {
            right: 0;
            transform-origin: right
        }

        [dir=rtl] .topRight-2gfP1 .dashDetail-26mBt {
            left: 0;
            transform-origin: left
        }

        [dir=ltr] .topRight-2gfP1 .secondSide-2g7B7 {
            right: 0;
            transform-origin: right
        }

        [dir=rtl] .topRight-2gfP1 .secondSide-2g7B7 {
            left: 0;
            transform-origin: left
        }

        .topLeft-2ypaU {
            top: 0
        }

        [dir=ltr] .topLeft-2ypaU {
            left: 0
        }

        [dir=rtl] .topLeft-2ypaU {
            right: 0
        }

        .topLeft-2ypaU .firstSide-2PgwT {
            bottom: 0
        }

        [dir] .topLeft-2ypaU .firstSide-2PgwT {
            transform-origin: top
        }

        [dir=ltr] .topLeft-2ypaU .firstSide-2PgwT {
            left: 0
        }

        [dir=rtl] .topLeft-2ypaU .firstSide-2PgwT {
            right: 0
        }

        .topLeft-2ypaU .dashDetail-26mBt {
            top: -.375rem
        }

        [dir=ltr] .topLeft-2ypaU .dashDetail-26mBt {
            left: 0;
            transform-origin: left
        }

        [dir=rtl] .topLeft-2ypaU .dashDetail-26mBt {
            right: 0;
            transform-origin: right
        }

        [dir=ltr] .topLeft-2ypaU .secondSide-2g7B7 {
            left: 0;
            transform-origin: left
        }

        [dir=rtl] .topLeft-2ypaU .secondSide-2g7B7 {
            right: 0;
            transform-origin: right
        }

        .bottomLeft-XSvX5 {
            bottom: 0
        }

        [dir=ltr] .bottomLeft-XSvX5 {
            left: 0
        }

        [dir=rtl] .bottomLeft-XSvX5 {
            right: 0
        }

        .bottomLeft-XSvX5 .firstSide-2PgwT {
            top: 0
        }

        [dir] .bottomLeft-XSvX5 .firstSide-2PgwT {
            transform-origin: top
        }

        [dir=ltr] .bottomLeft-XSvX5 .firstSide-2PgwT {
            left: 0
        }

        [dir=rtl] .bottomLeft-XSvX5 .firstSide-2PgwT {
            right: 0
        }

        .bottomLeft-XSvX5 .dashDetail-26mBt {
            bottom: -.375rem
        }

        [dir=ltr] .bottomLeft-XSvX5 .dashDetail-26mBt {
            left: 0;
            transform-origin: left
        }

        [dir=rtl] .bottomLeft-XSvX5 .dashDetail-26mBt {
            right: 0;
            transform-origin: right
        }

        .bottomLeft-XSvX5 .secondSide-2g7B7 {
            bottom: 0
        }

        [dir=ltr] .bottomLeft-XSvX5 .secondSide-2g7B7 {
            left: 0;
            transform-origin: left
        }

        [dir=rtl] .bottomLeft-XSvX5 .secondSide-2g7B7 {
            right: 0;
            transform-origin: right
        }

        .bottomRight-1lUOm {
            bottom: 0
        }

        [dir=ltr] .bottomRight-1lUOm {
            right: 0
        }

        [dir=rtl] .bottomRight-1lUOm {
            left: 0
        }

        .bottomRight-1lUOm .firstSide-2PgwT {
            top: 0
        }

        [dir] .bottomRight-1lUOm .firstSide-2PgwT {
            transform-origin: bottom
        }

        [dir=ltr] .bottomRight-1lUOm .firstSide-2PgwT {
            right: 0
        }

        [dir=rtl] .bottomRight-1lUOm .firstSide-2PgwT {
            left: 0
        }

        .bottomRight-1lUOm .dashDetail-26mBt {
            bottom: -.375rem
        }

        [dir=ltr] .bottomRight-1lUOm .dashDetail-26mBt {
            right: 0;
            transform-origin: right
        }

        [dir=rtl] .bottomRight-1lUOm .dashDetail-26mBt {
            left: 0;
            transform-origin: left
        }

        .bottomRight-1lUOm .secondSide-2g7B7 {
            bottom: 0
        }

        [dir=ltr] .bottomRight-1lUOm .secondSide-2g7B7 {
            right: 0;
            transform-origin: right
        }

        [dir=rtl] .bottomRight-1lUOm .secondSide-2g7B7 {
            left: 0;
            transform-origin: left
        }

        .border-ojuPz {
            height: 72%;
            width: 75.6%;
            pointer-events: none
        }

        .border-ojuPz, .border-ojuPz .borderLine-24GpQ, .border-ojuPz .dashDetail-26mBt {
            position: absolute
        }

        .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt {
            height: .375rem;
            width: 22%
        }

        [dir] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt {
            background: #fff
        }

        .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
            border-bottom: .375rem solid #fff
        }

        [dir=ltr] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
            left: -.375rem;
            border-left: .375rem solid transparent
        }

        [dir=rtl] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
            right: -.375rem;
            border-right: .375rem solid transparent
        }

        html[dir=ltr][browser=Safari] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=rtl][browser=Safari] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=ltr][browser=Firefox] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=rtl][browser=Firefox] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
            right: -.275rem
        }

        .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt {
            height: .375rem;
            width: 22%
        }

        [dir] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt {
            background: #fff
        }

        .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
            border-bottom: .375rem solid #fff
        }

        [dir=ltr] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
            right: -.375rem;
            border-right: .375rem solid transparent
        }

        [dir=rtl] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
            left: -.375rem;
            border-left: .375rem solid transparent
        }

        html[dir=ltr][browser=Safari] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=rtl][browser=Safari] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=ltr][browser=Firefox] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=rtl][browser=Firefox] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
            left: -.275rem
        }

        .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt {
            height: .375rem;
            width: 22%
        }

        [dir] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt {
            background: #fff
        }

        .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            border-top: .375rem solid #fff
        }

        [dir=ltr] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            right: -.375rem;
            border-right: .375rem solid transparent
        }

        [dir=rtl] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            left: -.375rem;
            border-left: .375rem solid transparent
        }

        html[dir=ltr][browser=Safari] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=rtl][browser=Safari] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=ltr][browser=Firefox] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=rtl][browser=Firefox] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            left: -.275rem
        }

        .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt {
            height: .375rem;
            width: 22%
        }

        [dir] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt {
            background: #fff
        }

        .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
            border-top: .375rem solid #fff
        }

        [dir=ltr] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
            left: -.375rem;
            border-left: .375rem solid transparent
        }

        [dir=rtl] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
            right: -.375rem;
            border-right: .375rem solid transparent
        }

        html[dir=ltr][browser=Safari] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=rtl][browser=Safari] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=ltr][browser=Firefox] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=rtl][browser=Firefox] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
            right: -.275rem
        }

        [dir] .border-ojuPz.light-3sJ1f .borderLine-24GpQ {
            background-color: #fff
        }

        .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt {
            height: .375rem;
            width: 22%
        }

        [dir] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt {
            background: #141e37
        }

        .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
            border-bottom: .375rem solid #141e37
        }

        [dir=ltr] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
            left: -.375rem;
            border-left: .375rem solid transparent
        }

        [dir=rtl] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
            right: -.375rem;
            border-right: .375rem solid transparent
        }

        html[dir=ltr][browser=Safari] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=rtl][browser=Safari] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=ltr][browser=Firefox] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=rtl][browser=Firefox] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
            right: -.275rem
        }

        .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt {
            height: .375rem;
            width: 22%
        }

        [dir] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt {
            background: #141e37
        }

        .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
            border-bottom: .375rem solid #141e37
        }

        [dir=ltr] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
            right: -.375rem;
            border-right: .375rem solid transparent
        }

        [dir=rtl] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
            left: -.375rem;
            border-left: .375rem solid transparent
        }

        html[dir=ltr][browser=Safari] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=rtl][browser=Safari] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=ltr][browser=Firefox] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=rtl][browser=Firefox] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
            left: -.275rem
        }

        .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt {
            height: .375rem;
            width: 22%
        }

        [dir] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt {
            background: #141e37
        }

        .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            border-top: .375rem solid #141e37
        }

        [dir=ltr] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            right: -.375rem;
            border-right: .375rem solid transparent
        }

        [dir=rtl] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            left: -.375rem;
            border-left: .375rem solid transparent
        }

        html[dir=ltr][browser=Safari] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=rtl][browser=Safari] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=ltr][browser=Firefox] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=rtl][browser=Firefox] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            left: -.275rem
        }

        .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt {
            height: .375rem;
            width: 22%
        }

        [dir] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt {
            background: #141e37
        }

        .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
            border-top: .375rem solid #141e37
        }

        [dir=ltr] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
            left: -.375rem;
            border-left: .375rem solid transparent
        }

        [dir=rtl] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
            right: -.375rem;
            border-right: .375rem solid transparent
        }

        html[dir=ltr][browser=Safari] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=rtl][browser=Safari] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=ltr][browser=Firefox] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=rtl][browser=Firefox] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
            right: -.275rem
        }

        [dir] .border-ojuPz.dark-2F1bx .borderLine-24GpQ {
            background-color: #141e37
        }

        .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt {
            height: .375rem;
            width: 22%
        }

        [dir] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt {
            background: #32c8ff
        }

        .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
            border-bottom: .375rem solid #32c8ff
        }

        [dir=ltr] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
            left: -.375rem;
            border-left: .375rem solid transparent
        }

        [dir=rtl] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
            right: -.375rem;
            border-right: .375rem solid transparent
        }

        html[dir=ltr][browser=Safari] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=rtl][browser=Safari] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=ltr][browser=Firefox] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=rtl][browser=Firefox] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
            right: -.275rem
        }

        .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt {
            height: .375rem;
            width: 22%
        }

        [dir] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt {
            background: #32c8ff
        }

        .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
            border-bottom: .375rem solid #32c8ff
        }

        [dir=ltr] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
            right: -.375rem;
            border-right: .375rem solid transparent
        }

        [dir=rtl] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
            left: -.375rem;
            border-left: .375rem solid transparent
        }

        html[dir=ltr][browser=Safari] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=rtl][browser=Safari] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=ltr][browser=Firefox] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=rtl][browser=Firefox] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
            left: -.275rem
        }

        .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt {
            height: .375rem;
            width: 22%
        }

        [dir] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt {
            background: #32c8ff
        }

        .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            border-top: .375rem solid #32c8ff
        }

        [dir=ltr] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            right: -.375rem;
            border-right: .375rem solid transparent
        }

        [dir=rtl] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            left: -.375rem;
            border-left: .375rem solid transparent
        }

        html[dir=ltr][browser=Safari] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=rtl][browser=Safari] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=ltr][browser=Firefox] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=rtl][browser=Firefox] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
            left: -.275rem
        }

        .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt {
            height: .375rem;
            width: 22%
        }

        [dir] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt {
            background: #32c8ff
        }

        .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
            border-top: .375rem solid #32c8ff
        }

        [dir=ltr] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
            left: -.375rem;
            border-left: .375rem solid transparent
        }

        [dir=rtl] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
            right: -.375rem;
            border-right: .375rem solid transparent
        }

        html[dir=ltr][browser=Safari] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=rtl][browser=Safari] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
            right: -.275rem
        }

        html[dir=ltr][browser=Firefox] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
            left: -.275rem
        }

        html[dir=rtl][browser=Firefox] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
            right: -.275rem
        }

        [dir] .border-ojuPz.electric-3FAPB .borderLine-24GpQ {
            background-color: #32c8ff
        }

        @media (min-width: 768px) {
            .firstSide-2PgwT {
                width: .3rem;
                height: 100%
            }

            .secondSide-2g7B7 {
                width: 100%;
                height: .3rem
            }

            .topRight-2gfP1 {
                top: 0
            }

            [dir=ltr] .topRight-2gfP1 {
                right: 0
            }

            [dir=rtl] .topRight-2gfP1 {
                left: 0
            }

            .topRight-2gfP1 .firstSide-2PgwT {
                bottom: 0
            }

            [dir] .topRight-2gfP1 .firstSide-2PgwT {
                transform-origin: top
            }

            [dir=ltr] .topRight-2gfP1 .firstSide-2PgwT {
                right: 0
            }

            [dir=rtl] .topRight-2gfP1 .firstSide-2PgwT {
                left: 0
            }

            .topRight-2gfP1 .dashDetail-26mBt {
                top: -.45rem
            }

            [dir=ltr] .topRight-2gfP1 .dashDetail-26mBt {
                right: 0;
                transform-origin: right
            }

            [dir=rtl] .topRight-2gfP1 .dashDetail-26mBt {
                left: 0;
                transform-origin: left
            }

            [dir=ltr] .topRight-2gfP1 .secondSide-2g7B7 {
                right: 0;
                transform-origin: right
            }

            [dir=rtl] .topRight-2gfP1 .secondSide-2g7B7 {
                left: 0;
                transform-origin: left
            }

            .topLeft-2ypaU {
                top: 0
            }

            [dir=ltr] .topLeft-2ypaU {
                left: 0
            }

            [dir=rtl] .topLeft-2ypaU {
                right: 0
            }

            .topLeft-2ypaU .firstSide-2PgwT {
                bottom: 0
            }

            [dir] .topLeft-2ypaU .firstSide-2PgwT {
                transform-origin: top
            }

            [dir=ltr] .topLeft-2ypaU .firstSide-2PgwT {
                left: 0
            }

            [dir=rtl] .topLeft-2ypaU .firstSide-2PgwT {
                right: 0
            }

            .topLeft-2ypaU .dashDetail-26mBt {
                top: -.45rem
            }

            [dir=ltr] .topLeft-2ypaU .dashDetail-26mBt {
                left: 0;
                transform-origin: left
            }

            [dir=rtl] .topLeft-2ypaU .dashDetail-26mBt {
                right: 0;
                transform-origin: right
            }

            [dir=ltr] .topLeft-2ypaU .secondSide-2g7B7 {
                left: 0;
                transform-origin: left
            }

            [dir=rtl] .topLeft-2ypaU .secondSide-2g7B7 {
                right: 0;
                transform-origin: right
            }

            .bottomLeft-XSvX5 {
                bottom: 0
            }

            [dir=ltr] .bottomLeft-XSvX5 {
                left: 0
            }

            [dir=rtl] .bottomLeft-XSvX5 {
                right: 0
            }

            .bottomLeft-XSvX5 .firstSide-2PgwT {
                top: 0
            }

            [dir] .bottomLeft-XSvX5 .firstSide-2PgwT {
                transform-origin: top
            }

            [dir=ltr] .bottomLeft-XSvX5 .firstSide-2PgwT {
                left: 0
            }

            [dir=rtl] .bottomLeft-XSvX5 .firstSide-2PgwT {
                right: 0
            }

            .bottomLeft-XSvX5 .dashDetail-26mBt {
                bottom: -.45rem
            }

            [dir=ltr] .bottomLeft-XSvX5 .dashDetail-26mBt {
                left: 0;
                transform-origin: left
            }

            [dir=rtl] .bottomLeft-XSvX5 .dashDetail-26mBt {
                right: 0;
                transform-origin: right
            }

            .bottomLeft-XSvX5 .secondSide-2g7B7 {
                bottom: 0
            }

            [dir=ltr] .bottomLeft-XSvX5 .secondSide-2g7B7 {
                left: 0;
                transform-origin: left
            }

            [dir=rtl] .bottomLeft-XSvX5 .secondSide-2g7B7 {
                right: 0;
                transform-origin: right
            }

            .bottomRight-1lUOm {
                bottom: 0
            }

            [dir=ltr] .bottomRight-1lUOm {
                right: 0
            }

            [dir=rtl] .bottomRight-1lUOm {
                left: 0
            }

            .bottomRight-1lUOm .firstSide-2PgwT {
                top: 0
            }

            [dir] .bottomRight-1lUOm .firstSide-2PgwT {
                transform-origin: bottom
            }

            [dir=ltr] .bottomRight-1lUOm .firstSide-2PgwT {
                right: 0
            }

            [dir=rtl] .bottomRight-1lUOm .firstSide-2PgwT {
                left: 0
            }

            .bottomRight-1lUOm .dashDetail-26mBt {
                bottom: -.45rem
            }

            [dir=ltr] .bottomRight-1lUOm .dashDetail-26mBt {
                right: 0;
                transform-origin: right
            }

            [dir=rtl] .bottomRight-1lUOm .dashDetail-26mBt {
                left: 0;
                transform-origin: left
            }

            .bottomRight-1lUOm .secondSide-2g7B7 {
                bottom: 0
            }

            [dir=ltr] .bottomRight-1lUOm .secondSide-2g7B7 {
                right: 0;
                transform-origin: right
            }

            [dir=rtl] .bottomRight-1lUOm .secondSide-2g7B7 {
                left: 0;
                transform-origin: left
            }

            .border-ojuPz {
                height: 72%;
                width: 75.6%;
                pointer-events: none
            }

            .border-ojuPz, .border-ojuPz .borderLine-24GpQ, .border-ojuPz .dashDetail-26mBt {
                position: absolute
            }

            .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt {
                height: .45rem;
                width: 22%
            }

            [dir] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt {
                background: #fff
            }

            .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
                border-bottom: .45rem solid #fff
            }

            [dir=ltr] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.35rem
            }

            .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt {
                height: .45rem;
                width: 22%
            }

            [dir] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt {
                background: #fff
            }

            .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
                border-bottom: .45rem solid #fff
            }

            [dir=ltr] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.35rem
            }

            .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt {
                height: .45rem;
                width: 22%
            }

            [dir] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt {
                background: #fff
            }

            .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                border-top: .45rem solid #fff
            }

            [dir=ltr] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.35rem
            }

            .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt {
                height: .45rem;
                width: 22%
            }

            [dir] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt {
                background: #fff
            }

            .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
                border-top: .45rem solid #fff
            }

            [dir=ltr] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.35rem
            }

            [dir] .border-ojuPz.light-3sJ1f .borderLine-24GpQ {
                background-color: #fff
            }

            .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt {
                height: .45rem;
                width: 22%
            }

            [dir] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt {
                background: #141e37
            }

            .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
                border-bottom: .45rem solid #141e37
            }

            [dir=ltr] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.35rem
            }

            .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt {
                height: .45rem;
                width: 22%
            }

            [dir] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt {
                background: #141e37
            }

            .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
                border-bottom: .45rem solid #141e37
            }

            [dir=ltr] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.35rem
            }

            .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt {
                height: .45rem;
                width: 22%
            }

            [dir] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt {
                background: #141e37
            }

            .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                border-top: .45rem solid #141e37
            }

            [dir=ltr] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.35rem
            }

            .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt {
                height: .45rem;
                width: 22%
            }

            [dir] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt {
                background: #141e37
            }

            .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
                border-top: .45rem solid #141e37
            }

            [dir=ltr] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.35rem
            }

            [dir] .border-ojuPz.dark-2F1bx .borderLine-24GpQ {
                background-color: #141e37
            }

            .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt {
                height: .45rem;
                width: 22%
            }

            [dir] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt {
                background: #32c8ff
            }

            .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
                border-bottom: .45rem solid #32c8ff
            }

            [dir=ltr] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.35rem
            }

            .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt {
                height: .45rem;
                width: 22%
            }

            [dir] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt {
                background: #32c8ff
            }

            .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
                border-bottom: .45rem solid #32c8ff
            }

            [dir=ltr] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.35rem
            }

            .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt {
                height: .45rem;
                width: 22%
            }

            [dir] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt {
                background: #32c8ff
            }

            .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                border-top: .45rem solid #32c8ff
            }

            [dir=ltr] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.35rem
            }

            .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt {
                height: .45rem;
                width: 22%
            }

            [dir] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt {
                background: #32c8ff
            }

            .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
                border-top: .45rem solid #32c8ff
            }

            [dir=ltr] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.35rem
            }

            [dir] .border-ojuPz.electric-3FAPB .borderLine-24GpQ {
                background-color: #32c8ff
            }
        }

        @media (min-width: 1024px) {
            .firstSide-2PgwT {
                width: .3rem;
                height: 100%
            }

            .secondSide-2g7B7 {
                width: 100%;
                height: .3rem
            }

            .topRight-2gfP1 {
                top: 0
            }

            [dir=ltr] .topRight-2gfP1 {
                right: 0
            }

            [dir=rtl] .topRight-2gfP1 {
                left: 0
            }

            .topRight-2gfP1 .firstSide-2PgwT {
                bottom: 0
            }

            [dir] .topRight-2gfP1 .firstSide-2PgwT {
                transform-origin: top
            }

            [dir=ltr] .topRight-2gfP1 .firstSide-2PgwT {
                right: 0
            }

            [dir=rtl] .topRight-2gfP1 .firstSide-2PgwT {
                left: 0
            }

            .topRight-2gfP1 .dashDetail-26mBt {
                top: -.45rem
            }

            [dir=ltr] .topRight-2gfP1 .dashDetail-26mBt {
                right: 0;
                transform-origin: right
            }

            [dir=rtl] .topRight-2gfP1 .dashDetail-26mBt {
                left: 0;
                transform-origin: left
            }

            [dir=ltr] .topRight-2gfP1 .secondSide-2g7B7 {
                right: 0;
                transform-origin: right
            }

            [dir=rtl] .topRight-2gfP1 .secondSide-2g7B7 {
                left: 0;
                transform-origin: left
            }

            .topLeft-2ypaU {
                top: 0
            }

            [dir=ltr] .topLeft-2ypaU {
                left: 0
            }

            [dir=rtl] .topLeft-2ypaU {
                right: 0
            }

            .topLeft-2ypaU .firstSide-2PgwT {
                bottom: 0
            }

            [dir] .topLeft-2ypaU .firstSide-2PgwT {
                transform-origin: top
            }

            [dir=ltr] .topLeft-2ypaU .firstSide-2PgwT {
                left: 0
            }

            [dir=rtl] .topLeft-2ypaU .firstSide-2PgwT {
                right: 0
            }

            .topLeft-2ypaU .dashDetail-26mBt {
                top: -.45rem
            }

            [dir=ltr] .topLeft-2ypaU .dashDetail-26mBt {
                left: 0;
                transform-origin: left
            }

            [dir=rtl] .topLeft-2ypaU .dashDetail-26mBt {
                right: 0;
                transform-origin: right
            }

            [dir=ltr] .topLeft-2ypaU .secondSide-2g7B7 {
                left: 0;
                transform-origin: left
            }

            [dir=rtl] .topLeft-2ypaU .secondSide-2g7B7 {
                right: 0;
                transform-origin: right
            }

            .bottomLeft-XSvX5 {
                bottom: 0
            }

            [dir=ltr] .bottomLeft-XSvX5 {
                left: 0
            }

            [dir=rtl] .bottomLeft-XSvX5 {
                right: 0
            }

            .bottomLeft-XSvX5 .firstSide-2PgwT {
                top: 0
            }

            [dir] .bottomLeft-XSvX5 .firstSide-2PgwT {
                transform-origin: top
            }

            [dir=ltr] .bottomLeft-XSvX5 .firstSide-2PgwT {
                left: 0
            }

            [dir=rtl] .bottomLeft-XSvX5 .firstSide-2PgwT {
                right: 0
            }

            .bottomLeft-XSvX5 .dashDetail-26mBt {
                bottom: -.45rem
            }

            [dir=ltr] .bottomLeft-XSvX5 .dashDetail-26mBt {
                left: 0;
                transform-origin: left
            }

            [dir=rtl] .bottomLeft-XSvX5 .dashDetail-26mBt {
                right: 0;
                transform-origin: right
            }

            .bottomLeft-XSvX5 .secondSide-2g7B7 {
                bottom: 0
            }

            [dir=ltr] .bottomLeft-XSvX5 .secondSide-2g7B7 {
                left: 0;
                transform-origin: left
            }

            [dir=rtl] .bottomLeft-XSvX5 .secondSide-2g7B7 {
                right: 0;
                transform-origin: right
            }

            .bottomRight-1lUOm {
                bottom: 0
            }

            [dir=ltr] .bottomRight-1lUOm {
                right: 0
            }

            [dir=rtl] .bottomRight-1lUOm {
                left: 0
            }

            .bottomRight-1lUOm .firstSide-2PgwT {
                top: 0
            }

            [dir] .bottomRight-1lUOm .firstSide-2PgwT {
                transform-origin: bottom
            }

            [dir=ltr] .bottomRight-1lUOm .firstSide-2PgwT {
                right: 0
            }

            [dir=rtl] .bottomRight-1lUOm .firstSide-2PgwT {
                left: 0
            }

            .bottomRight-1lUOm .dashDetail-26mBt {
                bottom: -.45rem
            }

            [dir=ltr] .bottomRight-1lUOm .dashDetail-26mBt {
                right: 0;
                transform-origin: right
            }

            [dir=rtl] .bottomRight-1lUOm .dashDetail-26mBt {
                left: 0;
                transform-origin: left
            }

            .bottomRight-1lUOm .secondSide-2g7B7 {
                bottom: 0
            }

            [dir=ltr] .bottomRight-1lUOm .secondSide-2g7B7 {
                right: 0;
                transform-origin: right
            }

            [dir=rtl] .bottomRight-1lUOm .secondSide-2g7B7 {
                left: 0;
                transform-origin: left
            }

            .border-ojuPz {
                height: 72%;
                width: 75.6%;
                pointer-events: none
            }

            .border-ojuPz, .border-ojuPz .borderLine-24GpQ, .border-ojuPz .dashDetail-26mBt {
                position: absolute
            }

            .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt {
                height: .45rem;
                width: 28%
            }

            [dir] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt {
                background: #fff
            }

            .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
                border-bottom: .45rem solid #fff
            }

            [dir=ltr] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.light-3sJ1f.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.35rem
            }

            .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt {
                height: .45rem;
                width: 28%
            }

            [dir] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt {
                background: #fff
            }

            .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
                border-bottom: .45rem solid #fff
            }

            [dir=ltr] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.light-3sJ1f.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.35rem
            }

            .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt {
                height: .45rem;
                width: 28%
            }

            [dir] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt {
                background: #fff
            }

            .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                border-top: .45rem solid #fff
            }

            [dir=ltr] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.light-3sJ1f.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.35rem
            }

            .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt {
                height: .45rem;
                width: 28%
            }

            [dir] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt {
                background: #fff
            }

            .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
                border-top: .45rem solid #fff
            }

            [dir=ltr] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.light-3sJ1f.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.35rem
            }

            [dir] .border-ojuPz.light-3sJ1f .borderLine-24GpQ {
                background-color: #fff
            }

            .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt {
                height: .45rem;
                width: 28%
            }

            [dir] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt {
                background: #141e37
            }

            .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
                border-bottom: .45rem solid #141e37
            }

            [dir=ltr] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.dark-2F1bx.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.35rem
            }

            .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt {
                height: .45rem;
                width: 28%
            }

            [dir] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt {
                background: #141e37
            }

            .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
                border-bottom: .45rem solid #141e37
            }

            [dir=ltr] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.dark-2F1bx.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.35rem
            }

            .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt {
                height: .45rem;
                width: 28%
            }

            [dir] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt {
                background: #141e37
            }

            .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                border-top: .45rem solid #141e37
            }

            [dir=ltr] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.dark-2F1bx.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.35rem
            }

            .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt {
                height: .45rem;
                width: 28%
            }

            [dir] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt {
                background: #141e37
            }

            .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
                border-top: .45rem solid #141e37
            }

            [dir=ltr] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.dark-2F1bx.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.35rem
            }

            [dir] .border-ojuPz.dark-2F1bx .borderLine-24GpQ {
                background-color: #141e37
            }

            .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt {
                height: .45rem;
                width: 28%
            }

            [dir] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt {
                background: #32c8ff
            }

            .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
                border-bottom: .45rem solid #32c8ff
            }

            [dir=ltr] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.electric-3FAPB.topRight-2gfP1 .dashDetail-26mBt:after {
                right: -.35rem
            }

            .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt {
                height: .45rem;
                width: 28%
            }

            [dir] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt {
                background: #32c8ff
            }

            .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
                border-bottom: .45rem solid #32c8ff
            }

            [dir=ltr] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.electric-3FAPB.topLeft-2ypaU .dashDetail-26mBt:after {
                left: -.35rem
            }

            .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt {
                height: .45rem;
                width: 28%
            }

            [dir] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt {
                background: #32c8ff
            }

            .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                border-top: .45rem solid #32c8ff
            }

            [dir=ltr] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.electric-3FAPB.bottomLeft-XSvX5 .dashDetail-26mBt:after {
                left: -.35rem
            }

            .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt {
                height: .45rem;
                width: 28%
            }

            [dir] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt {
                background: #32c8ff
            }

            .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
                border-top: .45rem solid #32c8ff
            }

            [dir=ltr] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.45rem;
                border-left: .45rem solid transparent
            }

            [dir=rtl] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.45rem;
                border-right: .45rem solid transparent
            }

            html[dir=ltr][browser=Safari] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Safari] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.35rem
            }

            html[dir=ltr][browser=Firefox] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
                left: -.35rem
            }

            html[dir=rtl][browser=Firefox] .border-ojuPz.electric-3FAPB.bottomRight-1lUOm .dashDetail-26mBt:after {
                right: -.35rem
            }

            [dir] .border-ojuPz.electric-3FAPB .borderLine-24GpQ {
                background-color: #32c8ff
            }
        }

        .youtubeBlock-1Q62L {
            position: relative
        }

        [dir] .youtubeBlock-1Q62L {
            margin-bottom: 3.5rem
        }

        .videoContainer-3153g {
            position: relative
        }

        .videoContainer-3153g:before {
            content: "";
            display: block
        }

        [dir] .videoContainer-3153g:before {
            padding-top: 56.25%
        }

        .videoContainer-3153g > iframe {
            position: absolute;
            top: 0
        }

        [dir=ltr] .videoContainer-3153g > iframe {
            left: 0
        }

        [dir=rtl] .videoContainer-3153g > iframe {
            right: 0
        }

        .videoBorder-2yH8I {
            position: absolute;
            bottom: -.6rem
        }

        [dir=ltr] .videoBorder-2yH8I {
            right: -.6rem
        }

        [dir=rtl] .videoBorder-2yH8I {
            left: -.6rem
        }

        @media (min-width: 768px) {
            [dir] .youtubeBlock-1Q62L {
                margin-bottom: 3.5rem
            }

            .videoBorder-2yH8I {
                bottom: -1.2rem
            }

            [dir=ltr] .videoBorder-2yH8I {
                right: -1.2rem
            }

            [dir=rtl] .videoBorder-2yH8I {
                left: -1.2rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .youtubeBlock-1Q62L {
                margin-top: 4rem;
                margin-bottom: 6rem
            }

            .videoBorder-2yH8I {
                bottom: -2rem
            }

            [dir=ltr] .videoBorder-2yH8I {
                right: -2rem
            }

            [dir=rtl] .videoBorder-2yH8I {
                left: -2rem
            }
        }

        .authorCard-90o_T {
            position: relative;
            color: #fff
        }

        .authorCard-90o_T .dashDetail-1dWYF {
            height: .4rem;
            width: 4.25rem;
            position: absolute;
            top: -.4rem
        }

        [dir] .authorCard-90o_T .dashDetail-1dWYF {
            background: #141e37
        }

        [dir=ltr] .authorCard-90o_T .dashDetail-1dWYF {
            left: 0
        }

        [dir=rtl] .authorCard-90o_T .dashDetail-1dWYF {
            right: 0
        }

        .authorCard-90o_T .dashDetail-1dWYF:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .authorCard-90o_T .dashDetail-1dWYF:after {
            border-bottom: .4rem solid #141e37
        }

        [dir=ltr] .authorCard-90o_T .dashDetail-1dWYF:after {
            right: -.4rem;
            border-right: .4rem solid transparent
        }

        [dir=rtl] .authorCard-90o_T .dashDetail-1dWYF:after {
            left: -.4rem;
            border-left: .4rem solid transparent
        }

        html[dir=ltr][browser=Safari] .authorCard-90o_T .dashDetail-1dWYF:after {
            right: -.3rem
        }

        html[dir=rtl][browser=Safari] .authorCard-90o_T .dashDetail-1dWYF:after {
            left: -.3rem
        }

        html[dir=ltr][browser=Firefox] .authorCard-90o_T .dashDetail-1dWYF:after {
            right: -.3rem
        }

        html[dir=rtl][browser=Firefox] .authorCard-90o_T .dashDetail-1dWYF:after {
            left: -.3rem
        }

        .authorCard-90o_T .contentWrapper-6yqtB {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 1.25rem), calc(100% - 1.5rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 1.25rem), calc(100% - 1.5rem) 100%, 0 100%)
        }

        [dir] .authorCard-90o_T .contentWrapper-6yqtB {
            padding: 2.5rem;
            background-color: #32c8ff
        }

        [dir=rtl] .authorCard-90o_T .contentWrapper-6yqtB {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 1.5rem 100%, 0 calc(100% - 1.25rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 1.5rem 100%, 0 calc(100% - 1.25rem))
        }

        .authorCard-90o_T .headerWrapper-3Brnr {
            display: flex;
            align-items: center
        }

        .authorCard-90o_T .authorInfoWrapper-3TSMN {
            display: flex;
            flex-direction: column
        }

        .authorCard-90o_T .ieAuthorPicture-ZBjfD {
            position: relative;
            width: 5rem
        }

        [dir] .authorCard-90o_T .ieAuthorPicture-ZBjfD {
            background-repeat: no-repeat;
            background-position: 50%;
            background-size: cover;
            border-radius: 100%
        }

        [dir=ltr] .authorCard-90o_T .ieAuthorPicture-ZBjfD {
            margin-left: .25rem;
            margin-right: 1.5rem
        }

        [dir=rtl] .authorCard-90o_T .ieAuthorPicture-ZBjfD {
            margin-right: .25rem;
            margin-left: 1.5rem
        }

        .authorCard-90o_T .ieAuthorPicture-ZBjfD:before {
            content: "";
            display: block;
            width: 100%
        }

        [dir] .authorCard-90o_T .ieAuthorPicture-ZBjfD:before {
            padding-top: 100%
        }

        .authorCard-90o_T .ieAuthorPicture-ZBjfD:after {
            content: "";
            display: block;
            position: absolute;
            height: 100%;
            width: 100%;
            top: 0
        }

        [dir] .authorCard-90o_T .ieAuthorPicture-ZBjfD:after {
            border: .15rem solid #fff;
            border-radius: 100%;
            transform: scale(1.15)
        }

        [dir=ltr] .authorCard-90o_T .ieAuthorPicture-ZBjfD:after {
            left: 0
        }

        [dir=rtl] .authorCard-90o_T .ieAuthorPicture-ZBjfD:after {
            right: 0
        }

        .authorCard-90o_T .authorPictureWrapper-2yWuo {
            position: relative;
            width: 5rem
        }

        [dir] .authorCard-90o_T .authorPictureWrapper-2yWuo {
            border-radius: 100%
        }

        [dir=ltr] .authorCard-90o_T .authorPictureWrapper-2yWuo {
            margin-left: .25rem;
            margin-right: 1.5rem
        }

        [dir=rtl] .authorCard-90o_T .authorPictureWrapper-2yWuo {
            margin-right: .25rem;
            margin-left: 1.5rem
        }

        .authorCard-90o_T .authorPictureWrapper-2yWuo .authorPicture-ThEDv {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0;
            -o-object-fit: cover;
            object-fit: cover;
            -o-object-position: center;
            object-position: center
        }

        [dir] .authorCard-90o_T .authorPictureWrapper-2yWuo .authorPicture-ThEDv {
            border-radius: 100%
        }

        [dir=ltr] .authorCard-90o_T .authorPictureWrapper-2yWuo .authorPicture-ThEDv {
            left: 0
        }

        [dir=rtl] .authorCard-90o_T .authorPictureWrapper-2yWuo .authorPicture-ThEDv {
            right: 0
        }

        .authorCard-90o_T .authorPictureWrapper-2yWuo:before {
            display: block;
            content: "";
            width: 100%
        }

        [dir] .authorCard-90o_T .authorPictureWrapper-2yWuo:before {
            padding-top: 100%
        }

        .authorCard-90o_T .authorPictureWrapper-2yWuo:after {
            content: "";
            display: block;
            position: absolute;
            height: 100%;
            width: 100%;
            top: 0
        }

        [dir] .authorCard-90o_T .authorPictureWrapper-2yWuo:after {
            border: .15rem solid #fff;
            border-radius: 100%;
            transform: scale(1.15)
        }

        [dir=ltr] .authorCard-90o_T .authorPictureWrapper-2yWuo:after {
            left: 0
        }

        [dir=rtl] .authorCard-90o_T .authorPictureWrapper-2yWuo:after {
            right: 0
        }

        [dir] .authorCard-90o_T .authorName-1R472 {
            padding-bottom: 0
        }

        .authorCard-90o_T .authorRole-rMVH- {
            color: #141e37
        }

        [dir] .authorCard-90o_T .description-2Sxfb {
            border-top: 1px solid #fff;
            padding-top: 1.25rem;
            margin-top: 2.5rem
        }

        @media (min-width: 768px) {
            .authorCard-90o_T .dashDetail-1dWYF {
                height: .8rem;
                width: 8.5rem;
                top: -.8rem
            }

            [dir] .authorCard-90o_T .dashDetail-1dWYF {
                background: #141e37
            }

            .authorCard-90o_T .dashDetail-1dWYF:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .authorCard-90o_T .dashDetail-1dWYF:after {
                border-bottom: .8rem solid #141e37
            }

            [dir=ltr] .authorCard-90o_T .dashDetail-1dWYF:after {
                right: -.8rem;
                border-right: .8rem solid transparent
            }

            [dir=rtl] .authorCard-90o_T .dashDetail-1dWYF:after {
                left: -.8rem;
                border-left: .8rem solid transparent
            }

            html[dir=ltr][browser=Safari] .authorCard-90o_T .dashDetail-1dWYF:after {
                right: -.7rem
            }

            html[dir=rtl][browser=Safari] .authorCard-90o_T .dashDetail-1dWYF:after {
                left: -.7rem
            }

            html[dir=ltr][browser=Firefox] .authorCard-90o_T .dashDetail-1dWYF:after {
                right: -.7rem
            }

            html[dir=rtl][browser=Firefox] .authorCard-90o_T .dashDetail-1dWYF:after {
                left: -.7rem
            }

            .authorCard-90o_T .contentWrapper-6yqtB {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 2.5rem), calc(100% - 3rem) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 2.5rem), calc(100% - 3rem) 100%, 0 100%)
            }

            [dir] .authorCard-90o_T .contentWrapper-6yqtB {
                padding: 4rem
            }

            [dir=rtl] .authorCard-90o_T .contentWrapper-6yqtB {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 3rem 100%, 0 calc(100% - 2.5rem));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 3rem 100%, 0 calc(100% - 2.5rem))
            }

            .authorCard-90o_T .authorPictureWrapper-2yWuo, .authorCard-90o_T .ieAuthorPicture-ZBjfD {
                width: 10rem
            }

            [dir=ltr] .authorCard-90o_T .authorPictureWrapper-2yWuo, [dir=ltr] .authorCard-90o_T .ieAuthorPicture-ZBjfD {
                margin-left: .5rem;
                margin-right: 3rem
            }

            [dir=rtl] .authorCard-90o_T .authorPictureWrapper-2yWuo, [dir=rtl] .authorCard-90o_T .ieAuthorPicture-ZBjfD {
                margin-right: .5rem;
                margin-left: 3rem
            }

            [dir] .authorCard-90o_T .authorPictureWrapper-2yWuo:after, [dir] .authorCard-90o_T .ieAuthorPicture-ZBjfD:after {
                border: .2rem solid #fff;
                transform: scale(1.15)
            }

            [dir] .authorCard-90o_T .authorName-1R472 {
                padding-bottom: .1rem
            }

            [dir] .authorCard-90o_T .description-2Sxfb {
                margin-top: 5rem;
                padding-top: 2.5rem
            }
        }

        @media (min-width: 1024px) {
            .authorCard-90o_T .contentWrapper-6yqtB {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 2rem), calc(100% - 2.5rem) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 2rem), calc(100% - 2.5rem) 100%, 0 100%)
            }

            [dir] .authorCard-90o_T .contentWrapper-6yqtB {
                padding: 3rem
            }

            [dir=rtl] .authorCard-90o_T .contentWrapper-6yqtB {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 2.5rem 100%, 0 calc(100% - 2rem));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 2.5rem 100%, 0 calc(100% - 2rem))
            }

            .authorCard-90o_T .authorPictureWrapper-2yWuo, .authorCard-90o_T .ieAuthorPicture-ZBjfD {
                width: 6.5rem
            }

            [dir=ltr] .authorCard-90o_T .authorPictureWrapper-2yWuo, [dir=ltr] .authorCard-90o_T .ieAuthorPicture-ZBjfD {
                margin-left: .5rem;
                margin-right: 2.7rem
            }

            [dir=rtl] .authorCard-90o_T .authorPictureWrapper-2yWuo, [dir=rtl] .authorCard-90o_T .ieAuthorPicture-ZBjfD {
                margin-right: .5rem;
                margin-left: 2.7rem
            }

            [dir] .authorCard-90o_T .authorPictureWrapper-2yWuo:after, [dir] .authorCard-90o_T .ieAuthorPicture-ZBjfD:after {
                border: .2rem solid #fff;
                transform: scale(1.15)
            }

            [dir] .authorCard-90o_T .authorName-1R472 {
                padding-bottom: .1rem
            }

            [dir] .authorCard-90o_T .description-2Sxfb {
                margin-top: 3.5rem;
                padding-top: 2rem
            }

            [dir=ltr] .authorCard-90o_T .description-2Sxfb {
                padding-right: 7rem
            }

            [dir=rtl] .authorCard-90o_T .description-2Sxfb {
                padding-left: 7rem
            }
        }

        [dir] .authorBlockList-2ogWj {
            margin: 3rem 0 3.5rem
        }

        [dir] .authorBlockList-2ogWj .authorBlockItem--50ZV:not(:last-child) {
            margin-bottom: 3rem
        }

        @media (min-width: 768px) {
            [dir] .authorBlockList-2ogWj {
                margin: 6rem 0 7rem
            }

            [dir] .authorBlockList-2ogWj .authorBlockItem--50ZV:not(:last-child) {
                margin-bottom: 6rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .authorBlockList-2ogWj {
                margin: 5rem 0 6rem
            }

            [dir] .authorBlockList-2ogWj .authorBlockItem--50ZV:not(:last-child) {
                margin-bottom: 5rem
            }
        }

        .articleFooter-1QQgu {
            z-index: 4
        }

        .articleFooter-1QQgu .social-3mv6z, .articleFooter-1QQgu .tags-11UzK {
            display: flex;
            align-items: center
        }

        .articleFooter-1QQgu .socialList-3TIT1 {
            width: 100%
        }

        .articleFooter-1QQgu .socialList-3TIT1, .articleFooter-1QQgu .tagsList-1g69D {
            display: flex;
            align-items: center
        }

        .articleFooter-1QQgu .socialIcon-Q44CH {
            height: 100%;
            width: 100%
        }

        .articleFooter-1QQgu .label-3p8mc {
            min-width: 4.5rem;
            flex-shrink: 0
        }

        [dir=ltr] .articleFooter-1QQgu .label-3p8mc {
            margin-right: .5rem
        }

        [dir=rtl] .articleFooter-1QQgu .label-3p8mc {
            margin-left: .5rem
        }

        .articleFooter-1QQgu .socialIconWrapper-2_Qd5 {
            height: 2.25rem;
            width: 2.25rem
        }

        [dir] .articleFooter-1QQgu .socialIconWrapper-2_Qd5 {
            cursor: pointer
        }

        .articleFooter-1QQgu .socialIconItem-Y1a7D {
            height: auto;
            width: auto
        }

        [dir=ltr] .articleFooter-1QQgu .socialIconItem-Y1a7D:not(:last-child) {
            margin-right: 12%
        }

        [dir=rtl] .articleFooter-1QQgu .socialIconItem-Y1a7D:not(:last-child) {
            margin-left: 12%
        }

        .articleFooter-1QQgu .tagsList-1g69D {
            flex-wrap: wrap
        }

        .articleFooter-1QQgu .tagItem-3rWw0 {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - .7rem), calc(100% - .7rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - .7rem), calc(100% - .7rem) 100%, 0 100%);
            position: relative;
            overflow: hidden;
            color: #fff;
            flex-shrink: 0
        }

        [dir] .articleFooter-1QQgu .tagItem-3rWw0 {
            background-color: #141e37;
            margin-bottom: 1.25rem
        }

        [dir=rtl] .articleFooter-1QQgu .tagItem-3rWw0 {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, .7rem 100%, 0 calc(100% - .7rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, .7rem 100%, 0 calc(100% - .7rem))
        }

        [dir=ltr] .articleFooter-1QQgu .tagItem-3rWw0:not(:first-child) {
            margin-left: 2rem
        }

        [dir=rtl] .articleFooter-1QQgu .tagItem-3rWw0:not(:first-child) {
            margin-right: 2rem
        }

        html[browser="Internet Explorer"] .articleFooter-1QQgu .tagItem-3rWw0:before {
            content: "";
            position: absolute;
            top: calc(100% - 1rem);
            width: 2rem;
            height: 2rem
        }

        html[dir][browser="Internet Explorer"] .articleFooter-1QQgu .tagItem-3rWw0:before {
            background-color: #fff
        }

        html[dir=ltr][browser="Internet Explorer"] .articleFooter-1QQgu .tagItem-3rWw0:before {
            left: calc(100% - 1rem);
            transform: rotate(45deg)
        }

        html[dir=rtl][browser="Internet Explorer"] .articleFooter-1QQgu .tagItem-3rWw0:before {
            right: calc(100% - 1rem);
            transform: rotate(-45deg)
        }

        [dir] .articleFooter-1QQgu .tagCopy-3gODe {
            padding: .6rem 1rem
        }

        [dir] .articleFooter-1QQgu .tags-11UzK .label-3p8mc {
            margin-bottom: 1.25rem
        }

        .articleFooter-1QQgu .separator-2vtc_ {
            height: .2rem;
            width: 100%;
            position: relative;
            opacity: .2
        }

        [dir] .articleFooter-1QQgu .separator-2vtc_ {
            margin: 2.5rem 0;
            background-color: #111
        }

        .articleFooter-1QQgu .dashDetail-hJuAR {
            position: absolute;
            bottom: -.3rem;
            height: .3rem;
            width: 18.25%
        }

        [dir] .articleFooter-1QQgu .dashDetail-hJuAR {
            background: #111
        }

        [dir=ltr] .articleFooter-1QQgu .dashDetail-hJuAR {
            right: 0
        }

        [dir=rtl] .articleFooter-1QQgu .dashDetail-hJuAR {
            left: 0
        }

        .articleFooter-1QQgu .dashDetail-hJuAR:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .articleFooter-1QQgu .dashDetail-hJuAR:after {
            border-top: .3rem solid #111
        }

        [dir=ltr] .articleFooter-1QQgu .dashDetail-hJuAR:after {
            left: -.3rem;
            border-left: .3rem solid transparent
        }

        [dir=rtl] .articleFooter-1QQgu .dashDetail-hJuAR:after {
            right: -.3rem;
            border-right: .3rem solid transparent
        }

        html[dir=ltr][browser=Safari] .articleFooter-1QQgu .dashDetail-hJuAR:after {
            left: -.2rem
        }

        html[dir=rtl][browser=Safari] .articleFooter-1QQgu .dashDetail-hJuAR:after {
            right: -.2rem
        }

        html[dir=ltr][browser=Firefox] .articleFooter-1QQgu .dashDetail-hJuAR:after {
            left: -.2rem
        }

        html[dir=rtl][browser=Firefox] .articleFooter-1QQgu .dashDetail-hJuAR:after {
            right: -.2rem
        }

        @media (min-width: 480px) {
            [dir=ltr] .articleFooter-1QQgu .socialIconItem-Y1a7D:not(:last-child) {
                margin-right: 4.5rem
            }

            [dir=rtl] .articleFooter-1QQgu .socialIconItem-Y1a7D:not(:last-child) {
                margin-left: 4.5rem
            }
        }

        @media (min-width: 768px) {
            [dir=ltr] .articleFooter-1QQgu .socialIconItem-Y1a7D:not(:last-child) {
                margin-right: 9rem
            }

            [dir=rtl] .articleFooter-1QQgu .socialIconItem-Y1a7D:not(:last-child) {
                margin-left: 9rem
            }

            .articleFooter-1QQgu .label-3p8mc {
                min-width: 9rem
            }

            [dir=ltr] .articleFooter-1QQgu .label-3p8mc {
                margin-right: 1rem
            }

            [dir=rtl] .articleFooter-1QQgu .label-3p8mc {
                margin-left: 1rem
            }

            .articleFooter-1QQgu .socialIconWrapper-2_Qd5 {
                height: 4.5rem;
                width: 4.5rem
            }

            [dir] .articleFooter-1QQgu .separator-2vtc_ {
                margin: 5rem 0
            }

            .articleFooter-1QQgu .dashDetail-hJuAR {
                bottom: -.6rem;
                height: .6rem;
                width: 18.25%
            }

            [dir] .articleFooter-1QQgu .dashDetail-hJuAR {
                background: #111
            }

            .articleFooter-1QQgu .dashDetail-hJuAR:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .articleFooter-1QQgu .dashDetail-hJuAR:after {
                border-top: .6rem solid #111
            }

            [dir=ltr] .articleFooter-1QQgu .dashDetail-hJuAR:after {
                left: -.6rem;
                border-left: .6rem solid transparent
            }

            [dir=rtl] .articleFooter-1QQgu .dashDetail-hJuAR:after {
                right: -.6rem;
                border-right: .6rem solid transparent
            }

            html[dir=ltr][browser=Safari] .articleFooter-1QQgu .dashDetail-hJuAR:after {
                left: -.5rem
            }

            html[dir=rtl][browser=Safari] .articleFooter-1QQgu .dashDetail-hJuAR:after {
                right: -.5rem
            }

            html[dir=ltr][browser=Firefox] .articleFooter-1QQgu .dashDetail-hJuAR:after {
                left: -.5rem
            }

            html[dir=rtl][browser=Firefox] .articleFooter-1QQgu .dashDetail-hJuAR:after {
                right: -.5rem
            }

            .articleFooter-1QQgu .tagItem-3rWw0 {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 1.4rem), calc(100% - 1.4rem) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 1.4rem), calc(100% - 1.4rem) 100%, 0 100%)
            }

            [dir] .articleFooter-1QQgu .tagItem-3rWw0 {
                margin-bottom: 2.5rem
            }

            [dir=rtl] .articleFooter-1QQgu .tagItem-3rWw0 {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 1.4rem 100%, 0 calc(100% - 1.4rem));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 1.4rem 100%, 0 calc(100% - 1.4rem))
            }

            [dir=ltr] .articleFooter-1QQgu .tagItem-3rWw0:not(:first-child) {
                margin-left: 4rem
            }

            [dir=rtl] .articleFooter-1QQgu .tagItem-3rWw0:not(:first-child) {
                margin-right: 4rem
            }

            [dir] .articleFooter-1QQgu .tagCopy-3gODe {
                padding: 1rem 2rem
            }

            [dir] .articleFooter-1QQgu .tags-11UzK .label-3p8mc {
                margin-bottom: 2.5rem
            }
        }

        @media (min-width: 1024px) {
            .articleFooter-1QQgu .socialIconWrapper-2_Qd5 {
                height: 2rem;
                width: 2rem
            }

            .articleFooter-1QQgu .socialIcon-Q44CH {
                transition: transform .2s ease-out
            }

            [dir] .articleFooter-1QQgu .socialIcon-Q44CH {
                transform-origin: center;
                transform: scale(1)
            }

            .articleFooter-1QQgu .socialIcon-Q44CH path {
                transition: fill .2s ease-out
            }

            [dir=ltr] .articleFooter-1QQgu .socialIconItem-Y1a7D:not(:last-child) {
                margin-right: 8rem
            }

            [dir=rtl] .articleFooter-1QQgu .socialIconItem-Y1a7D:not(:last-child) {
                margin-left: 8rem
            }

            [dir] .articleFooter-1QQgu .socialIconItem-Y1a7D:hover .socialIcon-Q44CH {
                transform: scale(1.2)
            }

            .articleFooter-1QQgu .socialIconItem-Y1a7D:hover .socialIcon-Q44CH path {
                fill: #32c8ff
            }

            [dir] .articleFooter-1QQgu .separator-2vtc_ {
                margin: 3rem 0
            }

            [dir=ltr] .articleFooter-1QQgu .tagItem-3rWw0:not(:first-child) {
                margin-left: 2.5rem
            }

            [dir=rtl] .articleFooter-1QQgu .tagItem-3rWw0:not(:first-child) {
                margin-right: 2.5rem
            }
        }

        .category-3HjRL {
            display: inline-block;
            color: #32c8ff
        }

        [dir=ltr] .category-3HjRL {
            margin-left: 1.5rem
        }

        [dir=rtl] .category-3HjRL {
            margin-right: 1.5rem
        }

        .category-3HjRL:before {
            content: "\B7";
            font-size: 2rem;
            color: #111
        }

        [dir=ltr] .category-3HjRL:before {
            margin-right: 1.5rem
        }

        [dir=rtl] .category-3HjRL:before {
            margin-left: 1.5rem
        }

        .title-x8C3s {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            text-transform: uppercase;
            line-height: .87;
            font-size: 4.5rem;
            color: #141e37
        }

        [dir] .title-x8C3s {
            margin: 2rem 0
        }

        html[lang=th-th] .title-x8C3s {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-weight: 700;
            line-height: 1.4;
            font-style: normal
        }

        html[lang=ar-ae] .title-x8C3s {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal;
            line-height: 1.15
        }

        html[lang=ru-ru] .title-x8C3s {
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif
        }

        html[lang=tr-tr] .title-x8C3s {
            line-height: 1.1
        }

        html[lang=ko-kr] .title-x8C3s {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        html[lang=vi-vn] .title-x8C3s {
            font-family: RobotoCondensed-Bold, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            letter-spacing: 1px;
            line-height: 1.18
        }

        html[lang=zh-hk] .title-x8C3s, html[lang=zh-tw] .title-x8C3s {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-style: normal;
            font-weight: 500;
            line-height: 1.1
        }

        html[lang=ja-jp] .title-x8C3s {
            font-family: BeaufortforLOLJa-Regular, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        [dir] .description-25bAm {
            margin-bottom: 2rem
        }

        .authorsWrapper-2skCD {
            display: flex;
            flex-direction: column
        }

        .authorsList-1LbtA {
            display: inline-flex
        }

        .authorsList-1LbtA .authorName-21UP6:not(:last-child):after {
            content: ", ";
            white-space: pre
        }

        .authors-39xGe {
            text-transform: uppercase;
            display: block;
            font-size: 1.6rem;
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-style: italic;
            color: #32c8ff
        }

        html[lang=th-th] .authors-39xGe {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-weight: 700;
            line-height: 1.4;
            font-style: normal
        }

        html[lang=ar-ae] .authors-39xGe {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal;
            line-height: 1.15
        }

        html[lang=ru-ru] .authors-39xGe {
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif
        }

        html[lang=tr-tr] .authors-39xGe {
            line-height: 1.1
        }

        html[lang=ko-kr] .authors-39xGe {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        html[lang=vi-vn] .authors-39xGe {
            font-family: RobotoCondensed-Bold, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            letter-spacing: 1px;
            line-height: 1.18
        }

        html[lang=zh-hk] .authors-39xGe, html[lang=zh-tw] .authors-39xGe {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-style: normal;
            font-weight: 500;
            line-height: 1.1
        }

        html[lang=ja-jp] .authors-39xGe {
            font-family: BeaufortforLOLJa-Regular, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        html[dir=ltr][browser="Internet Explorer"] .authors-39xGe {
            float: left
        }

        html[dir=rtl][browser="Internet Explorer"] .authors-39xGe {
            float: right
        }

        @media (min-width: 1024px) {
            .title-x8C3s {
                font-size: 8rem
            }

            [dir] .title-x8C3s {
                margin-top: 2.5rem
            }

            [dir] .description-25bAm, [dir] .title-x8C3s {
                margin-bottom: 2.5rem
            }

            .authorsWrapper-2skCD {
                flex-direction: row;
                align-items: flex-end
            }

            .authors-39xGe {
                display: initial;
                font-size: 1.9rem
            }

            [dir=ltr] .authors-39xGe {
                margin-right: 1.6rem
            }

            [dir=rtl] .authors-39xGe {
                margin-left: 1.6rem
            }
        }

        .articleHero-2RqIB {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 3rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 3rem) 100%, 0 100%);
            position: relative;
            overflow: hidden;
            height: 30.5rem
        }

        [dir=rtl] .articleHero-2RqIB {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 3rem 100%, 0 calc(100% - 3rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 3rem 100%, 0 calc(100% - 3rem))
        }

        .ieBackgroundImage-2HF4b {
            height: 100%;
            width: 100%
        }

        [dir] .ieBackgroundImage-2HF4b {
            background-repeat: no-repeat;
            background-size: cover
        }

        .backgroundarticleHero-2iC8e {
            height: 100%;
            width: 100%
        }

        .backgroundImage-3S4Xv {
            height: 100%;
            width: 100%;
            -o-object-fit: cover;
            object-fit: cover
        }

        .backButton-3JYqx {
            position: absolute;
            top: 3rem
        }

        [dir=ltr] .backButton-3JYqx {
            left: -1rem
        }

        [dir=rtl] .backButton-3JYqx {
            right: -1rem
        }

        @media (min-width: 768px) {
            .articleHero-2RqIB {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 6rem), calc(100% - 6rem) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 6rem), calc(100% - 6rem) 100%, 0 100%);
                height: 61rem
            }

            [dir=rtl] .articleHero-2RqIB {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 6rem 100%, 0 calc(100% - 6rem));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 6rem 100%, 0 calc(100% - 6rem))
            }

            .backButton-3JYqx {
                top: 6rem
            }

            [dir=ltr] .backButton-3JYqx {
                left: -1.3rem
            }

            [dir=rtl] .backButton-3JYqx {
                right: -1.3rem
            }
        }

        @media (min-width: 1024px) {
            .articleHero-2RqIB {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 8rem), calc(100% - 7rem) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 8rem), calc(100% - 7rem) 100%, 0 100%);
                height: auto
            }

            [dir=rtl] .articleHero-2RqIB {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 7rem 100%, 0 calc(100% - 8rem));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 7rem 100%, 0 calc(100% - 8rem))
            }

            .articleHero-2RqIB:before {
                content: "";
                display: block;
                width: 100%
            }

            [dir] .articleHero-2RqIB:before {
                padding-top: 32.3%
            }

            .articleHero-2RqIB .backgroundImageWrapper-1fIdK, .articleHero-2RqIB .ieBackgroundImage-2HF4b {
                height: 100%;
                width: 100%;
                position: absolute;
                top: 0
            }

            [dir=ltr] .articleHero-2RqIB .backgroundImageWrapper-1fIdK, [dir=ltr] .articleHero-2RqIB .ieBackgroundImage-2HF4b {
                left: 0
            }

            [dir=rtl] .articleHero-2RqIB .backgroundImageWrapper-1fIdK, [dir=rtl] .articleHero-2RqIB .ieBackgroundImage-2HF4b {
                right: 0
            }
        }

        .articleBackButton-3zVlz {
            z-index: 1;
            transition: color .2s ease-out, background-color .2s ease-out;
            -webkit-appearance: none
        }

        [dir] .articleBackButton-3zVlz {
            padding: 2rem
        }

        [dir=ltr] .articleBackButton-3zVlz {
            transform: skewX(-15deg)
        }

        [dir=rtl] .articleBackButton-3zVlz {
            transform: skewX(15deg)
        }

        .articleBackButton-3zVlz.disabled-3Tlrs {
            opacity: .7
        }

        .buttonContent-3uIAE {
            display: flex;
            justify-content: center;
            align-items: center
        }

        [dir=ltr] .buttonContent-3uIAE {
            transform: skewX(15deg)
        }

        [dir=rtl] .buttonContent-3uIAE {
            transform: skewX(-15deg)
        }

        .icon-2APC4 {
            width: .8rem
        }

        [dir] .icon-2APC4 {
            transform: translateX(0) scale(-1)
        }

        [dir=ltr] .icon-2APC4 {
            margin-right: 1rem
        }

        [dir=rtl] .icon-2APC4 {
            margin-left: 1rem;
            transform: translateX(0) scale(1)
        }

        .icon-2APC4 path {
            stroke-width: .8rem;
            transition: stroke .2s ease-out
        }

        html[browser="Internet Explorer"] .icon-2APC4 svg {
            max-height: 3rem
        }

        .default-36iu1 {
            color: #fff
        }

        [dir] .default-36iu1 {
            background-color: #141e37
        }

        .default-36iu1 .icon-2APC4 path {
            stroke: #fff
        }

        .light-3mL5a {
            color: #141e37
        }

        [dir] .light-3mL5a {
            background-color: #fff
        }

        .light-3mL5a .icon-2APC4 path {
            stroke: #141e37
        }

        @media (min-width: 768px) {
            [dir] .articleBackButton-3zVlz {
                padding: 3rem 4rem
            }

            .icon-2APC4 {
                width: 1.2rem
            }

            [dir=ltr] .icon-2APC4 {
                margin-right: 2rem
            }

            [dir=rtl] .icon-2APC4 {
                margin-left: 2rem
            }

            .icon-2APC4 path {
                stroke-width: .7rem
            }
        }

        @media (min-width: 1024px) {
            [dir=ltr] .articleBackButton-3zVlz {
                padding: 2rem 3rem 2rem 4.5rem
            }

            [dir=rtl] .articleBackButton-3zVlz {
                padding: 2rem 4.5rem 2rem 3rem
            }

            .articleBackButton-3zVlz .icon-2APC4 {
                width: .7rem;
                transition: transform .3s ease-out
            }

            [dir=ltr] .articleBackButton-3zVlz .icon-2APC4 {
                margin-left: 1.2rem
            }

            [dir=rtl] .articleBackButton-3zVlz .icon-2APC4 {
                margin-right: 1.2rem
            }

            .articleBackButton-3zVlz .icon-2APC4 path {
                stroke-width: .7rem
            }

            [dir=ltr] .articleBackButton-3zVlz:hover .icon-2APC4 {
                transform: translateX(-.3rem) scale(-1)
            }

            [dir=rtl] .articleBackButton-3zVlz:hover .icon-2APC4 {
                transform: translateX(.3rem) scale(-1);
                transform: translateX(.3rem) scale(1)
            }

            .articleBackButton-3zVlz:hover.default-36iu1 {
                color: #141e37
            }

            [dir] .articleBackButton-3zVlz:hover.default-36iu1 {
                background-color: #fff
            }

            .articleBackButton-3zVlz:hover.default-36iu1 .icon-2APC4 path {
                stroke: #141e37
            }

            .articleBackButton-3zVlz:hover.light-3mL5a {
                color: #fff
            }

            [dir] .articleBackButton-3zVlz:hover.light-3mL5a {
                background-color: #141e37
            }

            .articleBackButton-3zVlz:hover.light-3mL5a .icon-2APC4 path {
                stroke: #fff
            }
        }

        .sectionTitle-2fIox {
            overflow: hidden;
            visibility: hidden
        }

        [dir=ltr] .sectionTitle-2fIox {
            margin-right: -1rem
        }

        [dir=rtl] .sectionTitle-2fIox {
            margin-left: -1rem;
            padding: 1rem 1.2rem
        }

        .sectionTitle-2fIox.dark-1RI-y {
            color: #141e37
        }

        .sectionTitle-2fIox.light-1Cd0M {
            color: #fff
        }

        @media (min-width: 1024px) {
            .sectionTitle-2fIox {
                width: 110%
            }
        }

        .detailLine-1Lbvr {
            position: relative
        }

        .dashDetailWrapper-ppq1R {
            position: absolute;
            top: .5rem;
            height: calc(100% - 1rem);
            opacity: 0
        }

        [dir=ltr] .dashDetailWrapper-ppq1R {
            left: calc(.25rem - 7.5vw)
        }

        [dir=rtl] .dashDetailWrapper-ppq1R {
            right: calc(.25rem - 7.5vw)
        }

        .dashDetailWrapper-ppq1R .dashDetail-33_7u {
            position: absolute;
            top: 0
        }

        [dir] .dashDetailWrapper-ppq1R .dashDetail-33_7u {
            transform-origin: top
        }

        [dir=ltr] .dashDetailWrapper-ppq1R .dashDetail-33_7u {
            right: 0
        }

        [dir=rtl] .dashDetailWrapper-ppq1R .dashDetail-33_7u {
            left: 0
        }

        .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u {
            height: 100%;
            width: .25rem
        }

        [dir] .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u {
            background: #141e37
        }

        .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
            content: "";
            display: block;
            position: absolute;
            bottom: -.25rem
        }

        [dir] .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
            border-top: .25rem solid #141e37
        }

        [dir=ltr] .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
            right: 0;
            border-right: .25rem solid transparent
        }

        [dir=rtl] .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
            left: 0;
            border-left: .25rem solid transparent
        }

        .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u {
            height: 100%;
            width: .25rem
        }

        [dir] .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u {
            background: #fff
        }

        .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
            content: "";
            display: block;
            position: absolute;
            bottom: -.25rem
        }

        [dir] .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
            border-top: .25rem solid #fff
        }

        [dir=ltr] .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
            right: 0;
            border-right: .25rem solid transparent
        }

        [dir=rtl] .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
            left: 0;
            border-left: .25rem solid transparent
        }

        @media (min-width: 768px) {
            .dashDetailWrapper-ppq1R {
                top: 1rem;
                height: calc(100% - 2rem)
            }

            [dir=ltr] .dashDetailWrapper-ppq1R {
                left: calc(.5rem - 7.5vw)
            }

            [dir=rtl] .dashDetailWrapper-ppq1R {
                right: calc(.5rem - 7.5vw)
            }

            .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u {
                height: 100%;
                width: .5rem
            }

            [dir] .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u {
                background: #141e37
            }

            .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                content: "";
                display: block;
                position: absolute;
                bottom: -.5rem
            }

            [dir] .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                border-top: .5rem solid #141e37
            }

            [dir=ltr] .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                right: 0;
                border-right: .5rem solid transparent
            }

            [dir=rtl] .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                left: 0;
                border-left: .5rem solid transparent
            }

            .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u {
                height: 100%;
                width: .5rem
            }

            [dir] .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u {
                background: #fff
            }

            .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                content: "";
                display: block;
                position: absolute;
                bottom: -.5rem
            }

            [dir] .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                border-top: .5rem solid #fff
            }

            [dir=ltr] .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                right: 0;
                border-right: .5rem solid transparent
            }

            [dir=rtl] .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                left: 0;
                border-left: .5rem solid transparent
            }
        }

        @media (min-width: 1024px) {
            .halfTop-3qJHF .dashDetail-33_7u, .top-1jJmO .dashDetail-33_7u {
                position: absolute;
                top: 0
            }

            [dir=ltr] .halfTop-3qJHF .dashDetail-33_7u, [dir=ltr] .top-1jJmO .dashDetail-33_7u {
                left: 0;
                transform-origin: left
            }

            [dir=rtl] .halfTop-3qJHF .dashDetail-33_7u, [dir=rtl] .top-1jJmO .dashDetail-33_7u {
                right: 0;
                transform-origin: right
            }

            [dir] .top-1jJmO {
                padding-top: 2rem
            }

            .top-1jJmO .dashDetailWrapper-ppq1R {
                max-width: calc(40% - 1.8rem);
                width: 100%;
                height: .8rem;
                top: -1rem
            }

            [dir=ltr] .top-1jJmO .dashDetailWrapper-ppq1R {
                left: 0
            }

            [dir=rtl] .top-1jJmO .dashDetailWrapper-ppq1R {
                right: 0
            }

            .top-1jJmO .dashDetailWrapper-ppq1R .borderLine-3aHhe {
                height: .3rem;
                width: 100%;
                position: absolute;
                bottom: 0
            }

            [dir=ltr] .top-1jJmO .dashDetailWrapper-ppq1R .borderLine-3aHhe {
                transform-origin: left;
                left: 0
            }

            [dir=rtl] .top-1jJmO .dashDetailWrapper-ppq1R .borderLine-3aHhe {
                transform-origin: right;
                right: 0
            }

            .top-1jJmO .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u {
                height: .8rem;
                width: 40%
            }

            [dir] .top-1jJmO .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u {
                background: #141e37
            }

            .top-1jJmO .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                top: auto;
                bottom: auto
            }

            [dir] .top-1jJmO .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                border: none
            }

            [dir=ltr] .top-1jJmO .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after, [dir=rtl] .top-1jJmO .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                right: auto;
                left: auto
            }

            .top-1jJmO .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .top-1jJmO .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                border-bottom: .8rem solid #141e37
            }

            [dir=ltr] .top-1jJmO .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                right: -.8rem;
                border-right: .8rem solid transparent
            }

            [dir=rtl] .top-1jJmO .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                left: -.8rem;
                border-left: .8rem solid transparent
            }

            .top-1jJmO .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u {
                height: .8rem;
                width: 40%
            }

            [dir] .top-1jJmO .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u {
                background: #fff
            }

            .top-1jJmO .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                top: auto;
                bottom: auto
            }

            [dir] .top-1jJmO .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                border: none
            }

            [dir=ltr] .top-1jJmO .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after, [dir=rtl] .top-1jJmO .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                right: auto;
                left: auto
            }

            .top-1jJmO .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .top-1jJmO .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                border-bottom: .8rem solid #fff
            }

            [dir=ltr] .top-1jJmO .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                right: -.8rem;
                border-right: .8rem solid transparent
            }

            [dir=rtl] .top-1jJmO .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                left: -.8rem;
                border-left: .8rem solid transparent
            }

            [dir] .halfTop-3qJHF {
                padding-top: 2rem
            }

            .halfTop-3qJHF .dashDetailWrapper-ppq1R {
                max-width: calc(20% - 2.4rem);
                width: 100%;
                height: .8rem;
                top: -1rem
            }

            [dir=ltr] .halfTop-3qJHF .dashDetailWrapper-ppq1R {
                left: 0
            }

            [dir=rtl] .halfTop-3qJHF .dashDetailWrapper-ppq1R {
                right: 0
            }

            .halfTop-3qJHF .dashDetailWrapper-ppq1R .borderLine-3aHhe {
                height: .3rem;
                width: 100%;
                position: absolute;
                bottom: 0
            }

            [dir=ltr] .halfTop-3qJHF .dashDetailWrapper-ppq1R .borderLine-3aHhe {
                transform-origin: left;
                left: 0
            }

            [dir=rtl] .halfTop-3qJHF .dashDetailWrapper-ppq1R .borderLine-3aHhe {
                transform-origin: right;
                right: 0
            }

            .halfTop-3qJHF .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u {
                height: .8rem;
                width: 40%
            }

            [dir] .halfTop-3qJHF .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u {
                background: #141e37
            }

            .halfTop-3qJHF .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                top: auto;
                bottom: auto
            }

            [dir] .halfTop-3qJHF .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                border: none
            }

            [dir=ltr] .halfTop-3qJHF .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after, [dir=rtl] .halfTop-3qJHF .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                right: auto;
                left: auto
            }

            .halfTop-3qJHF .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .halfTop-3qJHF .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                border-bottom: .8rem solid #141e37
            }

            [dir=ltr] .halfTop-3qJHF .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                right: -.8rem;
                border-right: .8rem solid transparent
            }

            [dir=rtl] .halfTop-3qJHF .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                left: -.8rem;
                border-left: .8rem solid transparent
            }

            .halfTop-3qJHF .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u {
                height: .8rem;
                width: 40%
            }

            [dir] .halfTop-3qJHF .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u {
                background: #fff
            }

            .halfTop-3qJHF .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                top: auto;
                bottom: auto
            }

            [dir] .halfTop-3qJHF .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                border: none
            }

            [dir=ltr] .halfTop-3qJHF .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after, [dir=rtl] .halfTop-3qJHF .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                right: auto;
                left: auto
            }

            .halfTop-3qJHF .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .halfTop-3qJHF .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                border-bottom: .8rem solid #fff
            }

            [dir=ltr] .halfTop-3qJHF .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                right: -.8rem;
                border-right: .8rem solid transparent
            }

            [dir=rtl] .halfTop-3qJHF .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                left: -.8rem;
                border-left: .8rem solid transparent
            }

            [dir=ltr] .left-3ehf0 {
                padding-left: 2.5rem
            }

            [dir=rtl] .left-3ehf0 {
                padding-right: 2.5rem
            }

            [dir=ltr] .left-3ehf0 .dashDetailWrapper-ppq1R {
                left: 0
            }

            [dir=rtl] .left-3ehf0 .dashDetailWrapper-ppq1R {
                right: 0
            }

            .left-3ehf0 .dashDetailWrapper-ppq1R .borderLine-3aHhe {
                display: none
            }

            [dir] .left-3ehf0 .dashDetailWrapper-ppq1R .borderLine-3aHhe {
                transform-origin: top
            }

            .left-3ehf0 .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u {
                height: calc(100% - .5rem);
                width: .8rem
            }

            [dir] .left-3ehf0 .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u {
                background: #141e37;
                transform-origin: top
            }

            .left-3ehf0 .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                top: auto;
                bottom: auto
            }

            [dir] .left-3ehf0 .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                border: none
            }

            [dir=ltr] .left-3ehf0 .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after, [dir=rtl] .left-3ehf0 .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                right: auto;
                left: auto
            }

            .left-3ehf0 .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                content: "";
                display: block;
                position: absolute;
                bottom: -.8rem
            }

            [dir] .left-3ehf0 .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                border-top: .8rem solid #141e37
            }

            [dir=ltr] .left-3ehf0 .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                right: 0;
                border-left: .8rem solid transparent
            }

            [dir=rtl] .left-3ehf0 .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                left: 0;
                border-right: .8rem solid transparent
            }

            .left-3ehf0 .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u {
                height: calc(100% - .5rem);
                width: .8rem
            }

            [dir] .left-3ehf0 .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u {
                background: #fff;
                transform-origin: top
            }

            .left-3ehf0 .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                top: auto;
                bottom: auto
            }

            [dir] .left-3ehf0 .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                border: none
            }

            [dir=ltr] .left-3ehf0 .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after, [dir=rtl] .left-3ehf0 .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                right: auto;
                left: auto
            }

            .left-3ehf0 .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                content: "";
                display: block;
                position: absolute;
                bottom: -.8rem
            }

            [dir] .left-3ehf0 .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                border-top: .8rem solid #fff
            }

            [dir=ltr] .left-3ehf0 .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                right: 0;
                border-left: .8rem solid transparent
            }

            [dir=rtl] .left-3ehf0 .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                left: 0;
                border-right: .8rem solid transparent
            }

            [dir=ltr] .halfLeft-1HqDN {
                padding-left: 2.5rem
            }

            [dir=rtl] .halfLeft-1HqDN {
                padding-right: 2.5rem
            }

            [dir=ltr] .halfLeft-1HqDN .dashDetailWrapper-ppq1R {
                left: 0
            }

            [dir=rtl] .halfLeft-1HqDN .dashDetailWrapper-ppq1R {
                right: 0
            }

            .halfLeft-1HqDN .dashDetailWrapper-ppq1R .borderLine-3aHhe {
                height: 100%;
                width: .3rem;
                position: absolute;
                top: 0
            }

            [dir] .halfLeft-1HqDN .dashDetailWrapper-ppq1R .borderLine-3aHhe {
                transform-origin: top
            }

            [dir=ltr] .halfLeft-1HqDN .dashDetailWrapper-ppq1R .borderLine-3aHhe {
                right: 0
            }

            [dir=rtl] .halfLeft-1HqDN .dashDetailWrapper-ppq1R .borderLine-3aHhe {
                left: 0
            }

            .halfLeft-1HqDN .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u {
                height: 40%;
                width: .8rem
            }

            [dir] .halfLeft-1HqDN .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u {
                transform-origin: top;
                background: #141e37
            }

            .halfLeft-1HqDN .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                top: auto;
                bottom: auto
            }

            [dir] .halfLeft-1HqDN .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                border: none
            }

            [dir=ltr] .halfLeft-1HqDN .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after, [dir=rtl] .halfLeft-1HqDN .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                right: auto;
                left: auto
            }

            .halfLeft-1HqDN .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                content: "";
                display: block;
                position: absolute;
                bottom: -.8rem
            }

            [dir] .halfLeft-1HqDN .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                border-top: .8rem solid #141e37
            }

            [dir=ltr] .halfLeft-1HqDN .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                right: 0;
                border-left: .8rem solid transparent
            }

            [dir=rtl] .halfLeft-1HqDN .dashDetailWrapper-ppq1R.dark-2kOE_ .dashDetail-33_7u:after {
                left: 0;
                border-right: .8rem solid transparent
            }

            .halfLeft-1HqDN .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u {
                height: 40%;
                width: .8rem
            }

            [dir] .halfLeft-1HqDN .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u {
                transform-origin: top;
                background: #fff
            }

            .halfLeft-1HqDN .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                top: auto;
                bottom: auto
            }

            [dir] .halfLeft-1HqDN .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                border: none
            }

            [dir=ltr] .halfLeft-1HqDN .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after, [dir=rtl] .halfLeft-1HqDN .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                right: auto;
                left: auto
            }

            .halfLeft-1HqDN .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                content: "";
                display: block;
                position: absolute;
                bottom: -.8rem
            }

            [dir] .halfLeft-1HqDN .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                border-top: .8rem solid #fff
            }

            [dir=ltr] .halfLeft-1HqDN .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                right: 0;
                border-left: .8rem solid transparent
            }

            [dir=rtl] .halfLeft-1HqDN .dashDetailWrapper-ppq1R.light-UQWdL .dashDetail-33_7u:after {
                left: 0;
                border-right: .8rem solid transparent
            }

            [dir] .detailLine-1Lbvr .dashDetailWrapper-ppq1R.dark-2kOE_ .borderLine-3aHhe {
                background-color: #141e37
            }

            [dir] .detailLine-1Lbvr .dashDetailWrapper-ppq1R.light-UQWdL .borderLine-3aHhe {
                background-color: #fff
            }
        }

        .pageNotFound-1xTPX {
            position: relative;
            height: calc(100vh - 8rem);
            z-index: 2;
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 6rem 100%, 0 calc(100% - 6.5rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 6rem 100%, 0 calc(100% - 6.5rem))
        }

        [dir] .pageNotFound-1xTPX {
            padding-top: 2.5rem;
            padding-bottom: 5rem;
            background-repeat: no-repeat;
            background-position: 50%;
            background-size: cover
        }

        .contentWrapper-2Gw8r {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: space-between;
            height: 100%
        }

        .mainLogo-3D7Qj {
            width: 10rem
        }

        [dir] .mainLogo-3D7Qj {
            margin-bottom: 2rem
        }

        .white-2sbyS {
            filter: brightness(0) invert(1)
        }

        .headingWrapper-7w1ZS {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center
        }

        [dir] .headingWrapper-7w1ZS {
            text-align: center
        }

        .copyWrapper-3H9S7 {
            position: relative;
            overflow: hidden
        }

        [dir] .backToHomeButton-1VPmq, [dir] .copyWrapper-3H9S7 {
            margin: 0 9.5%
        }

        @media (min-width: 768px) {
            [dir] .pageNotFound-1xTPX {
                padding-top: 5rem;
                padding-bottom: 10rem
            }

            .mainLogo-3D7Qj {
                width: 20rem
            }

            [dir] .mainLogo-3D7Qj {
                margin-bottom: 4rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .pageNotFound-1xTPX {
                padding-top: 7rem;
                padding-bottom: 12rem
            }

            .contentWrapper-2Gw8r {
                align-items: flex-start;
                justify-content: flex-start
            }

            .mainLogo-3D7Qj {
                width: 16rem
            }

            [dir=ltr] .mainLogo-3D7Qj {
                margin-left: 4rem
            }

            [dir=rtl] .mainLogo-3D7Qj {
                margin-right: 4rem
            }

            .headingWrapper-7w1ZS {
                max-width: calc(75% - .75rem);
                width: 100%;
                align-items: flex-start
            }

            [dir=ltr] .headingWrapper-7w1ZS {
                text-align: left
            }

            [dir=rtl] .headingWrapper-7w1ZS {
                text-align: right
            }

            .copyWrapper-3H9S7 {
                overflow: visible
            }

            [dir] .copyWrapper-3H9S7 {
                margin: 0 0 7rem
            }

            .backToHomeButton-1VPmq {
                max-width: calc(33.33333% - 2rem);
                width: 100%
            }

            [dir] .backToHomeButton-1VPmq {
                margin: 0
            }

            [dir=ltr] .backToHomeButton-1VPmq {
                margin-left: 4rem
            }

            [dir=rtl] .backToHomeButton-1VPmq {
                margin-right: 4rem
            }
        }

        @media (min-width: 1280px) {
            .headingWrapper-7w1ZS {
                max-width: calc(50% - 1.5rem);
                width: 100%
            }

            .backToHomeButton-1VPmq {
                max-width: calc(25% - 2.25rem);
                width: 100%
            }
        }

        .championAbilities-2FDBK {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 5.5rem), calc(100% - 5.5rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 5.5rem), calc(100% - 5.5rem) 100%, 0 100%);
            overflow: visible
        }

        [dir] .championAbilities-2FDBK {
            padding-top: 2.5rem;
            padding-bottom: 7rem
        }

        [dir=rtl] .championAbilities-2FDBK {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 5.5rem 100%, 0 calc(100% - 5.5rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 5.5rem 100%, 0 calc(100% - 5.5rem))
        }

        @media (min-width: 768px) {
            [dir] .championAbilities-2FDBK {
                padding-top: 5rem;
                padding-bottom: 14rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .championAbilities-2FDBK {
                padding-top: 9rem;
                padding-bottom: 10.25rem
            }
        }

        @media (min-width: 1280px) {
            [dir] .championAbilities-2FDBK {
                padding-bottom: 20.5rem
            }
        }

        .championAbilities-2FDBK .sectionDashDetail-171G5 {
            height: .4rem;
            width: 29%;
            position: absolute;
            bottom: -.4rem
        }

        [dir] .championAbilities-2FDBK .sectionDashDetail-171G5 {
            background: #fff
        }

        [dir=ltr] .championAbilities-2FDBK .sectionDashDetail-171G5 {
            right: 0
        }

        [dir=rtl] .championAbilities-2FDBK .sectionDashDetail-171G5 {
            left: 0
        }

        .championAbilities-2FDBK .sectionDashDetail-171G5:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .championAbilities-2FDBK .sectionDashDetail-171G5:after {
            border-top: .4rem solid #fff
        }

        [dir=ltr] .championAbilities-2FDBK .sectionDashDetail-171G5:after {
            left: -.4rem;
            border-left: .4rem solid transparent
        }

        [dir=rtl] .championAbilities-2FDBK .sectionDashDetail-171G5:after {
            right: -.4rem;
            border-right: .4rem solid transparent
        }

        .championAbilities-2FDBK .videoBorder-3il97.top-1H-UT {
            top: -1.2rem
        }

        [dir=ltr] .championAbilities-2FDBK .videoBorder-3il97.top-1H-UT {
            right: 1.2rem
        }

        [dir=rtl] .championAbilities-2FDBK .videoBorder-3il97.top-1H-UT {
            left: 1.2rem
        }

        .championAbilities-2FDBK .videoBorder-3il97.bottom-hlnwx {
            bottom: -1.2rem
        }

        [dir=ltr] .championAbilities-2FDBK .videoBorder-3il97.bottom-hlnwx {
            left: 1.2rem
        }

        [dir=rtl] .championAbilities-2FDBK .videoBorder-3il97.bottom-hlnwx {
            right: 1.2rem
        }

        .championAbilities-2FDBK .copyWrapper-103Fl {
            position: relative;
            color: #fff
        }

        [dir] .championAbilities-2FDBK .copyWrapper-103Fl {
            margin-bottom: 3.75rem
        }

        @media (min-width: 768px) {
            [dir] .championAbilities-2FDBK .copyWrapper-103Fl {
                margin-bottom: 7.5rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .championAbilities-2FDBK .copyWrapper-103Fl {
                margin-bottom: 8rem
            }
        }

        .championAbilities-2FDBK .abilityContentWrapper-2O7Jv {
            display: flex;
            flex-direction: column
        }

        @media (min-width: 1024px) {
            .championAbilities-2FDBK .abilityContentWrapper-2O7Jv {
                flex-direction: row;
                justify-content: space-between;
                align-items: center
            }
        }

        .championAbilities-2FDBK .abilitiesWrapper-26Q4q {
            width: 100%;
            display: flex;
            flex-direction: column;
            color: #fff
        }

        @media (min-width: 1024px) {
            .championAbilities-2FDBK .abilitiesWrapper-26Q4q {
                max-width: calc(50% - 1.5rem);
                width: 100%
            }

            [dir=ltr] .championAbilities-2FDBK .abilitiesWrapper-26Q4q {
                padding-left: 3rem
            }

            [dir=rtl] .championAbilities-2FDBK .abilitiesWrapper-26Q4q {
                padding-right: 3rem
            }
        }

        @media (min-width: 1280px) {
            .championAbilities-2FDBK .abilitiesWrapper-26Q4q {
                max-width: calc(41.66667% - 1.75rem);
                width: 100%
            }
        }

        .championAbilities-2FDBK .abilitiesDetailsWrapper-3nyLR {
            width: 100%
        }

        @media (min-width: 1024px) {
            [dir=ltr] .championAbilities-2FDBK .description-ToWJR {
                padding-right: calc(14.28571% + .42857rem);
                padding-left: 3rem
            }

            [dir=rtl] .championAbilities-2FDBK .description-ToWJR {
                padding-left: calc(14.28571% + .42857rem);
                padding-right: 3rem
            }
        }

        .championAbilities-2FDBK .abilityDescription-AJFbd {
            min-height: 9rem;
            color: #141e37
        }

        [dir=ltr] .championAbilities-2FDBK .abilityDescription-AJFbd {
            padding-right: 15.7%
        }

        [dir=rtl] .championAbilities-2FDBK .abilityDescription-AJFbd {
            padding-left: 15.7%
        }

        @media (min-width: 768px) {
            .championAbilities-2FDBK .abilityDescription-AJFbd {
                min-height: 18rem
            }
        }

        @media (min-width: 1024px) {
            [dir=ltr] .championAbilities-2FDBK .abilityDescription-AJFbd {
                padding-right: 7%
            }

            [dir=rtl] .championAbilities-2FDBK .abilityDescription-AJFbd {
                padding-left: 7%
            }
        }

        .championAbilities-2FDBK .videoWrapper-5IzN_ {
            position: relative
        }

        [dir] .championAbilities-2FDBK .videoWrapper-5IzN_ {
            margin-bottom: 4rem
        }

        @media (min-width: 768px) {
            [dir] .championAbilities-2FDBK .videoWrapper-5IzN_ {
                margin-bottom: 6.5rem
            }
        }

        @media (min-width: 1024px) {
            .championAbilities-2FDBK .videoWrapper-5IzN_ {
                width: calc(50% + 7.3vw);
                max-width: 73.5rem;
                order: 2
            }

            [dir] .championAbilities-2FDBK .videoWrapper-5IzN_ {
                margin-bottom: 0
            }

            [dir=ltr] .championAbilities-2FDBK .videoWrapper-5IzN_ {
                transform: translateX(7.3vw)
            }

            [dir=rtl] .championAbilities-2FDBK .videoWrapper-5IzN_ {
                transform: translateX(-7.3vw)
            }
        }

        .championAbilities-2FDBK .videoContainer-3HG7h {
            position: relative;
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 3rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 3rem) 100%, 0 100%);
            position: position;
            height: auto
        }

        [dir] .championAbilities-2FDBK .videoContainer-3HG7h {
            background-color: #111
        }

        .championAbilities-2FDBK .videoContainer-3HG7h:before {
            content: "";
            display: block
        }

        [dir] .championAbilities-2FDBK .videoContainer-3HG7h:before {
            padding-top: 56.25%
        }

        [dir=rtl] .championAbilities-2FDBK .videoContainer-3HG7h {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 3rem 100%, 0 calc(100% - 3rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 3rem 100%, 0 calc(100% - 3rem))
        }

        .championAbilities-2FDBK .videoContainer-3HG7h > video {
            position: absolute;
            top: 0
        }

        [dir=ltr] .championAbilities-2FDBK .videoContainer-3HG7h > video {
            left: 0
        }

        [dir=rtl] .championAbilities-2FDBK .videoContainer-3HG7h > video {
            right: 0
        }

        @media (min-width: 1024px) {
            .championAbilities-2FDBK .videoContainer-3HG7h {
                -webkit-clip-path: none;
                clip-path: none
            }
        }

        @media (min-width: 1441px) {
            .championAbilities-2FDBK .videoContainer-3HG7h {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 3rem) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 3rem) 100%, 0 100%)
            }

            [dir=rtl] .championAbilities-2FDBK .videoContainer-3HG7h {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 3rem 100%, 0 calc(100% - 3rem));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 3rem 100%, 0 calc(100% - 3rem))
            }
        }

        .championAbilities-2FDBK .thumbnailsWrapper-1nztP {
            display: flex;
            justify-content: space-between;
            max-width: 40rem
        }

        [dir] .championAbilities-2FDBK .thumbnailsWrapper-1nztP {
            margin-bottom: 3.5rem
        }

        @media (min-width: 768px) {
            .championAbilities-2FDBK .thumbnailsWrapper-1nztP {
                max-width: 70rem
            }

            [dir] .championAbilities-2FDBK .thumbnailsWrapper-1nztP {
                margin-bottom: 6rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .championAbilities-2FDBK .thumbnailsWrapper-1nztP {
                margin-bottom: 3.5rem
            }
        }

        .championAbilities-2FDBK .thumbnail-21xPX {
            height: 4.8rem;
            width: 4.8rem;
            overflow: hidden;
            opacity: .6;
            transition: opacity .2s ease-out, transform .3s ease-out
        }

        [dir] .championAbilities-2FDBK .thumbnail-21xPX {
            cursor: pointer;
            border-radius: 100%
        }

        .championAbilities-2FDBK .thumbnail-21xPX.isActive-8peBN {
            opacity: 1
        }

        [dir] .championAbilities-2FDBK .thumbnail-21xPX:not(.isActive-8peBN):hover {
            transform: scale(1.2)
        }

        @media (min-width: 768px) {
            .championAbilities-2FDBK .thumbnail-21xPX {
                height: 9.6rem;
                width: 9.6rem
            }

            [dir=ltr] .championAbilities-2FDBK .thumbnail-21xPX:not(:last-child) {
                margin-right: 3%
            }

            [dir=rtl] .championAbilities-2FDBK .thumbnail-21xPX:not(:last-child) {
                margin-left: 3%
            }
        }

        @media (min-width: 1024px) {
            .championAbilities-2FDBK .thumbnail-21xPX {
                height: 6.5rem;
                width: 6.5rem
            }

            [dir] .championAbilities-2FDBK .thumbnail-21xPX:not(:last-child) {
                margin: 0
            }
        }

        .championAbilities-2FDBK .abilityName-36fya {
            color: #32c8ff
        }

        [dir] .championAbilities-2FDBK .abilityName-36fya {
            margin-bottom: 1rem
        }

        @media (min-width: 768px) {
            [dir] .championAbilities-2FDBK .abilityName-36fya {
                margin-bottom: 1.5rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .championAbilities-2FDBK .abilityName-36fya {
                margin-bottom: 1rem
            }
        }

        .championAbilities-2FDBK .textDivider-1gVLX {
            height: auto;
            width: .6rem
        }

        [dir=ltr] .championAbilities-2FDBK .textDivider-1gVLX {
            margin: 0 2% .25rem 3%
        }

        [dir=rtl] .championAbilities-2FDBK .textDivider-1gVLX {
            margin: 0 3% .25rem 2%
        }

        html[dir][lang=ar-ae] .championAbilities-2FDBK .textDivider-1gVLX {
            margin: 0 3% .25rem
        }

        .championAbilities-2FDBK .textDivider-1gVLX svg path {
            fill: #32c8ff
        }

        @media (min-width: 768px) {
            .championAbilities-2FDBK .textDivider-1gVLX {
                width: 1.2rem
            }

            [dir] .championAbilities-2FDBK .textDivider-1gVLX {
                margin-bottom: .5rem
            }
        }

        @media (min-width: 1024px) {
            .championAbilities-2FDBK .textDivider-1gVLX {
                width: .7rem
            }

            [dir] .championAbilities-2FDBK .textDivider-1gVLX {
                margin-bottom: .4rem
            }
        }

        .championDetailHero-1l-6Q {
            z-index: 4;
            position: relative;
            overflow: hidden
        }

        [dir] .championDetailHero-1l-6Q {
            padding-bottom: 18.5rem
        }

        @media (min-width: 768px) {
            [dir] .championDetailHero-1l-6Q {
                padding-bottom: 37rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .championDetailHero-1l-6Q {
                padding-bottom: 17rem
            }
        }

        .championDetailHero-1l-6Q .heroMediaWrapper-3FJNh {
            position: relative;
            overflow: hidden
        }

        @media (min-width: 1024px) {
            .championDetailHero-1l-6Q .heroMediaWrapper-3FJNh:before {
                content: "";
                display: block;
                width: 100%
            }

            [dir] .championDetailHero-1l-6Q .heroMediaWrapper-3FJNh:before {
                padding-top: 40%
            }
        }

        .championDetailHero-1l-6Q .ieBackgroundImage-GnH4h {
            height: 100%;
            width: 100%
        }

        [dir] .championDetailHero-1l-6Q .ieBackgroundImage-GnH4h {
            background-repeat: no-repeat;
            background-size: cover
        }

        [dir=ltr] .championDetailHero-1l-6Q .ieBackgroundImage-GnH4h {
            background-position: 70%
        }

        [dir=rtl] .championDetailHero-1l-6Q .ieBackgroundImage-GnH4h {
            background-position: 30%
        }

        @media (min-width: 1024px) {
            .championDetailHero-1l-6Q .ieBackgroundImage-GnH4h {
                height: 100%;
                width: 100%;
                position: absolute;
                top: 0
            }

            [dir] .championDetailHero-1l-6Q .ieBackgroundImage-GnH4h {
                background-position: top
            }

            [dir=ltr] .championDetailHero-1l-6Q .ieBackgroundImage-GnH4h {
                left: 0
            }

            [dir=rtl] .championDetailHero-1l-6Q .ieBackgroundImage-GnH4h {
                right: 0
            }
        }

        .championDetailHero-1l-6Q .backgroundImage-zARlR {
            height: 100%;
            width: 100%;
            -o-object-fit: cover;
            object-fit: cover;
            -o-object-position: 70% center;
            object-position: 70% center
        }

        @media (min-width: 1024px) {
            .championDetailHero-1l-6Q .backgroundImage-zARlR {
                -o-object-position: center top;
                object-position: center top;
                height: 100%;
                width: 100%;
                position: absolute;
                top: 0
            }

            [dir=ltr] .championDetailHero-1l-6Q .backgroundImage-zARlR {
                left: 0
            }

            [dir=rtl] .championDetailHero-1l-6Q .backgroundImage-zARlR {
                right: 0
            }
        }

        .championDetailHero-1l-6Q .heroVideo-1Jeta video {
            -o-object-position: 70% center;
            object-position: 70% center
        }

        @media (min-width: 1024px) {
            .championDetailHero-1l-6Q .heroVideo-1Jeta video {
                -o-object-position: center top;
                object-position: center top
            }
        }

        [dir=ltr] .championDetailHero-1l-6Q .heroVideo-1Jeta > div {
            background-position: 70%
        }

        [dir=rtl] .championDetailHero-1l-6Q .heroVideo-1Jeta > div {
            background-position: 30%
        }

        @media (min-width: 1024px) {
            [dir] .championDetailHero-1l-6Q .heroVideo-1Jeta > div {
                background-position: top
            }
        }

        @media (min-width: 1024px) {
            .championDetailHero-1l-6Q .heroVideo-1Jeta {
                height: 100%;
                width: 100%;
                position: absolute;
                top: 0
            }

            [dir=ltr] .championDetailHero-1l-6Q .heroVideo-1Jeta {
                left: 0
            }

            [dir=rtl] .championDetailHero-1l-6Q .heroVideo-1Jeta {
                right: 0
            }
        }

        .championDetailHero-1l-6Q .backgroundImageWrapper-347P9, .championDetailHero-1l-6Q .heroVideo-1Jeta, .championDetailHero-1l-6Q .ieBackgroundImage-GnH4h {
            height: 46.7rem;
            width: 100%
        }

        @media (min-width: 768px) {
            .championDetailHero-1l-6Q .backgroundImageWrapper-347P9, .championDetailHero-1l-6Q .heroVideo-1Jeta, .championDetailHero-1l-6Q .ieBackgroundImage-GnH4h {
                height: 93.4rem
            }
        }

        @media (min-width: 1024px) {
            .championDetailHero-1l-6Q .backgroundImageWrapper-347P9, .championDetailHero-1l-6Q .heroVideo-1Jeta, .championDetailHero-1l-6Q .ieBackgroundImage-GnH4h {
                height: auto
            }
        }

        .championDetailHero-1l-6Q .backButton-2RWUY {
            position: absolute;
            top: 3rem;
            visibility: hidden
        }

        [dir=ltr] .championDetailHero-1l-6Q .backButton-2RWUY {
            left: -1rem
        }

        [dir=rtl] .championDetailHero-1l-6Q .backButton-2RWUY {
            right: -1rem
        }

        @media (min-width: 768px) {
            .championDetailHero-1l-6Q .backButton-2RWUY {
                top: 6rem
            }

            [dir=ltr] .championDetailHero-1l-6Q .backButton-2RWUY {
                left: -1.5rem
            }

            [dir=rtl] .championDetailHero-1l-6Q .backButton-2RWUY {
                right: -1.5rem
            }
        }

        .championDetailHero-1l-6Q .heroContentWrapper-3g_Fj {
            position: absolute;
            bottom: 3.85rem;
            overflow: visible
        }

        [dir=ltr] .championDetailHero-1l-6Q .heroContentWrapper-3g_Fj {
            left: 0
        }

        [dir=rtl] .championDetailHero-1l-6Q .heroContentWrapper-3g_Fj {
            right: 0
        }

        @media (min-width: 768px) {
            .championDetailHero-1l-6Q .heroContentWrapper-3g_Fj {
                bottom: 7.7rem
            }
        }

        @media (min-width: 1024px) {
            .championDetailHero-1l-6Q .heroContentWrapper-3g_Fj {
                bottom: calc(22% - 8rem)
            }
        }

        @media (min-width: 1441px) {
            .championDetailHero-1l-6Q .heroContentWrapper-3g_Fj {
                bottom: calc(18% - 8rem)
            }
        }

        @media (min-width: 1921px) {
            .championDetailHero-1l-6Q .heroContentWrapper-3g_Fj {
                bottom: 9rem
            }
        }

        .championDetailHero-1l-6Q .contentWrapper-IJtH2 {
            position: relative;
            visibility: hidden
        }

        .championDetailHero-1l-6Q .dashDetail-3vjj9 {
            height: .5rem;
            width: 19%;
            position: absolute;
            top: -.5rem
        }

        [dir] .championDetailHero-1l-6Q .dashDetail-3vjj9 {
            background: #fff
        }

        [dir=ltr] .championDetailHero-1l-6Q .dashDetail-3vjj9 {
            transform-origin: left;
            left: 0
        }

        [dir=rtl] .championDetailHero-1l-6Q .dashDetail-3vjj9 {
            transform-origin: right;
            right: 0
        }

        .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
            border-bottom: .5rem solid #fff
        }

        [dir=ltr] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
            right: -.5rem;
            border-right: .5rem solid transparent
        }

        [dir=rtl] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
            left: -.5rem;
            border-left: .5rem solid transparent
        }

        html[dir=ltr][browser=Safari] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
            right: -.4rem
        }

        html[dir=rtl][browser=Safari] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
            left: -.4rem
        }

        html[dir=ltr][browser=Firefox] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
            right: -.4rem
        }

        html[dir=rtl][browser=Firefox] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
            left: -.4rem
        }

        @media (min-width: 768px) {
            .championDetailHero-1l-6Q .dashDetail-3vjj9 {
                height: 1rem;
                width: 19%;
                top: -1rem
            }

            [dir] .championDetailHero-1l-6Q .dashDetail-3vjj9 {
                background: #fff
            }

            .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
                border-bottom: 1rem solid #fff
            }

            [dir=ltr] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
                right: -1rem;
                border-right: 1rem solid transparent
            }

            [dir=rtl] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
                left: -1rem;
                border-left: 1rem solid transparent
            }

            html[dir=ltr][browser=Safari] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
                right: -.9rem
            }

            html[dir=rtl][browser=Safari] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
                left: -.9rem
            }

            html[dir=ltr][browser=Firefox] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
                right: -.9rem
            }

            html[dir=rtl][browser=Firefox] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
                left: -.9rem
            }
        }

        @media (min-width: 1024px) {
            .championDetailHero-1l-6Q .dashDetail-3vjj9 {
                height: 1rem;
                width: 10%
            }

            [dir] .championDetailHero-1l-6Q .dashDetail-3vjj9 {
                background: #fff
            }

            .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
                border-bottom: 1rem solid #fff
            }

            [dir=ltr] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
                right: -1rem;
                border-right: 1rem solid transparent
            }

            [dir=rtl] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
                left: -1rem;
                border-left: 1rem solid transparent
            }

            html[dir=ltr][browser=Safari] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
                right: -.9rem
            }

            html[dir=rtl][browser=Safari] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
                left: -.9rem
            }

            html[dir=ltr][browser=Firefox] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
                right: -.9rem
            }

            html[dir=rtl][browser=Firefox] .championDetailHero-1l-6Q .dashDetail-3vjj9:after {
                left: -.9rem
            }
        }

        .championDetailHero-1l-6Q .heroContent-1_EhD {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 2.5rem), calc(100% - 2.5rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 2.5rem), calc(100% - 2.5rem) 100%, 0 100%)
        }

        [dir] .championDetailHero-1l-6Q .heroContent-1_EhD {
            background: #32c8ff;
            padding: 2rem
        }

        [dir=rtl] .championDetailHero-1l-6Q .heroContent-1_EhD {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 2.5rem 100%, 0 calc(100% - 2.5rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 2.5rem 100%, 0 calc(100% - 2.5rem))
        }

        @media (min-width: 768px) {
            [dir] .championDetailHero-1l-6Q .heroContent-1_EhD {
                padding: 4rem
            }
        }

        @media (min-width: 1024px) {
            .championDetailHero-1l-6Q .heroContent-1_EhD {
                flex-direction: row
            }

            [dir] .championDetailHero-1l-6Q .heroContent-1_EhD {
                padding: 3.5rem 0
            }
        }

        .championDetailHero-1l-6Q .descriptionWrapper-2jkAB {
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center
        }

        [dir] .championDetailHero-1l-6Q .descriptionWrapper-2jkAB {
            margin-bottom: 2.5rem
        }

        @media (min-width: 768px) {
            [dir] .championDetailHero-1l-6Q .descriptionWrapper-2jkAB {
                margin-bottom: 5rem
            }
        }

        @media (min-width: 1024px) {
            .championDetailHero-1l-6Q .descriptionWrapper-2jkAB {
                width: 45.85366%
            }

            [dir] .championDetailHero-1l-6Q .descriptionWrapper-2jkAB {
                margin-bottom: 0;
                border: none
            }
        }

        [dir] .championDetailHero-1l-6Q .championName-1JnC5 {
            margin-bottom: 1.25rem
        }

        @media (min-width: 768px) {
            [dir] .championDetailHero-1l-6Q .championName-1JnC5 {
                margin-bottom: 2.5rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .championDetailHero-1l-6Q .championName-1JnC5 {
                margin-bottom: 1.5rem
            }
        }

        .championDetailHero-1l-6Q .championSubtitle-YAx7w {
            color: #141e37
        }

        .championDetailHero-1l-6Q .detailsWrapper-2KT-L {
            width: 100%;
            display: flex;
            justify-content: center;
            align-items: flex-end
        }

        [dir] .championDetailHero-1l-6Q .detailsWrapper-2KT-L {
            border-top: 1px solid hsla(0, 0%, 100%, .5)
        }

        @media (min-width: 1024px) {
            .championDetailHero-1l-6Q .detailsWrapper-2KT-L {
                width: 54.14634%
            }

            [dir] .championDetailHero-1l-6Q .detailsWrapper-2KT-L {
                border: none
            }
        }

        .championDetailHero-1l-6Q .difficultyWrapper-3rRZf, .championDetailHero-1l-6Q .roleWrapper-1dBnJ {
            height: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center
        }

        .championDetailHero-1l-6Q .roleWrapper-1dBnJ {
            min-width: 37.3%
        }

        [dir] .championDetailHero-1l-6Q .roleWrapper-1dBnJ {
            padding: 1.75rem 1rem .5rem
        }

        [dir=ltr] .championDetailHero-1l-6Q .roleWrapper-1dBnJ {
            border-right: 1px solid hsla(0, 0%, 100%, .5)
        }

        [dir=rtl] .championDetailHero-1l-6Q .roleWrapper-1dBnJ {
            border-left: 1px solid hsla(0, 0%, 100%, .5)
        }

        @media (min-width: 768px) {
            [dir] .championDetailHero-1l-6Q .roleWrapper-1dBnJ {
                padding: 3.5rem 2rem 1rem
            }
        }

        @media (min-width: 1024px) {
            .championDetailHero-1l-6Q .roleWrapper-1dBnJ {
                width: 42.79279%
            }

            [dir] .championDetailHero-1l-6Q .roleWrapper-1dBnJ {
                padding: 2rem 0
            }

            [dir=ltr] .championDetailHero-1l-6Q .roleWrapper-1dBnJ {
                border-left: 1px solid hsla(0, 0%, 100%, .5)
            }

            [dir=rtl] .championDetailHero-1l-6Q .roleWrapper-1dBnJ {
                border-right: 1px solid hsla(0, 0%, 100%, .5)
            }
        }

        .championDetailHero-1l-6Q .iconWrapper-2To1h {
            display: flex
        }

        .championDetailHero-1l-6Q .roleIcon-1GxJR {
            height: 3rem;
            width: 3rem
        }

        [dir=ltr] .championDetailHero-1l-6Q .roleIcon-1GxJR:nth-child(2) {
            margin-left: 1rem
        }

        [dir=rtl] .championDetailHero-1l-6Q .roleIcon-1GxJR:nth-child(2) {
            margin-right: 1rem
        }

        @media (min-width: 768px) {
            .championDetailHero-1l-6Q .roleIcon-1GxJR {
                height: 6rem;
                width: 6rem
            }

            [dir=ltr] .championDetailHero-1l-6Q .roleIcon-1GxJR:nth-child(2) {
                margin-left: 2rem
            }

            [dir=rtl] .championDetailHero-1l-6Q .roleIcon-1GxJR:nth-child(2) {
                margin-right: 2rem
            }
        }

        @media (min-width: 1024px) {
            .championDetailHero-1l-6Q .roleIcon-1GxJR {
                height: 5rem;
                width: 5rem
            }
        }

        .championDetailHero-1l-6Q .copyWrapper-3a6kV {
            display: flex;
            flex-direction: column;
            align-items: center
        }

        [dir] .championDetailHero-1l-6Q .copyWrapper-3a6kV {
            margin-top: 1.5rem;
            text-align: center
        }

        @media (min-width: 768px) {
            [dir] .championDetailHero-1l-6Q .copyWrapper-3a6kV {
                margin-top: 3rem
            }
        }

        [dir] .championDetailHero-1l-6Q .difficultyTitle-3Dq5b, [dir] .championDetailHero-1l-6Q .roleTitle-BcJVL {
            margin-bottom: .25rem
        }

        @media (min-width: 768px) {
            [dir] .championDetailHero-1l-6Q .difficultyTitle-3Dq5b, [dir] .championDetailHero-1l-6Q .roleTitle-BcJVL {
                margin-bottom: .5rem
            }
        }

        .championDetailHero-1l-6Q .difficultyName-3NSea, .championDetailHero-1l-6Q .roleName-33zEx {
            color: #141e37;
            letter-spacing: 1px
        }

        .championDetailHero-1l-6Q .difficultyWrapper-3rRZf {
            flex-grow: 1
        }

        [dir] .championDetailHero-1l-6Q .difficultyWrapper-3rRZf {
            padding-bottom: .5rem
        }

        @media (min-width: 768px) {
            [dir] .championDetailHero-1l-6Q .difficultyWrapper-3rRZf {
                padding-bottom: 1rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .championDetailHero-1l-6Q .difficultyWrapper-3rRZf {
                padding: 2rem 0
            }
        }

        [dir] .championDetailHero-1l-6Q .difficultyWrapper-3rRZf .copyWrapper-3a6kV {
            margin-top: 2.5rem
        }

        @media (min-width: 768px) {
            [dir] .championDetailHero-1l-6Q .difficultyWrapper-3rRZf .copyWrapper-3a6kV {
                margin-top: 5rem
            }
        }

        .championDetailHero-1l-6Q .difficultyBars-2777d[data-difficulty=high] .bar-2Sati .barInner-wpX1J, .championDetailHero-1l-6Q .difficultyBars-2777d[data-difficulty=low] .bar-2Sati:first-of-type .barInner-wpX1J, .championDetailHero-1l-6Q .difficultyBars-2777d[data-difficulty=moderate] .bar-2Sati:nth-of-type(-n+2) .barInner-wpX1J {
            display: block
        }

        .championDetailHero-1l-6Q .bar-2Sati {
            height: .6rem;
            width: 3.5rem;
            position: relative;
            display: inline-block
        }

        [dir] .championDetailHero-1l-6Q .bar-2Sati {
            background-color: hsla(0, 0%, 100%, .3)
        }

        [dir=ltr] .championDetailHero-1l-6Q .bar-2Sati {
            transform: skewX(-35deg)
        }

        [dir=rtl] .championDetailHero-1l-6Q .bar-2Sati {
            transform: skewX(35deg)
        }

        [dir=ltr] .championDetailHero-1l-6Q .bar-2Sati:not(:last-child) {
            margin-right: .3rem
        }

        [dir=rtl] .championDetailHero-1l-6Q .bar-2Sati:not(:last-child) {
            margin-left: .3rem
        }

        @media (min-width: 768px) {
            .championDetailHero-1l-6Q .bar-2Sati {
                height: 1rem;
                width: 7rem
            }
        }

        @media (min-width: 1024px) {
            .championDetailHero-1l-6Q .bar-2Sati {
                height: 1rem;
                width: 6rem
            }
        }

        .championDetailHero-1l-6Q .barInner-wpX1J {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0;
            display: none
        }

        [dir] .championDetailHero-1l-6Q .barInner-wpX1J {
            background-color: #fff
        }

        [dir=ltr] .championDetailHero-1l-6Q .barInner-wpX1J {
            left: 0
        }

        [dir=rtl] .championDetailHero-1l-6Q .barInner-wpX1J {
            right: 0
        }

        .championHeroBackButton-bHBuc {
            z-index: 1;
            transition: color .2s ease-out, background-color .2s ease-out;
            -webkit-appearance: none
        }

        [dir] .championHeroBackButton-bHBuc {
            padding: 2rem
        }

        [dir=ltr] .championHeroBackButton-bHBuc {
            transform: skewX(-15deg)
        }

        [dir=rtl] .championHeroBackButton-bHBuc {
            transform: skewX(15deg)
        }

        .championHeroBackButton-bHBuc.disabled-9El_1 {
            opacity: .7
        }

        .buttonContent-1fgoV {
            display: flex;
            justify-content: center;
            align-items: center
        }

        [dir=ltr] .buttonContent-1fgoV {
            transform: skewX(15deg)
        }

        [dir=rtl] .buttonContent-1fgoV {
            transform: skewX(-15deg)
        }

        .icon-28Jdp {
            height: 1rem;
            width: 1rem
        }

        [dir] .icon-28Jdp {
            transform: translateX(0) scale(-1)
        }

        [dir=ltr] .icon-28Jdp {
            margin-right: .5rem
        }

        [dir=rtl] .icon-28Jdp {
            margin-left: .5rem;
            transform: translateX(0) scale(1)
        }

        .icon-28Jdp path {
            stroke-width: .8rem;
            transition: stroke .2s ease-out
        }

        html[browser="Internet Explorer"] .icon-28Jdp svg {
            max-height: 3rem
        }

        .imageIcon-3MCuF {
            height: 2rem;
            width: 2rem
        }

        .default-3usFp {
            color: #fff
        }

        [dir] .default-3usFp {
            background-color: #141e37
        }

        .default-3usFp .icon-28Jdp path {
            stroke: #fff
        }

        .light-1Ruki {
            color: #141e37
        }

        [dir] .light-1Ruki {
            background-color: #fff
        }

        .light-1Ruki .icon-28Jdp path {
            stroke: #141e37
        }

        @media (min-width: 768px) {
            [dir] .championHeroBackButton-bHBuc {
                padding: 3rem 4rem
            }

            .icon-28Jdp {
                height: 2rem;
                width: 2rem
            }

            [dir=ltr] .icon-28Jdp {
                margin-right: 1rem
            }

            [dir=rtl] .icon-28Jdp {
                margin-left: 1rem
            }

            .icon-28Jdp path {
                stroke-width: .7rem
            }

            .imageIcon-3MCuF {
                height: 4rem;
                width: 4rem
            }
        }

        @media (min-width: 1024px) {
            [dir=ltr] .championHeroBackButton-bHBuc {
                padding: 2rem 3rem 2rem 4.5rem
            }

            [dir=rtl] .championHeroBackButton-bHBuc {
                padding: 2rem 4.5rem 2rem 3rem
            }

            .championHeroBackButton-bHBuc .icon-28Jdp {
                height: 1.2rem;
                width: 1.2rem;
                transition: transform .3s ease-out
            }

            [dir] .championHeroBackButton-bHBuc .icon-28Jdp {
                margin: 0 1.2rem
            }

            .championHeroBackButton-bHBuc .icon-28Jdp path {
                stroke-width: .7rem
            }

            .championHeroBackButton-bHBuc .imageIcon-3MCuF {
                height: 2.5rem;
                width: 2.5rem;
                transition: transform .2s ease-out
            }

            [dir=ltr] .championHeroBackButton-bHBuc:hover .icon-28Jdp {
                transform: translateX(-.4rem) scale(-1)
            }

            [dir=rtl] .championHeroBackButton-bHBuc:hover .icon-28Jdp {
                transform: translateX(.4rem) scale(-1);
                transform: translateX(.4rem) scale(1)
            }

            [dir=ltr] .championHeroBackButton-bHBuc:hover .imageIcon-3MCuF {
                transform: translateX(.2rem)
            }

            [dir=rtl] .championHeroBackButton-bHBuc:hover .imageIcon-3MCuF {
                transform: translateX(-.2rem)
            }
        }

        .skinSection-8KZH- {
            position: relative;
            z-index: 4;
            min-height: auto
        }

        [dir] .skinSection-8KZH- {
            padding-bottom: 10rem;
            padding-top: 9rem
        }

        [dir] .header-7qCIe {
            margin-bottom: 3.5rem
        }

        .skinsGallery-1OLzj {
            width: 100%;
            max-width: 116rem;
            display: block
        }

        [dir] .skinsGallery-1OLzj {
            margin: 0 auto
        }

        .imageWrapper-2s8XW {
            position: relative;
            width: 100%
        }

        [dir] .imageWrapper-2s8XW {
            margin-bottom: 4rem
        }

        .imageWrapper-2s8XW .imageBorderLeft-2QTG3 {
            z-index: -1;
            bottom: -1.3rem
        }

        [dir=ltr] .imageWrapper-2s8XW .imageBorderLeft-2QTG3 {
            left: 1.7rem
        }

        [dir=rtl] .imageWrapper-2s8XW .imageBorderLeft-2QTG3 {
            right: 1.7rem
        }

        .imageWrapper-2s8XW .skinImage-3Qlo6 {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 2.6rem), calc(100% - 2.6rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 2.6rem), calc(100% - 2.6rem) 100%, 0 100%)
        }

        [dir=rtl] .imageWrapper-2s8XW .skinImage-3Qlo6 {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 2.6rem 100%, 0 calc(100% - 2.6rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 2.6rem 100%, 0 calc(100% - 2.6rem))
        }

        .skinsList-yb6e9 {
            display: inline-flex;
            flex-direction: row
        }

        [dir=ltr] .skinsList-yb6e9 {
            margin-left: 7.5%
        }

        [dir=rtl] .skinsList-yb6e9 {
            margin-right: 7.5%
        }

        .skinsList-yb6e9 .thumbnail-3NKId {
            display: flex;
            flex-direction: column;
            align-items: center;
            flex: 1 1;
            min-width: 8rem;
            opacity: .6;
            transition: opacity .2s ease-out;
            position: relative
        }

        [dir] .skinsList-yb6e9 .thumbnail-3NKId {
            cursor: pointer
        }

        .skinsList-yb6e9 .thumbnail-3NKId svg {
            max-width: 8rem
        }

        .skinsList-yb6e9 .thumbnail-3NKId svg path {
            fill: none;
            stroke: #fff;
            stroke-miterlimit: 10;
            stroke-dasharray: 280;
            stroke-dashoffset: 280;
            stroke-width: 1.8;
            opacity: 0;
            transition: opacity 50ms ease, stroke-dashoffset .8s ease 50ms
        }

        .skinsList-yb6e9 .thumbnail-3NKId > [data-element=thumbnail-copy] {
            opacity: 0;
            max-height: 0;
            transition: opacity .3s ease-out .15s, max-height .3s ease-out .15s;
            color: #fff;
            text-transform: uppercase;
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif;
            font-size: 1.4rem;
            letter-spacing: 1px;
            position: absolute;
            width: 125%
        }

        [dir] .skinsList-yb6e9 .thumbnail-3NKId > [data-element=thumbnail-copy] {
            margin-top: 8rem;
            text-align: center
        }

        html[browser="Internet Explorer"] .skinsList-yb6e9 .thumbnail-3NKId > [data-element=thumbnail-copy] {
            top: 4.5rem
        }

        html[dir=ltr][browser="Internet Explorer"] .skinsList-yb6e9 .thumbnail-3NKId > [data-element=thumbnail-copy] {
            left: -1.5rem
        }

        html[dir=rtl][browser="Internet Explorer"] .skinsList-yb6e9 .thumbnail-3NKId > [data-element=thumbnail-copy] {
            right: -1.5rem
        }

        html[lang=ar-ae] .skinsList-yb6e9 .thumbnail-3NKId > [data-element=thumbnail-copy] {
            font-weight: 700
        }

        .skinsList-yb6e9 .thumbnail-3NKId span {
            position: relative;
            overflow: hidden;
            width: 6rem;
            transition: transform .3s ease
        }

        [dir=ltr] .skinsList-yb6e9 .thumbnail-3NKId span {
            transform-origin: 70% 50%
        }

        [dir=rtl] .skinsList-yb6e9 .thumbnail-3NKId span {
            transform-origin: 30% 50%
        }

        .skinsList-yb6e9 .thumbnail-3NKId img {
            position: absolute;
            top: 0;
            width: 4.8rem;
            height: 4.8rem
        }

        [dir] .skinsList-yb6e9 .thumbnail-3NKId img {
            border-radius: 50%
        }

        [dir=ltr] .skinsList-yb6e9 .thumbnail-3NKId img {
            left: 50%;
            transform: translate(-50%, 14%)
        }

        [dir=rtl] .skinsList-yb6e9 .thumbnail-3NKId img {
            right: 50%;
            transform: translate(50%, 14%)
        }

        html[dir=ltr][browser="Internet Explorer"] .skinsList-yb6e9 .thumbnail-3NKId img {
            transform: translate(-50%, 91%)
        }

        html[dir=rtl][browser="Internet Explorer"] .skinsList-yb6e9 .thumbnail-3NKId img {
            transform: translate(50%, 91%)
        }

        .skinsList-yb6e9 .thumbnail-3NKId.isActive-3DIZw {
            opacity: 1
        }

        .skinsList-yb6e9 .thumbnail-3NKId.isActive-3DIZw svg path {
            opacity: 1;
            stroke-dashoffset: 0
        }

        .skinsList-yb6e9 .thumbnail-3NKId.isActive-3DIZw > [data-element=thumbnail-copy] {
            opacity: 1;
            max-height: 4rem
        }

        [dir] .skinsList-yb6e9 .thumbnail-3NKId:hover:not(.isActive-3DIZw) span {
            transform: scale(1.2)
        }

        .sectionFooter-2htAm {
            display: flex;
            flex-direction: column;
            width: 80%
        }

        [dir] .sectionFooter-2htAm {
            margin: 6rem auto 0
        }

        [dir] .sectionFooter-2htAm div:first-child {
            margin-top: 2rem
        }

        @media (min-width: 768px) {
            .skinsList-yb6e9 {
                width: auto
            }

            [dir] .sectionFooter-2htAm {
                margin: 10rem auto 0
            }

            .imageWrapper-2s8XW .skinImage-3Qlo6 {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 5.6rem), calc(100% - 5.6rem) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 5.6rem), calc(100% - 5.6rem) 100%, 0 100%)
            }

            [dir=rtl] .imageWrapper-2s8XW .skinImage-3Qlo6 {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 5.6rem 100%, 0 calc(100% - 5.6rem));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 5.6rem 100%, 0 calc(100% - 5.6rem))
            }
        }

        @media (min-width: 1280px) {
            [dir] .skinSection-8KZH- {
                padding-bottom: 13rem;
                padding-top: 9rem
            }

            .header-7qCIe {
                z-index: 1;
                pointer-events: none
            }

            [dir] .header-7qCIe {
                transform: translateY(55%);
                margin-bottom: 0
            }

            [dir] .copyWrapper-3H-Oq {
                margin-top: 2rem
            }

            .skinsGallery-1OLzj {
                display: flex
            }

            .imageWrapper-2s8XW {
                width: 80%
            }

            .skinsList-yb6e9 {
                width: auto;
                width: 20%;
                flex-direction: column
            }

            [dir=ltr] .skinsList-yb6e9 {
                margin-left: auto;
                margin-right: 0
            }

            [dir=rtl] .skinsList-yb6e9 {
                margin-right: auto;
                margin-left: 0
            }

            .skinsList-yb6e9 .thumbnail-3NKId {
                min-width: 10rem;
                max-width: 13rem;
                flex: inherit
            }

            .skinsList-yb6e9 .thumbnail-3NKId > [data-element=thumbnail-copy] {
                font-size: 1.4rem;
                position: absolute;
                width: 100%;
                top: 50%
            }

            [dir] .skinsList-yb6e9 .thumbnail-3NKId > [data-element=thumbnail-copy] {
                margin-top: 0;
                margin-bottom: 2rem;
                transform: translateY(-24%)
            }

            [dir=ltr] .skinsList-yb6e9 .thumbnail-3NKId > [data-element=thumbnail-copy] {
                text-align: left;
                left: 14rem
            }

            [dir=rtl] .skinsList-yb6e9 .thumbnail-3NKId > [data-element=thumbnail-copy] {
                text-align: right;
                right: 14rem
            }

            html[browser="Internet Explorer"] .skinsList-yb6e9 .thumbnail-3NKId > [data-element=thumbnail-copy] {
                top: 50%
            }

            html[dir=ltr][browser="Internet Explorer"] .skinsList-yb6e9 .thumbnail-3NKId > [data-element=thumbnail-copy] {
                left: 14rem
            }

            html[dir=rtl][browser="Internet Explorer"] .skinsList-yb6e9 .thumbnail-3NKId > [data-element=thumbnail-copy] {
                right: 14rem
            }

            .skinsList-yb6e9 .thumbnail-3NKId span {
                width: auto
            }

            [dir=ltr] .skinsList-yb6e9 .thumbnail-3NKId span {
                transform: scale(1) rotate(-90deg)
            }

            [dir=rtl] .skinsList-yb6e9 .thumbnail-3NKId span {
                transform: scale(1) rotate(90deg)
            }

            .skinsList-yb6e9 .thumbnail-3NKId img {
                width: 6.6rem;
                height: 6.6rem
            }

            [dir=ltr] .skinsList-yb6e9 .thumbnail-3NKId img {
                transform: translate(-50%, 13%) rotate(90deg)
            }

            [dir=rtl] .skinsList-yb6e9 .thumbnail-3NKId img {
                transform: translate(50%, 13%) rotate(-90deg)
            }

            html[dir=ltr][browser="Internet Explorer"] .skinsList-yb6e9 .thumbnail-3NKId img {
                transform: translate(-50%, 50%) rotate(90deg)
            }

            html[dir=rtl][browser="Internet Explorer"] .skinsList-yb6e9 .thumbnail-3NKId img {
                transform: translate(50%, 50%) rotate(-90deg)
            }

            [dir=ltr] .skinsList-yb6e9 .thumbnail-3NKId:hover:not(.isActive-3DIZw) span {
                transform: scale(1.2) rotate(-90deg)
            }

            [dir=rtl] .skinsList-yb6e9 .thumbnail-3NKId:hover:not(.isActive-3DIZw) span {
                transform: scale(1.2) rotate(90deg)
            }

            .sectionFooter-2htAm {
                flex-direction: row;
                justify-content: center
            }

            [dir] .sectionFooter-2htAm {
                margin-top: 7.5rem
            }

            [dir] .sectionFooter-2htAm div:first-child {
                margin-top: 0
            }

            [dir=ltr] .sectionFooter-2htAm div:first-child {
                margin-right: 2.5rem
            }

            [dir=rtl] .sectionFooter-2htAm div:first-child {
                margin-left: 2.5rem
            }
        }

        .titleCopyWrapper-1zptg {
            width: 100%;
            position: relative
        }

        .titleCopyWrapper-1zptg .leftLine-1Z4OO {
            height: calc(100% - 1rem);
            width: .3rem;
            display: none;
            position: absolute;
            top: 1rem;
            opacity: 0
        }

        [dir=ltr] .titleCopyWrapper-1zptg .leftLine-1Z4OO {
            left: -.3rem
        }

        [dir=rtl] .titleCopyWrapper-1zptg .leftLine-1Z4OO {
            right: -.3rem
        }

        [dir] .titleCopyWrapper-1zptg .leftLine-1Z4OO.dark-bP-w5 {
            background-color: #141e37
        }

        [dir] .titleCopyWrapper-1zptg .leftLine-1Z4OO.light-3kShh {
            background-color: #fff
        }

        @media (min-width: 1024px) {
            .titleCopyWrapper-1zptg .leftLine-1Z4OO {
                display: block
            }
        }

        .imageHero-3P7j4 {
            position: relative;
            z-index: 1
        }

        .contentWrapper-30mx3 {
            display: flex;
            align-items: flex-end
        }

        .descriptionWrapper-32wDQ {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 3.25rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 3.25rem) 100%, 0 100%);
            position: relative;
            height: 40rem;
            display: flex;
            flex-direction: column;
            justify-content: flex-end
        }

        [dir] .descriptionWrapper-32wDQ {
            padding-bottom: 2.25rem;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: 50%
        }

        [dir=rtl] .descriptionWrapper-32wDQ {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 3.25rem 100%, 0 calc(100% - 3rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 3.25rem 100%, 0 calc(100% - 3rem))
        }

        .descriptionWrapper-32wDQ:before {
            content: "";
            display: block;
            position: absolute;
            height: 100%;
            width: 100%;
            top: 0;
            z-index: 0
        }

        [dir] .descriptionWrapper-32wDQ:before {
            background: #111;
            background: linear-gradient(0deg, #111 -15%, transparent 80%)
        }

        [dir=ltr] .descriptionWrapper-32wDQ:before {
            left: 0
        }

        [dir=rtl] .descriptionWrapper-32wDQ:before {
            right: 0
        }

        .copyWrapper-2lBaz {
            z-index: 1
        }

        .leftLine-3P_l5 {
            height: calc(100% - 1rem);
            width: .3rem;
            display: none;
            position: absolute;
            top: 1rem
        }

        [dir=ltr] .leftLine-3P_l5 {
            left: -.3rem
        }

        [dir=rtl] .leftLine-3P_l5 {
            right: -.3rem
        }

        [dir] .leftLine-3P_l5.light-2ZrzM {
            background-color: #fff
        }

        .mobileDescriptionWrapper-3ObqX {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 3.25rem 100%, 0 calc(100% - 3rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 3.25rem 100%, 0 calc(100% - 3rem))
        }

        [dir] .mobileDescriptionWrapper-3ObqX {
            background-color: #fff;
            padding-top: 2.5rem;
            padding-bottom: 6rem
        }

        [dir=rtl] .mobileDescriptionWrapper-3ObqX {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 3.25rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 3.25rem) 100%, 0 100%)
        }

        @media (min-width: 768px) {
            .descriptionWrapper-32wDQ {
                height: 80rem
            }

            [dir] .descriptionWrapper-32wDQ {
                padding-bottom: 4.5rem
            }

            [dir] .mobileDescriptionWrapper-3ObqX {
                padding-top: 5rem;
                padding-bottom: 12rem
            }

            .copyWrapper-2lBaz {
                max-width: calc(75% - .75rem);
                width: 100%
            }
        }

        @media (min-width: 1024px) {
            .descriptionWrapper-32wDQ {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 6rem), calc(100% - 6.5rem) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 6rem), calc(100% - 6.5rem) 100%, 0 100%);
                height: 77rem
            }

            [dir=rtl] .descriptionWrapper-32wDQ {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 6.5rem 100%, 0 calc(100% - 6rem));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 6.5rem 100%, 0 calc(100% - 6rem))
            }

            [dir] .descriptionWrapper-32wDQ:before {
                background: #111
            }

            [dir=ltr] .descriptionWrapper-32wDQ:before {
                background: linear-gradient(90deg, #111 -15%, transparent 80%)
            }

            [dir=rtl] .descriptionWrapper-32wDQ:before {
                background: linear-gradient(-90deg, #111 -15%, transparent 80%)
            }

            [dir] .desktopDescriptionWrapper-1Px4P {
                margin-bottom: 10rem
            }

            .copyWrapper-2lBaz {
                width: 100%
            }

            .leftLine-3P_l5 {
                display: block
            }

            [dir] .title-1Kl9s {
                margin-bottom: 2.5rem
            }

            .description-2NrWI {
                width: calc(50% - 1.5rem)
            }

            [dir=ltr] .description-2NrWI {
                padding-left: 3rem
            }

            [dir=rtl] .description-2NrWI {
                padding-right: 3rem
            }
        }

        @media (min-width: 1440px) {
            .description-2NrWI {
                width: calc(50% - 1.5rem)
            }
        }

        .circleWrapper-3iJcs {
            position: relative;
            height: 100%
        }

        .circleWrapper-3iJcs:before {
            content: "";
            display: block;
            position: absolute;
            height: 6vw;
            width: 6vw;
            bottom: -1.8%
        }

        [dir] .circleWrapper-3iJcs:before {
            transform-origin: center;
            background-color: #fff
        }

        [dir=ltr] .circleWrapper-3iJcs:before {
            left: 50%;
            transform: translateX(-50%) rotate(45deg)
        }

        [dir=rtl] .circleWrapper-3iJcs:before {
            right: 50%;
            transform: translateX(50%) rotate(-45deg)
        }

        .circleContainer-3BQya {
            height: 100%
        }

        .circle-2l7KC {
            position: relative;
            overflow: visible
        }

        .cls1-1nHU2 {
            fill: #32c8ff;
            stroke: #32c8ff
        }

        .cls2-IvVNl {
            stroke: #32c8ff;
            stroke-miterlimit: 10px;
            stroke-width: 2px
        }

        .circleDetail-2guJk {
            position: absolute;
            height: 15%;
            width: 100%;
            bottom: -11.6%;
            display: flex;
            justify-content: center
        }

        [dir=ltr] .circleDetail-2guJk {
            left: 0
        }

        [dir=rtl] .circleDetail-2guJk {
            right: 0
        }

        @media (min-width: 1024px) {
            .circleWrapper-3iJcs:before {
                display: none
            }
        }

        .contentWrapper-1fAih {
            display: flex;
            flex-direction: column;
            align-items: center
        }

        .championWrapper-fbj44 {
            width: 100%;
            position: relative
        }

        [dir] .championWrapper-fbj44 {
            margin-bottom: 15%
        }

        .championWrapper-fbj44:before {
            content: "";
            display: block;
            width: 100%
        }

        [dir] .championWrapper-fbj44:before {
            padding-bottom: 100%
        }

        .championWrapper-fbj44 .backgroundCircle-1HnlD {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0
        }

        [dir=ltr] .championWrapper-fbj44 .backgroundCircle-1HnlD {
            left: 0
        }

        [dir=rtl] .championWrapper-fbj44 .backgroundCircle-1HnlD {
            right: 0
        }

        .championBackgroundWrapper-EH766 {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0
        }

        [dir=ltr] .championBackgroundWrapper-EH766 {
            left: 0
        }

        [dir=rtl] .championBackgroundWrapper-EH766 {
            right: 0
        }

        .championBackground-1rcA2 {
            height: auto;
            width: 94%;
            position: absolute;
            top: 49.5%;
            overflow: hidden
        }

        [dir] .championBackground-1rcA2 {
            border-radius: 100%
        }

        [dir=ltr] .championBackground-1rcA2 {
            left: 50%;
            transform: translate(-50%, -50%)
        }

        [dir=rtl] .championBackground-1rcA2 {
            right: 50%;
            transform: translate(50%, -50%)
        }

        .animationWrapper-1qJEr {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0
        }

        [dir=ltr] .animationWrapper-1qJEr {
            left: 0
        }

        [dir=rtl] .animationWrapper-1qJEr {
            right: 0
        }

        .championImageWrapper-1tMfm {
            width: 120%;
            height: auto;
            transition: opacity .2s ease-out;
            opacity: 1;
            position: absolute;
            top: 50%
        }

        [dir=ltr] .championImageWrapper-1tMfm {
            left: 50%;
            transform: translate(-50%, -50%)
        }

        [dir=rtl] .championImageWrapper-1tMfm {
            right: 50%;
            transform: translate(50%, -50%)
        }

        .championImageWrapper-1tMfm:before {
            content: "";
            display: block;
            position: relative;
            width: 100%
        }

        [dir] .championImageWrapper-1tMfm:before {
            padding-bottom: 110%
        }

        .imageWrapper-_KMgp {
            height: 100%;
            width: 100%;
            opacity: 0;
            position: absolute;
            top: 0
        }

        [dir=ltr] .imageWrapper-_KMgp {
            left: 0
        }

        [dir=rtl] .imageWrapper-_KMgp {
            right: 0
        }

        .championImage-2DTTY {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0
        }

        [dir=ltr] .championImage-2DTTY {
            left: 0
        }

        [dir=rtl] .championImage-2DTTY {
            right: 0
        }

        .championDataWrapper-3t7Wg {
            display: flex;
            flex-direction: column;
            align-items: center;
            z-index: 2
        }

        .championNameWrapper-2GQ1V {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center
        }

        [dir] .championName-pyV6l {
            margin-bottom: .25rem
        }

        .skinName-1lQhM {
            color: #32c8ff
        }

        .controlsWrapper-3EUem {
            position: relative;
            width: 100%
        }

        [dir] .controlsWrapper-3EUem {
            margin-bottom: 2rem
        }

        .sliderControlsWrapper-kNO2k {
            display: flex;
            justify-content: space-between;
            align-items: center;
            position: absolute;
            width: 100%;
            top: 50%
        }

        [dir] .sliderControlsWrapper-kNO2k {
            padding: 0 10%
        }

        [dir=ltr] .sliderControlsWrapper-kNO2k {
            left: 50%;
            transform: translate(-50%, -50%)
        }

        [dir=rtl] .sliderControlsWrapper-kNO2k {
            right: 50%;
            transform: translate(50%, -50%)
        }

        .ellipseIcon-LDZi2 {
            height: .3rem;
            width: .3rem;
            display: none
        }

        [dir] .ellipseIcon-LDZi2 {
            margin: 0 2rem
        }

        .ellipseIcon-LDZi2 circle {
            fill: #32c8ff
        }

        .sliderButton-1NSDm {
            display: flex;
            justify-content: center;
            align-items: center;
            transition: opacity .3s ease-out, transform .3s ease-out;
            height: 4rem;
            width: 1.1rem
        }

        .sliderButton-1NSDm path {
            stroke: #32c8ff;
            stroke-width: 3px
        }

        [dir=ltr] .sliderButton-1NSDm.prevButton-1UU2n {
            transform: scale(1) rotate(-180deg)
        }

        [dir=rtl] .sliderButton-1NSDm.prevButton-1UU2n {
            transform: scale(1) rotate(180deg);
            transform: scale(1) rotate(0deg)
        }

        [dir] .sliderButton-1NSDm.nextButton-nZmGc {
            transform: scale(1) rotate(0deg)
        }

        [dir=rtl] .sliderButton-1NSDm.nextButton-nZmGc {
            transform: scale(1) rotate(-180deg)
        }

        .championDescription-3JWWa {
            min-height: 7.2rem
        }

        [dir] .championDescription-3JWWa {
            text-align: center
        }

        .championInfoWrapper-21jgd {
            min-height: 15rem
        }

        .thumbnailsWrapper-1CGXP {
            display: flex;
            justify-content: center;
            align-items: center;
            opacity: 0
        }

        [dir] .thumbnailsWrapper-1CGXP {
            margin-bottom: 2rem
        }

        .thumbnail-18oX0 {
            height: 5rem;
            width: 5rem
        }

        [dir] .thumbnail-18oX0 {
            cursor: pointer
        }

        .thumbnail-18oX0 img {
            opacity: .6;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            transition: opacity .2s ease-out
        }

        .thumbnail-18oX0 span {
            display: block;
            overflow: hidden;
            transition: transform .3s ease-out
        }

        [dir] .thumbnail-18oX0 span {
            border-radius: 100%
        }

        [dir] .thumbnail-18oX0:not(.isActive-2hsXD):hover span {
            transform: scale(1.2)
        }

        .thumbnail-18oX0.isActive-2hsXD img {
            opacity: 1
        }

        [dir=ltr] .thumbnail-18oX0:not(:last-child) {
            margin-right: 1.25rem
        }

        [dir=rtl] .thumbnail-18oX0:not(:last-child) {
            margin-left: 1.25rem
        }

        .bulletsWrapper-14sgk {
            display: flex;
            justify-content: center
        }

        [dir] .bulletsWrapper-14sgk {
            margin-top: 2.5rem
        }

        .bulletsWrapper-14sgk .active-AT_ew {
            opacity: 1
        }

        .bulletWrapper-3tGtf {
            width: .75rem;
            transition: opacity .2s ease-out;
            opacity: .4
        }

        [dir=ltr] .bulletWrapper-3tGtf:not(:last-child) {
            margin-right: 1rem
        }

        [dir=rtl] .bulletWrapper-3tGtf:not(:last-child) {
            margin-left: 1rem
        }

        .bulletIcon-2LERj path {
            fill: #32c8ff
        }

        @media (min-width: 768px) {
            [dir] .championName-pyV6l {
                margin-bottom: .5rem
            }

            .championDescription-3JWWa {
                min-height: 11rem
            }

            .championWrapper-fbj44 {
                max-width: 69.2rem
            }

            [dir] .controlsWrapper-3EUem {
                margin-bottom: 4rem
            }

            [dir] .sliderControlsWrapper-kNO2k {
                padding: 0 16%
            }

            .sliderButton-1NSDm {
                width: 2.2rem
            }

            .championInfoWrapper-21jgd {
                min-height: 25rem
            }

            [dir] .thumbnailsWrapper-1CGXP {
                margin-bottom: 4rem
            }

            .thumbnail-18oX0 {
                height: 10rem;
                width: 10rem
            }

            [dir=ltr] .thumbnail-18oX0:not(:last-child) {
                margin-right: 3.5rem
            }

            [dir=rtl] .thumbnail-18oX0:not(:last-child) {
                margin-left: 3.5rem
            }

            [dir] .bulletsWrapper-14sgk {
                margin-top: 5rem
            }

            .bulletWrapper-3tGtf {
                width: 1.5rem
            }

            [dir=ltr] .bulletWrapper-3tGtf:not(:last-child) {
                margin-right: 2rem
            }

            [dir=rtl] .bulletWrapper-3tGtf:not(:last-child) {
                margin-left: 2rem
            }
        }

        @media (min-width: 1024px) {
            .contentWrapper-1fAih {
                display: flex;
                flex-direction: row-reverse;
                justify-content: space-between
            }

            .championWrapper-fbj44 {
                max-width: calc(50% - 1.5rem);
                width: 100%;
                max-width: none
            }

            [dir] .championWrapper-fbj44 {
                margin-bottom: 0
            }

            .championDataWrapper-3t7Wg {
                max-width: calc(58.33333% - 1.25rem);
                width: 100%;
                align-items: flex-start;
                justify-content: flex-end
            }

            [dir=ltr] .championDataWrapper-3t7Wg {
                padding-left: 3rem
            }

            [dir=rtl] .championDataWrapper-3t7Wg {
                padding-right: 3rem
            }

            .titleDetailWrapper-2ZHd- {
                position: relative
            }

            [dir] .titleDetailWrapper-2ZHd- {
                margin-bottom: 9rem
            }

            [dir] .sectionTitle-2W0Uk {
                margin-bottom: 2.5rem
            }

            .sectionDescription-2c_Bq {
                max-width: calc(83.33333% - .5rem);
                width: 100%
            }

            [dir=ltr] .sectionDescription-2c_Bq {
                padding-left: 3rem;
                padding-right: calc(16.66667% + .5rem)
            }

            [dir=rtl] .sectionDescription-2c_Bq {
                padding-right: 3rem;
                padding-left: calc(16.66667% + .5rem)
            }

            .championNameWrapper-2GQ1V {
                align-items: flex-start
            }

            [dir] .championName-pyV6l {
                margin-bottom: .25rem
            }

            .detailsWrapper-1LG50 {
                width: calc(83.33333% - .5rem)
            }

            [dir=ltr] .detailsWrapper-1LG50 {
                padding-right: calc(16.66667% + .5rem)
            }

            [dir=rtl] .detailsWrapper-1LG50 {
                padding-left: calc(16.66667% + .5rem)
            }

            .controlsWrapper-3EUem {
                display: flex;
                justify-content: space-between;
                align-items: center
            }

            [dir] .controlsWrapper-3EUem {
                margin-bottom: 3rem
            }

            .sliderControlsWrapper-kNO2k {
                position: relative;
                width: auto;
                top: auto
            }

            [dir] .sliderControlsWrapper-kNO2k {
                transform: none;
                padding: 0
            }

            [dir=ltr] .sliderControlsWrapper-kNO2k {
                left: auto
            }

            [dir=rtl] .sliderControlsWrapper-kNO2k {
                right: auto
            }

            .ellipseIcon-LDZi2 {
                display: block
            }

            .sliderButton-1NSDm {
                width: 1.8rem
            }

            .sliderButton-1NSDm path {
                stroke-width: 3px
            }

            .championInfoWrapper-21jgd {
                min-height: 17rem
            }

            .thumbnailsWrapper-1CGXP {
                justify-content: flex-start
            }

            [dir] .thumbnailsWrapper-1CGXP {
                margin-bottom: 3rem
            }

            .thumbnail-18oX0 {
                height: 5rem;
                width: 5rem
            }

            [dir=ltr] .thumbnail-18oX0:not(:last-child) {
                margin-right: 1.5rem
            }

            [dir=rtl] .thumbnail-18oX0:not(:last-child) {
                margin-left: 1.5rem
            }

            .championDescription-3JWWa {
                min-height: 9rem
            }

            [dir=ltr] .championDescription-3JWWa {
                text-align: left
            }

            [dir=rtl] .championDescription-3JWWa {
                text-align: right
            }

            .bulletsWrapper-14sgk {
                justify-content: flex-start
            }

            [dir] .bulletsWrapper-14sgk {
                margin-top: 2.5rem
            }

            .bulletWrapper-3tGtf {
                width: 1.1rem
            }
        }

        @media (min-width: 1280px) {
            .championWrapper-fbj44 {
                max-width: calc(50% - 1.5rem);
                width: 100%
            }
        }

        .championOverview-1mc8e {
            z-index: 4;
            overflow: visible
        }

        [dir] .championOverview-1mc8e {
            padding-bottom: 6rem
        }

        .contentWrapper-3Z8WB {
            display: flex;
            flex-direction: column;
            align-items: center
        }

        .titleDetailWrapper-2cMEj {
            position: relative
        }

        [dir] .titleDetailWrapper-2cMEj {
            margin-bottom: 5rem
        }

        [dir] .sectionTitle-1viHd {
            margin-bottom: 1.6rem
        }

        .leftLine-18uLz {
            height: calc(100% - 1rem);
            width: .3rem;
            display: none;
            position: absolute;
            top: 1rem
        }

        [dir=ltr] .leftLine-18uLz {
            left: -.3rem
        }

        [dir=rtl] .leftLine-18uLz {
            right: -.3rem
        }

        [dir] .leftLine-18uLz.dark-1wFrG {
            background-color: #141e37
        }

        [dir] .championCta-2406M {
            margin-top: 4.5rem
        }

        @media (min-width: 768px) {
            [dir] .championOverview-1mc8e {
                padding-bottom: 12rem
            }

            [dir] .sectionTitle-1viHd {
                margin-bottom: 3.2rem
            }

            [dir] .titleDetailWrapper-2cMEj {
                margin-bottom: 10rem
            }

            [dir] .championCta-2406M {
                margin-top: 9rem
            }
        }

        @media (min-width: 1024px) {
            .contentWrapper-3Z8WB {
                flex-direction: column;
                align-items: stretch
            }

            .championCta-2406M {
                align-self: flex-start
            }

            [dir] .championCta-2406M {
                margin-top: 6rem
            }

            [dir=ltr] .championCta-2406M {
                margin-left: 3rem
            }

            [dir=rtl] .championCta-2406M {
                margin-right: 3rem
            }
        }

        .dropdown-2TrDC {
            position: relative;
            justify-content: space-between;
            align-items: center;
            height: 6rem;
            display: flex;
            text-transform: uppercase;
            font-size: 1.4rem;
            color: #111;
            font-family: Spiegel-SemiBold, arial, georgia, sans-serif
        }

        [dir] .dropdown-2TrDC {
            background: transparent;
            cursor: pointer;
            border: 2px solid #111
        }

        [dir=ltr] .dropdown-2TrDC {
            padding-right: 2rem
        }

        [dir=rtl] .dropdown-2TrDC {
            padding-left: 2rem
        }

        .dropdown-2TrDC:before {
            content: "";
            position: absolute;
            top: -2px;
            width: 3px;
            height: 60%;
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 0 80%);
            clip-path: polygon(0 0, 100% 0, 100% 100%, 0 80%)
        }

        [dir] .dropdown-2TrDC:before {
            background: #111
        }

        [dir=ltr] .dropdown-2TrDC:before {
            left: -4px
        }

        [dir=rtl] .dropdown-2TrDC:before {
            right: -4px
        }

        .dropdownWrapper-9ivgN {
            height: 100%;
            width: 100%;
            font-weight: 700;
            display: flex;
            align-items: center;
            color: #111;
            position: relative
        }

        [dir=ltr] .dropdownWrapper-9ivgN > span {
            padding-right: 4rem
        }

        [dir=rtl] .dropdownWrapper-9ivgN > span {
            padding-left: 4rem
        }

        .dropdownOptions-2mbky {
            display: flex;
            flex-direction: column;
            position: absolute;
            width: calc(100% + 4px);
            max-height: 20.75rem;
            overflow-y: auto;
            z-index: 40
        }

        [dir] .dropdownOptions-2mbky {
            border: 2px solid;
            background-color: #fff
        }

        [dir=ltr] .dropdownOptions-2mbky {
            left: 0;
            transform: translateX(-2px)
        }

        [dir=rtl] .dropdownOptions-2mbky {
            right: 0;
            transform: translateX(2px)
        }

        [dir=ltr] .dropdownOptions-2mbky.active-cR7Xh, [dir=rtl] .dropdownOptions-2mbky.active-cR7Xh {
            animation: openDropdown-3p9Ex .2s .1s both
        }

        .dropdownOptions-2mbky[data-open-direction=up] {
            bottom: calc(100% + 2px)
        }

        [dir] .dropdownOptions-2mbky[data-open-direction=up] {
            transform-origin: bottom;
            border-bottom: none
        }

        .dropdownOptions-2mbky[data-open-direction=down] {
            top: calc(100% + 2px)
        }

        [dir] .dropdownOptions-2mbky[data-open-direction=down] {
            border-top: none
        }

        .dropdownOptions-2mbky .option-3IYjG {
            color: #111;
            transition: background-color .1s ease-out, color .1s
        }

        [dir=ltr] .dropdownOptions-2mbky .option-3IYjG {
            padding: 1rem 0 1rem 1.5rem
        }

        [dir=rtl] .dropdownOptions-2mbky .option-3IYjG {
            padding: 1rem 1.5rem 1rem 0
        }

        .dropdownOptions-2mbky .option-3IYjG:hover {
            color: #fff
        }

        [dir] .dropdownOptions-2mbky .option-3IYjG:hover {
            background-color: #32c8ff
        }

        .dropdownOptions-2mbky::-webkit-scrollbar {
            width: .7rem
        }

        [dir=ltr] .dropdownOptions-2mbky::-webkit-scrollbar-track {
            border-left: 2px solid grey
        }

        [dir=rtl] .dropdownOptions-2mbky::-webkit-scrollbar-track {
            border-right: 2px solid grey
        }

        [dir] .dropdownOptions-2mbky::-webkit-scrollbar-thumb {
            background-color: #000
        }

        [dir=ltr] .dropdownOptions-2mbky::-webkit-scrollbar-thumb {
            border-left: 1px solid grey
        }

        [dir=rtl] .dropdownOptions-2mbky::-webkit-scrollbar-thumb {
            border-right: 1px solid grey
        }

        .arrow-2Ri7L {
            height: 1.6rem;
            width: 1.6rem;
            position: absolute;
            top: 50%
        }

        [dir] .arrow-2Ri7L {
            transform: translateY(-50%)
        }

        [dir=ltr] .arrow-2Ri7L {
            right: 0;
            margin-right: 2rem
        }

        [dir=rtl] .arrow-2Ri7L {
            left: 0;
            margin-left: 2rem
        }

        .arrow-2Ri7L svg {
            transition: transform .2s
        }

        [dir=ltr] .arrow-2Ri7L svg {
            transform: rotate(90deg)
        }

        [dir=rtl] .arrow-2Ri7L svg {
            transform: rotate(-90deg);
            transform: rotate(90deg)
        }

        .arrow-2Ri7L svg path {
            stroke: #000
        }

        [dir=ltr] .arrow-2Ri7L.rotateIcon-1kxVl svg {
            transform: rotate(-90deg)
        }

        [dir=rtl] .arrow-2Ri7L.rotateIcon-1kxVl svg {
            transform: rotate(90deg);
            transform: rotate(-90deg)
        }

        @media (min-width: 768px) {
            [dir=ltr] .dropdownOptions-2mbky .option-3IYjG {
                padding: 1rem 0 1rem 3rem
            }

            [dir=rtl] .dropdownOptions-2mbky .option-3IYjG {
                padding: 1rem 3rem 1rem 0
            }
        }

        @media (min-width: 1024px) {
            .dropdown-2TrDC {
                font-size: 1.2rem
            }

            [dir] .dropdownWrapper-9ivgN {
                padding: 2.8rem 0 2.7rem
            }
        }

        @keyframes openDropdown-3p9Ex {
            0% {
                opacity: 0
            }
            to {
                opacity: 1
            }
        }

        [class*=heading-] {
            text-transform: uppercase;
            font-style: italic
        }

        html[lang=ar-ae] [class*=heading-] {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal
        }

        html[lang=fr-fr] [class*=heading-] {
            line-height: 1.1
        }

        html[lang=th-th] [class*=heading-] {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-weight: 700;
            line-height: 1.4;
            font-style: normal
        }

        html[lang=ko-kr] [class*=heading-] {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-style: normal
        }

        html[lang=zh-hk] [class*=heading-], html[lang=zh-tw] [class*=heading-] {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-style: normal;
            font-weight: 700
        }

        html[lang=ru-ru] [class*=heading-] {
            font-family: BeaufortforLOL-Heavy, arial, georgia, sans-serif
        }

        html[lang=pt-br] [class*=heading-] {
            line-height: 1.18
        }

        html[lang=vi-vn] [class*=heading-] {
            font-family: RobotoCondensed-Bold, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            letter-spacing: 1px;
            line-height: 1.26
        }

        html[lang=ja-jp] [class*=heading-] {
            font-family: BeaufortforLOLJa-Bold, GT-America-Compressed-Medium, arial, georgia, sans-serif
        }

        [class*=heading-].font-normal-TAq2T {
            font-style: normal
        }

        html[lang=th-th] [class*=label-] {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-weight: 700;
            line-height: 1.4;
            font-style: normal
        }

        html[lang=ar-ae] [class*=label-] {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal
        }

        html[lang=ru-ru] [class*=label-] {
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif
        }

        html[lang=ko-kr] [class*=label-] {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif
        }

        html[lang=vi-vn] [class*=label-] {
            font-family: RobotoCondensed-Bold, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            letter-spacing: 1px;
            line-height: 1.18
        }

        html[lang=zh-hk] [class*=label-], html[lang=zh-tw] [class*=label-] {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-style: normal;
            font-weight: 500
        }

        html[lang=ja-jp] [class*=label-] {
            font-family: BeaufortforLOLJa-Regular, GT-America-Compressed-Medium, arial, georgia, sans-serif
        }

        [class*=label-].font-normal-TAq2T {
            font-style: normal
        }

        html[lang=th-th] [class*=copy-] {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif
        }

        html[lang=ar-ae] [class*=copy-] {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal
        }

        html[lang=ru-ru] [class*=copy-] {
            font-family: Spiegel-Regular, arial, georgia, sans-serif
        }

        html[lang=ko-kr] [class*=copy-] {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            word-break: keep-all
        }

        html[lang=vi-vn] [class*=copy-] {
            font-family: RobotoCondensed-Regular, Spiegel-Regular, arial, georgia, sans-serif
        }

        html[lang=zh-hk] [class*=copy-], html[lang=zh-tw] [class*=copy-] {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-weight: 400
        }

        html[lang=ja-jp] [class*=copy-] {
            font-family: SpiegelJa-Regular, Spiegel-Regular, arial, georgia, sans-serif;
            line-height: 1.3
        }

        .heading-01-aiwDl, .heading-08-1mB70 {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-size: 4.5rem;
            line-height: .95;
            letter-spacing: 2px
        }

        html[lang=ja-jp] .heading-01-aiwDl, html[lang=ja-jp] .heading-08-1mB70 {
            font-weight: 700;
            font-size: 3.5rem;
            line-height: 1.1
        }

        html[lang=tr-tr] .heading-01-aiwDl, html[lang=tr-tr] .heading-08-1mB70 {
            line-height: 1.2
        }

        html[lang=ru-ru] .heading-01-aiwDl, html[lang=ru-ru] .heading-08-1mB70 {
            font-family: BeaufortforLOL-Heavy, arial, georgia, sans-serif;
            line-height: 1
        }

        html[lang=zh-hk] .heading-01-aiwDl, html[lang=zh-hk] .heading-08-1mB70, html[lang=zh-tw] .heading-01-aiwDl, html[lang=zh-tw] .heading-08-1mB70 {
            line-height: 1.1
        }

        html[lang=ko-kr] .heading-01-aiwDl, html[lang=ko-kr] .heading-08-1mB70 {
            font-size: 3.5rem;
            line-height: 1.1
        }

        html[lang=ar-ae] .heading-01-aiwDl, html[lang=ar-ae] .heading-08-1mB70 {
            font-weight: 700;
            line-height: 1.6
        }

        html[lang=vi-vn] .heading-01-aiwDl, html[lang=vi-vn] .heading-08-1mB70 {
            font-size: 3.5rem
        }

        html[lang=es-es] .heading-01-aiwDl, html[lang=es-es] .heading-08-1mB70, html[lang=es-mx] .heading-01-aiwDl, html[lang=es-mx] .heading-08-1mB70 {
            line-height: 1.05
        }

        .heading-02-gk_UG {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-size: 2.6rem;
            line-height: 1.04;
            letter-spacing: 2px
        }

        html[lang=ja-jp] .heading-02-gk_UG {
            font-size: 2rem;
            line-height: 1.1
        }

        html[lang=ru-ru] .heading-02-gk_UG {
            font-size: 2.4rem;
            line-height: 1.2
        }

        .heading-03-1f0tA {
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif;
            font-size: 1.4rem;
            line-height: 1.17
        }

        .heading-04-1S7PV {
            font-family: BeaufortforLOL-Heavy, arial, georgia, sans-serif;
            font-size: 1.8rem
        }

        html[lang=ja-jp] .heading-04-1S7PV {
            line-height: 1.6
        }

        .heading-05-3H0PL {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-size: 1.8rem;
            letter-spacing: 1px
        }

        .heading-06-33sB0, .heading-09-17fY4 {
            font-family: Spiegel-SemiBold, arial, georgia, sans-serif;
            font-size: 1.3rem;
            letter-spacing: 1px
        }

        .heading-07-3PJ5y {
            font-size: 2.6rem;
            letter-spacing: 1px
        }

        .heading-07-3PJ5y, .heading-10-3sCOv {
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif
        }

        .heading-10-3sCOv {
            font-size: 1.4rem
        }

        .copy-01-HKrDL, .copy-03-3qbr6 {
            font-family: Spiegel-Regular, arial, georgia, sans-serif;
            font-size: 1.3rem;
            line-height: 1.42
        }

        .copy-02-dcmi9, .label-03-3vYMq {
            font-family: Spiegel-Regular, arial, georgia, sans-serif;
            font-size: 1rem;
            line-height: 1.5
        }

        .copy-03-3qbr6 {
            font-family: Spiegel-SemiBold, arial, georgia, sans-serif
        }

        .label-01-1Rnrm {
            text-transform: uppercase;
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-style: italic;
            font-size: 1.8rem;
            line-height: 1.27;
            letter-spacing: 1px
        }

        html[lang=ar-ae] .label-01-1Rnrm {
            font-weight: 700
        }

        .label-02-3xMsz {
            text-transform: uppercase;
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif;
            font-size: 1.4rem;
            letter-spacing: 1px
        }

        html[lang=ar-ae] .label-02-3xMsz {
            font-weight: 700
        }

        .dropdownStyles-2R-oD span, .label-04-1_zGL {
            font-family: Spiegel-SemiBold, arial, georgia, sans-serif;
            font-size: 1rem
        }

        .dropdownStyles-2R-oD html[lang=ar-ae] span, html[lang=ar-ae] .dropdownStyles-2R-oD span, html[lang=ar-ae] .label-04-1_zGL {
            font-weight: 500
        }

        .label-05-1woq7 {
            text-transform: uppercase;
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif;
            font-size: 1rem
        }

        @media (min-width: 768px) {
            .heading-01-aiwDl, .heading-08-1mB70 {
                font-size: 9rem
            }

            html[lang=ja-jp] .heading-01-aiwDl, html[lang=ja-jp] .heading-08-1mB70 {
                font-size: 6.5rem
            }

            html[lang=ko-kr] .heading-01-aiwDl, html[lang=ko-kr] .heading-08-1mB70, html[lang=vi-vn] .heading-01-aiwDl, html[lang=vi-vn] .heading-08-1mB70 {
                font-size: 7rem
            }

            .heading-02-gk_UG {
                font-size: 5.2rem
            }

            html[lang=ja-jp] .heading-02-gk_UG {
                font-size: 4rem;
                line-height: 1.1
            }

            html[lang=ru-ru] .heading-02-gk_UG {
                font-size: 4rem
            }

            .heading-03-1f0tA, .heading-10-3sCOv, .label-02-3xMsz {
                font-size: 2.8rem
            }

            .heading-04-1S7PV, .heading-05-3H0PL {
                font-size: 3.6rem
            }

            .copy-01-HKrDL, .copy-03-3qbr6, .heading-06-33sB0, .heading-09-17fY4 {
                font-size: 2.6rem
            }

            .heading-07-3PJ5y {
                font-size: 5.2rem
            }

            .copy-02-dcmi9, .dropdownStyles-2R-oD span, .label-03-3vYMq, .label-04-1_zGL, .label-05-1woq7 {
                font-size: 2rem
            }

            .label-01-1Rnrm {
                font-size: 3.6rem;
                line-height: 1
            }

            html[lang=ar-ae] .label-01-1Rnrm {
                line-height: 1.1
            }
        }

        @media (min-width: 1024px) {
            .heading-01-aiwDl, .heading-08-1mB70 {
                font-size: 11rem;
                line-height: .9;
                font-size: 85px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            .heading-01-aiwDl, .heading-08-1mB70 {
                font-size: calc(-15px + 9.76563vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            .heading-01-aiwDl, .heading-08-1mB70 {
                font-size: 110px
            }
        }

        @media (min-width: 1024px) {
            html[lang=zh-tw] .heading-01-aiwDl, html[lang=zh-tw] .heading-08-1mB70 {
                font-size: 80px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=zh-tw] .heading-01-aiwDl, html[lang=zh-tw] .heading-08-1mB70 {
                font-size: calc(40px + 3.90625vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=zh-tw] .heading-01-aiwDl, html[lang=zh-tw] .heading-08-1mB70 {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=zh-hk] .heading-01-aiwDl, html[lang=zh-hk] .heading-08-1mB70 {
                font-size: 80px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=zh-hk] .heading-01-aiwDl, html[lang=zh-hk] .heading-08-1mB70 {
                font-size: calc(40px + 3.90625vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=zh-hk] .heading-01-aiwDl, html[lang=zh-hk] .heading-08-1mB70 {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=pt-br] .heading-01-aiwDl, html[lang=pt-br] .heading-08-1mB70 {
                font-size: 75px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=pt-br] .heading-01-aiwDl, html[lang=pt-br] .heading-08-1mB70 {
                font-size: calc(-25px + 9.76563vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=pt-br] .heading-01-aiwDl, html[lang=pt-br] .heading-08-1mB70 {
                font-size: 100px
            }
        }

        @media (min-width: 1024px) {
            html[lang=es-es] .heading-01-aiwDl, html[lang=es-es] .heading-08-1mB70, html[lang=es-mx] .heading-01-aiwDl, html[lang=es-mx] .heading-08-1mB70 {
                line-height: 1.05
            }

            html[lang=id-id] .heading-01-aiwDl, html[lang=id-id] .heading-08-1mB70 {
                font-size: 50px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=id-id] .heading-01-aiwDl, html[lang=id-id] .heading-08-1mB70 {
                font-size: calc(-90px + 13.67188vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=id-id] .heading-01-aiwDl, html[lang=id-id] .heading-08-1mB70 {
                font-size: 85px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ms-my] .heading-01-aiwDl, html[lang=ms-my] .heading-08-1mB70 {
                font-size: 80px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ms-my] .heading-01-aiwDl, html[lang=ms-my] .heading-08-1mB70 {
                font-size: calc(20px + 5.85938vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ms-my] .heading-01-aiwDl, html[lang=ms-my] .heading-08-1mB70 {
                font-size: 95px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ko-kr] .heading-01-aiwDl, html[lang=ko-kr] .heading-08-1mB70 {
                font-size: 80px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ko-kr] .heading-01-aiwDl, html[lang=ko-kr] .heading-08-1mB70 {
                font-size: calc(40px + 3.90625vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ko-kr] .heading-01-aiwDl, html[lang=ko-kr] .heading-08-1mB70 {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ru-ru] .heading-01-aiwDl, html[lang=ru-ru] .heading-08-1mB70 {
                font-size: 50px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ru-ru] .heading-01-aiwDl, html[lang=ru-ru] .heading-08-1mB70 {
                font-size: calc(-30px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ru-ru] .heading-01-aiwDl, html[lang=ru-ru] .heading-08-1mB70 {
                font-size: 70px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ja-jp] .heading-01-aiwDl, html[lang=ja-jp] .heading-08-1mB70 {
                font-size: 60px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ja-jp] .heading-01-aiwDl, html[lang=ja-jp] .heading-08-1mB70 {
                font-size: 5.85938vw
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ja-jp] .heading-01-aiwDl, html[lang=ja-jp] .heading-08-1mB70 {
                font-size: 75px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ar-ae] .heading-01-aiwDl, html[lang=ar-ae] .heading-08-1mB70 {
                font-size: 60px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ar-ae] .heading-01-aiwDl, html[lang=ar-ae] .heading-08-1mB70 {
                font-size: calc(-20px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ar-ae] .heading-01-aiwDl, html[lang=ar-ae] .heading-08-1mB70 {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            html[lang=th-th] .heading-01-aiwDl, html[lang=th-th] .heading-08-1mB70 {
                font-size: 70px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=th-th] .heading-01-aiwDl, html[lang=th-th] .heading-08-1mB70 {
                font-size: calc(-10px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=th-th] .heading-01-aiwDl, html[lang=th-th] .heading-08-1mB70 {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=vi-vn] .heading-01-aiwDl, html[lang=vi-vn] .heading-08-1mB70 {
                font-size: 60px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=vi-vn] .heading-01-aiwDl, html[lang=vi-vn] .heading-08-1mB70 {
                font-size: calc(-20px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=vi-vn] .heading-01-aiwDl, html[lang=vi-vn] .heading-08-1mB70 {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            .heading-02-gk_UG {
                font-size: 4rem
            }

            html[lang=ja-jp] .heading-02-gk_UG {
                font-size: 3rem;
                line-height: 1.1
            }

            html[lang=ru-ru] .heading-02-gk_UG {
                font-size: 2.8rem
            }

            .heading-03-1f0tA, .heading-06-33sB0, .heading-09-17fY4, .label-02-3xMsz, .label-05-1woq7 {
                font-size: 1.4rem
            }

            .heading-04-1S7PV {
                font-size: 2.4rem
            }

            .heading-05-3H0PL, .heading-10-3sCOv, .label-01-1Rnrm {
                font-size: 1.9rem
            }

            .heading-07-3PJ5y {
                font-size: 3rem
            }

            .heading-08-1mB70 {
                font-size: 8rem;
                font-size: 70px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            .heading-08-1mB70 {
                font-size: calc(30px + 3.90625vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            .heading-08-1mB70 {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ja-jp] .heading-08-1mB70 {
                font-size: 55px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ja-jp] .heading-08-1mB70 {
                font-size: calc(-5px + 5.85938vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ja-jp] .heading-08-1mB70 {
                font-size: 70px
            }
        }

        @media (min-width: 1024px) {
            html[lang=th-th] .heading-08-1mB70 {
                font-size: 60px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=th-th] .heading-08-1mB70 {
                font-size: calc(-20px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=th-th] .heading-08-1mB70 {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            html[lang=id-id] .heading-08-1mB70 {
                font-size: 45px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=id-id] .heading-08-1mB70 {
                font-size: calc(-95px + 13.67188vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=id-id] .heading-08-1mB70 {
                font-size: 80px
            }
        }

        @media (min-width: 1024px) {
            html[lang=ms-my] .heading-08-1mB70 {
                font-size: 75px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=ms-my] .heading-08-1mB70 {
                font-size: calc(15px + 5.85938vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=ms-my] .heading-08-1mB70 {
                font-size: 90px
            }
        }

        @media (min-width: 1024px) {
            html[lang=vi-vn] .heading-08-1mB70 {
                font-size: 55px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            html[lang=vi-vn] .heading-08-1mB70 {
                font-size: calc(-25px + 7.8125vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1280px) {
            html[lang=vi-vn] .heading-08-1mB70 {
                font-size: 75px
            }
        }

        @media (min-width: 1024px) {
            .copy-01-HKrDL, .copy-03-3qbr6, .heading-09-17fY4 {
                font-size: 1.6rem
            }

            .copy-02-dcmi9, .label-03-3vYMq {
                font-size: 1rem;
                line-height: 1.6
            }

            .label-03-3vYMq {
                font-size: 1.6rem
            }

            .dropdownStyles-2R-oD span, .label-04-1_zGL {
                font-size: 1rem
            }
        }

        .championFilter-1RLf5 {
            overflow: visible
        }

        [dir] .championFilter-1RLf5 {
            padding-top: 5rem
        }

        @media (min-width: 768px) {
            [dir] .championFilter-1RLf5 {
                padding-top: 10rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .championFilter-1RLf5 {
                padding-top: 12rem
            }
        }

        .dashDetailWrapper-1OVK5 {
            top: 0
        }

        @media (min-width: 1024px) {
            .dashDetailWrapper-1OVK5 {
                height: calc(100% - 4rem)
            }
        }

        .eyebrow-2arGy {
            visibility: hidden
        }

        [dir] .titleWrapper-2PvKV {
            margin-bottom: 3.5rem
        }

        @media (min-width: 768px) {
            [dir] .titleWrapper-2PvKV {
                margin-bottom: 7rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .titleWrapper-2PvKV {
                margin-bottom: 10rem
            }
        }

        .description-3vD8h {
            visibility: hidden
        }

        @media (min-width: 1024px) {
            .description-3vD8h {
                max-width: calc(41.66667% - 1.75rem);
                width: 100%
            }
        }

        .filterWrapper-HKw_a {
            display: flex;
            align-items: center;
            justify-content: space-between;
            height: 4rem;
            visibility: hidden
        }

        [dir] .filterWrapper-HKw_a {
            margin-bottom: 2.25rem
        }

        @media (min-width: 768px) {
            .filterWrapper-HKw_a {
                height: 8rem
            }

            [dir] .filterWrapper-HKw_a {
                margin-bottom: 4.5rem
            }
        }

        @media (min-width: 1024px) {
            .filterWrapper-HKw_a {
                height: 6rem
            }

            [dir] .filterWrapper-HKw_a {
                margin-bottom: 4rem
            }
        }

        .dashDetail-3Wwsh {
            height: 2.25rem;
            width: .3rem;
            position: absolute;
            top: 0
        }

        [dir] .dashDetail-3Wwsh {
            background: #141e37
        }

        [dir=ltr] .dashDetail-3Wwsh {
            left: -.3rem
        }

        [dir=rtl] .dashDetail-3Wwsh {
            right: -.3rem
        }

        .dashDetail-3Wwsh:after {
            content: "";
            display: block;
            position: absolute;
            bottom: -.3rem
        }

        [dir] .dashDetail-3Wwsh:after {
            border-top: .3rem solid #141e37
        }

        [dir=ltr] .dashDetail-3Wwsh:after {
            right: 0;
            border-left: .3rem solid transparent
        }

        [dir=rtl] .dashDetail-3Wwsh:after {
            left: 0;
            border-right: .3rem solid transparent
        }

        @media (min-width: 768px) {
            .dashDetail-3Wwsh {
                height: 4.5rem;
                width: .6rem
            }

            [dir] .dashDetail-3Wwsh {
                background: #141e37
            }

            [dir=ltr] .dashDetail-3Wwsh {
                left: -.6rem
            }

            [dir=rtl] .dashDetail-3Wwsh {
                right: -.6rem
            }

            .dashDetail-3Wwsh:after {
                content: "";
                display: block;
                position: absolute;
                bottom: -.6rem
            }

            [dir] .dashDetail-3Wwsh:after {
                border-top: .6rem solid #141e37
            }

            [dir=ltr] .dashDetail-3Wwsh:after {
                right: 0;
                border-left: .6rem solid transparent
            }

            [dir=rtl] .dashDetail-3Wwsh:after {
                left: 0;
                border-right: .6rem solid transparent
            }
        }

        @media (min-width: 1024px) {
            .dashDetail-3Wwsh {
                height: 4rem;
                width: .4rem
            }

            [dir] .dashDetail-3Wwsh {
                background: #141e37
            }

            [dir=ltr] .dashDetail-3Wwsh {
                left: -.4rem
            }

            [dir=rtl] .dashDetail-3Wwsh {
                right: -.4rem
            }

            .dashDetail-3Wwsh:after {
                content: "";
                display: block;
                position: absolute;
                bottom: -.4rem
            }

            [dir] .dashDetail-3Wwsh:after {
                border-top: .4rem solid #141e37
            }

            [dir=ltr] .dashDetail-3Wwsh:after {
                right: 0;
                border-left: .4rem solid transparent
            }

            [dir=rtl] .dashDetail-3Wwsh:after {
                left: 0;
                border-right: .4rem solid transparent
            }
        }

        .roleSelectionWrapper-2hpGL {
            position: relative;
            height: 100%;
            width: 50%
        }

        @media (min-width: 1024px) {
            .roleSelectionWrapper-2hpGL {
                height: 100%;
                width: 100%
            }
        }

        .desktopRolesWrapper-9rH_- {
            display: flex;
            height: 100%;
            overflow: hidden
        }

        [dir] .desktopRolesWrapper-9rH_- {
            padding: 0 3rem
        }

        .roleItemWrapper-kmCmr {
            position: relative;
            display: flex
        }

        [dir=ltr] .roleItemWrapper-kmCmr:not(:last-child) {
            margin-right: 5rem
        }

        [dir=rtl] .roleItemWrapper-kmCmr:not(:last-child) {
            margin-left: 5rem
        }

        .roleItemWrapper-kmCmr:after {
            content: "";
            display: block;
            position: absolute;
            height: .5rem;
            width: 100%;
            bottom: 0;
            opacity: 0;
            transition: transform .2s ease-out, opacity .1s ease-out
        }

        [dir] .roleItemWrapper-kmCmr:after {
            transform-origin: center;
            transform: scaleX(0);
            background-color: #32c8ff
        }

        [dir=ltr] .roleItemWrapper-kmCmr:after {
            left: 0
        }

        [dir=rtl] .roleItemWrapper-kmCmr:after {
            right: 0
        }

        .roleItemWrapper-kmCmr.isActive-1mdEt:after {
            opacity: 1
        }

        [dir] .roleItemWrapper-kmCmr.isActive-1mdEt:after {
            transform: scaleX(1)
        }

        [dir] .roleItemWrapper-kmCmr:hover:not(.isActive-1mdEt) .filterOption-3mwRs {
            transform: translateY(-10%)
        }

        .roleItemWrapper-kmCmr:hover:not(.isActive-1mdEt):after {
            opacity: 1
        }

        [dir] .roleItemWrapper-kmCmr:hover:not(.isActive-1mdEt):after {
            transform: scaleX(.5)
        }

        .filterOption-3mwRs {
            text-transform: uppercase;
            opacity: .4;
            transition: opacity .2s ease-out, transform .2s ease-out
        }

        .isActive-1mdEt .filterOption-3mwRs {
            opacity: 1
        }

        .difficultySelectionWrapper-2LRez {
            position: relative;
            width: 50%;
            height: 100%
        }

        @media (min-width: 1024px) {
            .difficultySelectionWrapper-2LRez {
                width: 27rem
            }
        }

        .difficultyDropdown-1ZqqX, .roleDropdown-2gD64 {
            height: 100%;
            width: 100%;
            position: relative
        }

        [dir] .difficultyDropdown-1ZqqX, [dir] .roleDropdown-2gD64 {
            border: none
        }

        [dir=ltr] .difficultyDropdown-1ZqqX, [dir=ltr] .roleDropdown-2gD64, [dir=rtl] .difficultyDropdown-1ZqqX, [dir=rtl] .roleDropdown-2gD64 {
            padding-left: 1.5rem;
            padding-right: 1.5rem
        }

        .difficultyDropdown-1ZqqX:before, .roleDropdown-2gD64:before {
            display: none
        }

        @media (min-width: 768px) {
            [dir=ltr] .difficultyDropdown-1ZqqX, [dir=ltr] .roleDropdown-2gD64, [dir=rtl] .difficultyDropdown-1ZqqX, [dir=rtl] .roleDropdown-2gD64 {
                padding-left: 3rem;
                padding-right: 3rem
            }
        }

        [dir=ltr] .difficultyDropdown-1ZqqX {
            padding-right: .25rem
        }

        [dir=rtl] .difficultyDropdown-1ZqqX {
            padding-left: .25rem
        }

        @media (min-width: 768px) {
            [dir=ltr] .difficultyDropdown-1ZqqX {
                padding-right: 2.5rem
            }

            [dir=rtl] .difficultyDropdown-1ZqqX {
                padding-left: 2.5rem
            }
        }

        .dropdownStyles-2R-oD span {
            font-weight: 400
        }

        .difficultyDropdownOptionsStyles-2FsCQ, .roleDropdownOptionsStyles-RwkFr {
            top: 0
        }

        [dir] .difficultyDropdownOptionsStyles-2FsCQ, [dir] .roleDropdownOptionsStyles-RwkFr {
            border: 1px solid #141e37
        }

        .difficultyDropdownOptionsStyles-2FsCQ[data-open-direction=down], .roleDropdownOptionsStyles-RwkFr[data-open-direction=down] {
            top: 100%
        }

        [dir] .difficultyDropdownOptionsStyles-2FsCQ[data-open-direction=down], [dir] .roleDropdownOptionsStyles-RwkFr[data-open-direction=down] {
            transform: translateX(0)
        }

        .roleDropdownOptionsStyles-RwkFr {
            width: calc(100% + 1px)
        }

        .difficultyDropdownOptionsStyles-2FsCQ {
            width: 94.7%
        }

        @media (min-width: 1024px) {
            .difficultyDropdownOptionsStyles-2FsCQ {
                width: calc(100% - 1.2rem)
            }
        }

        .difficultiesDropdownFrame-1emY5, .rolesFrame-1F74c {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0
        }

        [dir=ltr] .difficultiesDropdownFrame-1emY5, [dir=ltr] .rolesFrame-1F74c {
            left: 0
        }

        [dir=rtl] .difficultiesDropdownFrame-1emY5, [dir=rtl] .rolesFrame-1F74c {
            right: 0
        }

        .rolesFrame-1F74c {
            height: 100%;
            width: calc(100% + .1rem)
        }

        .rolesFrame-1F74c, .rolesFrame-1F74c path {
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden
        }

        html[dir][lang=ar-ae] .difficultiesDropdownFrame-1emY5 {
            transform: scaleX(-1)
        }

        .animationWrapper-3hSJ5 {
            visibility: hidden
        }

        .championListCardWrapper-3Kf-3 {
            height: 100%;
            width: 100%
        }

        @media (min-width: 1024px) {
            [dir] .championListCardWrapper-3Kf-3:hover .dashDetail-3ZoIM {
                transform: scaleX(1.2)
            }

            [dir] .championListCardWrapper-3Kf-3:hover .championImage-2lwUs, [dir] .championListCardWrapper-3Kf-3:hover .ieChampionImage-3QcHc {
                transform: scale(1.1)
            }

            [dir] .championListCardWrapper-3Kf-3:hover .infoWrapper-2xJyP {
                background-color: #141e37
            }

            [dir=ltr] .championListCardWrapper-3Kf-3:hover .infoWrapper-2xJyP:after {
                transform: translate(6rem, 6rem) rotate(45deg)
            }

            [dir=rtl] .championListCardWrapper-3Kf-3:hover .infoWrapper-2xJyP:after {
                transform: translate(-6rem, 6rem) rotate(-45deg)
            }

            [dir=ltr] .championListCardWrapper-3Kf-3:hover .championName-2ThUT {
                transform: translateX(15%)
            }

            [dir=rtl] .championListCardWrapper-3Kf-3:hover .championName-2ThUT {
                transform: translateX(-15%)
            }
        }

        .championListCardWrapper-3Kf-3 .dashDetail-3ZoIM {
            position: absolute;
            top: -.5rem;
            transition: transform .4s ease-out;
            height: 1rem;
            width: 35%
        }

        [dir] .championListCardWrapper-3Kf-3 .dashDetail-3ZoIM {
            background: #32c8ff
        }

        [dir=ltr] .championListCardWrapper-3Kf-3 .dashDetail-3ZoIM {
            left: 0;
            transform-origin: left
        }

        [dir=rtl] .championListCardWrapper-3Kf-3 .dashDetail-3ZoIM {
            right: 0;
            transform-origin: right
        }

        .championListCardWrapper-3Kf-3 .dashDetail-3ZoIM:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .championListCardWrapper-3Kf-3 .dashDetail-3ZoIM:after {
            border-bottom: 1rem solid #32c8ff
        }

        [dir=ltr] .championListCardWrapper-3Kf-3 .dashDetail-3ZoIM:after {
            right: -1rem;
            border-right: 1rem solid transparent
        }

        [dir=rtl] .championListCardWrapper-3Kf-3 .dashDetail-3ZoIM:after {
            left: -1rem;
            border-left: 1rem solid transparent
        }

        html[dir=ltr][browser=Safari] .championListCardWrapper-3Kf-3 .dashDetail-3ZoIM:after {
            right: -.9rem
        }

        html[dir=rtl][browser=Safari] .championListCardWrapper-3Kf-3 .dashDetail-3ZoIM:after {
            left: -.9rem
        }

        html[dir=ltr][browser=Firefox] .championListCardWrapper-3Kf-3 .dashDetail-3ZoIM:after {
            right: -.9rem
        }

        html[dir=rtl][browser=Firefox] .championListCardWrapper-3Kf-3 .dashDetail-3ZoIM:after {
            left: -.9rem
        }

        .championListCardWrapper-3Kf-3 .ieChampionImageWrapper-1urQJ {
            position: relative;
            width: 100%;
            overflow: hidden;
            display: none
        }

        html[browser="Internet Explorer"] .championListCardWrapper-3Kf-3 .ieChampionImageWrapper-1urQJ {
            display: block
        }

        .championListCardWrapper-3Kf-3 .ieChampionImageWrapper-1urQJ:before {
            display: block;
            content: "";
            width: 100%
        }

        [dir] .championListCardWrapper-3Kf-3 .ieChampionImageWrapper-1urQJ:before {
            padding-top: 114%
        }

        .championListCardWrapper-3Kf-3 .ieChampionImage-3QcHc {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0;
            transition: transform .2s ease-out
        }

        [dir] .championListCardWrapper-3Kf-3 .ieChampionImage-3QcHc {
            background-position: 50%;
            background-repeat: no-repeat;
            background-size: cover;
            padding-top: 114%
        }

        [dir=ltr] .championListCardWrapper-3Kf-3 .ieChampionImage-3QcHc {
            left: 0
        }

        [dir=rtl] .championListCardWrapper-3Kf-3 .ieChampionImage-3QcHc {
            right: 0
        }

        .championListCardWrapper-3Kf-3 .championImageWrapper-1ykos {
            display: block;
            position: relative;
            width: 100%;
            overflow: hidden
        }

        html[browser="Internet Explorer"] .championListCardWrapper-3Kf-3 .championImageWrapper-1ykos {
            display: none
        }

        .championListCardWrapper-3Kf-3 .championImageWrapper-1ykos:before {
            display: block;
            content: "";
            width: 100%
        }

        [dir] .championListCardWrapper-3Kf-3 .championImageWrapper-1ykos:before {
            padding-top: 114%
        }

        .championListCardWrapper-3Kf-3 .championImage-2lwUs {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0;
            -o-object-position: center;
            object-position: center;
            -o-object-fit: cover;
            object-fit: cover;
            transition: transform .2s ease-out
        }

        [dir=ltr] .championListCardWrapper-3Kf-3 .championImage-2lwUs {
            left: 0
        }

        [dir=rtl] .championListCardWrapper-3Kf-3 .championImage-2lwUs {
            right: 0
        }

        .championListCardWrapper-3Kf-3 .infoWrapper-2xJyP {
            display: flex;
            align-items: center;
            height: 3rem;
            transition: background-color .2s ease-out;
            overflow: hidden
        }

        [dir] .championListCardWrapper-3Kf-3 .infoWrapper-2xJyP {
            padding: 0 1rem;
            background-color: #32c8ff
        }

        .championListCardWrapper-3Kf-3 .infoWrapper-2xJyP:after {
            content: "";
            display: block;
            position: absolute;
            height: 3rem;
            width: 3rem;
            bottom: 0
        }

        [dir] .championListCardWrapper-3Kf-3 .infoWrapper-2xJyP:after {
            transform-origin: center;
            background-color: #fff
        }

        [dir=ltr] .championListCardWrapper-3Kf-3 .infoWrapper-2xJyP:after {
            transform: translate(1.75rem, 1.75rem) rotate(45deg);
            right: 0
        }

        [dir=rtl] .championListCardWrapper-3Kf-3 .infoWrapper-2xJyP:after {
            transform: translate(-1.75rem, 1.75rem) rotate(-45deg);
            left: 0
        }

        @media (min-width: 768px) {
            .championListCardWrapper-3Kf-3 .infoWrapper-2xJyP:after {
                height: 4.2rem;
                width: 4.2rem
            }

            [dir=ltr] .championListCardWrapper-3Kf-3 .infoWrapper-2xJyP:after {
                transform: translate(2.5rem, 2.5rem) rotate(45deg)
            }

            [dir=rtl] .championListCardWrapper-3Kf-3 .infoWrapper-2xJyP:after {
                transform: translate(-2.5rem, 2.5rem) rotate(-45deg)
            }
        }

        @media (min-width: 1024px) {
            .championListCardWrapper-3Kf-3 .infoWrapper-2xJyP:after {
                transition: transform .3s ease-out
            }
        }

        @media (min-width: 768px) {
            .championListCardWrapper-3Kf-3 .infoWrapper-2xJyP {
                height: 6rem
            }

            [dir] .championListCardWrapper-3Kf-3 .infoWrapper-2xJyP {
                padding: 0 2rem
            }
        }

        @media (min-width: 768px) {
            .championListCardWrapper-3Kf-3 .infoWrapper-2xJyP {
                height: 5.5rem
            }
        }

        .championListCardWrapper-3Kf-3 .championName-2ThUT {
            color: #fff;
            transition: transform .4s ease-out
        }

        .championListWrapper-39Lre {
            z-index: 4;
            min-height: 48.6rem
        }

        [dir] .championListWrapper-39Lre {
            background-color: #fff;
            padding-top: 2rem;
            padding-bottom: 7.5rem
        }

        @media (min-width: 768px) {
            .championListWrapper-39Lre {
                min-height: 71.8rem
            }

            [dir] .championListWrapper-39Lre {
                padding-top: 4rem;
                padding-bottom: 15rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .championListWrapper-39Lre {
                padding-top: 7rem;
                padding-bottom: 17rem
            }
        }

        .championList-3vQv2 {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-around
        }

        @media (min-width: 1024px) {
            .championList-3vQv2 {
                justify-content: start
            }
        }

        .championCardItem-576IH {
            width: calc(50% - 2rem);
            position: relative
        }

        [dir] .championCardItem-576IH {
            margin-bottom: 2rem
        }

        [dir=ltr] .championCardItem-576IH {
            margin-left: 2rem
        }

        [dir=rtl] .championCardItem-576IH {
            margin-right: 2rem
        }

        [dir=ltr] .championCardItem-576IH:nth-of-type(odd) {
            margin-left: 0
        }

        [dir=rtl] .championCardItem-576IH:nth-of-type(odd) {
            margin-right: 0
        }

        @media (min-width: 768px) {
            [dir] .championCardItem-576IH {
                margin-bottom: 4rem
            }

            [dir=ltr] .championCardItem-576IH {
                margin-left: 4rem
            }

            [dir=rtl] .championCardItem-576IH {
                margin-right: 4rem
            }
        }

        @media (min-width: 1024px) {
            .championCardItem-576IH {
                width: calc(25% - 3rem)
            }

            [dir] .championCardItem-576IH {
                margin-bottom: 3rem
            }

            [dir=ltr] .championCardItem-576IH {
                margin-left: 3rem
            }

            [dir=rtl] .championCardItem-576IH {
                margin-right: 3rem
            }

            [dir=ltr] .championCardItem-576IH:nth-of-type(odd) {
                margin-left: 3rem
            }

            [dir=rtl] .championCardItem-576IH:nth-of-type(odd) {
                margin-right: 3rem
            }

            [dir=ltr] .championCardItem-576IH:nth-of-type(4n+1) {
                margin-left: 0
            }

            [dir=rtl] .championCardItem-576IH:nth-of-type(4n+1) {
                margin-right: 0
            }
        }

        .cardWrapper-3ucNL {
            height: auto;
            width: 100%;
            position: relative
        }

        [dir] .cardWrapper-3ucNL {
            cursor: pointer
        }

        .cardWrapper-3ucNL .card-2dZ4i {
            height: 100%;
            width: 100%;
            display: flex
        }

        .cardWrapper-3ucNL .copyWrapper-25jPp {
            height: auto;
            width: 54.3%;
            display: flex;
            flex-direction: column
        }

        [dir] .cardWrapper-3ucNL .copyWrapper-25jPp {
            padding: 0 1.25rem 2.5rem
        }

        @media (min-width: 768px) {
            [dir] .cardWrapper-3ucNL .copyWrapper-25jPp {
                padding: 0 2.5rem 3rem
            }
        }

        @media (min-width: 1024px) {
            [dir=ltr] .cardWrapper-3ucNL .copyWrapper-25jPp {
                padding: 0 .8rem 2.25rem 5.9%
            }

            [dir=rtl] .cardWrapper-3ucNL .copyWrapper-25jPp {
                padding: 0 5.9% 2.25rem .8rem
            }
        }

        @media (min-width: 1280px) {
            [dir=ltr] .cardWrapper-3ucNL .copyWrapper-25jPp {
                padding: 0 1rem 2.5rem 5.9%
            }

            [dir=rtl] .cardWrapper-3ucNL .copyWrapper-25jPp {
                padding: 0 5.9% 2.5rem 1rem
            }
        }

        .cardWrapper-3ucNL .championName-SQ-dk {
            font-size: 1rem;
            text-transform: uppercase;
            color: #141e37;
            transition: color .2s ease-out
        }

        [dir] .cardWrapper-3ucNL .championName-SQ-dk {
            padding-bottom: .5rem
        }

        @media (min-width: 768px) {
            .cardWrapper-3ucNL .championName-SQ-dk {
                font-size: 2rem
            }
        }

        @media (min-width: 1024px) {
            .cardWrapper-3ucNL .championName-SQ-dk {
                font-size: 1rem;
                text-transform: uppercase
            }
        }

        .cardWrapper-3ucNL .titleWrapper-24iK5 {
            height: 100%;
            display: flex;
            align-items: center
        }

        .cardWrapper-3ucNL .cardTitle-1tt8u {
            font-size: 1.4rem;
            transition: color .2s ease-out
        }

        [dir] .cardWrapper-3ucNL .cardTitle-1tt8u {
            margin: 0
        }

        @media (min-width: 768px) {
            .cardWrapper-3ucNL .cardTitle-1tt8u {
                font-size: 2.6rem
            }
        }

        @media (min-width: 1024px) {
            .cardWrapper-3ucNL .cardTitle-1tt8u {
                text-transform: uppercase;
                font-size: 12px
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1024px) {
            .cardWrapper-3ucNL .cardTitle-1tt8u {
                font-size: calc(4.61538px + .72115vw)
            }
        }

        @media screen and (min-width: 1024px) and (min-width: 1440px) {
            .cardWrapper-3ucNL .cardTitle-1tt8u {
                font-size: 15px
            }
        }

        .cardWrapper-3ucNL .ieImageWrapper-2c6jW {
            position: relative;
            width: 45.7%;
            align-self: flex-end
        }

        [dir] .cardWrapper-3ucNL .ieImageWrapper-2c6jW {
            background-repeat: no-repeat;
            background-position: 50%;
            background-size: cover
        }

        .cardWrapper-3ucNL .ieImageWrapper-2c6jW:before {
            height: auto;
            width: 100%;
            content: "";
            display: block
        }

        [dir] .cardWrapper-3ucNL .ieImageWrapper-2c6jW:before {
            padding-top: 56.25%
        }

        .cardWrapper-3ucNL .imageWrapper-vrP1q {
            position: relative;
            width: 45.7%;
            flex-shrink: 0;
            overflow: hidden;
            align-self: flex-end
        }

        .cardWrapper-3ucNL .imageWrapper-vrP1q:before {
            height: auto;
            width: 100%;
            content: "";
            display: block
        }

        [dir] .cardWrapper-3ucNL .imageWrapper-vrP1q:before {
            padding-top: 56.25%
        }

        .cardWrapper-3ucNL .image-2XF_G {
            height: auto;
            width: 100%;
            position: absolute;
            top: 0;
            -o-object-fit: cover;
            object-fit: cover;
            -o-object-position: center;
            object-position: center;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            transition: transform .2s ease-out
        }

        [dir=ltr] .cardWrapper-3ucNL .image-2XF_G {
            left: 0
        }

        [dir=rtl] .cardWrapper-3ucNL .image-2XF_G {
            right: 0
        }

        .cardWrapper-3ucNL .border-2CE4d {
            height: 92%;
            width: 88.2%;
            position: absolute;
            bottom: .75rem;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden
        }

        [dir=ltr] .cardWrapper-3ucNL .border-2CE4d {
            left: 11.8%
        }

        [dir=rtl] .cardWrapper-3ucNL .border-2CE4d {
            right: 11.8%
        }

        @media (min-width: 768px) {
            .cardWrapper-3ucNL .border-2CE4d {
                bottom: 1.5rem
            }
        }

        @media (min-width: 1024px) {
            .cardWrapper-3ucNL .border-2CE4d {
                bottom: 1rem
            }
        }

        .cardWrapper-3ucNL .detailBorder-beCpu {
            height: 100%;
            width: 100%;
            position: absolute;
            bottom: 0;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden
        }

        [dir=ltr] .cardWrapper-3ucNL .detailBorder-beCpu {
            left: 0;
            transform: skew(-15deg);
            transform-origin: bottom left
        }

        [dir=rtl] .cardWrapper-3ucNL .detailBorder-beCpu {
            right: 0;
            transform: skew(15deg);
            transform-origin: bottom right
        }

        .cardWrapper-3ucNL .detailBorder-beCpu:after, .cardWrapper-3ucNL .detailBorder-beCpu:before {
            content: "";
            display: block;
            position: absolute;
            bottom: 0
        }

        [dir] .cardWrapper-3ucNL .detailBorder-beCpu:after, [dir] .cardWrapper-3ucNL .detailBorder-beCpu:before {
            background: #fff
        }

        [dir=ltr] .cardWrapper-3ucNL .detailBorder-beCpu:after, [dir=ltr] .cardWrapper-3ucNL .detailBorder-beCpu:before {
            right: 0
        }

        [dir=rtl] .cardWrapper-3ucNL .detailBorder-beCpu:after, [dir=rtl] .cardWrapper-3ucNL .detailBorder-beCpu:before {
            left: 0
        }

        .cardWrapper-3ucNL .detailBorder-beCpu:before {
            height: .2rem;
            width: 100%
        }

        .cardWrapper-3ucNL .detailBorder-beCpu:after {
            height: 100%;
            width: .2rem
        }

        .cardWrapper-3ucNL .dashDetail-321gS {
            height: 50%;
            width: .25rem;
            position: absolute;
            bottom: 0;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden
        }

        [dir] .cardWrapper-3ucNL .dashDetail-321gS {
            background: #fff;
            transform-origin: bottom
        }

        [dir=ltr] .cardWrapper-3ucNL .dashDetail-321gS {
            right: -.2rem
        }

        [dir=rtl] .cardWrapper-3ucNL .dashDetail-321gS {
            left: -.2rem
        }

        .cardWrapper-3ucNL .dashDetail-321gS:after {
            content: "";
            display: block;
            position: absolute;
            top: -.25rem
        }

        [dir] .cardWrapper-3ucNL .dashDetail-321gS:after {
            border-bottom: .25rem solid #fff
        }

        [dir=ltr] .cardWrapper-3ucNL .dashDetail-321gS:after {
            right: 0;
            border-right: .25rem solid transparent
        }

        [dir=rtl] .cardWrapper-3ucNL .dashDetail-321gS:after {
            left: 0;
            border-left: .25rem solid transparent
        }

        @media (min-width: 768px) {
            .cardWrapper-3ucNL .dashDetail-321gS {
                height: 50%;
                width: .5rem
            }

            [dir] .cardWrapper-3ucNL .dashDetail-321gS {
                background: #fff
            }

            [dir=ltr] .cardWrapper-3ucNL .dashDetail-321gS {
                right: -.35rem
            }

            [dir=rtl] .cardWrapper-3ucNL .dashDetail-321gS {
                left: -.35rem
            }

            .cardWrapper-3ucNL .dashDetail-321gS:after {
                content: "";
                display: block;
                position: absolute;
                top: -.5rem
            }

            [dir] .cardWrapper-3ucNL .dashDetail-321gS:after {
                border-bottom: .5rem solid #fff
            }

            [dir=ltr] .cardWrapper-3ucNL .dashDetail-321gS:after {
                right: 0;
                border-right: .5rem solid transparent
            }

            [dir=rtl] .cardWrapper-3ucNL .dashDetail-321gS:after {
                left: 0;
                border-left: .5rem solid transparent
            }
        }

        @media (min-width: 1024px) {
            .cardWrapper-3ucNL .dashDetail-321gS {
                height: 45%;
                width: .3rem
            }

            [dir] .cardWrapper-3ucNL .dashDetail-321gS {
                background: #fff
            }

            [dir=ltr] .cardWrapper-3ucNL .dashDetail-321gS {
                right: -.25rem
            }

            [dir=rtl] .cardWrapper-3ucNL .dashDetail-321gS {
                left: -.25rem
            }

            .cardWrapper-3ucNL .dashDetail-321gS:after {
                content: "";
                display: block;
                position: absolute;
                top: -.3rem
            }

            [dir] .cardWrapper-3ucNL .dashDetail-321gS:after {
                border-bottom: .3rem solid #fff
            }

            [dir=ltr] .cardWrapper-3ucNL .dashDetail-321gS:after {
                right: 0;
                border-right: .3rem solid transparent
            }

            [dir=rtl] .cardWrapper-3ucNL .dashDetail-321gS:after {
                left: 0;
                border-left: .3rem solid transparent
            }
        }

        .cardWrapper-3ucNL:hover .championName-SQ-dk {
            color: #fff
        }

        .cardWrapper-3ucNL:hover .cardTitle-1tt8u {
            color: #141e37
        }

        [dir] .cardWrapper-3ucNL:hover .image-2XF_G {
            transform: scale(1.2)
        }

        [dir] .contentWrapper-oyGa0 {
            background: #551ed7;
            padding: 1.7rem 0 2.5rem
        }

        [dir=ltr] .contentWrapper-oyGa0 {
            background: linear-gradient(90deg, #551ed7 -10%, #6e87ff 80%)
        }

        [dir=rtl] .contentWrapper-oyGa0 {
            background: linear-gradient(-90deg, #551ed7 -10%, #6e87ff 80%)
        }

        @media (min-width: 768px) {
            [dir] .contentWrapper-oyGa0 {
                padding: 3.3rem 0 5.5rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .contentWrapper-oyGa0 {
                padding: 3.5rem 0 5.5rem
            }
        }

        .contentWrapper-oyGa0 .latestReleases-1fOV0 {
            overflow: visible
        }

        .contentWrapper-oyGa0 .mainTitle-1c-2G {
            text-transform: uppercase
        }

        [dir] .contentWrapper-oyGa0 .mainTitle-1c-2G {
            margin-bottom: 1.7rem
        }

        .contentWrapper-oyGa0 .mainTitle-1c-2G:before {
            content: "/ "
        }

        @media (min-width: 768px) {
            [dir] .contentWrapper-oyGa0 .mainTitle-1c-2G {
                margin-bottom: 3rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .contentWrapper-oyGa0 .mainTitle-1c-2G {
                margin-bottom: 2.5rem
            }
        }

        .contentWrapper-oyGa0 .sliderWrapper-357lK {
            width: 100%;
            position: relative
        }

        .contentWrapper-oyGa0 .latestReleasesList-9olau, .contentWrapper-oyGa0 .sliderContent-2_FQQ {
            display: flex;
            width: 100%;
            justify-content: space-between;
            align-items: flex-end;
            opacity: 0
        }

        .contentWrapper-oyGa0 .sliderItem-3BqgN {
            position: relative;
            min-width: 24em
        }

        [dir=ltr] .contentWrapper-oyGa0 .sliderItem-3BqgN {
            padding-right: 5rem
        }

        [dir=rtl] .contentWrapper-oyGa0 .sliderItem-3BqgN {
            padding-left: 5rem
        }

        @media (min-width: 768px) {
            .contentWrapper-oyGa0 .sliderItem-3BqgN {
                min-width: 42em
            }

            [dir=ltr] .contentWrapper-oyGa0 .sliderItem-3BqgN {
                padding-right: 10rem
            }

            [dir=rtl] .contentWrapper-oyGa0 .sliderItem-3BqgN {
                padding-left: 10rem
            }
        }

        .contentWrapper-oyGa0 .latestReleasesItem-3SyOz {
            display: flex;
            align-items: flex-end
        }

        @media (min-width: 1024px) {
            .contentWrapper-oyGa0 .latestReleasesItem-3SyOz {
                width: 100%;
                max-width: 35rem
            }

            [dir=ltr] .contentWrapper-oyGa0 .latestReleasesItem-3SyOz:not(:last-child) {
                margin-right: 3.25%
            }

            [dir=rtl] .contentWrapper-oyGa0 .latestReleasesItem-3SyOz:not(:last-child) {
                margin-left: 3.25%
            }
        }

        @media (min-width: 1280px) {
            [dir=ltr] .contentWrapper-oyGa0 .latestReleasesItem-3SyOz:not(:last-child) {
                margin-right: 6.5%
            }

            [dir=rtl] .contentWrapper-oyGa0 .latestReleasesItem-3SyOz:not(:last-child) {
                margin-left: 6.5%
            }
        }

        .customCursor-3WHQJ {
            height: 9rem;
            width: 9rem;
            color: #fff;
            position: absolute;
            z-index: 2;
            pointer-events: none;
            opacity: 1;
            transition: opacity .2s ease-out
        }

        [dir] .customCursor-3WHQJ {
            background-color: transparent
        }

        [dir=ltr] .customCursor-3WHQJ {
            transform: translateX(-50%) translateY(-50%)
        }

        [dir=rtl] .customCursor-3WHQJ {
            transform: translateX(50%) translateY(-50%);
            transform: translateX(-50%) translateY(-50%)
        }

        [dir=ltr] .customCursor-3WHQJ[data-direction=vertical] {
            transform: translateX(-50%) translateY(-50%) rotate(90deg)
        }

        [dir=rtl] .customCursor-3WHQJ[data-direction=vertical] {
            transform: translateX(50%) translateY(-50%) rotate(-90deg);
            transform: translateX(-50%) translateY(-50%) rotate(90deg)
        }

        .icon-3Oe95 svg {
            opacity: 0
        }

        [dir] .icon-3Oe95 svg {
            transform: scale(.2);
            transform-origin: center
        }

        [dir=ltr] .icon-3Oe95 svg, [dir=rtl] .icon-3Oe95 svg {
            animation: animateIn-1tOuL .35s ease-out forwards
        }

        [dir=ltr] .icon-3Oe95 svg [data-arrow-left] {
            animation: loopArrowLeft-ltr-2psQY 1.2s ease infinite alternate
        }

        [dir=rtl] .icon-3Oe95 svg [data-arrow-left] {
            animation: loopArrowLeft-rtl-14sHa 1.2s ease infinite alternate
        }

        [dir=ltr] .icon-3Oe95 svg [data-arrow-right] {
            animation: loopArrowRight-ltr-3S8dt 1.2s ease infinite alternate
        }

        [dir=rtl] .icon-3Oe95 svg [data-arrow-right] {
            animation: loopArrowRight-rtl-1GpO4 1.2s ease infinite alternate
        }

        @keyframes loopArrowLeft-ltr-2psQY {
            0% {
                transform: translateX(10%)
            }
            20% {
                transform: translate(0)
            }
            to {
                transform: translateX(0)
            }
        }

        @keyframes loopArrowLeft-rtl-14sHa {
            0% {
                transform: translateX(-10%)
            }
            20% {
                transform: translate(0)
            }
            to {
                transform: translateX(0)
            }
        }

        @keyframes loopArrowRight-ltr-3S8dt {
            0% {
                transform: translateX(-10%)
            }
            20% {
                transform: translate(0)
            }
            to {
                transform: translateX(0)
            }
        }

        @keyframes loopArrowRight-rtl-1GpO4 {
            0% {
                transform: translateX(10%)
            }
            20% {
                transform: translate(0)
            }
            to {
                transform: translateX(0)
            }
        }

        @keyframes animateIn-1tOuL {
            to {
                opacity: 1;
                transform: scale(1)
            }
        }

        .cardWrapper-XEHQ0 {
            height: 100%;
            width: 100%;
            position: relative
        }

        [dir] .cardWrapper-XEHQ0 {
            padding-bottom: .5rem
        }

        .dashDetail-1U4dV {
            height: .5rem;
            width: 20%;
            visibility: hidden;
            position: absolute;
            bottom: 0
        }

        [dir] .dashDetail-1U4dV {
            background: #fff
        }

        [dir=ltr] .dashDetail-1U4dV {
            transform-origin: left;
            left: 0
        }

        [dir=rtl] .dashDetail-1U4dV {
            transform-origin: right;
            right: 0
        }

        .dashDetail-1U4dV:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .dashDetail-1U4dV:after {
            border-top: .5rem solid #fff
        }

        [dir=ltr] .dashDetail-1U4dV:after {
            right: -.5rem;
            border-right: .5rem solid transparent
        }

        [dir=rtl] .dashDetail-1U4dV:after {
            left: -.5rem;
            border-left: .5rem solid transparent
        }

        .newsCard-1JDzV {
            color: #fff;
            position: relative;
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 5.2vw), calc(100% - 5.5vw) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 5.2vw), calc(100% - 5.5vw) 100%, 0 100%)
        }

        [dir] .newsCard-1JDzV {
            background-position: 50%;
            background-repeat: no-repeat;
            background-size: cover;
            padding-bottom: 100%
        }

        [dir=rtl] .newsCard-1JDzV {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 5.2vw 100%, 0 calc(100% - 5.5vw));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 5.2vw 100%, 0 calc(100% - 5.5vw))
        }

        .newsCard-1JDzV:before {
            content: "";
            display: block;
            position: absolute;
            height: 100%;
            width: 100%
        }

        [dir] .newsCard-1JDzV:before {
            background: #141e37;
            background: linear-gradient(0deg, #141e37, transparent 65%)
        }

        .cardContentWrapper-1jpf_ {
            position: absolute;
            width: 100%;
            bottom: 0
        }

        [dir] .cardContentWrapper-1jpf_ {
            padding: 1.5rem 2rem
        }

        [dir=ltr] .cardContentWrapper-1jpf_ {
            left: 0
        }

        [dir=rtl] .cardContentWrapper-1jpf_ {
            right: 0
        }

        .cardHeading-1--rk {
            display: flex
        }

        [dir] .cardHeading-1--rk {
            margin-bottom: .5rem
        }

        .category-1T7h7 {
            color: #fff
        }

        .ellipseIcon-2tObJ {
            width: .2rem
        }

        [dir] .ellipseIcon-2tObJ {
            margin: 0 .75rem
        }

        .cardTitle-30TGS {
            display: flex;
            align-items: center;
            width: 80%
        }

        [dir] .cardTitle-30TGS {
            margin-bottom: 2rem
        }

        .cardDescription-3CaFb {
            width: 70%
        }

        .titleIcon-1lI7j {
            width: 2.5rem
        }

        [dir=ltr] .titleIcon-1lI7j {
            margin-right: 1rem
        }

        [dir=rtl] .titleIcon-1lI7j {
            margin-left: 1rem
        }

        @media (min-width: 768px) {
            [dir] .cardWrapper-XEHQ0 {
                padding-bottom: 1rem
            }

            .dashDetail-1U4dV {
                height: 1rem;
                width: 20%
            }

            [dir] .dashDetail-1U4dV {
                background: #fff
            }

            .dashDetail-1U4dV:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .dashDetail-1U4dV:after {
                border-top: 1rem solid #fff
            }

            [dir=ltr] .dashDetail-1U4dV:after {
                right: -1rem;
                border-right: 1rem solid transparent
            }

            [dir=rtl] .dashDetail-1U4dV:after {
                left: -1rem;
                border-left: 1rem solid transparent
            }

            [dir] .cardContentWrapper-1jpf_ {
                padding: 3rem 4rem
            }

            .cardHeading-1--rk {
                display: flex
            }

            [dir] .cardHeading-1--rk {
                margin-bottom: 2rem
            }

            .ellipseIcon-2tObJ {
                width: .4rem
            }

            [dir] .ellipseIcon-2tObJ {
                margin: 0 1.5rem
            }

            .titleIcon-1lI7j {
                width: 5rem
            }

            [dir=ltr] .titleIcon-1lI7j {
                margin-right: 2rem
            }

            [dir=rtl] .titleIcon-1lI7j {
                margin-left: 2rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .cardWrapper-XEHQ0 {
                padding-bottom: .4rem;
                cursor: pointer
            }

            .cardWrapper-XEHQ0 .category-1T7h7, .cardWrapper-XEHQ0 .date-1b6J3, .cardWrapper-XEHQ0 .title-2S683 {
                transition: color .2s ease-out
            }

            [dir] .cardWrapper-XEHQ0 .cardContentWrapper-1jpf_ {
                transform: translateY(-10%)
            }

            [dir=ltr] .cardWrapper-XEHQ0 .cardContentWrapper-1jpf_ {
                padding: 1.5rem 0 1.5rem 3rem
            }

            [dir=rtl] .cardWrapper-XEHQ0 .cardContentWrapper-1jpf_ {
                padding: 1.5rem 3rem 1.5rem 0
            }

            .cardWrapper-XEHQ0:hover .dashDetail-1U4dV {
                height: .4rem;
                width: 12rem
            }

            [dir] .cardWrapper-XEHQ0:hover .dashDetail-1U4dV {
                background: #fff
            }

            .cardWrapper-XEHQ0:hover .dashDetail-1U4dV:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .cardWrapper-XEHQ0:hover .dashDetail-1U4dV:after {
                border-top: .4rem solid #fff
            }

            [dir=ltr] .cardWrapper-XEHQ0:hover .dashDetail-1U4dV:after {
                right: -.4rem;
                border-right: .4rem solid transparent
            }

            [dir=rtl] .cardWrapper-XEHQ0:hover .dashDetail-1U4dV:after {
                left: -.4rem;
                border-left: .4rem solid transparent
            }

            [dir] .cardWrapper-XEHQ0:hover .newsCard-1JDzV {
                background-size: 110%
            }

            .cardWrapper-XEHQ0:hover .newsCard-1JDzV:after {
                opacity: 1
            }

            .cardWrapper-XEHQ0:hover .date-1b6J3, .cardWrapper-XEHQ0:hover .title-2S683 {
                color: #32c8ff
            }

            .cardWrapper-XEHQ0:hover .category-1T7h7 {
                color: #fff
            }

            .dashDetail-1U4dV {
                height: .4rem;
                width: 9rem;
                transition: width .3s ease-out
            }

            [dir] .dashDetail-1U4dV {
                background: #fff
            }

            .dashDetail-1U4dV:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .dashDetail-1U4dV:after {
                border-top: .4rem solid #fff
            }

            [dir=ltr] .dashDetail-1U4dV:after {
                right: -.4rem;
                border-right: .4rem solid transparent
            }

            [dir=rtl] .dashDetail-1U4dV:after {
                left: -.4rem;
                border-left: .4rem solid transparent
            }

            .newsCard-1JDzV {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 2.2vw), calc(100% - 2.5vw) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 2.2vw), calc(100% - 2.5vw) 100%, 0 100%);
                transition: background-size .3s ease-out
            }

            [dir] .newsCard-1JDzV {
                padding-bottom: 56.35%;
                background-size: 100%
            }

            [dir=rtl] .newsCard-1JDzV {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 2.2vw 100%, 0 calc(100% - 2.5vw));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 2.2vw 100%, 0 calc(100% - 2.5vw))
            }

            .newsCard-1JDzV:after {
                content: "";
                display: block;
                position: absolute;
                top: 0;
                height: 100%;
                width: 100%;
                opacity: 0;
                transition: opacity .2s ease-out;
                z-index: -1
            }

            [dir] .newsCard-1JDzV:after {
                background-color: rgba(20, 30, 55, .4)
            }

            [dir=ltr] .newsCard-1JDzV:after {
                left: 0
            }

            [dir=rtl] .newsCard-1JDzV:after {
                right: 0
            }

            .cardContentWrapper-1jpf_ {
                transition: transform .2s ease-out
            }

            [dir] .cardContentWrapper-1jpf_ {
                padding: 1.5rem
            }

            .ellipseIcon-2tObJ {
                width: .2rem
            }

            .titleIcon-1lI7j {
                width: 2.5rem
            }

            [dir=ltr] .titleIcon-1lI7j {
                margin-right: 1.5rem
            }

            [dir=rtl] .titleIcon-1lI7j {
                margin-left: 1.5rem
            }
        }

        @media (min-width: 1440px) {
            .newsCard-1JDzV {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3.1rem), calc(100% - 3.5rem) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3.1rem), calc(100% - 3.5rem) 100%, 0 100%)
            }

            [dir=rtl] .newsCard-1JDzV {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 3.1rem 100%, 0 calc(100% - 3.5rem));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 3.1rem 100%, 0 calc(100% - 3.5rem))
            }
        }

        .cardWrapper-2YpWt {
            height: 100%;
            width: 100%;
            position: relative
        }

        [dir] .cardWrapper-2YpWt {
            padding-bottom: .5rem
        }

        .dashDetail-2fLJd {
            height: .5rem;
            width: 20%;
            position: absolute;
            bottom: 0
        }

        [dir] .dashDetail-2fLJd {
            background: #32c8ff
        }

        [dir=ltr] .dashDetail-2fLJd {
            transform-origin: left;
            left: 0
        }

        [dir=rtl] .dashDetail-2fLJd {
            transform-origin: right;
            right: 0
        }

        .dashDetail-2fLJd:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .dashDetail-2fLJd:after {
            border-top: .5rem solid #32c8ff
        }

        [dir=ltr] .dashDetail-2fLJd:after {
            right: -.5rem;
            border-right: .5rem solid transparent
        }

        [dir=rtl] .dashDetail-2fLJd:after {
            left: -.5rem;
            border-left: .5rem solid transparent
        }

        .newsCard-2JC_Q {
            color: #fff;
            position: relative;
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 5.2vw), calc(100% - 5.5vw) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 5.2vw), calc(100% - 5.5vw) 100%, 0 100%)
        }

        [dir] .newsCard-2JC_Q {
            background-position: 50%;
            background-repeat: no-repeat;
            background-size: cover;
            padding-bottom: 56.35%
        }

        [dir=rtl] .newsCard-2JC_Q {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 5.2vw 100%, 0 calc(100% - 5.5vw));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 5.2vw 100%, 0 calc(100% - 5.5vw))
        }

        .newsCard-2JC_Q:before {
            content: "";
            display: block;
            position: absolute;
            height: 100%;
            width: 100%
        }

        [dir] .newsCard-2JC_Q:before {
            background: #141e37;
            background: linear-gradient(0deg, #141e37, transparent 65%)
        }

        .cardContentWrapper-3L0CZ {
            position: absolute;
            width: 100%;
            bottom: 0
        }

        [dir] .cardContentWrapper-3L0CZ {
            padding: 1.5rem 2rem
        }

        [dir=ltr] .cardContentWrapper-3L0CZ {
            left: 0
        }

        [dir=rtl] .cardContentWrapper-3L0CZ {
            right: 0
        }

        .cardHeading-3FUHN {
            display: flex;
            align-items: center
        }

        [dir] .cardHeading-3FUHN {
            margin-bottom: .5rem
        }

        .category-1nX3- {
            color: #32c8ff
        }

        .ellipseIcon-KhzGf {
            height: .2rem;
            width: .2rem
        }

        [dir] .ellipseIcon-KhzGf {
            margin: 0 .75rem
        }

        .cardTitle-3NISe {
            display: flex;
            align-items: center
        }

        .titleIcon-WcO0c {
            height: 2.5rem;
            width: 2.5rem
        }

        [dir=ltr] .titleIcon-WcO0c {
            margin-right: 1rem
        }

        [dir=rtl] .titleIcon-WcO0c {
            margin-left: 1rem
        }

        @media (min-width: 768px) {
            [dir] .cardWrapper-2YpWt {
                padding-bottom: 1rem
            }

            .dashDetail-2fLJd {
                height: 1rem;
                width: 20%
            }

            [dir] .dashDetail-2fLJd {
                background: #32c8ff
            }

            .dashDetail-2fLJd:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .dashDetail-2fLJd:after {
                border-top: 1rem solid #32c8ff
            }

            [dir=ltr] .dashDetail-2fLJd:after {
                right: -1rem;
                border-right: 1rem solid transparent
            }

            [dir=rtl] .dashDetail-2fLJd:after {
                left: -1rem;
                border-left: 1rem solid transparent
            }

            [dir] .cardContentWrapper-3L0CZ {
                padding: 3rem 4rem
            }

            .cardHeading-3FUHN {
                display: flex
            }

            [dir] .cardHeading-3FUHN {
                margin-bottom: 1.5rem
            }

            .ellipseIcon-KhzGf {
                height: .4rem;
                width: .4rem
            }

            [dir] .ellipseIcon-KhzGf {
                margin: 0 1.5rem
            }

            .titleIcon-WcO0c {
                height: 5rem;
                width: 5rem
            }

            [dir=ltr] .titleIcon-WcO0c {
                margin-right: 2rem
            }

            [dir=rtl] .titleIcon-WcO0c {
                margin-left: 2rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .cardWrapper-2YpWt {
                padding-bottom: .4rem;
                cursor: pointer
            }

            .cardWrapper-2YpWt .category-1nX3-, .cardWrapper-2YpWt .date-NDmi_, .cardWrapper-2YpWt .title-Pl837 {
                transition: color .2s ease-out
            }

            .cardWrapper-2YpWt:hover .dashDetail-2fLJd {
                height: .4rem;
                width: 12rem
            }

            [dir] .cardWrapper-2YpWt:hover .dashDetail-2fLJd {
                background: #32c8ff
            }

            .cardWrapper-2YpWt:hover .dashDetail-2fLJd:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .cardWrapper-2YpWt:hover .dashDetail-2fLJd:after {
                border-top: .4rem solid #32c8ff
            }

            [dir=ltr] .cardWrapper-2YpWt:hover .dashDetail-2fLJd:after {
                right: -.4rem;
                border-right: .4rem solid transparent
            }

            [dir=rtl] .cardWrapper-2YpWt:hover .dashDetail-2fLJd:after {
                left: -.4rem;
                border-left: .4rem solid transparent
            }

            [dir] .cardWrapper-2YpWt:hover .cardContentWrapper-3L0CZ {
                transform: translateY(-10%)
            }

            [dir] .cardWrapper-2YpWt:hover .newsCard-2JC_Q {
                background-size: 110%
            }

            .cardWrapper-2YpWt:hover .newsCard-2JC_Q:after {
                opacity: 1
            }

            .cardWrapper-2YpWt:hover .date-NDmi_, .cardWrapper-2YpWt:hover .title-Pl837 {
                color: #32c8ff
            }

            .cardWrapper-2YpWt:hover .category-1nX3- {
                color: #fff
            }

            .dashDetail-2fLJd {
                height: .4rem;
                width: 9rem;
                transition: width .3s ease-out
            }

            [dir] .dashDetail-2fLJd {
                background: #32c8ff
            }

            .dashDetail-2fLJd:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .dashDetail-2fLJd:after {
                border-top: .4rem solid #32c8ff
            }

            [dir=ltr] .dashDetail-2fLJd:after {
                right: -.4rem;
                border-right: .4rem solid transparent
            }

            [dir=rtl] .dashDetail-2fLJd:after {
                left: -.4rem;
                border-left: .4rem solid transparent
            }

            .newsCard-2JC_Q {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 2.2vw), calc(100% - 2.5vw) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 2.2vw), calc(100% - 2.5vw) 100%, 0 100%);
                transition: background-size .3s ease-out
            }

            [dir] .newsCard-2JC_Q {
                background-size: 100%
            }

            [dir=rtl] .newsCard-2JC_Q {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 2.2vw 100%, 0 calc(100% - 2.5vw));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 2.2vw 100%, 0 calc(100% - 2.5vw))
            }

            .newsCard-2JC_Q:after {
                content: "";
                display: block;
                position: absolute;
                top: 0;
                height: 100%;
                width: 100%;
                opacity: 0;
                transition: opacity .2s ease-out;
                z-index: -1
            }

            [dir] .newsCard-2JC_Q:after {
                background-color: rgba(20, 30, 55, .4)
            }

            [dir=ltr] .newsCard-2JC_Q:after {
                left: 0
            }

            [dir=rtl] .newsCard-2JC_Q:after {
                right: 0
            }

            .cardContentWrapper-3L0CZ {
                transition: transform .2s ease-out
            }

            [dir] .cardContentWrapper-3L0CZ {
                padding: 1.5rem
            }

            [dir] .cardHeading-3FUHN {
                margin-bottom: 1rem
            }

            .ellipseIcon-KhzGf {
                height: .2rem;
                width: .2rem
            }

            .titleIcon-WcO0c {
                height: 2.5rem;
                width: 2.5rem
            }

            [dir=ltr] .titleIcon-WcO0c {
                margin-right: 1.5rem
            }

            [dir=rtl] .titleIcon-WcO0c {
                margin-left: 1.5rem
            }
        }

        @media (min-width: 1440px) {
            .newsCard-2JC_Q {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3.1rem), calc(100% - 3.5rem) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3.1rem), calc(100% - 3.5rem) 100%, 0 100%)
            }

            [dir=rtl] .newsCard-2JC_Q {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 3.1rem 100%, 0 calc(100% - 3.5rem));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 3.1rem 100%, 0 calc(100% - 3.5rem))
            }
        }

        .promoAdWrapper-RHN7u {
            position: relative;
            width: 100%;
            height: 0;
            overflow: hidden
        }

        [dir] .promoAdWrapper-RHN7u {
            padding-bottom: 37.7%;
            margin-bottom: 6rem;
            background: #fff
        }

        .promoAd-1QGHp {
            display: block;
            position: absolute;
            width: 100%;
            top: 50%
        }

        [dir] .promoAd-1QGHp {
            transform: translateY(-50%)
        }

        [dir=ltr] .promoAd-1QGHp {
            left: 0
        }

        [dir=rtl] .promoAd-1QGHp {
            right: 0
        }

        @media (min-width: 1024px) {
            [dir] .promoAdWrapper-RHN7u {
                padding-bottom: 16.18%
            }
        }

        .articleCardWrapper-1JIOy {
            display: inline-block;
            width: 100%
        }

        .articleCardWrapper-1JIOy .imageWrapper-3whpd {
            display: block;
            position: relative;
            width: 100%;
            overflow: hidden;
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 85%, 90% 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% 85%, 90% 100%, 0 100%)
        }

        [dir] .articleCardWrapper-1JIOy .imageWrapper-3whpd {
            padding-bottom: 56.25%;
            margin-bottom: 1.2rem
        }

        .articleCardWrapper-1JIOy .imageWrapper-3whpd:before {
            content: "";
            display: block;
            position: absolute;
            top: 0;
            bottom: 0;
            z-index: 1
        }

        [dir] .articleCardWrapper-1JIOy .imageWrapper-3whpd:before {
            background: #111
        }

        [dir=ltr] .articleCardWrapper-1JIOy .imageWrapper-3whpd:before {
            background: linear-gradient(145deg, #111 -10%, transparent 35%);
            left: 0;
            right: 0
        }

        [dir=rtl] .articleCardWrapper-1JIOy .imageWrapper-3whpd:before {
            background: linear-gradient(-145deg, #111 -10%, transparent 35%);
            right: 0;
            left: 0
        }

        .articleCardWrapper-1JIOy .imageWrapper-3whpd:after {
            content: "";
            position: absolute;
            top: 0;
            bottom: 0;
            opacity: 0;
            transition: opacity .2s ease-out
        }

        [dir] .articleCardWrapper-1JIOy .imageWrapper-3whpd:after {
            background: #141e37
        }

        [dir=ltr] .articleCardWrapper-1JIOy .imageWrapper-3whpd:after, [dir=rtl] .articleCardWrapper-1JIOy .imageWrapper-3whpd:after {
            left: 0;
            right: 0
        }

        .articleCardWrapper-1JIOy .imageWrapper-3whpd .image-NeGf2 {
            position: absolute;
            top: 0;
            transition: transform .4s ease
        }

        [dir=ltr] .articleCardWrapper-1JIOy .imageWrapper-3whpd .image-NeGf2 {
            left: 0
        }

        [dir=rtl] .articleCardWrapper-1JIOy .imageWrapper-3whpd .image-NeGf2 {
            right: 0
        }

        [dir] .articleCardWrapper-1JIOy:hover .image-NeGf2 {
            transform: scale(1.2)
        }

        .articleCardWrapper-1JIOy:hover .imageWrapper-3whpd:after {
            opacity: .4
        }

        .articleCardWrapper-1JIOy .title--HVLV {
            max-width: 90%
        }

        [dir] .articleCardWrapper-1JIOy .title--HVLV {
            margin-top: 1rem;
            margin-bottom: .8rem
        }

        .articleCardWrapper-1JIOy .category-1WTtr {
            display: inline-block;
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif;
            text-transform: uppercase;
            color: #32c8ff
        }

        [dir=ltr] .articleCardWrapper-1JIOy .category-1WTtr {
            margin-left: 1.5rem
        }

        [dir=rtl] .articleCardWrapper-1JIOy .category-1WTtr {
            margin-right: 1.5rem
        }

        .articleCardWrapper-1JIOy .category-1WTtr:before {
            content: "\B7";
            font-size: 2rem;
            color: #111
        }

        [dir=ltr] .articleCardWrapper-1JIOy .category-1WTtr:before {
            margin-right: 1.5rem
        }

        [dir=rtl] .articleCardWrapper-1JIOy .category-1WTtr:before {
            margin-left: 1.5rem
        }

        .articleCardWrapper-1JIOy.sliderCard-3o5XK {
            display: block;
            width: 100%;
            color: #fff
        }

        .articleCardWrapper-1JIOy.sliderCard-3o5XK .copyWrapper-4hjwU {
            display: flex;
            width: 100%
        }

        [dir] .articleCardWrapper-1JIOy.sliderCard-3o5XK .copyWrapper-4hjwU {
            margin-top: 2.2rem
        }

        .articleCardWrapper-1JIOy.sliderCard-3o5XK .copy-1Gpas {
            width: 35%
        }

        .articleCardWrapper-1JIOy.sliderCard-3o5XK .title--HVLV {
            font-style: normal;
            width: 80%
        }

        [dir] .articleCardWrapper-1JIOy.sliderCard-3o5XK .title--HVLV {
            margin-top: 0;
            margin-bottom: 0;
            padding: 0
        }

        [dir=ltr] .articleCardWrapper-1JIOy.sliderCard-3o5XK .title--HVLV {
            margin-right: 0
        }

        [dir=rtl] .articleCardWrapper-1JIOy.sliderCard-3o5XK .title--HVLV {
            margin-left: 0
        }

        [dir] .articleCardWrapper-1JIOy.sliderCard-3o5XK .image-NeGf2[data-type="Youtube video"] {
            transform: translateY(-12.5%)
        }

        [dir] .articleCardWrapper-1JIOy.sliderCard-3o5XK:hover .image-NeGf2[data-type="Youtube video"] {
            transform: translateY(-12.5%) scale(1.1)
        }

        .articleType-3wLeW {
            position: absolute;
            top: 0;
            width: 3rem;
            height: 3rem;
            z-index: 2
        }

        [dir] .articleType-3wLeW {
            margin-top: 1rem;
            background-repeat: no-repeat
        }

        [dir=ltr] .articleType-3wLeW {
            left: 0;
            margin-left: 1rem
        }

        [dir=rtl] .articleType-3wLeW {
            right: 0;
            margin-right: 1rem
        }

        [dir] .articleType-3wLeW[data-type="External link"] {
            background-image: url(data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJUZXh0Qm94MSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4PSIwIiB5PSIwIiB2aWV3Qm94PSIwIDAgNDAgNDAiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxjaXJjbGUgY3g9IjE5LjgiIGN5PSIyMCIgcj0iMTUuOCIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjZmZmIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIvPjxwYXRoIGZpbGw9IiNmZmYiIGQ9Ik0yNS43IDEzLjZoLTkuNXYyLjFIMjJsLTguOCA4LjgtLjEuMVYyNi41SDE1bC4xLS4xIDguOC04Ljh2NS44SDI2di05Ljh6Ii8+PC9zdmc+)
        }

        [dir] .articleType-3wLeW[data-type="Normal article"] {
            background-image: url(data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJUZXh0Qm94MSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4PSIwIiB5PSIwIiB2aWV3Qm94PSIwIDAgNDAgNDAiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxjaXJjbGUgY3g9IjIwLjEiIGN5PSIyMCIgcj0iMTUuOCIgZmlsbD0ibm9uZSIgc3Ryb2tlPSIjZmZmIiBzdHJva2Utd2lkdGg9IjIiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIvPjxwYXRoIGQ9Ik0yMS41IDEzLjl2Mi41SDI5di0yLjVoLTcuNXptMCA3LjRIMjl2LTIuNWgtNy41djIuNXptLTEwIDVIMjl2LTIuNUgxMS41djIuNXoiIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBmaWxsPSIjZmZmIi8+PHBhdGggZD0iTTE2LjggMTYuMXYzLjFoLTMuMXYtMy4xaDMuMW0yLjItMi4yaC03LjV2Ny41SDE5di03LjV6IiBmaWxsPSIjZmZmIi8+PC9zdmc+)
        }

        [dir] .articleType-3wLeW[data-type="Youtube video"], [dir] .articleType-3wLeW[data-type=Youtube] {
            background-image: url(data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJUZXh0Qm94MSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4PSIwIiB5PSIwIiB2aWV3Qm94PSIwIDAgNDAgNDAiIHhtbDpzcGFjZT0icHJlc2VydmUiPjxwYXRoIGQ9Ik0yNC42IDIwbC03LjEtNC44djkuNWw3LjEtNC43eiIgZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGZpbGw9IiNmZmYiLz48Y2lyY2xlIGN4PSIyMCIgY3k9IjIwIiByPSIxNS44IiBmaWxsPSJub25lIiBzdHJva2U9IiNmZmYiIHN0cm9rZS13aWR0aD0iMiIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIi8+PC9zdmc+)
        }

        @media (min-width: 768px) {
            [dir] .articleCardWrapper-1JIOy {
                margin-bottom: 6rem
            }

            .articleType-3wLeW {
                width: 6rem;
                height: 6rem
            }
        }

        @media (min-width: 1024px) {
            .articleType-3wLeW {
                width: 3rem;
                height: 3rem
            }

            .articleCardWrapper-1JIOy {
                display: inline-block;
                width: 32%
            }

            [dir] .articleCardWrapper-1JIOy {
                margin-bottom: 6rem
            }

            [dir=ltr] .articleCardWrapper-1JIOy {
                margin-left: 2%
            }

            [dir=rtl] .articleCardWrapper-1JIOy {
                margin-right: 2%
            }

            [dir=ltr] .articleCardWrapper-1JIOy:nth-of-type(3n+1) {
                margin-left: 0
            }

            [dir=rtl] .articleCardWrapper-1JIOy:nth-of-type(3n+1) {
                margin-right: 0
            }

            .articleCardWrapper-1JIOy.sliderCard-3o5XK .copyWrapper-4hjwU {
                display: flex;
                width: 90%
            }

            [dir] .articleCardWrapper-1JIOy.sliderCard-3o5XK .copyWrapper-4hjwU {
                margin-top: 1.6rem
            }

            .articleCardWrapper-1JIOy.sliderCard-3o5XK .copyWrapper-4hjwU .copy-1Gpas {
                width: 35%
            }

            .articleCardWrapper-1JIOy.sliderCard-3o5XK .copyWrapper-4hjwU .halfWidth-2rbgC {
                width: 60%
            }

            [dir=ltr] .articleCardWrapper-1JIOy.sliderCard-3o5XK .copyWrapper-4hjwU .halfWidth-2rbgC {
                margin-right: 5%
            }

            [dir=rtl] .articleCardWrapper-1JIOy.sliderCard-3o5XK .copyWrapper-4hjwU .halfWidth-2rbgC {
                margin-left: 5%
            }
        }

        [dir] .container-22xg_ {
            margin: 4rem 0 10rem
        }

        .category-2BqXP {
            font-size: 1.5rem
        }

        .title-17FLB {
            font-size: 3.4rem;
            letter-spacing: 1px;
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            text-transform: uppercase
        }

        [dir] .title-17FLB {
            margin-top: .5rem
        }

        html[lang=th-th] .title-17FLB {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-weight: 700;
            line-height: 1.4;
            font-style: normal
        }

        html[lang=ar-ae] .title-17FLB {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal;
            line-height: 1.15
        }

        html[lang=ru-ru] .title-17FLB {
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif
        }

        html[lang=tr-tr] .title-17FLB {
            line-height: 1.1
        }

        html[lang=ko-kr] .title-17FLB {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        html[lang=vi-vn] .title-17FLB {
            font-family: RobotoCondensed-Bold, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            letter-spacing: 1px;
            line-height: 1.18
        }

        html[lang=zh-hk] .title-17FLB, html[lang=zh-tw] .title-17FLB {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-style: normal;
            font-weight: 500;
            line-height: 1.1
        }

        html[lang=ja-jp] .title-17FLB {
            font-family: BeaufortforLOLJa-Regular, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        .articlesList-28r1F {
            display: flex;
            flex-direction: column
        }

        [dir] .articlesList-28r1F {
            margin: 3rem 0
        }

        .dashDetail-K0si9 {
            position: relative;
            width: 92%;
            height: .2rem;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden
        }

        [dir] .dashDetail-K0si9 {
            margin-top: 2.1rem;
            margin-bottom: 4rem;
            background: #e1e1e1
        }

        [dir=ltr] .dashDetail-K0si9 {
            transform-origin: left
        }

        [dir=rtl] .dashDetail-K0si9 {
            transform-origin: right
        }

        .dashDetail-K0si9:before {
            content: "";
            position: absolute;
            bottom: -.3rem;
            height: .4rem;
            width: 21%;
            -webkit-clip-path: polygon(0 0, 0 100%, 90% 100%, 100% 0);
            clip-path: polygon(0 0, 0 100%, 90% 100%, 100% 0)
        }

        [dir] .dashDetail-K0si9:before {
            background-color: #e1e1e1
        }

        [dir=ltr] .dashDetail-K0si9:before {
            left: 0
        }

        [dir=rtl] .dashDetail-K0si9:before {
            right: 0
        }

        .buttonWrapper-1xky7 {
            display: flex;
            justify-content: center
        }

        [dir] .buttonWrapper-1xky7 {
            margin-bottom: 4rem
        }

        @media (min-width: 1024px) {
            [dir] .container-22xg_ {
                margin: 8rem 0 18rem
            }

            .title-17FLB {
                font-size: 5.4rem
            }

            .articlesList-28r1F {
                flex-direction: row;
                flex-wrap: wrap
            }

            [dir] .articlesList-28r1F {
                margin-bottom: 0
            }

            .dashDetail-K0si9 {
                display: none
            }

            [dir] .buttonWrapper-1xky7 {
                margin-bottom: 8rem
            }
        }

        [dir] .container-2oWnq {
            margin: 4rem 0 10rem
        }

        .category-1jNbs {
            font-size: 1.5rem
        }

        .title-Ua0Xs {
            font-size: 3.4rem;
            letter-spacing: 1px;
            font-family: GT-America-Compressed-Bold, arial, georgia, sans-serif;
            text-transform: uppercase
        }

        [dir] .title-Ua0Xs {
            margin-top: .5rem
        }

        html[lang=th-th] .title-Ua0Xs {
            font-family: NeueFrutigerThaiModern, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            font-weight: 700;
            line-height: 1.4;
            font-style: normal
        }

        html[lang=ar-ae] .title-Ua0Xs {
            font-family: Cairo, arial, georgia, sans-serif;
            font-style: normal;
            line-height: 1.15
        }

        html[lang=ru-ru] .title-Ua0Xs {
            font-family: BeaufortforLOL-Bold, arial, georgia, sans-serif
        }

        html[lang=tr-tr] .title-Ua0Xs {
            line-height: 1.1
        }

        html[lang=ko-kr] .title-Ua0Xs {
            font-family: RixSGoM, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        html[lang=vi-vn] .title-Ua0Xs {
            font-family: RobotoCondensed-Bold, GT-America-Compressed-Bold, arial, georgia, sans-serif;
            letter-spacing: 1px;
            line-height: 1.18
        }

        html[lang=zh-hk] .title-Ua0Xs, html[lang=zh-tw] .title-Ua0Xs {
            font-family: WildRift-TraditionalChinese, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            font-style: normal;
            font-weight: 500;
            line-height: 1.1
        }

        html[lang=ja-jp] .title-Ua0Xs {
            font-family: BeaufortforLOLJa-Regular, GT-America-Compressed-Medium, arial, georgia, sans-serif;
            line-height: 1
        }

        .articlesList-w-eWn {
            display: flex;
            flex-direction: column
        }

        [dir] .articlesList-w-eWn {
            margin: 3rem 0
        }

        .dashDetail-1Dryr {
            position: relative;
            width: 92%;
            height: .2rem;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden
        }

        [dir] .dashDetail-1Dryr {
            margin-top: 2.1rem;
            margin-bottom: 4rem;
            background: #e1e1e1
        }

        [dir=ltr] .dashDetail-1Dryr {
            transform-origin: left
        }

        [dir=rtl] .dashDetail-1Dryr {
            transform-origin: right
        }

        .dashDetail-1Dryr:before {
            content: "";
            position: absolute;
            bottom: -.3rem;
            height: .4rem;
            width: 21%;
            -webkit-clip-path: polygon(0 0, 0 100%, 90% 100%, 100% 0);
            clip-path: polygon(0 0, 0 100%, 90% 100%, 100% 0)
        }

        [dir] .dashDetail-1Dryr:before {
            background-color: #e1e1e1
        }

        [dir=ltr] .dashDetail-1Dryr:before {
            left: 0
        }

        [dir=rtl] .dashDetail-1Dryr:before {
            right: 0
        }

        .buttonWrapper-2yWtR {
            display: flex;
            justify-content: center
        }

        [dir] .buttonWrapper-2yWtR {
            margin-bottom: 4rem
        }

        @media (min-width: 1024px) {
            [dir] .container-2oWnq {
                margin: 8rem 0 18rem
            }

            .title-Ua0Xs {
                font-size: 5.4rem
            }

            .articlesList-w-eWn {
                flex-direction: row;
                flex-wrap: wrap
            }

            [dir] .articlesList-w-eWn {
                margin-bottom: 0
            }

            .dashDetail-1Dryr {
                display: none
            }

            [dir] .buttonWrapper-2yWtR {
                margin-bottom: 8rem
            }
        }

        .volt-v0O2t {
            height: 50%;
            width: 100%;
            position: absolute;
            pointer-events: none
        }

        [dir=ltr] .volt-v0O2t {
            left: 0
        }

        [dir=rtl] .volt-v0O2t {
            right: 0
        }

        .voltImage-UYrS0 {
            position: absolute
        }

        [dir=ltr] .voltImage-UYrS0 {
            left: 50%
        }

        [dir=rtl] .voltImage-UYrS0 {
            right: 50%
        }

        .top-1myq9 {
            top: 0
        }

        [dir=rtl] .top-1myq9 {
            transform: scaleX(-1)
        }

        .top-1myq9 .voltImage-UYrS0 {
            top: -.5rem
        }

        [dir=ltr] .top-1myq9 .voltImage-UYrS0 {
            transform: translate(-50%)
        }

        [dir=rtl] .top-1myq9 .voltImage-UYrS0 {
            transform: translate(50%)
        }

        .bottom-b1FgS {
            bottom: 0
        }

        [dir=rtl] .bottom-b1FgS {
            transform: scaleX(-1)
        }

        .bottom-b1FgS .voltImage-UYrS0 {
            bottom: 0
        }

        [dir=ltr] .bottom-b1FgS .voltImage-UYrS0 {
            transform: translate(-50%, 50%)
        }

        [dir=rtl] .bottom-b1FgS .voltImage-UYrS0 {
            transform: translate(50%, 50%)
        }

        .abilitiesOverview-1Bz08 {
            overflow: visible
        }

        [dir] .abilitiesOverview-1Bz08 {
            padding-top: 11rem;
            padding-bottom: 6.75rem
        }

        .topVolt-3gD9i {
            width: 102.4rem;
            top: -10rem
        }

        .croppedCorner-3J8rT {
            width: 100%;
            position: absolute;
            bottom: 0
        }

        [dir=ltr] .croppedCorner-3J8rT {
            left: 0
        }

        [dir=rtl] .croppedCorner-3J8rT {
            right: 0
        }

        .croppedCorner-3J8rT:after {
            content: "";
            display: block;
            position: absolute;
            width: calc(100% - 3rem);
            height: .2rem;
            bottom: 0
        }

        [dir] .croppedCorner-3J8rT:after {
            background: #fff
        }

        [dir=ltr] .croppedCorner-3J8rT:after {
            left: 3rem
        }

        [dir=rtl] .croppedCorner-3J8rT:after {
            right: 3rem
        }

        .sectionDashDetail-YAEfo {
            height: .4rem;
            width: 29%;
            position: absolute;
            bottom: -.4rem
        }

        [dir] .sectionDashDetail-YAEfo {
            background: #fff
        }

        [dir=ltr] .sectionDashDetail-YAEfo {
            right: 0
        }

        [dir=rtl] .sectionDashDetail-YAEfo {
            left: 0
        }

        .sectionDashDetail-YAEfo:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .sectionDashDetail-YAEfo:after {
            border-top: .4rem solid #fff
        }

        [dir=ltr] .sectionDashDetail-YAEfo:after {
            left: -.4rem;
            border-left: .4rem solid transparent
        }

        [dir=rtl] .sectionDashDetail-YAEfo:after {
            right: -.4rem;
            border-right: .4rem solid transparent
        }

        .sectionCroppedCorner-19L7p {
            height: 4rem;
            width: 3rem;
            position: absolute;
            bottom: 0;
            z-index: 1
        }

        [dir=ltr] .sectionCroppedCorner-19L7p {
            left: -1.5rem;
            transform: skew(42.5deg);
            border-right: 3px solid #fff
        }

        [dir=rtl] .sectionCroppedCorner-19L7p {
            right: -1.5rem;
            transform: skew(-42.5deg);
            border-left: 3px solid #fff
        }

        .videoBorder-3GfDt.top-hvJfh {
            top: -1.2rem
        }

        [dir=ltr] .videoBorder-3GfDt.top-hvJfh {
            right: 1.2rem
        }

        [dir=rtl] .videoBorder-3GfDt.top-hvJfh {
            left: 1.2rem
        }

        .videoBorder-3GfDt.bottom-1lkm_ {
            bottom: -1.2rem
        }

        [dir=ltr] .videoBorder-3GfDt.bottom-1lkm_ {
            left: 1.2rem
        }

        [dir=rtl] .videoBorder-3GfDt.bottom-1lkm_ {
            right: 1.2rem
        }

        .copyWrapper-3gyUC {
            position: relative;
            color: #fff
        }

        [dir] .copyWrapper-3gyUC {
            margin-bottom: 4rem
        }

        .leftLine-3f9DI {
            height: calc(100% - 1rem);
            width: .3rem;
            display: none;
            position: absolute;
            top: 1rem
        }

        [dir=ltr] .leftLine-3f9DI {
            left: -.3rem
        }

        [dir=rtl] .leftLine-3f9DI {
            right: -.3rem
        }

        [dir] .leftLine-3f9DI.light-25rdZ {
            background-color: #fff
        }

        [dir] .title-20Woc {
            margin-bottom: 1.5rem
        }

        .textDivider-36rUO {
            height: auto;
            width: .6rem
        }

        [dir=ltr] .textDivider-36rUO {
            margin: 0 2% .25rem 3%
        }

        [dir=rtl] .textDivider-36rUO {
            margin: 0 3% .25rem 2%
        }

        html[dir][lang=ar-ae] .textDivider-36rUO {
            margin: 0 3% .25rem
        }

        .abilityContentWrapper-3Fdm5 {
            display: flex;
            flex-direction: column
        }

        .abilityDescription-3n4d4 {
            min-height: 8rem
        }

        .videoWrapper-2Zlka {
            position: relative
        }

        [dir] .videoWrapper-2Zlka {
            margin-bottom: 4rem
        }

        .videoContainer-1nv3W {
            position: relative;
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 3rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 3rem) 100%, 0 100%);
            position: position;
            height: auto
        }

        [dir] .videoContainer-1nv3W {
            background-color: #111
        }

        .videoContainer-1nv3W:before {
            content: "";
            display: block
        }

        [dir] .videoContainer-1nv3W:before {
            padding-top: 56.25%
        }

        [dir=rtl] .videoContainer-1nv3W {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 3rem 100%, 0 calc(100% - 3rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 3rem 100%, 0 calc(100% - 3rem))
        }

        .videoContainer-1nv3W > video {
            position: absolute;
            top: 0
        }

        [dir=ltr] .videoContainer-1nv3W > video {
            left: 0
        }

        [dir=rtl] .videoContainer-1nv3W > video {
            right: 0
        }

        .thumbnailsWrapper-1aBbz {
            display: flex;
            justify-content: space-between;
            max-width: 40rem
        }

        [dir] .thumbnailsWrapper-1aBbz {
            margin-bottom: 3.5rem
        }

        .thumbnail-1fNQU {
            height: 4.8rem;
            width: 4.8rem;
            overflow: hidden;
            opacity: .6;
            transition: opacity .2s ease-out, transform .3s ease-out
        }

        [dir] .thumbnail-1fNQU {
            cursor: pointer;
            border-radius: 100%
        }

        .thumbnail-1fNQU.isActive-16L6O {
            opacity: 1
        }

        [dir] .thumbnail-1fNQU:not(.isActive-16L6O):hover {
            transform: scale(1.2)
        }

        .abilitiesDetailsWrapper-PralU {
            width: 100%;
            display: flex;
            flex-direction: column;
            color: #fff
        }

        [dir] .abilityName-1dqbD {
            margin-bottom: 1rem
        }

        @media (min-width: 768px) {
            [dir] .abilitiesOverview-1Bz08 {
                padding-top: 22rem;
                padding-bottom: 13.5rem
            }

            [dir] .copyWrapper-3gyUC {
                margin-bottom: 8rem
            }

            [dir] .title-20Woc {
                margin-bottom: 3.5rem
            }

            .textDivider-36rUO {
                width: 1.2rem
            }

            [dir] .textDivider-36rUO {
                margin-bottom: .5rem
            }

            [dir] .videoWrapper-2Zlka {
                margin-bottom: 6.5rem
            }

            .thumbnailsWrapper-1aBbz {
                max-width: 70rem
            }

            [dir] .thumbnailsWrapper-1aBbz {
                margin-bottom: 6rem
            }

            .thumbnail-1fNQU {
                height: 9.6rem;
                width: 9.6rem
            }

            [dir=ltr] .thumbnail-1fNQU:not(:last-child) {
                margin-right: 3%
            }

            [dir=rtl] .thumbnail-1fNQU:not(:last-child) {
                margin-left: 3%
            }

            [dir] .abilityName-1dqbD {
                margin-bottom: 1.5rem
            }

            .abilityDescription-3n4d4 {
                min-height: 12rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .abilitiesOverview-1Bz08 {
                padding-top: calc(8rem + 14vw);
                padding-bottom: 6rem
            }

            .croppedCorner-3J8rT {
                display: none
            }

            .topVolt-3gD9i {
                top: -8rem;
                width: 100%
            }

            .copyWrapper-3gyUC {
                max-width: calc(58.33333% - 1.25rem);
                width: 100%
            }

            [dir] .copyWrapper-3gyUC {
                margin-bottom: 3rem
            }

            .leftLine-3f9DI {
                display: block
            }

            [dir] .title-20Woc {
                margin-bottom: 2.8rem
            }

            .textDivider-36rUO {
                width: .7rem
            }

            [dir] .textDivider-36rUO {
                margin-bottom: .4rem
            }

            [dir=ltr] .description-2ixtn {
                padding-right: calc(14.28571% + .42857rem);
                padding-left: 3rem
            }

            [dir=rtl] .description-2ixtn {
                padding-left: calc(14.28571% + .42857rem);
                padding-right: 3rem
            }

            .abilityContentWrapper-3Fdm5 {
                display: flex;
                flex-direction: row;
                justify-content: space-between;
                align-items: center
            }

            .abilitiesDetailsWrapper-PralU {
                max-width: calc(50% - 1.5rem);
                width: 100%
            }

            [dir] .abilitiesDetailsWrapper-PralU {
                padding-top: 7%
            }

            [dir=ltr] .abilitiesDetailsWrapper-PralU {
                padding-left: 3rem
            }

            [dir=rtl] .abilitiesDetailsWrapper-PralU {
                padding-right: 3rem
            }

            [dir=ltr] .detailsWrapper-1UWKH {
                padding-right: calc(16.66667% + .5rem)
            }

            [dir=rtl] .detailsWrapper-1UWKH {
                padding-left: calc(16.66667% + .5rem)
            }

            .videoWrapper-2Zlka {
                max-width: calc(50% - 1.5rem);
                width: 100%;
                order: 2
            }

            [dir] .videoWrapper-2Zlka {
                margin-bottom: 0
            }

            [dir] .thumbnailsWrapper-1aBbz {
                margin-bottom: 3.5rem
            }

            .thumbnail-1fNQU {
                height: 6.5rem;
                width: 6.5rem
            }

            [dir] .thumbnail-1fNQU:not(:last-child) {
                margin: 0
            }

            [dir] .abilityName-1dqbD {
                margin-bottom: 1rem
            }
        }

        @media (min-width: 1280px) {
            .copyWrapper-3gyUC {
                max-width: calc(50% - 1.5rem);
                width: 100%
            }

            [dir] .copyWrapper-3gyUC {
                margin-bottom: 0
            }

            [dir=ltr] .description-2ixtn {
                padding-right: calc(16.66667% + .5rem)
            }

            [dir=rtl] .description-2ixtn {
                padding-left: calc(16.66667% + .5rem)
            }

            .abilitiesDetailsWrapper-PralU {
                max-width: calc(41.66667% - 1.75rem);
                width: 100%
            }

            [dir=ltr] .detailsWrapper-1UWKH {
                padding-right: calc(20% + .6rem)
            }

            [dir=rtl] .detailsWrapper-1UWKH {
                padding-left: calc(20% + .6rem)
            }
        }

        .gameDetails-2vXEv {
            position: relative
        }

        [dir] .gameDetails-2vXEv {
            padding-top: 0
        }

        .contentWrapper-1GKNO {
            display: flex;
            flex-direction: column;
            align-items: flex-end
        }

        .imageWrapper-3mAHH {
            position: relative;
            z-index: 1
        }

        .titleWrapper-3LzhL {
            position: relative;
            width: 100%
        }

        [dir] .translateTitle-1oUNz {
            transform: translateY(50%);
            padding-top: 3rem;
            padding-bottom: 3rem
        }

        .detailsWrapper-1SCG0 {
            width: 100%;
            position: relative;
            min-height: auto;
            z-index: 3
        }

        [dir] .detailsWrapper-1SCG0 {
            margin-bottom: 0;
            padding-bottom: 3rem
        }

        .detailsWrapper-1SCG0:before {
            position: absolute;
            display: block;
            top: 0;
            bottom: 0;
            content: ""
        }

        [dir] .detailsWrapper-1SCG0:before {
            background: #fff
        }

        [dir=ltr] .detailsWrapper-1SCG0:before, [dir=rtl] .detailsWrapper-1SCG0:before {
            left: 0;
            right: 0
        }

        .detailsWrapper-1SCG0:after {
            content: "";
            display: block;
            position: absolute;
            top: -3rem
        }

        [dir] .detailsWrapper-1SCG0:after {
            background-color: transparent;
            border-bottom: 1.5rem solid #fff;
            border-top: 1.5rem solid transparent
        }

        [dir=ltr] .detailsWrapper-1SCG0:after {
            right: 0;
            border-right: 1.5rem solid #fff;
            border-left: 1.5rem solid transparent
        }

        [dir=rtl] .detailsWrapper-1SCG0:after {
            left: 0;
            border-left: 1.5rem solid #fff;
            border-right: 1.5rem solid transparent
        }

        .thumbnailWrapper-2hyH4 {
            display: flex;
            width: 70%;
            justify-content: space-between
        }

        [dir] .thumbnailWrapper-2hyH4 {
            margin: 0 auto;
            transform: translateY(-3rem)
        }

        .thumbnailItem-M6V0O {
            position: relative;
            display: flex;
            flex-direction: column;
            align-items: center;
            width: 5.5rem;
            height: auto;
            flex: 1 1
        }

        [dir] .thumbnailItem-M6V0O {
            cursor: pointer
        }

        .thumbnailItem-M6V0O [data-element=thumbnail-copy] {
            opacity: .5;
            transition: opacity .3s ease;
            bottom: -1rem;
            color: #141e37
        }

        .thumbnailItem-M6V0O [data-element=thumbnail-icon-wrapper] {
            height: 5rem;
            width: 5rem;
            position: relative
        }

        [dir] .thumbnailItem-M6V0O [data-element=thumbnail-icon-wrapper] {
            margin-bottom: 2.3rem
        }

        .thumbnailItem-M6V0O [data-element=thumbnail-icon-wrapper]:before {
            content: "";
            position: absolute;
            top: 0;
            width: 100%;
            height: 100%;
            display: block;
            opacity: .5;
            transition: opacity .3s ease
        }

        [dir] .thumbnailItem-M6V0O [data-element=thumbnail-icon-wrapper]:before {
            background: #141e37;
            border-radius: 50%
        }

        [dir=ltr] .thumbnailItem-M6V0O [data-element=thumbnail-icon-wrapper]:before {
            left: 0
        }

        [dir=rtl] .thumbnailItem-M6V0O [data-element=thumbnail-icon-wrapper]:before {
            right: 0
        }

        .thumbnailItem-M6V0O [data-element=thumbnail-icon-wrapper]:after {
            content: "";
            position: absolute;
            top: 50%;
            height: 5.5rem;
            width: 5.5rem;
            z-index: -1
        }

        [dir] .thumbnailItem-M6V0O [data-element=thumbnail-icon-wrapper]:after {
            background: #141e37;
            border-radius: 50%
        }

        [dir=ltr] .thumbnailItem-M6V0O [data-element=thumbnail-icon-wrapper]:after {
            left: 50%;
            transform: translate(-50%, -50%)
        }

        [dir=rtl] .thumbnailItem-M6V0O [data-element=thumbnail-icon-wrapper]:after {
            right: 50%;
            transform: translate(50%, -50%)
        }

        .thumbnailItem-M6V0O.active-1ZyuQ [data-element=thumbnail-icon-wrapper]:before {
            opacity: 0
        }

        .thumbnailItem-M6V0O.active-1ZyuQ [data-element=thumbnail-copy] {
            opacity: 1
        }

        html[lang=ru-ru] .thumbnailItem-M6V0O {
            flex-grow: 0
        }

        .detailsCopyWrapper-1Nits {
            position: relative;
            width: 90%
        }

        [dir] .detailsCopyWrapper-1Nits {
            margin: 0 auto
        }

        .detailsCopyWrapper-1Nits:after {
            content: "";
            display: none
        }

        .detailsCopy-2BfOR {
            width: 100%;
            color: #141e37
        }

        .dashDetail-5ABpO {
            position: absolute;
            bottom: -.35rem;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
            height: .35rem;
            width: 5rem
        }

        [dir] .dashDetail-5ABpO {
            background: rgba(17, 17, 17, .2)
        }

        [dir=ltr] .dashDetail-5ABpO {
            left: 0;
            transform-origin: left
        }

        [dir=rtl] .dashDetail-5ABpO {
            right: 0;
            transform-origin: right
        }

        .dashDetail-5ABpO:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .dashDetail-5ABpO:after {
            border-top: .35rem solid rgba(17, 17, 17, .2)
        }

        [dir=ltr] .dashDetail-5ABpO:after {
            right: -.35rem;
            border-right: .35rem solid transparent
        }

        [dir=rtl] .dashDetail-5ABpO:after {
            left: -.35rem;
            border-left: .35rem solid transparent
        }

        @media (min-width: 768px) {
            .detailsWrapper-1SCG0:after {
                top: -6rem
            }

            [dir] .detailsWrapper-1SCG0:after {
                border-bottom: 3rem solid #fff;
                border-top: 3rem solid transparent
            }

            [dir=ltr] .detailsWrapper-1SCG0:after {
                border-right: 3rem solid #fff;
                border-left: 3rem solid transparent
            }

            [dir=rtl] .detailsWrapper-1SCG0:after {
                border-left: 3rem solid #fff;
                border-right: 3rem solid transparent
            }

            .thumbnailItem-M6V0O {
                width: 8rem
            }

            .thumbnailItem-M6V0O [data-element=thumbnail-icon-wrapper] {
                height: 7rem;
                width: 7rem
            }
        }

        @media (min-width: 1024px) {
            .gameDetails-2vXEv {
                position: relative
            }

            [dir] .gameDetails-2vXEv {
                padding-top: 20rem
            }

            .gameDetails-2vXEv:before {
                content: "";
                display: block;
                position: absolute;
                bottom: 0
            }

            [dir] .gameDetails-2vXEv:before {
                background-color: transparent;
                border-bottom: 3rem solid #fff;
                border-top: 3rem solid transparent
            }

            [dir=ltr] .gameDetails-2vXEv:before {
                right: 0;
                border-right: 3rem solid #fff;
                border-left: 3rem solid transparent
            }

            [dir=rtl] .gameDetails-2vXEv:before {
                left: 0;
                border-left: 3rem solid #fff;
                border-right: 3rem solid transparent
            }

            .titleWrapper-3LzhL {
                position: relative;
                max-width: calc(41.66667% - 1.75rem);
                width: 100%
            }

            [dir] .titleWrapper-3LzhL {
                margin-bottom: 8rem;
                padding-top: 3rem
            }

            .imageWrapper-3mAHH {
                position: absolute;
                bottom: 0
            }

            [dir=ltr] .imageWrapper-3mAHH {
                left: -5%
            }

            [dir=rtl] .imageWrapper-3mAHH {
                right: -5%
            }

            .detailsWrapper-1SCG0 {
                max-width: calc(41.66667% - 1.75rem);
                width: 100%;
                position: relative;
                min-height: 35rem
            }

            [dir] .detailsWrapper-1SCG0 {
                margin-bottom: 7rem;
                padding-top: 0;
                padding-bottom: 0
            }

            .detailsWrapper-1SCG0:after, .detailsWrapper-1SCG0:before {
                display: none
            }

            .thumbnailIcon-2WH2S {
                height: 6.6rem;
                width: 6.6rem
            }

            [dir] .thumbnailIcon-2WH2S {
                margin-bottom: 2.3rem
            }

            .detailsCopyWrapper-1Nits {
                width: 100%
            }

            [dir] .detailsCopyWrapper-1Nits {
                margin: 0
            }

            .detailsCopyWrapper-1Nits:after {
                content: "";
                position: relative;
                display: block;
                width: 85%;
                height: 2px;
                opacity: .2
            }

            [dir] .detailsCopyWrapper-1Nits:after {
                background: #111;
                margin-top: 3rem
            }

            .detailsCopy-2BfOR {
                max-width: calc(80% - .6rem);
                width: 100%;
                color: #141e37
            }

            .thumbnailWrapper-2hyH4 {
                width: 60%;
                justify-content: space-between
            }

            [dir] .thumbnailWrapper-2hyH4 {
                margin: 0 0 4rem;
                transform: translateY(0)
            }

            html[lang=ru-ru] .thumbnailWrapper-2hyH4 {
                width: 70%
            }

            .thumbnailItem-M6V0O {
                width: auto
            }

            .thumbnailItem-M6V0O [data-element=thumbnail-icon-wrapper] {
                height: 5rem;
                width: 5rem
            }
        }

        @media (min-width: 1440px) {
            [dir=ltr] .imageWrapper-3mAHH {
                left: -9%
            }

            [dir=rtl] .imageWrapper-3mAHH {
                right: -9%
            }
        }

        .featuredContent-2BEnV {
            overflow: visible;
            z-index: 4
        }

        [dir] .featuredContent-2BEnV {
            padding-top: 4rem;
            padding-bottom: 10rem;
            margin-bottom: .6rem
        }

        .featuredContent-2BEnV:after {
            content: "";
            display: block;
            position: absolute;
            width: calc(100% - 3rem);
            height: .4rem;
            bottom: 0
        }

        [dir] .featuredContent-2BEnV:after {
            background: #c89b3c
        }

        [dir=ltr] .featuredContent-2BEnV:after {
            left: 3rem
        }

        [dir=rtl] .featuredContent-2BEnV:after {
            right: 3rem
        }

        .sectionDashDetail-8H3YA {
            height: .6rem;
            width: 29%;
            position: absolute;
            bottom: -.6rem
        }

        [dir] .sectionDashDetail-8H3YA {
            background: #c89b3c
        }

        [dir=ltr] .sectionDashDetail-8H3YA {
            right: 0
        }

        [dir=rtl] .sectionDashDetail-8H3YA {
            left: 0
        }

        .sectionDashDetail-8H3YA:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .sectionDashDetail-8H3YA:after {
            border-top: .6rem solid #c89b3c
        }

        [dir=ltr] .sectionDashDetail-8H3YA:after {
            left: -.6rem;
            border-left: .6rem solid transparent
        }

        [dir=rtl] .sectionDashDetail-8H3YA:after {
            right: -.6rem;
            border-right: .6rem solid transparent
        }

        .background-24J8K {
            height: 100%;
            width: 100%;
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 3rem 100%, 0 calc(100% - 3.25rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 3rem 100%, 0 calc(100% - 3.25rem));
            position: absolute;
            top: 0
        }

        [dir] .background-24J8K {
            background-color: #fff
        }

        [dir=ltr] .background-24J8K {
            left: 0
        }

        [dir=rtl] .background-24J8K {
            right: 0;
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3.25rem), calc(100% - 3rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3.25rem), calc(100% - 3rem) 100%, 0 100%)
        }

        .sectionCroppedCorner-3jLLN {
            height: 4rem;
            width: 3rem;
            position: absolute;
            bottom: 0;
            z-index: 1
        }

        [dir=ltr] .sectionCroppedCorner-3jLLN {
            left: -1.5rem;
            transform: skew(42.5deg);
            border-right: 6px solid #c89b3c
        }

        [dir=rtl] .sectionCroppedCorner-3jLLN {
            right: -1.5rem;
            transform: skew(-42.5deg);
            border-left: 6px solid #c89b3c
        }

        .featuredContentWrapper-2B_Vt {
            display: flex;
            flex-direction: column;
            align-items: center
        }

        .featuredContentCard-3-8x1 {
            position: relative;
            width: 100%;
            max-width: 69rem
        }

        [dir] .featuredContentCard-3-8x1:not(:last-child) {
            margin-bottom: 3.7rem
        }

        .backgrounImage-UwrvB {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 1.5rem), calc(100% - 1.5rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 1.5rem), calc(100% - 1.5rem) 100%, 0 100%);
            display: block;
            width: 100%
        }

        [dir] .backgrounImage-UwrvB {
            padding-top: 56.25%;
            background-repeat: no-repeat;
            background-size: contain;
            background-position: 50%
        }

        [dir=rtl] .backgrounImage-UwrvB {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 1.5rem 100%, 0 calc(100% - 1.5rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 1.5rem 100%, 0 calc(100% - 1.5rem))
        }

        .backgrounImage-UwrvB:after {
            content: "";
            display: block;
            position: absolute;
            height: 100%;
            width: 100%;
            top: 0
        }

        [dir] .backgrounImage-UwrvB:after {
            background: #111;
            background: linear-gradient(0deg, #111 -10%, transparent 80%)
        }

        [dir=ltr] .backgrounImage-UwrvB:after {
            left: 0
        }

        [dir=rtl] .backgrounImage-UwrvB:after {
            right: 0
        }

        .title-2yOMx {
            position: absolute;
            bottom: 0;
            opacity: 0;
            visibility: hidden
        }

        [dir] .title-2yOMx {
            padding: 1.5rem 2.25rem
        }

        [dir=ltr] .title-2yOMx {
            left: 0
        }

        [dir=rtl] .title-2yOMx {
            right: 0
        }

        .cardBorder-kX2oR {
            height: 72%;
            width: 75.6%;
            position: absolute;
            bottom: -.7rem;
            pointer-events: none;
            z-index: 1;
            opacity: 0;
            visibility: hidden
        }

        [dir=ltr] .cardBorder-kX2oR {
            left: .6rem
        }

        [dir=rtl] .cardBorder-kX2oR {
            right: .6rem
        }

        .cardBorder-kX2oR .borderLine-zDI08 {
            position: absolute
        }

        [dir] .cardBorder-kX2oR .borderLine-zDI08 {
            background-color: #32c8ff
        }

        .cardBorder-kX2oR .borderLine-zDI08.left-3Gzvo {
            width: .2rem;
            height: 100%;
            top: 0
        }

        [dir] .cardBorder-kX2oR .borderLine-zDI08.left-3Gzvo {
            transform-origin: top
        }

        [dir=ltr] .cardBorder-kX2oR .borderLine-zDI08.left-3Gzvo {
            left: 0
        }

        [dir=rtl] .cardBorder-kX2oR .borderLine-zDI08.left-3Gzvo {
            right: 0
        }

        .cardBorder-kX2oR .borderLine-zDI08.bottom-2YbeE {
            width: 100%;
            height: .2rem;
            bottom: 0
        }

        [dir=ltr] .cardBorder-kX2oR .borderLine-zDI08.bottom-2YbeE {
            left: 0;
            transform-origin: left
        }

        [dir=rtl] .cardBorder-kX2oR .borderLine-zDI08.bottom-2YbeE {
            right: 0;
            transform-origin: right
        }

        .cardBorder-kX2oR .dashDetail-TYawu {
            height: .4rem;
            width: 10rem;
            position: absolute;
            bottom: -.4rem
        }

        [dir] .cardBorder-kX2oR .dashDetail-TYawu {
            background: #32c8ff
        }

        [dir=ltr] .cardBorder-kX2oR .dashDetail-TYawu {
            transform-origin: left
        }

        [dir=rtl] .cardBorder-kX2oR .dashDetail-TYawu {
            transform-origin: right
        }

        .cardBorder-kX2oR .dashDetail-TYawu:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .cardBorder-kX2oR .dashDetail-TYawu:after {
            border-top: .4rem solid #32c8ff
        }

        [dir=ltr] .cardBorder-kX2oR .dashDetail-TYawu:after {
            right: -.4rem;
            border-right: .4rem solid transparent
        }

        [dir=rtl] .cardBorder-kX2oR .dashDetail-TYawu:after {
            left: -.4rem;
            border-left: .4rem solid transparent
        }

        @media (min-width: 768px) {
            [dir] .featuredContent-2BEnV {
                padding-top: 8rem;
                padding-bottom: 20rem
            }

            .featuredContent-2BEnV:after {
                width: calc(100% - 6rem)
            }

            [dir=ltr] .featuredContent-2BEnV:after {
                left: 6rem
            }

            [dir=rtl] .featuredContent-2BEnV:after {
                right: 6rem
            }

            .sectionDashDetail-8H3YA {
                height: .6rem;
                width: 15.5rem
            }

            [dir] .sectionDashDetail-8H3YA {
                background: #c89b3c
            }

            .sectionDashDetail-8H3YA:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .sectionDashDetail-8H3YA:after {
                border-top: .6rem solid #c89b3c
            }

            [dir=ltr] .sectionDashDetail-8H3YA:after {
                left: -.6rem;
                border-left: .6rem solid transparent
            }

            [dir=rtl] .sectionDashDetail-8H3YA:after {
                right: -.6rem;
                border-right: .6rem solid transparent
            }

            .background-24J8K {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 6rem 100%, 0 calc(100% - 6.5rem));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 6rem 100%, 0 calc(100% - 6.5rem))
            }

            [dir=rtl] .background-24J8K {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 6.5rem), calc(100% - 6rem) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 6.5rem), calc(100% - 6rem) 100%, 0 100%)
            }

            .sectionCroppedCorner-3jLLN {
                height: 8rem;
                width: 6rem
            }

            [dir=ltr] .sectionCroppedCorner-3jLLN {
                left: -3.2rem;
                transform: skew(42.5deg)
            }

            [dir=rtl] .sectionCroppedCorner-3jLLN {
                right: -3.2rem;
                transform: skew(-42.5deg)
            }

            [dir] .featuredContentCard-3-8x1:not(:last-child) {
                margin-bottom: 7.5rem
            }

            .backgrounImage-UwrvB {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 3rem) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3rem), calc(100% - 3rem) 100%, 0 100%)
            }

            [dir=rtl] .backgrounImage-UwrvB {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 3rem 100%, 0 calc(100% - 3rem));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 3rem 100%, 0 calc(100% - 3rem))
            }

            [dir] .title-2yOMx {
                padding: 3rem 4.5rem
            }

            .cardBorder-kX2oR {
                bottom: -1.4rem
            }

            [dir=ltr] .cardBorder-kX2oR {
                left: 1.2rem
            }

            [dir=rtl] .cardBorder-kX2oR {
                right: 1.2rem
            }

            .cardBorder-kX2oR .borderLine-zDI08.left-3Gzvo {
                width: .3rem
            }

            .cardBorder-kX2oR .borderLine-zDI08.bottom-2YbeE {
                height: .3rem
            }

            .cardBorder-kX2oR .dashDetail-TYawu {
                height: .5rem;
                width: 12.5rem;
                bottom: -.45rem
            }

            [dir] .cardBorder-kX2oR .dashDetail-TYawu {
                background: #32c8ff
            }

            .cardBorder-kX2oR .dashDetail-TYawu:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .cardBorder-kX2oR .dashDetail-TYawu:after {
                border-top: .5rem solid #32c8ff
            }

            [dir=ltr] .cardBorder-kX2oR .dashDetail-TYawu:after {
                right: -.5rem;
                border-right: .5rem solid transparent
            }

            [dir=rtl] .cardBorder-kX2oR .dashDetail-TYawu:after {
                left: -.5rem;
                border-left: .5rem solid transparent
            }
        }

        @media (min-width: 1024px) {
            [dir] .featuredContent-2BEnV {
                padding-top: 14.5rem;
                padding-bottom: 15rem
            }

            .featuredContentWrapper-2B_Vt {
                flex-direction: row;
                justify-content: space-between
            }

            .featuredContentCard-3-8x1 {
                max-width: calc(33.33333% - 2rem);
                width: 100%
            }

            [dir] .featuredContentCard-3-8x1:not(:last-child) {
                margin-bottom: 0
            }

            .backgrounImage-UwrvB {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 2rem), calc(100% - 2rem) 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 100% calc(100% - 2rem), calc(100% - 2rem) 100%, 0 100%)
            }

            [dir=rtl] .backgrounImage-UwrvB {
                -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 2rem 100%, 0 calc(100% - 2rem));
                clip-path: polygon(0 0, 100% 0, 100% 100%, 2rem 100%, 0 calc(100% - 2rem))
            }

            [dir] .title-2yOMx {
                padding: 2rem 3rem
            }

            .cardBorder-kX2oR {
                bottom: -.8rem
            }

            [dir=ltr] .cardBorder-kX2oR {
                left: .8rem
            }

            [dir=rtl] .cardBorder-kX2oR {
                right: .8rem
            }

            .cardBorder-kX2oR .borderLine-zDI08.left-3Gzvo {
                width: .2rem
            }

            .cardBorder-kX2oR .borderLine-zDI08.bottom-2YbeE {
                height: .2rem
            }

            .cardBorder-kX2oR .dashDetail-TYawu {
                height: .4rem;
                width: 8rem;
                bottom: -.4rem
            }

            [dir] .cardBorder-kX2oR .dashDetail-TYawu {
                background: #32c8ff
            }

            .cardBorder-kX2oR .dashDetail-TYawu:after {
                content: "";
                display: block;
                position: absolute;
                top: 0
            }

            [dir] .cardBorder-kX2oR .dashDetail-TYawu:after {
                border-top: .4rem solid #32c8ff
            }

            [dir=ltr] .cardBorder-kX2oR .dashDetail-TYawu:after {
                right: -.4rem;
                border-right: .4rem solid transparent
            }

            [dir=rtl] .cardBorder-kX2oR .dashDetail-TYawu:after {
                left: -.4rem;
                border-left: .4rem solid transparent
            }
        }

        .slider-2KrV2 {
            width: 100%
        }

        .header-2sFJM {
            justify-content: space-between
        }

        .header-2sFJM, .titleWrapper-sQpnN {
            display: flex;
            align-items: center
        }

        .titleIcon-3vEt6 {
            height: 2.4rem;
            width: 2.4rem
        }

        [dir=ltr] .titleIcon-3vEt6 {
            margin-right: 1.25rem
        }

        [dir=rtl] .titleIcon-3vEt6 {
            margin-left: 1.25rem
        }

        .contentWrapper-3tL1n {
            overflow: hidden;
            width: 100%
        }

        .content-3Pxay {
            display: flex
        }

        [dir] .content-3Pxay {
            padding: 2.5rem 0
        }

        .slide-1RdTx {
            min-width: 100%
        }

        [dir=ltr] .slide-1RdTx:not(:last-child) {
            padding-right: 1rem
        }

        [dir=rtl] .slide-1RdTx:not(:last-child) {
            padding-left: 1rem
        }

        .bulletWrapper-2V8ne {
            height: 1rem;
            width: 1rem;
            opacity: .4;
            transition: opacity .2s ease-out
        }

        [dir=ltr] .bulletWrapper-2V8ne:not(:last-child) {
            margin-right: 1.5rem
        }

        [dir=rtl] .bulletWrapper-2V8ne:not(:last-child) {
            margin-left: 1.5rem
        }

        .bullets-1tAM7 {
            display: flex
        }

        .bullets-1tAM7 .active-1QHAi {
            opacity: 1
        }

        .sliderControlsWrapper-29M5D {
            display: flex;
            justify-content: center;
            align-items: center
        }

        .ellipseIcon-3Si_- {
            width: .3rem
        }

        [dir] .ellipseIcon-3Si_- {
            margin: 0 2rem
        }

        .sliderButton-Xu2Kf {
            display: flex;
            justify-content: center;
            align-items: center;
            transition: opacity .3s ease-out, transform .3s ease-out;
            width: 1.2rem
        }

        .sliderButton-Xu2Kf path {
            stroke-width: 4px
        }

        [dir=ltr] .sliderButton-Xu2Kf.prevButton-3l1ud {
            transform: scale(1) rotate(-180deg)
        }

        [dir=rtl] .sliderButton-Xu2Kf.prevButton-3l1ud {
            transform: scale(1) rotate(180deg);
            transform: scale(1) rotate(0deg)
        }

        [dir] .sliderButton-Xu2Kf.nextButton--T_Xr {
            transform: scale(1) rotate(0deg)
        }

        [dir=rtl] .sliderButton-Xu2Kf.nextButton--T_Xr {
            transform: scale(1) rotate(-180deg)
        }

        .sliderButton-Xu2Kf.disabled-1yl4a {
            opacity: .5
        }

        [dir=ltr] .sliderButton-Xu2Kf.disabled-1yl4a.prevButton-3l1ud {
            transform: scale(.8) rotate(-180deg)
        }

        [dir=rtl] .sliderButton-Xu2Kf.disabled-1yl4a.prevButton-3l1ud {
            transform: scale(.8) rotate(180deg);
            transform: scale(.8) rotate(0deg)
        }

        [dir] .sliderButton-Xu2Kf.disabled-1yl4a.nextButton--T_Xr {
            transform: scale(.8) rotate(0deg)
        }

        [dir=rtl] .sliderButton-Xu2Kf.disabled-1yl4a.nextButton--T_Xr {
            transform: scale(.8) rotate(-180deg)
        }

        @media (min-width: 768px) {
            [dir=ltr] .bulletWrapper-2V8ne:not(:last-child) {
                margin-right: 1.5rem
            }

            [dir=rtl] .bulletWrapper-2V8ne:not(:last-child) {
                margin-left: 1.5rem
            }

            .titleIcon-3vEt6 {
                height: 4.75rem;
                width: 4.75rem
            }

            [dir=ltr] .titleIcon-3vEt6 {
                margin-right: 2.5rem
            }

            [dir=rtl] .titleIcon-3vEt6 {
                margin-left: 2.5rem
            }

            [dir] .content-3Pxay {
                padding: 5rem 0
            }

            [dir] .ellipseIcon-3Si_- {
                margin: 0 4rem
            }

            .sliderButton-Xu2Kf {
                width: 2.4rem
            }
        }

        [dir] .gameOverview-XPpwm {
            padding-top: 0;
            padding-bottom: 4rem;
            margin-top: 1.5rem;
            margin-bottom: 4rem;
            background: #551ed7
        }

        [dir=ltr] .gameOverview-XPpwm {
            background: linear-gradient(30deg, #551ed7 20%, #6e87ff 60%)
        }

        [dir=rtl] .gameOverview-XPpwm {
            background: linear-gradient(-30deg, #551ed7 20%, #6e87ff 60%)
        }

        .gameOverview-XPpwm .topVolt-qsFB- {
            width: 460%
        }

        .gameOverview-XPpwm .bottomVolt-K6psi {
            width: 180%
        }

        .contentWrapper-2xjF3 {
            position: relative
        }

        .contentWrapper-2xjF3, .sectionContent-3NYlN {
            display: flex;
            flex-direction: column
        }

        .videoWrapper-jHOQl {
            position: relative;
            order: 1
        }

        [dir] .videoWrapper-jHOQl {
            margin-top: 3.5rem
        }

        .videoContainer-1qUhF {
            position: relative
        }

        .videoContainer-1qUhF:before {
            content: "";
            display: block
        }

        [dir] .videoContainer-1qUhF:before {
            padding-top: 56.25%
        }

        .videoContainer-1qUhF > iframe {
            position: absolute;
            top: 0
        }

        [dir=ltr] .videoContainer-1qUhF > iframe {
            left: 0
        }

        [dir=rtl] .videoContainer-1qUhF > iframe {
            right: 0
        }

        .accordionWrapper-jYMQQ, .slider-Nkgga {
            order: 2
        }

        [dir] .slider-Nkgga {
            margin: 4.5rem 0
        }

        .learnMoreCTA-3VCMk {
            align-self: center
        }

        .videoBorder-wUX0n {
            bottom: -.8rem
        }

        [dir=ltr] .videoBorder-wUX0n {
            right: -.8rem
        }

        [dir=rtl] .videoBorder-wUX0n {
            left: -.8rem
        }

        @media (min-width: 768px) {
            [dir] .gameOverview-XPpwm {
                padding-top: 0;
                padding-bottom: 4rem;
                margin-top: 3rem;
                margin-bottom: 4rem
            }

            [dir] .videoWrapper-jHOQl {
                margin-top: 7rem
            }

            [dir] .slider-Nkgga {
                margin: 9rem 0
            }

            .videoBorder-wUX0n {
                bottom: -1.2rem
            }

            [dir=ltr] .videoBorder-wUX0n {
                right: -1.2rem
            }

            [dir=rtl] .videoBorder-wUX0n {
                left: -1.2rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .gameOverview-XPpwm {
                padding-top: 10rem;
                padding-bottom: 10rem;
                margin-top: 0;
                margin-bottom: 0
            }

            .gameOverview-XPpwm .bottomVolt-K6psi, .gameOverview-XPpwm .topVolt-qsFB- {
                width: 101%
            }

            .sectionContent-3NYlN {
                flex-direction: row;
                justify-content: space-between;
                align-items: flex-end
            }

            [dir] .sectionContent-3NYlN {
                margin-bottom: 8.5rem
            }

            .copyWrapper-2R7us {
                max-width: calc(50% - 1.5rem);
                width: 100%
            }

            .accordionWrapper-jYMQQ {
                position: relative;
                order: 1;
                min-height: 30rem
            }

            [dir=ltr] .accordionWrapper-jYMQQ {
                margin: 8rem 5rem 0 3rem
            }

            [dir=rtl] .accordionWrapper-jYMQQ {
                margin: 8rem 3rem 0 5rem
            }

            .accordion-2x0uj {
                width: 100%
            }

            [dir=ltr] .accordion-2x0uj {
                padding-right: 10%
            }

            [dir=rtl] .accordion-2x0uj {
                padding-left: 10%
            }

            .videoWrapper-jHOQl {
                max-width: calc(50% - 1.5rem);
                width: 100%;
                order: 2
            }

            [dir] .videoWrapper-jHOQl {
                margin-top: 0;
                margin-bottom: 1.5rem
            }

            [dir=ltr] .videoWrapper-jHOQl {
                transform: translateX(-3rem)
            }

            [dir=rtl] .videoWrapper-jHOQl {
                transform: translateX(3rem)
            }

            [dir=ltr] .videoContainer-1qUhF {
                margin-right: calc(-7.3vw - 3rem)
            }

            [dir=rtl] .videoContainer-1qUhF {
                margin-left: calc(-7.3vw - 3rem)
            }

            .learnMoreCTA-3VCMk {
                align-self: flex-end
            }

            .videoBorder-wUX0n {
                bottom: -1.3rem
            }

            [dir=ltr] .videoBorder-wUX0n {
                right: auto;
                left: -1.3rem
            }

            [dir=rtl] .videoBorder-wUX0n {
                left: auto;
                right: -1.3rem
            }
        }

        @media (min-width: 1280px) {
            [dir] .gameOverview-XPpwm {
                padding-top: 15rem;
                padding-bottom: 20rem
            }

            [dir=ltr] .accordion-2x0uj {
                padding-right: 15%
            }

            [dir=rtl] .accordion-2x0uj {
                padding-left: 15%
            }
        }

        @media (min-width: 1440px) {
            .gameOverview-XPpwm .topVolt-qsFB- {
                top: calc(11rem - 8vw)
            }
        }

        [dir] .gameVideoData-2MiX3 {
            padding-bottom: 2.5rem
        }

        .contentWrapper-2GeoW {
            display: flex;
            flex-direction: column
        }

        [dir] .copyWrapper-3tTRW {
            margin-bottom: 5rem
        }

        [dir=ltr] .description-1LzKr {
            padding-right: 4.5rem
        }

        [dir=rtl] .description-1LzKr {
            padding-left: 4.5rem
        }

        .detailLine-1Pn0w {
            position: relative
        }

        [dir] .detailLine-1Pn0w {
            margin-bottom: 2rem
        }

        .videoWrapper-3u_lJ {
            width: 100%;
            max-width: 67.8rem;
            position: relative;
            align-self: center
        }

        .videoWrapper-3u_lJ .whiteDiamond-IhRVM {
            position: absolute;
            width: 19%;
            top: .6rem;
            z-index: 0
        }

        [dir=ltr] .videoWrapper-3u_lJ .whiteDiamond-IhRVM {
            left: 50%;
            transform: translate(-50%, -50%)
        }

        [dir=rtl] .videoWrapper-3u_lJ .whiteDiamond-IhRVM {
            right: 50%;
            transform: translate(50%, -50%)
        }

        .videoWrapper-3u_lJ .borderDiamond-2ipXY {
            position: absolute;
            width: 12%;
            top: 1.3%;
            z-index: 1
        }

        [dir=ltr] .videoWrapper-3u_lJ .borderDiamond-2ipXY {
            left: 50%;
            transform: translate(-50%, -50%)
        }

        [dir=rtl] .videoWrapper-3u_lJ .borderDiamond-2ipXY {
            right: 50%;
            transform: translate(50%, -50%)
        }

        .videoWrapper-3u_lJ .borderOuterCircle-3en-3 {
            position: absolute;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: visible
        }

        [dir=ltr] .videoWrapper-3u_lJ .borderOuterCircle-3en-3 {
            left: 0;
            transform: rotate(-90deg)
        }

        [dir=rtl] .videoWrapper-3u_lJ .borderOuterCircle-3en-3 {
            right: 0;
            transform: rotate(90deg)
        }

        .videoWrapper-3u_lJ .borderOuterCircle-3en-3 path {
            stroke: #32c8ff
        }

        .videoWrapper-3u_lJ .borderCircleDetails-1sftD {
            stroke-width: .35rem;
            stroke: #32c8ff
        }

        .videoWrapper-3u_lJ .videoTestWrapper-3_xxs {
            width: 100%
        }

        [dir] .videoWrapper-3u_lJ .videoTestWrapper-3_xxs {
            padding-bottom: 100%
        }

        .videoWrapper-3u_lJ .gameVideo-2DQCg {
            height: 94%;
            width: 94%;
            position: absolute;
            top: 50%;
            overflow: hidden
        }

        [dir] .videoWrapper-3u_lJ .gameVideo-2DQCg {
            border-radius: 100%
        }

        [dir=ltr] .videoWrapper-3u_lJ .gameVideo-2DQCg {
            left: 50%;
            transform: translate(-50%, -50%)
        }

        [dir=rtl] .videoWrapper-3u_lJ .gameVideo-2DQCg {
            right: 50%;
            transform: translate(50%, -50%)
        }

        .videoWrapper-3u_lJ .cls1-fb8tv {
            fill: #32c8ff
        }

        .videoWrapper-3u_lJ .cls2-3nB18 {
            fill: #fff
        }

        @media (min-width: 768px) {
            [dir] .gameVideoData-2MiX3 {
                padding-top: 6rem;
                padding-bottom: 5rem
            }

            [dir] .copyWrapper-3tTRW {
                margin-bottom: 10rem
            }

            [dir=ltr] .description-1LzKr {
                padding-right: 9rem
            }

            [dir=rtl] .description-1LzKr {
                padding-left: 9rem
            }

            [dir] .detailLine-1Pn0w {
                margin-bottom: 4.5rem
            }

            .videoWrapper-3u_lJ .borderDiamond-2ipXY {
                top: .7rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .gameVideoData-2MiX3 {
                padding-bottom: 0
            }

            .contentWrapper-2GeoW {
                flex-direction: row;
                align-items: center
            }

            .copyWrapper-3tTRW {
                max-width: calc(50% - 1.5rem);
                width: 100%
            }

            [dir] .copyWrapper-3tTRW {
                margin-bottom: 0
            }

            [dir=ltr] .copyWrapper-3tTRW {
                padding-right: calc(8.33333% + .25rem)
            }

            [dir=rtl] .copyWrapper-3tTRW {
                padding-left: calc(8.33333% + .25rem)
            }

            [dir=ltr] .description-1LzKr {
                padding-right: calc(16.66667% + .5rem)
            }

            [dir=rtl] .description-1LzKr {
                padding-left: calc(16.66667% + .5rem)
            }

            [dir] .detailLine-1Pn0w {
                margin-bottom: 3rem
            }

            html[lang=tr-tr] .title-3a44c {
                font-size: 5rem
            }

            .videoWrapper-3u_lJ {
                max-width: calc(50% - 1.5rem);
                width: 100%
            }

            .videoWrapper-3u_lJ .borderDiamond-2ipXY {
                top: 1.2%
            }
        }

        @media (min-width: 1280px) {
            html[lang=tr-tr] .gameVideoData-2MiX3 .title-3a44c {
                font-size: 6rem
            }
        }

        @media (min-width: 1440px) {
            html[lang=tr-tr] .gameVideoData-2MiX3 .title-3a44c {
                font-size: 7rem
            }
        }

        .slider-3wYK0 {
            position: relative;
            overflow: hidden
        }

        .content-3whUU {
            display: flex
        }

        [dir=ltr] .content-3whUU {
            margin-left: -1rem
        }

        [dir=rtl] .content-3whUU {
            margin-right: -1rem
        }

        [dir] .mobileBullets-1e_Uk {
            margin-top: 3.3rem
        }

        .slide-WOKTc {
            min-width: 90%
        }

        [dir=ltr] .slide-WOKTc {
            padding-left: 1rem
        }

        [dir=rtl] .slide-WOKTc {
            padding-right: 1rem
        }

        .bulletWrapper-3JwQo {
            height: 1.1rem;
            width: 1.1rem;
            opacity: .4;
            transition: opacity .2s ease-out
        }

        [dir=ltr] .bulletWrapper-3JwQo:not(:last-child) {
            margin-right: 1.1rem
        }

        [dir=rtl] .bulletWrapper-3JwQo:not(:last-child) {
            margin-left: 1.1rem
        }

        .bullets-Z2Pex {
            display: flex
        }

        [dir] .bullets-Z2Pex {
            padding-bottom: .4rem
        }

        .bullets-Z2Pex .active-1znGu {
            opacity: 1
        }

        @media (min-width: 1024px) {
            .featuredNewsSlider-3SfVo {
                display: flex
            }

            [dir] .featuredNewsSlider-3SfVo {
                margin-top: 17rem
            }

            [dir=ltr] .featuredNewsSlider-3SfVo {
                transform: translateX(6.8%)
            }

            [dir=rtl] .featuredNewsSlider-3SfVo {
                transform: translateX(-6.8%)
            }

            .featuredNewsSlider-3SfVo .bulletsContainer-LMrWo {
                order: 1;
                display: flex;
                align-items: flex-end;
                flex-shrink: 0
            }

            [dir=ltr] .featuredNewsSlider-3SfVo .bulletsContainer-LMrWo {
                margin-right: 32rem
            }

            [dir=rtl] .featuredNewsSlider-3SfVo .bulletsContainer-LMrWo {
                margin-left: 32rem
            }

            .featuredNewsSlider-3SfVo .slider-3wYK0 {
                order: 2;
                width: 100%
            }

            [dir] .bullets-Z2Pex {
                padding-bottom: 0;
                margin-bottom: .5rem
            }

            .featuredNewsSliderContent-6ZTzI {
                width: 100%
            }

            [dir=ltr] .featuredNewsSliderContent-6ZTzI li {
                padding-right: 7rem
            }

            [dir=rtl] .featuredNewsSliderContent-6ZTzI li {
                padding-left: 7rem
            }

            html[dir][browser="Internet Explorer"] .featuredNewsSliderContent-6ZTzI {
                cursor: move
            }

            .customCursorWrapper-3gKvK {
                width: 100%;
                order: 2
            }
        }

        @media (min-width: 1280px) {
            .featuredNewsSliderContent-6ZTzI {
                width: 78%
            }
        }

        @media (min-width: 1440px) {
            [dir=ltr] .featuredNewsSlider-3SfVo {
                transform: translateX(calc(50% - 61.5rem))
            }

            [dir=rtl] .featuredNewsSlider-3SfVo {
                transform: translateX(calc(-50% - -61.5rem))
            }

            html[dir=ltr][browser="Internet Explorer"] .featuredNewsSlider-3SfVo {
                transform: translateX(50%) translateX(-61.5rem)
            }

            html[dir=rtl][browser="Internet Explorer"] .featuredNewsSlider-3SfVo {
                transform: translateX(-50%) translateX(61.5rem)
            }

            .featuredNewsSliderContent-6ZTzI {
                width: 60%
            }
        }

        [dir] .news-paedR {
            padding-bottom: 8rem
        }

        [dir] .news-paedR.addPaddingTop-2B-q5 {
            padding-top: 10rem
        }

        [dir] .headline-2QSde {
            margin-bottom: 2.15rem
        }

        [dir] .goToNewsPageButton-CRHHX {
            margin-top: 3.25rem
        }

        .contentWrapper-3YaON {
            display: flex
        }

        .contentItem-2Acwg {
            flex: 1 0
        }

        [dir=ltr] .contentItem-2Acwg:not(:last-child) {
            padding-right: 3rem
        }

        [dir=rtl] .contentItem-2Acwg:not(:last-child) {
            padding-left: 3rem
        }

        .sliderWrapper-1m1BR {
            overflow: visible
        }

        @media (min-width: 768px) {
            [dir] .headline-2QSde {
                margin-bottom: 4.5rem
            }

            [dir] .goToNewsPageButton-CRHHX {
                margin-top: 6.5rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .news-paedR.addPaddingTop-2B-q5 {
                padding-top: 14rem
            }

            [dir] .headline-2QSde {
                margin-bottom: 3.9rem
            }

            .goToNewsPageButton-CRHHX {
                position: absolute;
                top: 2rem
            }

            [dir] .goToNewsPageButton-CRHHX {
                margin: 0
            }

            [dir=ltr] .goToNewsPageButton-CRHHX {
                right: 0
            }

            [dir=rtl] .goToNewsPageButton-CRHHX {
                left: 0
            }
        }

        .newsletterSignup-bQZXC {
            overflow: visible
        }

        [dir] .newsletterSignup-bQZXC {
            padding-top: 5.75rem;
            padding-bottom: calc(10rem + 10vw)
        }

        .contentWrapper-kLi9R {
            max-width: 31.6rem
        }

        .background-GbTkn {
            width: 100%;
            height: calc(100% + 6rem);
            position: absolute;
            top: -6rem
        }

        [dir] .background-GbTkn {
            background-image: url(../static/newsletter-background-mobile-d9169faa5c490f6b86a3d22d8a6f1936.jpg);
            background-size: cover;
            background-repeat: no-repeat
        }

        [dir=ltr] .background-GbTkn {
            left: 0;
            background-position: 100%
        }

        [dir=rtl] .background-GbTkn {
            right: 0;
            background-position: 0
        }

        [dir] .titleWrapper-2MPEh {
            margin-bottom: 1.75rem;
            text-align: center
        }

        [dir] .title-3xrxX {
            margin-bottom: .1rem
        }

        .formContentWrapper-nS_Sx, .successSubmitWrapper-edPIV {
            min-height: 27.55rem
        }

        .successSubmitWrapper-edPIV {
            display: flex;
            justify-content: center;
            align-items: center
        }

        .loader-3F5-s {
            height: 12rem;
            width: 12rem
        }

        .policyWrapper-2fk1_ {
            display: flex;
            align-items: flex-start;
            opacity: 0
        }

        [dir] .policyWrapper-2fk1_ {
            margin-bottom: 3.25rem;
            margin-top: 3rem
        }

        html[browser="Internet Explorer"] .policyWrapper-2fk1_ {
            opacity: 1
        }

        .policyCheckbox-3wRMG {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            display: inline-block;
            position: relative
        }

        [dir] .policyCheckbox-3wRMG {
            cursor: pointer;
            background-color: transparent;
            border: 2px solid #fff;
            padding: .6rem;
            margin-top: .5rem
        }

        [dir=ltr] .policyCheckbox-3wRMG {
            margin-right: 1.25rem
        }

        [dir=rtl] .policyCheckbox-3wRMG {
            margin-left: 1.25rem
        }

        .policyCheckbox-3wRMG:after {
            content: "\2714";
            font-size: .9rem;
            position: absolute;
            color: transparent;
            top: 50%
        }

        [dir=ltr] .policyCheckbox-3wRMG:after {
            left: 50%;
            transform: translate(-50%, -50%)
        }

        [dir=rtl] .policyCheckbox-3wRMG:after {
            right: 50%;
            transform: translate(50%, -50%)
        }

        .policyCheckbox-3wRMG:checked:after {
            transition: color .1s ease-in;
            color: #fff
        }

        .registrationInputWrapper-3h-mk {
            position: relative;
            transition: margin-bottom .2s ease-out
        }

        [dir] .registrationInputWrapper-3h-mk:not(:last-child) {
            margin-bottom: 1.5rem
        }

        .registrationInputWrapper-3h-mk:before {
            content: "";
            display: block;
            position: absolute;
            height: calc(100% - .1rem);
            width: .2rem;
            top: .1rem;
            transition: background-color .2s ease-out
        }

        [dir] .registrationInputWrapper-3h-mk:before {
            background-color: #fff
        }

        [dir=ltr] .registrationInputWrapper-3h-mk:before {
            left: 0
        }

        [dir=rtl] .registrationInputWrapper-3h-mk:before {
            right: 0
        }

        [dir] .error-1G_g7 .registrationInputWrapper-3h-mk:before {
            background-color: #cc3c55
        }

        .dashDetail-3PTsn {
            height: 3.5rem;
            width: .25rem;
            position: absolute;
            top: 0
        }

        [dir] .dashDetail-3PTsn {
            background: #fff
        }

        [dir=ltr] .dashDetail-3PTsn {
            left: -.25rem;
            transform-origin: right
        }

        [dir=rtl] .dashDetail-3PTsn {
            right: -.25rem;
            transform-origin: left
        }

        .dashDetail-3PTsn:after {
            content: "";
            display: block;
            position: absolute;
            bottom: -.25rem
        }

        [dir] .dashDetail-3PTsn:after {
            border-top: .25rem solid #fff
        }

        [dir=ltr] .dashDetail-3PTsn:after {
            right: 0;
            border-left: .25rem solid transparent
        }

        [dir=rtl] .dashDetail-3PTsn:after {
            left: 0;
            border-right: .25rem solid transparent
        }

        .dashDetail-3PTsn:before {
            transition: background-color .2s ease-out
        }

        .error-1G_g7 .dashDetail-3PTsn {
            height: 3.5rem;
            width: .25rem
        }

        [dir] .error-1G_g7 .dashDetail-3PTsn {
            background: #cc3c55
        }

        .error-1G_g7 .dashDetail-3PTsn:after {
            content: "";
            display: block;
            position: absolute;
            bottom: -.25rem
        }

        [dir] .error-1G_g7 .dashDetail-3PTsn:after {
            border-top: .25rem solid #cc3c55
        }

        [dir=ltr] .error-1G_g7 .dashDetail-3PTsn:after {
            right: 0;
            border-left: .25rem solid transparent
        }

        [dir=rtl] .error-1G_g7 .dashDetail-3PTsn:after {
            left: 0;
            border-right: .25rem solid transparent
        }

        .inputsWrapper-1MXam {
            opacity: 0
        }

        html[browser="Internet Explorer"] .inputsWrapper-1MXam {
            opacity: 1
        }

        .inputFrame-38B2f {
            height: 100%;
            width: calc(100% + 1rem);
            position: absolute;
            top: 0
        }

        [dir=ltr] .inputFrame-38B2f {
            left: 0
        }

        [dir=rtl] .inputFrame-38B2f {
            right: 0;
            transform: scaleX(-1)
        }

        .inputFrame-38B2f path {
            stroke: #fff;
            transition: stroke .2s ease-out
        }

        .error-1G_g7 .inputFrame-38B2f path {
            stroke: #cc3c55
        }

        .error-1G_g7 {
            color: #cc3c55
        }

        [dir] .error-1G_g7 {
            margin-bottom: 2rem
        }

        .errorMessage-3xvhh {
            color: #cc3c55
        }

        [dir] .errorMessage-3xvhh {
            margin-top: -1.5rem;
            margin-bottom: 1rem
        }

        .policyCopy-1CvAm a {
            position: relative;
            display: inline-block;
            transition: color .2s ease-out
        }

        .policyCopy-1CvAm a:before {
            content: "";
            display: block;
            position: absolute;
            bottom: -.01rem;
            width: 100%;
            height: .1rem;
            transition: background-color .2s ease-out
        }

        [dir] .policyCopy-1CvAm a:before {
            background-color: #fff
        }

        [dir=ltr] .policyCopy-1CvAm a:before {
            left: 0
        }

        [dir=rtl] .policyCopy-1CvAm a:before {
            right: 0
        }

        .policyCopy-1CvAm a:hover {
            color: #141e37
        }

        [dir] .policyCopy-1CvAm a:hover:before {
            background-color: #141e37
        }

        html[browser="Internet Explorer"] .policyCopy-1CvAm p {
            width: 42%
        }

        .placeholdersWrapper-2_Dei {
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0;
            pointer-events: none;
            display: flex;
            align-items: center;
            justify-content: space-between
        }

        [dir] .placeholdersWrapper-2_Dei {
            padding: 1.8rem
        }

        [dir=ltr] .placeholdersWrapper-2_Dei {
            left: 0
        }

        [dir=rtl] .placeholdersWrapper-2_Dei {
            right: 0
        }

        .placeholdersWrapper-2_Dei.hide-2r88T {
            display: none
        }

        .placeholderText-3Ux1t {
            color: #fff;
            opacity: 0;
            transition: color .2s ease-out
        }

        html[browser="Internet Explorer"] .placeholderText-3Ux1t {
            opacity: 1
        }

        .error-1G_g7 .placeholderText-3Ux1t {
            color: #cc3c55
        }

        .dateFormat-2s2xP, html[browser="Internet Explorer"] .dateFormat-2s2xP {
            opacity: .5
        }

        .registrationInput-2hfmT {
            width: 100%;
            position: relative;
            color: #fff;
            transition: color .2s ease-out
        }

        [dir] .registrationInput-2hfmT {
            padding: 1.8rem;
            border: none;
            background: transparent
        }

        .error-1G_g7 .registrationInput-2hfmT {
            color: #cc3c55
        }

        .registrationInput-2hfmT:-ms-input-placeholder {
            opacity: .6;
            color: #fff;
            -ms-transition: color .2s ease-out;
            transition: color .2s ease-out
        }

        .registrationInput-2hfmT::placeholder {
            opacity: .6;
            color: #fff;
            transition: color .2s ease-out
        }

        .error-1G_g7 .registrationInput-2hfmT:-ms-input-placeholder {
            color: #cc3c55
        }

        .error-1G_g7 .registrationInput-2hfmT::placeholder {
            color: #cc3c55
        }

        .registrationInput-2hfmT:focus {
            outline: none
        }

        .submitWrapper-21lY4 {
            display: flex;
            flex-direction: column;
            align-items: center
        }

        .submitButton-3Syy0 {
            max-width: calc(50% - 1.5rem);
            width: 100%;
            transition: opacity .2s ease-out;
            display: flex;
            justify-content: center
        }

        .submitButton-3Syy0.disabled-1vkB8 {
            pointer-events: none;
            opacity: .4
        }

        [dir] .submitButton-3Syy0.disabled-1vkB8 {
            cursor: none
        }

        .bottomVolt-2g84E {
            width: 76.8rem
        }

        [dir] .bottomVolt-2g84E {
            transform: scaleX(1) translateY(-15%)
        }

        [dir=rtl] .bottomVolt-2g84E {
            transform: scaleX(-1) translateY(-15%)
        }

        @media (min-width: 768px) {
            [dir] .newsletterSignup-bQZXC {
                padding-top: 11.5rem;
                padding-bottom: calc(24rem + 5vw)
            }

            .contentWrapper-kLi9R {
                max-width: 63.2rem
            }

            [dir] .titleWrapper-2MPEh {
                margin-bottom: 4rem
            }

            [dir] .title-3xrxX {
                margin-bottom: .2rem
            }

            .formContentWrapper-nS_Sx, .successSubmitWrapper-edPIV {
                min-height: 54rem
            }

            .loader-3F5-s {
                height: 20rem;
                width: 20rem
            }

            [dir] .registrationInputWrapper-3h-mk:not(:last-child) {
                margin-bottom: 3rem
            }

            .dashDetail-3PTsn {
                position: absolute;
                top: 0;
                height: 7rem;
                width: .7rem
            }

            [dir] .dashDetail-3PTsn {
                background: #fff
            }

            [dir=ltr] .dashDetail-3PTsn {
                left: -.5rem
            }

            [dir=rtl] .dashDetail-3PTsn {
                right: -.5rem
            }

            .dashDetail-3PTsn:after {
                content: "";
                display: block;
                position: absolute;
                bottom: -.7rem
            }

            [dir] .dashDetail-3PTsn:after {
                border-top: .7rem solid #fff
            }

            [dir=ltr] .dashDetail-3PTsn:after {
                right: 0;
                border-left: .7rem solid transparent
            }

            [dir=rtl] .dashDetail-3PTsn:after {
                left: 0;
                border-right: .7rem solid transparent
            }

            .error-1G_g7 .dashDetail-3PTsn {
                height: 7rem;
                width: .7rem
            }

            [dir] .error-1G_g7 .dashDetail-3PTsn {
                background: #cc3c55
            }

            .error-1G_g7 .dashDetail-3PTsn:after {
                content: "";
                display: block;
                position: absolute;
                bottom: -.7rem
            }

            [dir] .error-1G_g7 .dashDetail-3PTsn:after {
                border-top: .7rem solid #cc3c55
            }

            [dir=ltr] .error-1G_g7 .dashDetail-3PTsn:after {
                right: 0;
                border-left: .7rem solid transparent
            }

            [dir=rtl] .error-1G_g7 .dashDetail-3PTsn:after {
                left: 0;
                border-right: .7rem solid transparent
            }

            [dir] .placeholdersWrapper-2_Dei, [dir] .registrationInput-2hfmT {
                padding: 3.5rem 4rem
            }

            .registrationInput-2hfmT:before {
                height: 80%;
                width: .25rem;
                top: 0
            }

            [dir=ltr] .registrationInput-2hfmT:before {
                left: .25rem
            }

            [dir=rtl] .registrationInput-2hfmT:before {
                right: .25rem
            }

            [dir] .errorMessage-3xvhh {
                margin-top: -1.5rem;
                margin-bottom: 2.5rem
            }

            [dir] .policyWrapper-2fk1_ {
                margin-bottom: 6.5rem;
                margin-top: 6rem
            }

            [dir] .policyCheckbox-3wRMG {
                padding: 1.2rem
            }

            [dir=ltr] .policyCheckbox-3wRMG {
                margin-right: 2.5rem
            }

            [dir=rtl] .policyCheckbox-3wRMG {
                margin-left: 2.5rem
            }

            .policyCheckbox-3wRMG:after {
                font-size: 1.8rem
            }

            .bottomVolt-2g84E {
                width: 102.4rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .newsletterSignup-bQZXC {
                padding-bottom: calc(9rem + 12vw);
                padding-top: 8.5rem
            }

            .contentWrapper-kLi9R {
                max-width: 123rem
            }

            [dir] .background-GbTkn {
                background-image: url(../static/newsletter-background-desktop-c105888e4ff9d9507ec1af0ef5a46505.jpg)
            }

            .newsletterForm-24Z2c {
                max-width: calc(83.33333% - .5rem);
                width: 100%
            }

            [dir] .newsletterForm-24Z2c {
                margin: 0 auto
            }

            .formContentWrapper-nS_Sx {
                display: flex
            }

            .formContentWrapper-nS_Sx, .successSubmitWrapper-edPIV {
                min-height: 14.1rem
            }

            .loader-3F5-s {
                height: 12rem;
                width: 12rem
            }

            .titleWrapper-2MPEh {
                max-width: calc(83.33333% - .5rem);
                width: 100%
            }

            [dir] .titleWrapper-2MPEh {
                margin: 0 auto 3.5rem
            }

            [dir=ltr] .titleWrapper-2MPEh {
                text-align: left
            }

            [dir=rtl] .titleWrapper-2MPEh {
                text-align: right
            }

            [dir] .title-3xrxX {
                margin-bottom: .1rem
            }

            .inputFrame-38B2f {
                width: calc(100% + 3rem)
            }

            .inputsWrapper-1MXam {
                max-width: calc(50% - 1.5rem);
                width: 100%
            }

            [dir=ltr] .inputsWrapper-1MXam {
                padding-right: calc(10% + .3rem)
            }

            [dir=rtl] .inputsWrapper-1MXam {
                padding-left: calc(10% + .3rem)
            }

            .dashDetail-3PTsn {
                height: 3.5rem;
                width: .5rem;
                position: absolute;
                top: 0
            }

            [dir] .dashDetail-3PTsn {
                background: #fff
            }

            [dir=ltr] .dashDetail-3PTsn {
                left: -.3rem
            }

            [dir=rtl] .dashDetail-3PTsn {
                right: -.3rem
            }

            .dashDetail-3PTsn:after {
                content: "";
                display: block;
                position: absolute;
                bottom: -.5rem
            }

            [dir] .dashDetail-3PTsn:after {
                border-top: .5rem solid #fff
            }

            [dir=ltr] .dashDetail-3PTsn:after {
                right: 0;
                border-left: .5rem solid transparent
            }

            [dir=rtl] .dashDetail-3PTsn:after {
                left: 0;
                border-right: .5rem solid transparent
            }

            .error-1G_g7 .dashDetail-3PTsn {
                height: 3.5rem;
                width: .5rem
            }

            [dir] .error-1G_g7 .dashDetail-3PTsn {
                background: #cc3c55
            }

            .error-1G_g7 .dashDetail-3PTsn:after {
                content: "";
                display: block;
                position: absolute;
                bottom: -.5rem
            }

            [dir] .error-1G_g7 .dashDetail-3PTsn:after {
                border-top: .5rem solid #cc3c55
            }

            [dir=ltr] .error-1G_g7 .dashDetail-3PTsn:after {
                right: 0;
                border-left: .5rem solid transparent
            }

            [dir=rtl] .error-1G_g7 .dashDetail-3PTsn:after {
                left: 0;
                border-right: .5rem solid transparent
            }

            [dir] .registrationInputWrapper-3h-mk:not(:last-child) {
                margin-bottom: 1.5rem
            }

            [dir=ltr] .placeholdersWrapper-2_Dei, [dir=ltr] .registrationInput-2hfmT {
                padding: 2.3rem 1rem 2.3rem 3rem
            }

            [dir=rtl] .placeholdersWrapper-2_Dei, [dir=rtl] .registrationInput-2hfmT {
                padding: 2.3rem 3rem 2.3rem 1rem
            }

            [dir] .errorMessage-3xvhh {
                margin-top: -1.5rem;
                margin-bottom: 1rem
            }

            .submitWrapper-21lY4 {
                max-width: calc(50% - 1.5rem);
                width: 100%;
                align-items: flex-start
            }

            [dir=ltr] .submitWrapper-21lY4 {
                padding-right: calc(10% + .3rem)
            }

            [dir=rtl] .submitWrapper-21lY4 {
                padding-left: calc(10% + .3rem)
            }

            [dir] .policyWrapper-2fk1_ {
                margin-bottom: 3.7rem;
                margin-top: 0
            }

            [dir] .policyCheckbox-3wRMG {
                padding: .6rem
            }

            [dir=ltr] .policyCheckbox-3wRMG {
                margin-right: 1.5rem
            }

            [dir=rtl] .policyCheckbox-3wRMG {
                margin-left: 1.5rem
            }

            .policyCheckbox-3wRMG:after {
                font-size: .9rem
            }

            .submitButton-3Syy0 {
                max-width: calc(50% - 1.5rem);
                width: 100%;
                justify-content: flex-start
            }

            .bottomVolt-2g84E {
                width: 100%
            }
        }

        @media (min-width: 1280px) {
            .newsletterForm-24Z2c {
                max-width: calc(66.66667% - 1rem);
                width: 100%
            }

            [dir] .newsletterForm-24Z2c {
                margin: 0 auto
            }

            .inputsWrapper-1MXam {
                max-width: calc(50% - 1.5rem);
                width: 100%
            }

            [dir=ltr] .inputsWrapper-1MXam {
                padding-right: calc(12.5% + .375rem)
            }

            [dir=rtl] .inputsWrapper-1MXam {
                padding-left: calc(12.5% + .375rem)
            }

            .titleWrapper-2MPEh {
                max-width: calc(66.66667% - 1rem);
                width: 100%
            }

            .submitWrapper-21lY4 {
                max-width: calc(62.5% - 1.125rem);
                width: 100%
            }

            [dir=ltr] .submitWrapper-21lY4 {
                padding-right: calc(12.5% + .375rem);
                padding-left: 1.5rem
            }

            [dir=rtl] .submitWrapper-21lY4 {
                padding-left: calc(12.5% + .375rem);
                padding-right: 1.5rem
            }
        }

        .errorMessage-3Pbwj {
            color: #981959;
            position: absolute;
            bottom: -2.5rem
        }

        [dir=ltr] .errorMessage-3Pbwj {
            left: 0
        }

        [dir=rtl] .errorMessage-3Pbwj {
            right: 0
        }

        @media (min-width: 768px) {
            .errorMessage-3Pbwj {
                bottom: -4rem
            }
        }

        @media (min-width: 1024px) {
            .errorMessage-3Pbwj {
                bottom: -1.5rem
            }
        }

        .registrationHero {
            position: relative;
            z-index: 1;
            min-height: 56rem;
            display: flex;
            align-items: flex-end
        }

        [dir] .registrationHero {
            text-align: center
        }

        .registrationHero:after {
            content: "";
            display: block;
            position: absolute;
            width: calc(100% - 6rem);
            height: .4rem;
            bottom: 0
        }

        [dir] .registrationHero:after {
            background: #c89b3c
        }

        [dir=ltr] .registrationHero:after {
            left: 0
        }

        [dir=rtl] .registrationHero:after {
            right: 0
        }

        html[browser="Internet Explorer"] .registrationHero:after {
            width: 100%
        }

        .registrationHero .dashDetail-2uqQY {
            height: .6rem;
            width: 28%;
            position: absolute;
            bottom: -.6rem
        }

        [dir] .registrationHero .dashDetail-2uqQY {
            background: #c89b3c
        }

        [dir=ltr] .registrationHero .dashDetail-2uqQY {
            left: 0
        }

        [dir=rtl] .registrationHero .dashDetail-2uqQY {
            right: 0
        }

        .registrationHero .dashDetail-2uqQY:after {
            content: "";
            display: block;
            position: absolute;
            top: 0
        }

        [dir] .registrationHero .dashDetail-2uqQY:after {
            border-top: .6rem solid #c89b3c
        }

        [dir=ltr] .registrationHero .dashDetail-2uqQY:after {
            right: -.6rem;
            border-right: .6rem solid transparent
        }

        [dir=rtl] .registrationHero .dashDetail-2uqQY:after {
            left: -.6rem;
            border-left: .6rem solid transparent
        }

        .registrationHero .croppedCorner-Zrd2g {
            height: 6.5rem;
            width: 6rem;
            position: absolute;
            bottom: 0;
            z-index: 1
        }

        [dir=ltr] .registrationHero .croppedCorner-Zrd2g {
            right: 2.5rem;
            transform: skew(-47deg);
            border-right: 6px solid #c89b3c
        }

        [dir=rtl] .registrationHero .croppedCorner-Zrd2g {
            left: 2.5rem;
            transform: skew(47deg);
            border-left: 6px solid #c89b3c
        }

        html[browser="Internet Explorer"] .registrationHero .croppedCorner-Zrd2g {
            display: none
        }

        .registrationHero .contentWrapper-3LPGz {
            display: flex;
            flex-direction: column;
            align-items: flex-end;
        }

        [dir] .registrationHero-2sgwh .contentWrapper-3LPGz {
            padding: 9.75rem 0 6.25rem
        }

        .registrationHero-2sgwh .heroVideo-3UE9M {
            overflow: hidden;
            position: absolute;
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 6rem), calc(100% - 6.5rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 6rem), calc(100% - 6.5rem) 100%, 0 100%);
            top: 0
        }

        [dir=ltr] .registrationHero-2sgwh .heroVideo-3UE9M {
            left: 0
        }

        [dir=rtl] .registrationHero-2sgwh .heroVideo-3UE9M {
            right: 0
        }

        .registrationHero-2sgwh .heroVideo-3UE9M:after {
            content: "";
            display: block;
            position: absolute;
            height: 100%;
            width: 100%;
            top: 0;
            opacity: .2
        }

        [dir] .registrationHero-2sgwh .heroVideo-3UE9M:after {
            background-color: #111
        }

        [dir=ltr] .registrationHero-2sgwh .heroVideo-3UE9M:after {
            left: 0
        }

        [dir=rtl] .registrationHero-2sgwh .heroVideo-3UE9M:after {
            right: 0
        }

        [dir=rtl] .registrationHero-2sgwh .heroVideo-3UE9M {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 6.5rem 100%, 0 calc(100% - 6rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 6.5rem 100%, 0 calc(100% - 6rem))
        }

        html[browser="Internet Explorer"] .registrationHero-2sgwh .heroVideo-3UE9M {
            -webkit-clip-path: none;
            clip-path: none
        }

        [dir] .registrationHero-2sgwh .subtitle-1gfbj {
            margin-bottom: 2.5rem
        }

        .registrationHero-2sgwh .mainLogo-3lkqG {
            width: 24.25rem;
            min-height: 15.9rem
        }

        [dir] .registrationHero-2sgwh .mainLogo-3lkqG {
            margin-bottom: 2rem
        }

        .registrationHero-2sgwh .tagline-2Rr0N {
            width: 100%;
            width: 32.5rem
        }

        [dir] .registrationHero-2sgwh .tagline-2Rr0N {
            padding: 0 2rem;
            margin-bottom: 3.5rem
        }


        html[lang=ko-kr] .registrationHero-2sgwh .tagline-2Rr0N {
            white-space: nowrap
        }

        [dir] .registrationHero-2sgwh .registerButton-1q_uN {
            margin-bottom: 5.9rem
        }


        .registrationHero-2sgwh .availableText-3om5c {
            flex-shrink: 0
        }

        [dir] .registrationHero-2sgwh .availableText-3om5c {
            margin-bottom: 1.75rem
        }

        .registrationHero-2sgwh .appButton-1Kjwz {
            height: 3.5rem
        }

        [dir] .registrationHero-2sgwh .appButton-1Kjwz {
            margin: 0 .75rem
        }

        .registrationHero-2sgwh .appButton-1Kjwz img {
            height: 100%;
            width: auto
        }

        [dir] .registrationHero-2sgwh .appButton-1Kjwz:not(:last-child) {
            margin-bottom: 1.5rem
        }

        .registrationHero-2sgwh .disabled-241-e {
            height: 3.5rem;
            width: auto;
            position: relative
        }

        .registrationHero-2sgwh .disabled-241-e:after {
            content: "";
            display: block;
            position: absolute;
            height: 100%;
            width: 100%;
            pointer-events: none;
            top: 0;
            opacity: .6
        }

        [dir] .registrationHero-2sgwh .disabled-241-e:after {
            background-color: #888;
            border-radius: .6rem
        }

        [dir=ltr] .registrationHero-2sgwh .disabled-241-e:after {
            left: 0
        }

        [dir=rtl] .registrationHero-2sgwh .disabled-241-e:after {
            right: 0
        }

        .registrationHero-2sgwh .storesWrapper-3IcOf {
            display: flex;
            flex-wrap: wrap;
            justify-content: center
        }

        @media (min-width: 480px) {
            [dir=ltr] .registrationHero-2sgwh .appButton-1Kjwz:not(:last-child) {
                margin-right: 1.5rem
            }

            [dir=rtl] .registrationHero-2sgwh .appButton-1Kjwz:not(:last-child) {
                margin-left: 1.5rem
            }
        }

        @media (min-width: 768px) {
            .registrationHero-2sgwh {
                min-height: 113.1rem
            }

            [dir] .registrationHero-2sgwh .contentWrapper-3LPGz {
                padding: 19.5rem 0 12.5rem
            }

            [dir] .registrationHero-2sgwh .subtitle-1gfbj {
                margin-bottom: 5rem
            }

            .registrationHero-2sgwh .mainLogo-3lkqG {
                width: 48.5rem;
                min-height: 31.6rem
            }

            [dir] .registrationHero-2sgwh .mainLogo-3lkqG {
                margin-bottom: 4rem
            }

            .registrationHero-2sgwh .tagline-2Rr0N {
                width: 62.5rem
            }

            [dir] .registrationHero-2sgwh .tagline-2Rr0N {
                margin-bottom: 7rem
            }


            [dir] .registrationHero-2sgwh .registerButton-1q_uN {
                margin-bottom: 11.8rem
            }

            [dir] .registrationHero-2sgwh .availableText-3om5c {
                margin-bottom: 3.5rem
            }

            .registrationHero-2sgwh .storesWrapper-3IcOf {
                height: 6rem
            }

            .registrationHero-2sgwh .appButton-1Kjwz, .registrationHero-2sgwh .disabled-241-e {
                height: 4rem
            }
        }

        @media (min-width: 1024px) {
            .registrationHero-2sgwh {
                min-height: 67rem
            }

            [dir] .registrationHero-2sgwh .contentWrapper-3LPGz {
                padding: 12rem 0 7rem
            }

            [dir] .registrationHero-2sgwh .subtitle-1gfbj {
                margin-bottom: 3rem
            }

            .registrationHero-2sgwh .mainLogo-3lkqG {
                width: 35rem;
                min-height: 22.8rem
            }

            [dir] .registrationHero-2sgwh .mainLogo-3lkqG {
                margin-bottom: 2.5rem
            }

            .registrationHero-2sgwh .tagline-2Rr0N {
                max-width: calc(33.33333% - 2rem);
                width: 100%
            }

            [dir] .registrationHero-2sgwh .tagline-2Rr0N {
                padding: 0
            }

            [dir] .registrationHero-2sgwh .availableText-3om5c {
                margin-bottom: 0;
                margin-top: 0
            }

            [dir=ltr] .registrationHero-2sgwh .availableText-3om5c {
                margin-right: 3rem
            }

            [dir=rtl] .registrationHero-2sgwh .availableText-3om5c {
                margin-left: 3rem
            }

            .registrationHero-2sgwh .dashDetail-2uqQY {
                height: .6rem;
                width: 21.5rem
            }

            .registrationHero-2sgwh .storesWrapper-3IcOf {
                height: 4rem
            }

        }

        @media (min-width: 1280px) {
            [dir] .registrationHero-2sgwh .tagline-2Rr0N {
                padding: 0 3.5%;
                margin-bottom: 4rem
            }

            [dir] .registrationHero-2sgwh .registerButton-1q_uN {
                margin-bottom: 7rem
            }
        }

        .slider-QTLO- {
            overflow: hidden;
            display: block
        }

        .carouselWrapper-36L0j {
            overflow: hidden;
            position: relative
        }

        [dir] .carouselWrapper-36L0j {
            margin-bottom: 1rem
        }

        .carouselContent-3mm1K {
            display: flex
        }

        [dir] .slideTitle-2UHxP {
            margin-bottom: 1.25rem
        }

        .textDivider-2w2oV {
            height: auto;
            width: .6rem
        }

        [dir=ltr] .textDivider-2w2oV {
            margin: 0 2% .25rem 3%
        }

        [dir=rtl] .textDivider-2w2oV {
            margin: 0 3% .25rem 2%
        }

        html[dir][lang=ar-ae] .textDivider-2w2oV {
            margin: 0 3% .25rem
        }

        @media (min-width: 768px) {
            .textDivider-2w2oV {
                width: 1.2rem
            }

            [dir] .textDivider-2w2oV {
                margin-bottom: .5rem
            }
        }

        .slide-xQgJO {
            position: relative;
            min-width: 100%
        }

        .copyWrapper-1e_WL {
            max-width: 70%;
            min-height: 9rem
        }

        [dir] .copyWrapper-1e_WL {
            margin: 1.6rem auto 2rem
        }

        .slideImage-1O9Ny {
            position: absolute;
            top: 0;
            width: 100%;
            height: 100%
        }

        [dir=ltr] .slideImage-1O9Ny {
            left: 0
        }

        [dir=rtl] .slideImage-1O9Ny {
            right: 0
        }

        .square-ux7uO {
            height: 0;
            overflow: hidden;
            position: relative
        }

        [dir] .square-ux7uO {
            padding-bottom: 100%
        }

        .square-ux7uO:after {
            content: "";
            position: absolute;
            top: 0;
            bottom: 0
        }

        [dir] .square-ux7uO:after {
            background: #000
        }

        [dir=ltr] .square-ux7uO:after {
            left: 0;
            right: 0;
            background: linear-gradient(90deg, rgba(0, 0, 0, .6), transparent 25%, transparent 75%, rgba(0, 0, 0, .6))
        }

        [dir=rtl] .square-ux7uO:after {
            right: 0;
            left: 0;
            background: linear-gradient(-90deg, rgba(0, 0, 0, .6), transparent 25%, transparent 75%, rgba(0, 0, 0, .6))
        }

        .sliderArrows-2-f5K {
            position: absolute;
            top: 0;
            bottom: 0
        }

        [dir=ltr] .sliderArrows-2-f5K, [dir=rtl] .sliderArrows-2-f5K {
            right: 0;
            left: 0
        }

        .sliderArrowsWrapper-2xWYa {
            position: absolute;
            top: 35.6%;
            width: 100%;
            display: flex;
            justify-content: space-between
        }

        [dir] .sliderArrowsWrapper-2xWYa {
            padding: 0 calc(.25rem + 2%)
        }

        [dir=ltr] .sliderArrowsWrapper-2xWYa {
            left: 0
        }

        [dir=rtl] .sliderArrowsWrapper-2xWYa {
            right: 0
        }

        .sliderArrow-1U8VE {
            display: flex;
            color: #fff;
            justify-content: center;
            align-items: center;
            transition: opacity .3s ease-out, transform .3s ease-out;
            width: calc(1rem + 5%)
        }

        [dir=ltr] .sliderArrow-1U8VE.arrowPrev-30IqQ {
            transform: rotate(-180deg)
        }

        [dir=rtl] .sliderArrow-1U8VE.arrowPrev-30IqQ {
            transform: rotate(180deg);
            transform: rotate(0deg)
        }

        [dir] .sliderArrow-1U8VE.arrowNext-2ZecR {
            transform: rotate(0deg)
        }

        [dir=rtl] .sliderArrow-1U8VE.arrowNext-2ZecR {
            transform: rotate(-180deg)
        }

        .sliderArrow-1U8VE.disabled-2Dm22 {
            opacity: .5
        }

        [dir=ltr] .sliderArrow-1U8VE.disabled-2Dm22.arrowPrev-30IqQ {
            transform: scale(.8) rotate(-180deg)
        }

        [dir=rtl] .sliderArrow-1U8VE.disabled-2Dm22.arrowPrev-30IqQ {
            transform: scale(.8) rotate(180deg);
            transform: scale(.8) rotate(0deg)
        }

        [dir] .sliderArrow-1U8VE.disabled-2Dm22.arrowNext-2ZecR {
            transform: scale(.8) rotate(0deg)
        }

        [dir=rtl] .sliderArrow-1U8VE.disabled-2Dm22.arrowNext-2ZecR {
            transform: scale(.8) rotate(-180deg)
        }

        .svgFrame-33NL2 {
            position: absolute;
            top: 0;
            bottom: 0;
            z-index: 1
        }

        [dir] .svgFrame-33NL2 {
            padding: 1rem 1.3rem;
            transform: translateY(8px)
        }

        [dir=ltr] .svgFrame-33NL2, [dir=rtl] .svgFrame-33NL2 {
            right: 0;
            left: 0
        }

        .bulletsWrapper-2nzKe {
            display: flex;
            justify-content: center
        }

        [dir] .bulletsWrapper-2nzKe {
            padding-bottom: 1rem
        }

        .bulletsWrapper-2nzKe .active-2IHVo {
            opacity: 1
        }

        .bulletWrapper-3UUOp {
            height: 1rem;
            width: 1rem;
            opacity: .4;
            transition: opacity .2s ease-out
        }

        [dir=ltr] .bulletWrapper-3UUOp:not(:last-child) {
            margin-right: 1.5rem
        }

        [dir=rtl] .bulletWrapper-3UUOp:not(:last-child) {
            margin-left: 1.5rem
        }

        @media (min-width: 480px) {
            .sliderArrowsWrapper-2xWYa {
                top: 35%
            }

            [dir] .sliderArrowsWrapper-2xWYa {
                padding: 0 calc(.5rem + 4%)
            }

            .sliderArrow-1U8VE {
                width: calc(1.5rem + 5%)
            }
        }

        @media (min-width: 768px) {
            [dir] .bulletsWrapper-2nzKe {
                margin-top: 3rem
            }

            [dir] .slideTitle-2UHxP {
                margin-bottom: 3rem
            }

            .copyWrapper-1e_WL {
                min-height: 15rem
            }

            [dir] .copyWrapper-1e_WL {
                margin: 3.5rem auto 3rem
            }

            [dir=ltr] .bulletWrapper-3UUOp:not(:last-child) {
                margin-right: 2rem
            }

            [dir=rtl] .bulletWrapper-3UUOp:not(:last-child) {
                margin-left: 2rem
            }

            .bulletIcon-32EPA {
                height: 1.5rem;
                width: 1.5rem
            }

            [dir] .content-qI8xn {
                padding: 5rem 0
            }

            [dir] .ellipseIcon-1yn8Q {
                margin: 0 4rem
            }

            .sliderButton-CLVsE {
                width: 2.4rem
            }

            .sliderArrowsWrapper-2xWYa {
                top: 34.3%
            }

            [dir] .sliderArrowsWrapper-2xWYa {
                padding: 0 3rem
            }

            .sliderArrow-1U8VE {
                width: calc(3rem + 5%)
            }
        }

        @media (min-width: 1024px) {
            [dir] .copyWrapper-1-vul {
                padding: 2rem 6rem
            }

            [dir] .slideTitle-3rDd5 {
                margin-bottom: .5rem
            }

            .textDivider-3J3im {
                height: auto;
                width: .7rem
            }

            [dir=ltr] .textDivider-3J3im {
                margin: 0 2% .4rem 3%
            }

            [dir=rtl] .textDivider-3J3im {
                margin: 0 3% .4rem 2%
            }

            html[dir][lang=ar-ae] .textDivider-3J3im {
                margin: 0 3% .4rem
            }

            .frameWrapper-3mqtm {
                position: absolute;
                top: 2.5rem;
                height: 100%;
                width: 46rem
            }

            [dir=ltr] .frameWrapper-3mqtm {
                left: 50%;
                transform: translateX(-50%)
            }

            [dir=rtl] .frameWrapper-3mqtm {
                right: 50%;
                transform: translateX(50%)
            }

            .svgFrame-3lPBN {
                position: relative;
                width: 100%;
                height: 100%;
                z-index: 1
            }

            .bulletsWrapper-3OXLk {
                position: absolute;
                bottom: 0;
                width: 100%;
                display: flex;
                justify-content: center
            }

            [dir=ltr] .bulletsWrapper-3OXLk {
                left: 0
            }

            [dir=rtl] .bulletsWrapper-3OXLk {
                right: 0
            }

            .bulletsWrapper-3OXLk .active-1RM8H {
                opacity: 1
            }

            .bulletWrapper-28DCA {
                height: 1.5rem;
                width: 1.5rem;
                opacity: .4;
                transition: opacity .2s ease-out
            }

            [dir=ltr] .bulletWrapper-28DCA:not(:last-child) {
                margin-right: 1rem
            }

            [dir=rtl] .bulletWrapper-28DCA:not(:last-child) {
                margin-left: 1rem
            }

            .desktopSlider-x9BLa {
                min-height: 76.5rem;
                opacity: 0
            }

            [dir=ltr] .desktopSlider-x9BLa, [dir=rtl] .desktopSlider-x9BLa {
                animation: fadeIn-31EOE .1s ease-out .3s forwards
            }

            .sliderWrapper-3LBL1 {
                position: relative;
                max-width: 192rem
            }

            [dir] .sliderWrapper-3LBL1 {
                margin: 0 auto
            }

            .slider-2AOh5 {
                display: flex
            }

            .slide-UzNBD {
                position: relative;
                min-width: 52rem;
                color: #fff;
                opacity: .6;
                transition: opacity .2s ease-out
            }

            .slide-UzNBD.isActive-1Sh-A {
                opacity: 1
            }

            .sliderArrows-1IMps {
                position: absolute;
                top: 0;
                bottom: 0
            }

            [dir=ltr] .sliderArrows-1IMps, [dir=rtl] .sliderArrows-1IMps {
                right: 0;
                left: 0
            }

            .sliderArrowsWrapper-1B-rN {
                position: absolute;
                top: 23rem;
                width: calc(100% + 4rem);
                display: flex;
                justify-content: space-between
            }

            [dir=ltr] .sliderArrowsWrapper-1B-rN {
                left: -2rem
            }

            [dir=rtl] .sliderArrowsWrapper-1B-rN {
                right: -2rem;
                transform: translateY(-1.3rem)
            }

            .sliderArrow-2UEu_ {
                display: flex;
                color: #fff;
                justify-content: center;
                align-items: center;
                transition: opacity .3s ease-out, transform .3s ease-out;
                width: 5rem;
                height: 3.2rem
            }

            [dir=ltr] .sliderArrow-2UEu_.arrowPrev-1Opdd {
                transform: rotate(-180deg)
            }

            [dir=rtl] .sliderArrow-2UEu_.arrowPrev-1Opdd {
                transform: rotate(180deg);
                transform: rotate(0deg)
            }

            [dir] .sliderArrow-2UEu_.arrowNext-1XK_W {
                transform: rotate(0deg)
            }

            [dir=rtl] .sliderArrow-2UEu_.arrowNext-1XK_W {
                transform: rotate(-180deg)
            }

            .sliderArrow-2UEu_.disabled-1fW2u {
                opacity: .5
            }

            [dir=ltr] .sliderArrow-2UEu_.disabled-1fW2u.arrowPrev-1Opdd {
                transform: scale(.8) rotate(-180deg)
            }

            [dir=rtl] .sliderArrow-2UEu_.disabled-1fW2u.arrowPrev-1Opdd {
                transform: scale(.8) rotate(180deg);
                transform: scale(.8) rotate(0deg)
            }

            [dir] .sliderArrow-2UEu_.disabled-1fW2u.arrowNext-1XK_W {
                transform: scale(.8) rotate(0deg)
            }

            [dir=rtl] .sliderArrow-2UEu_.disabled-1fW2u.arrowNext-1XK_W {
                transform: scale(.8) rotate(-180deg)
            }
        }

        @keyframes fadeIn-31EOE {
            0% {
                opacity: 0
            }
            to {
                opacity: 1
            }
        }

        [dir] .gameOverview-8zg-o {
            background: #2858f0;
            padding-bottom: 15rem;
            transform: translateY(-3.5rem)
        }

        [dir=ltr] .gameOverview-8zg-o {
            background: linear-gradient(30deg, #2858f0 20%, #32c8ff 60%)
        }

        [dir=rtl] .gameOverview-8zg-o {
            background: linear-gradient(-30deg, #2858f0 20%, #32c8ff 60%)
        }

        [dir] .gameOverviewContent-VT-gr {
            padding-top: 10rem
        }

        [dir] .titleDetailWrapper-1hatr {
            margin-bottom: 3.25rem
        }

        .volt-aBDFC.bottomVolt-17gMH {
            z-index: 1;
            width: 160%
        }

        [dir] .volt-aBDFC.bottomVolt-17gMH {
            transform: scaleX(1) translateY(-8%)
        }

        [dir=rtl] .volt-aBDFC.bottomVolt-17gMH {
            transform: scaleX(-1) translateY(-8%)
        }

        .mobileSlider-cJWHr [data-element=infinite-slider-arrows] {
            top: 35.6%
        }

        @media (min-width: 480px) {
            .mobileSlider-cJWHr [data-element=infinite-slider-arrows] {
                top: 35%
            }
        }

        @media (min-width: 768px) {
            [dir] .gameOverview-8zg-o {
                padding-bottom: 25rem
            }

            .mobileSlider-cJWHr [data-element=infinite-slider-arrows] {
                top: 34.1%
            }

            [dir] .mobileSlider-cJWHr [data-element=infinite-slider-arrows] {
                padding: 0 1rem
            }

            [dir] .gameOverviewContent-VT-gr {
                padding-top: 15.5rem
            }

            [dir] .titleDetailWrapper-1hatr {
                margin-bottom: 6.5rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .gameOverview-8zg-o {
                padding-bottom: calc(20rem + 10%);
                transform: translateY(-6.5rem)
            }

            .gameOverviewContent-VT-gr {
                position: relative
            }

            [dir] .gameOverviewContent-VT-gr {
                padding-top: 35.5rem
            }

            .container-2Ja6Y {
                position: absolute;
                top: 21rem;
                z-index: 1;
                overflow: visible
            }

            [dir=ltr] .container-2Ja6Y {
                left: 0
            }

            [dir=rtl] .container-2Ja6Y {
                right: 0
            }

            .titleDetailWrapper-1hatr {
                max-width: calc(58.33333% - 1.25rem);
                width: 100%;
                position: relative
            }

            [dir] .copyWrapper-2EMJJ {
                padding: 2rem 6rem
            }

            .textDivider-3YSrl {
                width: .7rem
            }

            [dir] .textDivider-3YSrl {
                margin: 0 3%
            }

            .slideTitle-3T8-M {
                display: flex;
                align-items: center
            }

            [dir] .slideTitle-3T8-M {
                margin-bottom: .5rem
            }

            .loaderWrapper-1Cavk {
                min-height: 76.5rem;
                display: flex;
                justify-content: center
            }

            [dir] .loaderWrapper-1Cavk {
                padding-top: 10rem
            }

            .loaderWrapper-1Cavk .loader-3WO5p {
                height: 25rem;
                width: 25rem
            }

            .volt-aBDFC.bottomVolt-17gMH {
                width: 192rem
            }
        }

        @media (min-width: 1440px) {
            .volt-aBDFC.bottomVolt-17gMH {
                width: 100%
            }
        }

        @keyframes fadeIn-YD-wO {
            0% {
                opacity: 0
            }
            to {
                opacity: 1
            }
        }

        [dir] .sectionsWrapper-2VXSr {
            background: #dc500f
        }

        [dir=ltr] .sectionsWrapper-2VXSr {
            background: linear-gradient(75deg, #dc500f, #ffa523 50%)
        }

        [dir=rtl] .sectionsWrapper-2VXSr {
            background: linear-gradient(-75deg, #dc500f, #ffa523 50%)
        }

        @media (min-width: 1024px) {
            [dir] .sectionsWrapper-2VXSr {
                background: #dc500f
            }

            [dir=ltr] .sectionsWrapper-2VXSr {
                background: linear-gradient(45deg, #dc500f, #ffa523 50%)
            }

            [dir=rtl] .sectionsWrapper-2VXSr {
                background: linear-gradient(-45deg, #dc500f, #ffa523 50%)
            }
        }

        .imageSlider-LOpcF {
            z-index: 2;
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 3.5rem 100%, 0 calc(100% - 3.5rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 3.5rem 100%, 0 calc(100% - 3.5rem))
        }

        [dir] .imageSlider-LOpcF {
            background: #981959;
            padding-bottom: 5.5rem;
            padding-top: calc(7rem + 15vw)
        }

        [dir=ltr] .imageSlider-LOpcF {
            background: linear-gradient(30deg, #981959 20%, #ff5f5f 60%)
        }

        [dir=rtl] .imageSlider-LOpcF {
            background: linear-gradient(-30deg, #981959 20%, #ff5f5f 60%);
            -webkit-clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3.5rem), calc(100% - 3.5rem) 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 100% calc(100% - 3.5rem), calc(100% - 3.5rem) 100%, 0 100%)
        }

        [dir] .title-xGveS {
            margin-bottom: 3.5rem
        }

        .topVolt-126E0 {
            top: -8rem;
            width: 102.4rem
        }

        .slider-3XhNX {
            display: flex
        }

        .sliderCopy-15Ah9 {
            position: relative;
            max-width: 80%;
            font-size: 1.3rem
        }

        [dir] .sliderCopy-15Ah9 {
            margin-top: 6%
        }

        [dir=ltr] .sliderCopy-15Ah9, [dir=rtl] .sliderCopy-15Ah9 {
            margin-right: auto;
            margin-left: auto
        }

        html[dir=ltr][browser="Internet Explorer"] .sliderCopy-15Ah9 {
            margin-left: 3rem
        }

        html[dir=rtl][browser="Internet Explorer"] .sliderCopy-15Ah9 {
            margin-right: 3rem
        }

        @media (min-width: 1280px) {
            .sliderCopy-15Ah9 {
                font-size: inherit
            }

            [dir] .sliderCopy-15Ah9 {
                margin-top: 1.5%
            }
        }

        .copyWrapper-31w81 {
            position: absolute;
            top: 0;
            bottom: 0;
            height: 140%;
            display: flex;
            flex-direction: column
        }

        [dir] .copyWrapper-31w81 {
            padding: 0 2rem
        }

        [dir=ltr] .copyWrapper-31w81, [dir=rtl] .copyWrapper-31w81 {
            left: 0;
            right: 0
        }

        .copyWrapper-31w81:before {
            content: "";
            width: 100%
        }

        [dir] .copyWrapper-31w81:before {
            padding-bottom: 115%
        }

        .sliderElementWrapper-2llA6 {
            width: 33.33%;
            position: relative;
            color: #fff
        }

        .svgFrame-3QEuk {
            position: absolute;
            top: 0;
            width: 100%;
            height: 100%
        }

        [dir] .svgFrame-3QEuk {
            padding: 2rem
        }

        [dir=ltr] .svgFrame-3QEuk {
            left: 0
        }

        [dir=rtl] .svgFrame-3QEuk {
            right: 0
        }

        .mobileContainer-28EvP {
            position: relative
        }

        @media (min-width: 768px) {
            [dir] .imageSlider-LOpcF {
                padding-bottom: 8rem;
                padding-top: calc(5rem + 20vw)
            }

            .topVolt-126E0 {
                top: -4rem
            }
        }

        @media (min-width: 1024px) {
            [dir] .imageSlider-LOpcF {
                padding-top: calc(15rem + 10vw);
                padding-bottom: 24rem
            }

            .topVolt-126E0 {
                width: 101%;
                top: -8rem
            }

            [dir] .title-xGveS {
                margin-bottom: 6rem
            }
        }

        @media (min-width: 1280px) {
            [dir] .imageSlider-LOpcF {
                padding-top: calc(15rem + 10vw);
                padding-bottom: 24rem
            }

            .topVolt-126E0 {
                top: -10rem
            }
        }

        @media (min-width: 1440px) {
            [dir] .imageSlider-LOpcF {
                padding-top: calc(20rem + 10vw);
                padding-bottom: 24rem
            }
        }

        .featuredNews-uhrKB {
            position: relative;
            overflow: hidden
        }

        [dir] .featuredNews-uhrKB {
            background: #2858f0;
            padding-top: 7rem;
            padding-bottom: 7rem;
            margin-bottom: 7rem
        }

        [dir=ltr] .featuredNews-uhrKB {
            background: linear-gradient(30deg, #2858f0 20%, #32c8ff 60%)
        }

        [dir=rtl] .featuredNews-uhrKB {
            background: linear-gradient(-30deg, #2858f0 20%, #32c8ff 60%)
        }

        .titleContainer-1nFNK {
            position: static
        }

        [dir] .titleContainer-1nFNK {
            margin-bottom: 3rem
        }

        .sliderWrapper-3Jg3l {
            position: relative;
            width: 100%;
            z-index: 2
        }

        [dir] .sliderWrapper-3Jg3l {
            margin-top: 0
        }

        [dir=ltr] .sliderWrapper-3Jg3l {
            margin-left: 0
        }

        [dir=rtl] .sliderWrapper-3Jg3l {
            margin-right: 0
        }

        .bottomVolt-3MX88 {
            z-index: 1;
            width: 200%
        }

        [dir=ltr] .bottomVolt-3MX88 img {
            transform: translate(-50%, 48%) rotate(2deg) !important
        }

        [dir=rtl] .bottomVolt-3MX88 img {
            transform: translate(50%, 48%) rotate(-2deg) !important
        }

        @media (min-width: 1024px) {
            [dir] .featuredNews-uhrKB {
                padding-bottom: 17rem
            }

            .titleContainer-1nFNK {
                position: absolute;
                top: 0
            }

            [dir] .titleContainer-1nFNK {
                margin-top: 7rem
            }

            [dir=ltr] .titleContainer-1nFNK {
                left: 0
            }

            [dir=rtl] .titleContainer-1nFNK {
                right: 0
            }

            .sliderWrapper-3Jg3l {
                position: relative;
                width: 100%;
                z-index: 2
            }

            [dir] .sliderWrapper-3Jg3l {
                margin-top: 8rem
            }

            [dir=ltr] .sliderWrapper-3Jg3l {
                margin-left: 36%
            }

            [dir=rtl] .sliderWrapper-3Jg3l {
                margin-right: 36%
            }

            .bottomVolt-3MX88 {
                width: 100%
            }
        }

        @media (min-width: 1280px) {
            [dir] .featuredNews-uhrKB {
                padding-bottom: 20rem
            }
        }

        @media (min-width: 1440px) {
            [dir] .featuredNews-uhrKB {
                padding-bottom: 30rem
            }
        }

        .articleList-2L0RB {
            overflow: initial;
            z-index: 4
        }

        [dir] .articleList-2L0RB {
            margin-bottom: 4.4rem;
            background-color: #fff
        }

        .articleList-2L0RB.lastSection-X_3Mi {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 3.5rem 100%, 0 calc(100% - 3.5rem));
            clip-path: polygon(0 0, 100% 0, 100% 100%, 3.5rem 100%, 0 calc(100% - 3.5rem))
        }

        [dir] .articleList-2L0RB.lastSection-X_3Mi {
            margin-bottom: 0;
            padding-bottom: 7rem
        }

        .contentWrapper-1yz8H {
            color: #111;
            display: flex
        }

        .dropdownWrapper-VRFEl {
            width: 100%
        }

        [dir] .dropdownWrapper-VRFEl {
            margin-bottom: 4.4rem
        }

        [dir=ltr] .dropdownWrapper-VRFEl {
            margin-left: auto;
            margin-right: 0
        }

        [dir=rtl] .dropdownWrapper-VRFEl {
            margin-right: auto;
            margin-left: 0
        }

        [dir=ltr] .newsDropdown-gLmou, [dir=rtl] .newsDropdown-gLmou {
            padding-left: 1.5rem;
            padding-right: 1.5rem
        }

        .cardsWrapper-16UN7 {
            flex-wrap: wrap
        }

        .dashDetail-3GYux {
            position: relative;
            width: 92%;
            height: .2rem;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden
        }

        [dir] .dashDetail-3GYux {
            margin-top: 2.1rem;
            margin-bottom: 4rem;
            background: #e1e1e1
        }

        [dir=ltr] .dashDetail-3GYux {
            transform-origin: left
        }

        [dir=rtl] .dashDetail-3GYux {
            transform-origin: right
        }

        .dashDetail-3GYux:before {
            content: "";
            position: absolute;
            bottom: -.3rem;
            height: .4rem;
            width: 21%;
            -webkit-clip-path: polygon(0 0, 0 100%, 90% 100%, 100% 0);
            clip-path: polygon(0 0, 0 100%, 90% 100%, 100% 0)
        }

        [dir] .dashDetail-3GYux:before {
            background-color: #e1e1e1
        }

        [dir=ltr] .dashDetail-3GYux:before {
            left: 0
        }

        [dir=rtl] .dashDetail-3GYux:before {
            right: 0
        }

        .footerCta-1qmQK {
            justify-content: center
        }

        [dir] .footerCta-1qmQK {
            padding-top: 0
        }

        @media (min-width: 768px) {
            [dir=ltr] .newsDropdown-gLmou, [dir=rtl] .newsDropdown-gLmou {
                padding-left: 3rem;
                padding-right: 3rem
            }
        }

        @media (min-width: 1024px) {
            .dropdownWrapper-VRFEl {
                max-width: calc(33.33333% - 2rem);
                width: 100%
            }

            .dashDetail-3GYux {
                display: none
            }

            [dir] .footerCTA-3JzfM {
                padding-top: 5rem
            }

            .footerCTA-3JzfM:before {
                content: "";
                position: absolute;
                top: 0;
                height: .2rem
            }

            [dir] .footerCTA-3JzfM:before {
                background: rgba(0, 0, 0, .2)
            }

            [dir=ltr] .footerCTA-3JzfM:before, [dir=rtl] .footerCTA-3JzfM:before {
                right: 0;
                left: 0
            }

            .footerCTA-3JzfM:after {
                content: "";
                position: absolute;
                top: .2rem;
                height: .2rem;
                -webkit-clip-path: polygon(0 0, 100% 0, 90% 100%, 0 100%);
                clip-path: polygon(0 0, 100% 0, 90% 100%, 0 100%)
            }

            [dir] .footerCTA-3JzfM:after {
                background: rgba(0, 0, 0, .2)
            }

            [dir=ltr] .footerCTA-3JzfM:after {
                right: 92.71%;
                left: 0
            }

            [dir=rtl] .footerCTA-3JzfM:after {
                left: 92.71%;
                right: 0
            }
        }

        .underlineSlider-3ASyD {
            display: flex;
            flex-direction: column
        }

        [dir=ltr] .underlineSlider-3ASyD {
            padding-left: 0
        }

        [dir=rtl] .underlineSlider-3ASyD {
            padding-right: 0
        }

        .sliderWrapper-2tdTS {
            position: relative;
            width: 100%;
            z-index: 2
        }

        [dir] .sliderWrapper-2tdTS {
            margin-top: 0
        }

        [dir=ltr] .sliderWrapper-2tdTS {
            margin-left: 0
        }

        [dir=rtl] .sliderWrapper-2tdTS {
            margin-right: 0
        }

        .sliderWrapper-2tdTS:before {
            content: "";
            position: absolute;
            bottom: 0;
            height: .2rem
        }

        [dir] .sliderWrapper-2tdTS:before {
            background: #fff
        }

        [dir=ltr] .sliderWrapper-2tdTS:before {
            right: 0;
            left: 7.5%
        }

        [dir=rtl] .sliderWrapper-2tdTS:before {
            left: 0;
            right: 7.5%
        }

        .sliderWrapper-2tdTS:after {
            content: "";
            position: absolute;
            bottom: -.3rem;
            height: .4rem;
            -webkit-clip-path: polygon(0 0, 100% 0, 90% 100%, 0 100%);
            clip-path: polygon(0 0, 100% 0, 90% 100%, 0 100%)
        }

        [dir] .sliderWrapper-2tdTS:after {
            background: #fff
        }

        [dir=ltr] .sliderWrapper-2tdTS:after {
            right: 80%;
            left: 7.5%
        }

        [dir=rtl] .sliderWrapper-2tdTS:after {
            left: 80%;
            right: 7.5%
        }

        .slider-2XhQ8 {
            overflow: hidden;
            width: 100%
        }

        [dir] .slider-2XhQ8 {
            margin-bottom: 2.2rem
        }

        .sliderContent-1Rqhm {
            display: flex
        }

        html[dir][browser="Internet Explorer"] .sliderContent-1Rqhm {
            cursor: move
        }

        .slide-23mA0 {
            position: relative;
            min-width: 90%
        }

        [dir=ltr] .slide-23mA0 {
            padding-right: 3rem;
            padding-left: 1rem
        }

        [dir=rtl] .slide-23mA0 {
            padding-left: 3rem;
            padding-right: 1rem
        }

        .sliderCard-grpNw {
            display: block;
            width: 100%
        }

        .bulletWrapper-1dKEV {
            height: 1.1rem;
            width: 1.1rem;
            opacity: .4;
            transition: opacity .2s ease-out
        }

        [dir=ltr] .bulletWrapper-1dKEV:not(:last-child) {
            margin-right: 1.1rem
        }

        [dir=rtl] .bulletWrapper-1dKEV:not(:last-child) {
            margin-left: 1.1rem
        }

        .bullets-i0o6X {
            display: flex;
            order: 1
        }

        [dir] .bullets-i0o6X {
            margin-top: 4rem
        }

        [dir=ltr] .bullets-i0o6X {
            margin-left: 7.5%
        }

        [dir=rtl] .bullets-i0o6X {
            margin-right: 7.5%
        }

        .bullets-i0o6X .active-WBqMi {
            opacity: 1
        }

        @media (min-width: 1024px) {
            .underlineSlider-3ASyD {
                flex-direction: row
            }

            [dir] .underlineSlider-3ASyD {
                margin-bottom: 0
            }

            [dir=ltr] .underlineSlider-3ASyD {
                transform: translateX(7.3%);
                padding-right: 0
            }

            [dir=rtl] .underlineSlider-3ASyD {
                transform: translateX(-7.3%);
                padding-left: 0
            }

            [dir] .sliderWrapper-2tdTS {
                margin-bottom: 5rem
            }

            [dir=ltr] .sliderWrapper-2tdTS {
                margin-left: 13rem
            }

            [dir=rtl] .sliderWrapper-2tdTS {
                margin-right: 13rem
            }

            [dir=ltr] .sliderWrapper-2tdTS:before {
                left: 0
            }

            [dir=rtl] .sliderWrapper-2tdTS:before {
                right: 0
            }

            [dir=ltr] .sliderWrapper-2tdTS:after {
                left: 0;
                right: 92.71%
            }

            [dir=rtl] .sliderWrapper-2tdTS:after {
                right: 0;
                left: 92.71%
            }

            .sliderContent-1Rqhm {
                width: 70%
            }

            .bullets-i0o6X {
                order: 0
            }

            [dir] .bullets-i0o6X {
                margin-top: 0
            }

            [dir=ltr] .bullets-i0o6X {
                margin-left: 0
            }

            [dir=rtl] .bullets-i0o6X {
                margin-right: 0
            }

            [dir] .slider-2XhQ8 {
                margin-bottom: 0
            }

            .slide-23mA0 {
                max-width: auto
            }

            [dir=ltr] .slide-23mA0 {
                padding-right: 6rem;
                padding-left: 1rem
            }

            [dir=rtl] .slide-23mA0 {
                padding-left: 6rem;
                padding-right: 1rem
            }
        }

        @media (min-width: 1280px) {
            .sliderContent-1Rqhm {
                width: 55%
            }
        }

        @media (min-width: 1440px) {
            [dir=ltr] .underlineSlider-3ASyD {
                transform: translateX(calc(50% - 61.5rem))
            }

            [dir=rtl] .underlineSlider-3ASyD {
                transform: translateX(calc(-50% - -61.5rem))
            }

            html[dir=ltr][browser="Internet Explorer"] .underlineSlider-3ASyD {
                transform: translateX(50%) translateX(-61.5rem)
            }

            html[dir=rtl][browser="Internet Explorer"] .underlineSlider-3ASyD {
                transform: translateX(-50%) translateX(61.5rem)
            }

            .sliderContent-1Rqhm {
                width: 40%
            }
        }

        .relatedContent-2kALi {
            position: relative;
            z-index: 3;
            min-height: auto;
            -webkit-clip-path: polygon(100% 0, 3rem 0, 0 -3rem, 0 calc(100% - 2.5rem), 2.5rem 100%, 100% 100%);
            clip-path: polygon(100% 0, 3rem 0, 0 -3rem, 0 calc(100% - 2.5rem), 2.5rem 100%, 100% 100%)
        }

        [dir] .relatedContent-2kALi {
            padding-top: 7rem;
            padding-bottom: 10rem
        }

        .relatedContent-2kALi:before {
            position: absolute;
            top: 0;
            bottom: 0;
            content: ""
        }

        [dir] .relatedContent-2kALi:before {
            background: #2858f0;
            transform: translateY(-6rem) scale(1.3)
        }

        [dir=ltr] .relatedContent-2kALi:before {
            left: 0;
            right: 0;
            background: linear-gradient(30deg, #2858f0 20%, #32c8ff 60%)
        }

        [dir=rtl] .relatedContent-2kALi:before {
            right: 0;
            left: 0;
            background: linear-gradient(-30deg, #2858f0 20%, #32c8ff 60%)
        }

        .relatedContent-2kALi.onlyAd-FjaBu {
            -webkit-clip-path: initial;
            clip-path: none
        }

        [dir] .relatedContent-2kALi.onlyAd-FjaBu {
            padding-top: 0;
            padding-bottom: 6rem
        }

        [dir] .relatedContent-2kALi.onlyAd-FjaBu:before {
            background: transparent;
            transform: translateY(0) scale(0)
        }

        [dir] .header-2NQ9D {
            margin-bottom: 4rem
        }

        [dir] .copyWrapper-3jH7H {
            margin-top: 0
        }

        @media (min-width: 1024px) {
            .relatedContent-2kALi {
                -webkit-clip-path: polygon(100% 0, 6rem 0, 0 -6rem, 0 calc(100% - 5rem), 5rem 100%, 100% 100%);
                clip-path: polygon(100% 0, 6rem 0, 0 -6rem, 0 calc(100% - 5rem), 5rem 100%, 100% 100%)
            }

            [dir] .relatedContent-2kALi {
                padding-top: 8rem
            }

            [dir] .copyWrapper-3jH7H {
                margin-top: 2rem
            }

            [dir] .header-2NQ9D {
                margin-bottom: 8rem
            }
        }

        .sectionsWrapper-xHqs- {
            position: relative;
            z-index: 4;
            -webkit-clip-path: polygon(100% 0, calc(100% - 3rem) 3rem, 0 3rem, 0 calc(100% - 2.5rem), 2.5rem 100%, 100% 100%);
            clip-path: polygon(100% 0, calc(100% - 3rem) 3rem, 0 3rem, 0 calc(100% - 2.5rem), 2.5rem 100%, 100% 100%)
        }

        [dir] .sectionsWrapper-xHqs- {
            background: #981959
        }

        [dir=ltr] .sectionsWrapper-xHqs- {
            background: linear-gradient(30deg, #981959 30%, #ff5f5f 90%)
        }

        [dir=rtl] .sectionsWrapper-xHqs- {
            background: linear-gradient(-30deg, #981959 30%, #ff5f5f 90%)
        }

        @media (min-width: 1024px) {
            .sectionsWrapper-xHqs- {
                -webkit-clip-path: polygon(100% 0, calc(100% - 6rem) 6rem, 0 6rem, 0 calc(100% - 5rem), 5rem 100%, 100% 100%);
                clip-path: polygon(100% 0, calc(100% - 6rem) 6rem, 0 6rem, 0 calc(100% - 5rem), 5rem 100%, 100% 100%)
            }

            [dir] .sectionsWrapper-xHqs- {
                background: #981959
            }

            [dir=ltr] .sectionsWrapper-xHqs- {
                background: linear-gradient(30deg, #981959, #ff5f5f 90%)
            }

            [dir=rtl] .sectionsWrapper-xHqs- {
                background: linear-gradient(-30deg, #981959, #ff5f5f 90%)
            }
        }

        .relatedArticles-FILhJ {
            position: relative;
            z-index: 4;
            min-height: auto;
            -webkit-clip-path: polygon(100% 0, 0 0, 0 calc(100% - 5rem), 5rem 100%, 100% 100%);
            clip-path: polygon(100% 0, 0 0, 0 calc(100% - 5rem), 5rem 100%, 100% 100%)
        }

        [dir] .relatedArticles-FILhJ {
            background: #2858f0;
            padding-top: 20rem;
            padding-bottom: 10rem
        }

        [dir=ltr] .relatedArticles-FILhJ {
            background: linear-gradient(30deg, #2858f0 20%, #32c8ff 60%)
        }

        [dir=rtl] .relatedArticles-FILhJ {
            background: linear-gradient(-30deg, #2858f0 20%, #32c8ff 60%)
        }

        [dir] .relatedArticles-FILhJ .header-1amIj {
            margin-bottom: 4.4rem
        }

        .relatedArticles-FILhJ .topVolt-iQaNl {
            width: 300%
        }

        [dir] .relatedArticles-FILhJ .copyWrapper-KUSzh {
            margin-top: 2rem
        }

        @media (min-width: 480px) {
            .relatedArticles-FILhJ .topVolt-iQaNl {
                top: -12%
            }
        }

        @media (min-width: 768px) {
            .relatedArticles-FILhJ .topVolt-iQaNl {
                width: 200%
            }
        }

        @media (min-width: 1024px) {
            [dir] .relatedArticles-FILhJ {
                padding-top: 36rem
            }

            [dir] .relatedArticles-FILhJ .header-1amIj {
                margin-bottom: 8rem
            }

            .relatedArticles-FILhJ .topVolt-iQaNl {
                width: 100%;
                top: 0
            }
        }

        @media (min-width: 1440px) {
            [dir] .relatedArticles-FILhJ {
                padding-top: 50rem
            }
        }

        .youtubeSection-3R4Ra {
            position: relative;
            z-index: 4;
            min-height: auto;
            -webkit-clip-path: polygon(100% 0, calc(100vw - 5rem) 5rem, 0 5rem, 0 calc(100% - 5rem), 5rem 100%, 100% 100%);
            clip-path: polygon(100% 0, calc(100vw - 5rem) 5rem, 0 5rem, 0 calc(100% - 5rem), 5rem 100%, 100% 100%)
        }

        [dir] .youtubeSection-3R4Ra {
            background: #981959;
            padding-top: 5rem;
            padding-bottom: 7rem
        }

        [dir=ltr] .youtubeSection-3R4Ra {
            background: linear-gradient(30deg, #981959 20%, #ff5f5f 60%)
        }

        [dir=rtl] .youtubeSection-3R4Ra {
            background: linear-gradient(-30deg, #981959 20%, #ff5f5f 60%)
        }

        .header-3ZNn5 {
            display: flex;
            justify-content: space-between;
            align-items: center
        }

        [dir] .header-3ZNn5 {
            margin-top: 6.25rem;
            margin-bottom: 5.35rem
        }

        .youtubeLogo-11Ote {
            width: 14rem
        }

        .desktopCTA-28ZrF {
            width: 22%;
            min-width: 31.6rem
        }

        [dir=ltr] .desktopCTA-28ZrF {
            margin-left: 0;
            margin-right: 10rem
        }

        [dir=rtl] .desktopCTA-28ZrF {
            margin-right: 0;
            margin-left: 10rem
        }

        .mobileCta-2GyRr {
            max-width: 75%
        }

        [dir] .mobileCta-2GyRr {
            margin-top: 5.8rem
        }

        [dir=ltr] .mobileCta-2GyRr, [dir=rtl] .mobileCta-2GyRr {
            margin-left: auto;
            margin-right: auto
        }

        @media (min-width: 768px) {
            [dir] .youtubeSection-3R4Ra {
                padding-bottom: 5rem
            }
        }

        @media (min-width: 1024px) {
            .youtubeSection-3R4Ra {
                -webkit-clip-path: polygon(100% 0, 96.52% 6.25%, 0 6.25%, 0 93.75%, 3.48% 100%, 100% 100%);
                clip-path: polygon(100% 0, 96.52% 6.25%, 0 6.25%, 0 93.75%, 3.48% 100%, 100% 100%)
            }

            [dir] .youtubeSection-3R4Ra {
                padding-bottom: 0;
                padding-top: 15.5rem
            }

            [dir] .header-3ZNn5 {
                margin-bottom: 7.5rem
            }

            .youtubeLogo-11Ote {
                width: 21rem
            }
        }

        @media (min-width: 1280px) {
            [dir] .youtubeSection-3R4Ra {
                padding-bottom: 7rem
            }
        }

        @font-face {
            font-family: "BeaufortforLOL";
            src: url("//db.onlinewebfonts.com/t/12420e8c141ca7c3dff41de2d59df13e.eot");
            src: url("//db.onlinewebfonts.com/t/12420e8c141ca7c3dff41de2d59df13e.eot?#iefix") format("embedded-opentype"), url("//db.onlinewebfonts.com/t/12420e8c141ca7c3dff41de2d59df13e.woff2") format("woff2"), url("//db.onlinewebfonts.com/t/12420e8c141ca7c3dff41de2d59df13e.woff") format("woff"), url("//db.onlinewebfonts.com/t/12420e8c141ca7c3dff41de2d59df13e.ttf") format("truetype"), url("//db.onlinewebfonts.com/t/12420e8c141ca7c3dff41de2d59df13e.svg#BeaufortforLOL-Bold") format("svg");
        }

        .appButton {
            display: flex;
            align-items: center;
            justify-content: center;
            padding: 10px 10px;
            min-width: 170px;
            /*min-height: 45px;*/
            background-color: #d96f1d;
            border-radius: 3px;
        }

        .bg-text .appButton {
            margin: 5px;
        }

        .bg-text * {
            font-family: BeaufortforLOL !important;
        }

        .bg-text h1 {
            font-size: 150px !important;
            font-weight: 900 !important;
            font-style: italic !important;
            color: #ffffff !important;
        }

        .bg-text h3 {
            max-width: 80% !important;
            font-size: 15px !important;
            line-height: 20px !important;
            font-weight: 900 !important;
            font-style: italic !important;
            color: #ffffff !important;
        }

        .bg-text > div {
            display: flex;
            flex-direction: column;
            align-items: center;
        }

    </style>
</head>
<body>

<div id="___gatsby">
    <div style="outline:none" tabindex="-1" id="gatsby-focus-wrapper">
        <div class="layout-3yaYV">
            <header class="header-1mER8">

            </header>
            <main>
                <section class="registrationHero-2sgwh"><span class="croppedCorner-Zrd2g"></span><span
                        class="dashDetail-2uqQY"></span>
                    <div class="heroVideo-3UE9M video-CB3lJ">

                        <div class="poster-3GL6m"
                             style="pointer-events:none;background-image:url({{asset('img/bg/header-image.jpg')}})"></div>
                    </div>

                    <div class="container">
                        <div class="content-wrapper contentWrapper-3LPGz bg-text">
                            <div>
                                <h1>KINDRED</h1>
                                <h3 class="heading-03 tagline-2Rr0N noCTA-FVACJ">
                                    TRUSTED SOURCE FOR BUYING<br>
                                    LEAGUE OF LEGENDS & VALORANT ACCOUNTS
                                </h3>
                                <div class="appStoreWrapper-4htWz"><h4 class="heading-03 availableText-3om5c"></h4>
                                    <div class="storesWrapper-3IcOf">
                                        @foreach($games as $game)
                                            <div class="appButton">
                                                <a href="{{route('payment.index',[$game->slug])}}">{{$game->shortName()}}</a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="news-paedR addPaddingTop-2B-q5">
                    <div class="container">
                        <div class="content-wrapper light"><h3 class="heading-02 headline-2QSde">LATEST NEWS</h3><a
                                class="button  goToNewsPageButton-CRHHX linkButton-1Ll1h default-2JTfs "
                                href="/">
                                <div class="label-01 undefined"><span>GO TO NEWS PAGE</span></div>
                                <div class="icon icon-1djbk icon-2tHD8 currentColor-LyOgN">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.72 42.18">
                                        <path data-name="2" d="M3.63 1.49l19.6 19.6-19.6 19.6" fill="none" stroke="#fff"
                                              stroke-width="5"></path>
                                    </svg>
                                </div>
                            </a></div>
                    </div>
                    <div class="container sliderWrapper-1m1BR">
                        <ul class="content-wrapper light contentWrapper-3YaON">
                            <li class="contentItem-2Acwg">
                                <div class="cardWrapper-2YpWt"><a class="undefined "
                                                                  href="/"
                                                                  target="_blank" rel="noopener noreferrer"><span
                                            class="dashDetail-2fLJd"></span>
                                        <div class="newsCard-2JC_Q"
                                             style="
                                             /*background-image:url(./&quot%3Bhttps:/images.contentstack.io/v3/assets/blt370612131b6e0756/blt55785e20d66474d5/6074ae991898af75a401b70b/WR-Stargazer-News-Website_Article_BannerV2.jpg&quot%3B.html)*/
">
                                            <div class="cardContentWrapper-3L0CZ">
                                                <div class="cardHeading-3FUHN"><span
                                                        class="heading-03 font-normal date-NDmi_">04-13-2021</span>
                                                    <div class="icon ellipseIcon-KhzGf icon-2tHD8 currentColor-LyOgN">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3.38 3.38">
                                                            <circle cx="1.69" cy="1.69" r="1.69" fill="#fff"></circle>
                                                        </svg>
                                                    </div>
                                                    <h5 class="heading-03 font-normal category-1nX3-">Announcements</h5>
                                                </div>
                                                <div class="cardTitle-3NISe">
                                                    <div class="icon titleIcon-WcO0c icon-2tHD8 currentColor-LyOgN">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                                            <circle cx="19.8" cy="20" r="15.8" fill="none" stroke="#fff"
                                                                    stroke-width="2" stroke-miterlimit="10"></circle>
                                                            <path fill="#fff"
                                                                  d="M25.7 13.6h-9.5v2.1H22l-8.8 8.8-.1.1v1.9H15l.1-.1 8.8-8.8v5.8H26v-9.8z"></path>
                                                        </svg>
                                                    </div>
                                                    <h4 class="heading-05 font-normal title-Pl837">Stargazer</h4></div>
                                            </div>
                                        </div>
                                    </a></div>
                            </li>
                            <li class="contentItem-2Acwg">
                                <div class="cardWrapper-2YpWt"><a class="undefined "
                                                                  href="/"><span
                                            class="dashDetail-2fLJd"></span>
                                        <div class="newsCard-2JC_Q"
                                             style="background-image:url(./&quot%3Bhttps:/images.contentstack.io/v3/assets/blt370612131b6e0756/blta3e8e071dad3ce53/606f8e00a9cf2022b935736a/4_12_21_Patch22article_Banner.jpg&quot%3B.html)">
                                            <div class="cardContentWrapper-3L0CZ">
                                                <div class="cardHeading-3FUHN"><span
                                                        class="heading-03 font-normal date-NDmi_">04-12-2021</span>
                                                    <div class="icon ellipseIcon-KhzGf icon-2tHD8 currentColor-LyOgN">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3.38 3.38">
                                                            <circle cx="1.69" cy="1.69" r="1.69" fill="#fff"></circle>
                                                        </svg>
                                                    </div>
                                                    <h5 class="heading-03 font-normal category-1nX3-">Game Updates</h5>
                                                </div>
                                                <div class="cardTitle-3NISe">
                                                    <div class="icon titleIcon-WcO0c icon-2tHD8 currentColor-LyOgN">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                                            <circle cx="20.1" cy="20" r="15.8" fill="none" stroke="#fff"
                                                                    stroke-width="2" stroke-miterlimit="10"></circle>
                                                            <path
                                                                d="M21.5 13.9v2.5H29v-2.5h-7.5zm0 7.4H29v-2.5h-7.5v2.5zm-10 5H29v-2.5H11.5v2.5z"
                                                                fill-rule="evenodd" clip-rule="evenodd"
                                                                fill="#fff"></path>
                                                            <path
                                                                d="M16.8 16.1v3.1h-3.1v-3.1h3.1m2.2-2.2h-7.5v7.5H19v-7.5z"
                                                                fill="#fff"></path>
                                                        </svg>
                                                    </div>
                                                    <h4 class="heading-05 font-normal title-Pl837">Wild Rift Patch Notes
                                                        2.2a</h4></div>
                                            </div>
                                        </div>
                                    </a></div>
                            </li>
                            <li class="contentItem-2Acwg">
                                <div class="cardWrapper-2YpWt"><a class="undefined "
                                                                  href="https://support-wildrift.riotgames.com/hc/articles/1500004482902"
                                                                  target="_blank" rel="noopener noreferrer"><span
                                            class="dashDetail-2fLJd"></span>
                                        <div class="newsCard-2JC_Q"
                                             style="background-image:url(./&quot%3Bhttps:/images.contentstack.io/v3/assets/blt370612131b6e0756/bltf271dc4c62f2008b/6063e2b1560dc1104de879b7/WR_WildPass_Article_Banner.jpg&quot%3B.html)">
                                            <div class="cardContentWrapper-3L0CZ">
                                                <div class="cardHeading-3FUHN"><span
                                                        class="heading-03 font-normal date-NDmi_">04-02-2021</span>
                                                    <div class="icon ellipseIcon-KhzGf icon-2tHD8 currentColor-LyOgN">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 3.38 3.38">
                                                            <circle cx="1.69" cy="1.69" r="1.69" fill="#fff"></circle>
                                                        </svg>
                                                    </div>
                                                    <h5 class="heading-03 font-normal category-1nX3-">Announcements</h5>
                                                </div>
                                                <div class="cardTitle-3NISe">
                                                    <div class="icon titleIcon-WcO0c icon-2tHD8 currentColor-LyOgN">
                                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40">
                                                            <circle cx="19.8" cy="20" r="15.8" fill="none" stroke="#fff"
                                                                    stroke-width="2" stroke-miterlimit="10"></circle>
                                                            <path fill="#fff"
                                                                  d="M25.7 13.6h-9.5v2.1H22l-8.8 8.8-.1.1v1.9H15l.1-.1 8.8-8.8v5.8H26v-9.8z"></path>
                                                        </svg>
                                                    </div>
                                                    <h4 class="heading-05 font-normal title-Pl837">Wild Pass</h4></div>
                                            </div>
                                        </div>
                                    </a></div>
                            </li>
                        </ul>
                    </div>
                    <div class="container">
                        <div class="content-wrapper light"></div>
                    </div>
                </section>
                <section class="container gameOverview-XPpwm">
                    <div class="volt-v0O2t topVolt-qsFB- top-1myq9"><img class="voltImage-UYrS0"
                                                                         src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAACgEAAAKLCAMAAADi0EqoAAAAM1BMVEUAAAD///////////////////////////////////////////////////////////////+3leKCAAAAEHRSTlMAQIDAEPBwkGCgUDCwIODQNDy4KAAAJJxJREFUeNrs1jENwDAABLFv90jhjzZAzkbhnQ8AgJBt/wUAIMQAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAeOzWgQAAAACAIH/rQS6KuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQgdutAAAAAAECQv/UgF0XAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIBC7dSAAAAAAIMjfepCLIgBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAid06EAAAAAAQ5G89yEURAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAAN7FbBwIAAAAAgvytB7koMkAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBABitw4EAAAAAAT5Ww9yUcSNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggALFbBwIAAAAAgvytB7koAm4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgCB2K0DAQAAAABB/taDXBQBcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3Bkjs1oEAAAAAgCB/60EuigCAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALiJ3ToQAAAAABDkbz3IRZEBAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAEHv3kqIwFEVR9JjUz9Jo5j9a4SnYsGGECL7ctUaxe5tqFCAAQDUKEACgGgUIAFBNkmnYj98zAABF5OZr9/OrAwEAKkijAwEA6kijAwEA6sjdQwf+DeNpBgBgY/KMDgQA2Jg0OhAAoI5cLe7A43kGAKBred3hXwcCAHQsjQ4EAKgjjQ4EAKgja5h2w36cAQDoQhodCABQRxodCABQR95jMpYDAPhUWYvBMABAJ/JABwIAbFve796BxnIAAB8gazEYBgDoRJbQgQAAG5LldCAAF/buJbdBGAACqD+QkIAb7n/aNqISkVDooqAg/N4F2I7w2AOcQvg4w3IAAKtOmADlQACAVadNgHIgAMAbJ0+Ak96wHADArIoEaFgOAOBFRQlw0rgwDABQWQJUEAQAqDQBToqCIABAZQlw0igIAgBUlgBfFkUcDAMA1JQA54Kgg2EAgJoS4FwQdDAMAFBTApwLgl6OAQCoKQH+ip2XYwAAjpUA+/QjxtiHPfV3L8cAABwlATaXoR0nbc5DSl2MTdhHicnLMQAAH0+AT+WeXoPZFAZvKcVYwvaaeDEtBwBwhB5gE9PtMS49s+Blh/+C0bQcAMAhboKsTP62OafUbVwY7E3LAQCVCwcx5cC3Hlv/Fiym5QCAaoVD+Xvh4ysPKd1j2eoculMQBACqEw5osfCx9wlxryAIANQkbKwMi1y298JHztdNPlkUBAGAOoTN9d01j2Obh9TFsv/Cx7IsGP6lURCEb/buKLdtGAgCKJdUFEeK3PvftkhdlIATGAVMWxT93gH0PVjtcgAYXaqax8BLKstzbK0bPh4fBeOUix/DAMCY0gNty78zi3N5z7E+s+Gj3o1ETBYEAQCq9GhTzHV6d64TwWc2fNQoaEEQAODxCbAO0y6hrf4a3qXh41xKzktsKoYBgNeVnunqyOLPsci0U8PH2yUKrg5FAICXk57h9j/V8pGX2Knho74mE3IgAPAq0i6ubyzqrcjjGz5aVM/VHGg/EAA4nrSrb5HtsiI47drwUavn3IkAAENK+/vpl2r5zEt00PBRynueI/4vdioYBgCOIfVi+p7YLn+Gty4aPv6OBdd0k9/CAMARpL5s8cMf3F8ln2LqpeHja1swR2y3HsJ2JQIA9Cz16MdNvrevm+H13s+2HNGVG3PBKcRAAKBXqV/r9ZlIrRWZ7lwQbB3OzqW85xzXYXA7fdgNBAD6k3oXNQc2ekSwLgg+Ip69lVJyzkvEJadus1EgANCZdAz1oOP6VGSOrZMFQQCAg0gHMtUc2PbtmHXxpB8A8ELS4dQmjsYDwS2yqjcA4BWkg6oPPjceCE4x5+J+AwAYWTq0mgNbDwTjlD8tCAIAY0oDqDmw9UBw1fkLAAwoDaM+I916ILjFbEEQABhIGkzNga0HglOcLAgCAENIQ1qXGtYal4rEYkEQADi4NLCbQ7v7WoY3C4K/2buD3AZhIAqghhCVhtL0/qet6GaiSnEolhNT3jsA6xH/zxgA2K/073VjpsT3eerH7T8EBwVBAGCP0jHkZ7Xz6b0v2BRREAQA9iUdyZDPbq8f/dgV7aAIhgGAPUjHs3T4Mrsc5zgds/HjgmEAoG3pqOJszP3TMVNJ6CwYBgBalY5tigpf5nRMSUHQ5RgAoDmJWBeusykyXVyOAQCakrhNbr8WVTZF5q53OQYAaENi/anneGR42D5muhwDAPyRCbBE+ZpIWILhaXvs7Gk5AGA1E+CTxJpIvWB48rQcALCGCfCpYk2kXjA8e1oOAMgzAT5Z9Pfid12NYHjwtBwAcI8J8JXmLup7dYLh7qIgCAD8ZgJ8vWVNJP7W1TglPSsIAgA3TICt6MY+2ntVTkkPCoIAwA8TYFNur0jXemNYQRAAMAG2ZwltV5T3rqd+7EqiZ8EwAGACbEvUA7POp7eygqBgGAAwAbYl6oF5pQVBwTAAYAJsStQDH7iWFgRdjgEATIAtiXrg48sxYzdsDp9djgEATIBtiXrgmoLgvH3adDkGADABfrN3B7kJw1AQQEPSQJpQuP9tq4rFZ0GF408kQ967AGI3iseepkQ9cNtpuaOXYwAACbAlUQ8sKgge6uOmaTkAQAJsSVk9MD8tt5iWAwAkwKZEPbCkINjXFwRNywEAEmBToh5YVBCcqwuCpuUAAAmwJVEPLCsITvUFwVlBEACQANsR9cACl9S0nIIgAOxdR0uiHlhgTE3LKQgCwH51tCfqgSWGzLScgiAA7FFHo1Z+pLv+TctlPj06GAaA/eho2srS3pgtCDoYBoA96HgDy8pLvNmCoINhAPhsHW9jWdnZu2YLgl6OAYAP1fFmoh5YPC031U/LzVu/HHMZhkEFEQCekAC5HdVGMistCCam5fqNXo453yXbw6HvT9IgADwiAVL1inQUBJf61Pn6l2P6r0d/a+pPjqABIEiAZK6JREHwkCgIxs/ljf3P/5FzlgQB4I8ESPqaSBQEj/UFwRdOy53nZ586J88VAoAESHpN5L4g2CcKgq+ZlhtPS8mfm71XCAASIOk1kfuC4FxfEJzyBcHLLQQKggDwgATIFteFoyA41RcEo4+4bQiMIOhoGAAkQDKrcuGSnparPw7+nlfmXC9XA4AESPLZmDCmpuUSBcHz9NXQi4UA8JE6dmOpOaYdMtNytQXB63GpPIh2aRiAX/buNsdNGIoCqD8ICZiE2f9qq6pR6UiZCWCiMuGcBUTh35V93zMSIBvlwDFXFATLmlx2O/dhd0+YAMC7CBzSqhx4qi4InhbeB19DCDtZXQ0A7yRwYFMOXKK2ILjkYng8x/DHDlZXA8DbCBzeuhw41hYEx9nzwZdmg9U4LoYBQAKkpq03udUVBOeez42phErTZAoAEKBmnd9UELy+dnDj1PVN2EQxMQzA4QWon6adCoJl/T3t8/O53F43/EaPywFwXAFqQtnjgmCsGuBdtCWmviDoYhiA4wnwvbi+INi8ZID3NFyasC1vywFwMAFmHwiu2SCYKgqC0wDvjPvgeqW3OQaAgwiwLJWlnFcUBPvVBcGvBnjHvyFwFyVIAPhJAizXxD51+ba0ILj15pixbcKLNLG1OQaA9xVgvWvs0znnhQXBNjYVB4KfS4ldDE95Wg4AJEBeoMQ+pWFuUXCI9UeQ53y/qr2EGfbwtNwt37ljBuC/C7CpJsY+/U6DOY+PQlBq46bZrIQKFZMps41dusTy6K/HS0pdNnsCwHMSID9PiTFew5uYJlNmGObF3RLbNDgZBOBrEiDsQeyfFgTHxWsLo3U0ADwmAcJ+fPOM8hgrxpCtowHgMwkQ9uZRZDvX/2jqTCEDcCcBwk79WxBswzbK5exaGICPDwkQdq30acg5bKm0TgMBkADheJqYHAYCIAHC8ZR2MCECgAQI20i3nIfUxhJ2r7TDB/CLvTvLcRsGoihagyROks39rzZAZ2CAtjtumxqs3LMCfT7UY5UAkACBDpaYPrY4Rk8WdZBD08C7QAAACRDoYlD707L6fOwguERGgQAAEiDQyZLNx1rfIQjmxKtAAAAJEOgbAxtPllUOKAdCIACABAj0LYUv9S+jB9NFDoZJIACABAj0Neinc8xXP1oznHkTCAAgAQK96Y2fs3myfJjrMcN0qQAAkACB3kp7HNhc3KZjDAQ11efEQWRQ1Wg2Oz8eAYD/hAB43KCWrrUeciA42Fif4qbSLKpmyTk5CACnJgCebIWPNxCMl/okt89frjrZ7PTLAHBGAuApS7b5VjoaPZkW2Un2+rRriuX24DNboCAGgFMRAC/QKXn95fPKsOxAvb7CQ17kDlUzkiAAnIIA6LEk0saBu98Q1Et9zej2EQNJggBwWgKg2+lAr7f5bFllO3Gsr/MQi3xJ1QIrIwDwlgTAGjdj9t0UGULtw8Ok/86+2RIbIwDwVgRA0/tmzL1NkUXWVq61m+tsjyTXopPNlMMA8BYEwDpKDD7eT1WrF8NWuxrdYpFHaLbASBAAjk0ArGjRdjNm62JYx9qdp4e/t+jEvggAHJUAWJ1OwWuzVTE8eF3FxS2XxyvxaMnHCgA4EgGwhbYksmkxXLIlr6vw9Pt1IN0wALwdAbChQW2+1mabYnjRKbT0ucPrwKaoGRdkAGB3AqCDnq1w/1PSrY9dZwrnIZZvZ1KCIAB8AwkQOIkSv2qF2ynpIn1p/jWG3D0GfoRSDsgAwCNIgMCZ3NkVXv8fw0V/PhA8QAwkCP5g745y3YSBKIAOGEICIdn/bluhqn5SK9XJw5UD52wg+bwyd2YA/k0ChMMZusvtWVC5m3YvCA5VCoLP26WLEAQBGhZAG7rrVJJ+amyO2QqCjcRAQRDgLyRAOLQ/Vsb8380x92crMVAQBPhKAoTjKykHbh6pv3bD7j9+S2MrMXALgqaGASRAOIlcDiwqCK67//qUHs3EQOtjACRAOJG8ObCoILjEvrb3t3ZiYMTauSwCIAHCKeTNgUUFwWuNgmBqKAZGLLNbwwASIJzAOhfM69Y9LddPaWwoBkaYFQGQAOEEhoIZkdqn5S59erQUA2NQEQSQAOH48oxIaUFwrlAQvD9bioHblZPJl2EACRAOrWCBdM3TcrmO11QM9GUYQAKEw8szIoUeaapQELxMadwlBi47/iczwwASIBxYnhEpNVY4LTfsc1ouTdcldtPN/c2DIIAECEeVZ0TKpXuFguDc359txcBtvbVJEQAJEI4qz4iUe9QoCHb9LbUVA2PxHgggAcJx5RmRF4wVCoLDtwuCY+rnNaLVrYYAnyuAI1rm/p2gU6EgGFtBsKkYuK0QNCgCnFoAh7XOL5YD82m5efcPw98rCD7u/a8nytZKiwAfKYBjG7occ14uCFY4LXdL34qB8UtbpUWATxPAGXSXnHJeLwiuEdFKQfDr9ugGtxoCfIYATmO55pDzRkFwiZ/a2CD4/D0o3M5/AvgkAZzLOvfvv7/df22OaaKMl6Z5jayd0iJA+wI4oVwOfK8geOmGNsp4Y+rnIb5oprQI0LQATusHe3e0oygQRAG0EFCQBf//b3ed3Uwnxow2S0s7c84XGJ9uqi7VqRy4riD4cTmmgjLenS9EKiktAtQqgJ8tlQPXFgSn+GP3Mt5lPE5RzvU3WQwD30cApMuBqyynKZI9C4LtaZijiFQQtBgGvoMA+Nx3jpeqQuCq53z79vnj0esLghbDwHsLgE22wpfjHE8q/5zvknM1cH1B0OUY4F0FwFZb4XboopD0nG89O+EPzeByDPCGAmDDb4XHc2Qon7n69KBwSdPZ5RjgrQTAg2yTt+vsT3NkKZ+5LuMwRUlpWe1yDPAeAuChLm8ceMnfBpfPXPm3o9f/WZ6WA2oXANuPA/tximzlP8pY0tHA4hpPywEVC4As17T13CAwXiddba5oGHg1eVoOqFIA5JufeVi4P3SxSvmrzUtqBr7C7Gk5oDIBUGwt3I9zZCt/tTl9Jhyv1DWDgiBQiQD4P1/nmg3mgCULgpfTuYvXmhQEgf0FQNEc2J9jAyULgss4zPFqs4IgkEECBGp290rf/hHw8eyt/3Wc4vU6BUHgKRIg8Aaa8+HQtn01Q8Db2VtVKfDfkNJiGPiCBAiw1eytqhT4d0hpMQzcJQEClCovphQ4xw7SkNJiGLghAQIkxQqCy3juYhdpSOlyDPBJAgRIihYE2+McO2sGl2OAKwkQ4EbBguBy2j0EfgwpXY4BJECAbOtf72ibqMPcHFyOASRAgMKa68e5fRs16Zqjp+UACRB+s3eHuQnDMBhA44SV0pKy+5922iZRaZoqBAEKfe8CVf99sh0btiiclgMkQIBNGp2WAyRAgE2qTssBEiDAmnQ/M3tTKSXnHDGme9lFb0AQkAAB1qH2+/I5+06D+9xF7NI9jAYEAQkQYB3G4Z8C3cfdsmA1IAhIgACrMK+b/icL5txHNP6eAUFAAgRYheX63Kkcc25bFow+F41hQAIEeLql+txcFhxibDkgqDEMSIAAT3fRhY9S9rmPaFaA1BgGJECA69WIRn3aix7wnsohdxFtCpA2x8C2JQCutOsPp59cNsTu7hc+2g8LRm9zDGxWAuAW0R1Ov7Esd1Efe+FjahAFx8HmGNigBMDN4rz2eSr7HOOjL3xMv+9G6i0DgjbHwJYkANqoQz5Ony2DYCxc+Fh8QlwNCAISIMDjfIeouTY3t4YfeeFjXjI9pis4MQzvLwHQXI1z8e48I7h7woWPeZvM6KEIIAECPMD4p5ZWjrmPNGtfn2u/TWYc5EB4RwmAZY1rdx/lkIfxThc+mm+Tmb+nLwzvJAFwgeazfFPZd1GfeeFjPj1nPhA2JwHQwLUt1dN3Z3j39Asfpewv7RBHZ28MvL4EwMXajwfOneEYbx4QbDCwN5XjJWXBGsYD4aUlAJ4h+nMp7c/ymJVc+CjlkHNEXf4H50TgNSUAGmi54qUc8xAppbVc+FjOgnXIusLwchJf7N1Lkp0wDAVQ2fA+DU2S/a82mWmQbia4q7B9zgIohipZugK4pv2aSJ4VOa6Vlw03N3KL+P3FGvHxfnkUhp4EALeQdeD/qyKR7pLsvCxLrbWU/Ld1e9kUhl4EADeS68JtV0X2rC/beyz/vGqtH3+APgQAt/PdJY7fy7XsmEOUC6ACBLi1fcs5vpYNwVW0M0wvALi1LNcaNwSLk78wrwCgA1kHNmsI5gKKUD+YTgDQjYyRbtwQXA0IwlwCgL5ktda8IWhAEGYRAPQo68CGDcHcRPYwDGMLAPq1nkzxPa4dFTlK/fAwDKMKAHp3ZB3Y7qhItho9DMN4AoAxHGfpLo/l9SzHpQFByTEwkABgJPtpyt/yWbdy4duSY2AQAcB4zpd6fy31WdYLb86SY6B3AcCoyruelWqXomPW8pQcA/0KAIaWsTEn0TEXikyn5aBHAcAEMjbmJ6JjdqfloDMBwDSOkh279psih9Ny0I0AYDL7Vj9zgq/1psjqtBx0IACY0kmh1uLIcNkMCMKNBQDzOl8TyZsiBgRhMAHA7HJN5ORh+HXhYdiAINxNAMDJmki7CEEDgvCXvTtIbRiIoQDqiY1Lkzrp/U9b6EaFkngyqkE0713A24/1R6pjAoBwOUdM++PBcLxDMRiGJ0mAACTlr4nkB8NbO70bDEM3CRCA40V97zPcfzG8Jb5gMAwdJEAAxiT+1kU98MFg+JwqCNocA49IgAAkHLFFOlznVEHQ5hi4RwIEIOHQLdJxY3ht2/gvR5tj4BcJEICEY7dI5zfHxDdsjoEfJEAAKthii/RuQbCN/3N0Wg6+SYAA1HGJ67/7BcFFQRDKmADg4HpgFARP4wVBp+VAAgSgmP7jv/P7+OaYxWk5kAABKKarHpg/LdfOCoIgAQJQStQDOwqC63hBsCkIggQIQClxXPjY03IKgiABAlBKHBfeN2dOyykIggQIQClxXHjfLXla7sNgGCRAAMqI48L7rvPb2pZE4DQYBgkQgDKiHtghWxA0GAYJEIAqnnu+ccsWBG2OAQkQgCJie2CPq4IgSIAA/BOd2wPjtNyaOC13sjkGJEAAqohtLt0Fwcv4CNrmGF7dBABVRD2wvyDYxjOn03K8rgkAalmiHthfEFyGZ9BOy32xd2e5scJAFEAZDDTz/nf7pJcPkvQQcNFRJM7ZAOLvyr7l4ooKAPiL5q0euLcgWOUXBK2W42IKAPi7xuFgZa/u81+OSe9cLTcN5X9DVVVT7diRgyRAAC5ne0V6p3UKvBxTDu8oCPbp7tyxrKrJySP7SIAAXNM2JnKgINjmFwTLkwuCTZWeXUHfqt5wMi9JgABcWjoczJrwarnmrAzYj68zZ+vZap6QAAGgmMvDF7V1ZLXcaQXBtZ1/Dp1D1bsc5isJEACObhPZrNHVcvGCYH1LxR6CIJ9IgADwxXj8fG6pu/yC4FyGk9n0cRIoCLKbBAgAJ93ThguCS+g6+OD/6QgiAQLAIzlzG2u0IJh7Mbx0hz87W2mMBAgAp83vLtGCYJ03HTwkq+zYQQIEgLflwKbu2sBquWr7YqAUuFdyIIgECACBHHhfEByL38tla1cWHxwI8pgECACZOTCjIFgGCoLTeug++JZiLyS2nZFhJEAAiObArSCYfuV8bq3GYhOYTAEJEADiObCpu6qc338+t3waDXExzEYCBICA2HN+dZ//ckza+2Rh3Y5nrc5zMYwECADn7Ptdp8DLMeWe1XJLNxR3TAwjAQLAnVgoyygItvkFwfLHe9rpNp+bdScFQSRAALgPZRkFwfBquebAaEj8HxUEkQAB4LuUd1hWR1fLLc9HQ4pHvByDBAgA54ekjGdV1uhquTVjdVwg6yoIXlgBALw6EaybjNVy6dwB3mko3qQcFAQvqQCAf+zdWVbiQBgG0BoCqQwF7H+1LZzTrVFBILGJcu8Cktfv/FPxhSFub06CcwYEwyl5vqsEDuH7VAOCzyYAAFeqsU8p39I7zcPMEmT3+rfchO/VGBB8HgEAuFk8ZsE253y5Hdw3ywTPmI6a8F/MGBDc5zalPr5VUkpd9lbd2gQAYJ54lN4qMQ7hZxtuHBDct19tQw+nCqpm8zoEAIAz6nVXEsfS3LZ90uk230YCBABmWP4Z5U2q3in5cQIAwDVi+exIYq4zu83OEl5BAgQAHmnoJ5dquoWKjJ1y4CUSIADweH9PV+ewHDHwPAkQAFiLJtbFv5gsiXxCAgQAfrlh2xkNnJIAAYD7pZRirGH9aunUAl9JgADA/ZpymrXb5S71sQnrNmzHAycSIACw2KxdHlNZdxDsWw3hFxIgALCA2rfHmy4/IQjWrRVhCRAAWMpQjiddpkEwrFFT9IMlQABgObWfHmDZ5C71Q1gbIVACBAAWj4HjdOBul9N2ZVvDjXawBAgALKyJ6cOrHHlM/Yo6w7V1I0YCBAAWF0ubD+9scpvWUhAs+XCv3Kb8QiVRAgQAzgwH7g7v7dexKnJ3IXDXDv/KnbFPaXzeMBgAAC51hVdYEGzK7nCfXVdqmBiOWTA/22vEAQDgclf403i0f+yEYLm/fLcZtzF8VGOf2pwPTyEAAHylxtdd4Yldbh+1MhxnhbXcluHch+M2jfl3v0USAP6wd7fJbcJQFEAlvgLG2N7/ajtu2o6VZlJcCQzknA0Y/7sjvXcFME/sHpZEEq/pEIyZKa2tq9P0Veztq+GgSTAAAGRUByYHgmu/LpfMA+bFwO+VBAMAQMZx4Gs3RZrqlip/KZwmwYOsjAQAgP8zJp0xL9oUmepbKfXQxTl/+z4nuPONkQAAkKGJ3ZDEob8fl2vCorr2VtDlPPcE8747vNdGwQAAkG3sq7r9YtZuWPJieCoew9q6Sm6F/3U5fN7b5XAAACjXGZOEsdUuhofbEq5D9cT5ZYw7OhIMAACh/JLIuhfDsUt6qwsfB8Yw37SLKcEAAFDceEpuhVfaGI59tVBry/X87NfG05YXhwMAwDKa5Fb4U/W5fJX0eEp+tqD67enTyzFWbxvsEgwAABkyboXTKulQ1hQXOoNr7znw+a/pqi0NCQYAgAzZu8JptJpCUU3sMo7giofWJvbbGBIMAABLSneFLzN6mas4hrJiv8wJ3OXnvfAOg2AAAFhc2iB9nbN6Uf5ieDwtk7vaJAfO18zfFpEAAYD9i30yHLhmc8wUq/fmmI3kwFlrwxIgAHAQSWXMys0xTeyWaY55f/5k60EwAABkWHg4MM1Wp/IDgpnNMeW/9dMZQQkQADicJlbDzCB2rRcZEByWCF3XP22HW1sWCQAA23BvbslYws01PT4t9/LimHfNry1mCRAAOLKxnx/D2nooPyD48LTcBopjHgqlLxIgAHBk04cdkfWflosZT8sVX2pJn5iTAAGA42o+7ois/7TcGDMGBEu3XqdLwxIgAHBYSYF0RitfjiYZECy7KBKzrqvPVwkQADiqewTLbOXL9zAguJVFkd8F1xcJkB/s3Vlu4zAQBFDKkpdYsXz/2w40zGIHQRa2GVDEeyfQZ6FVzQaAPuUdkdiQLW5+Lwi2cVHk5s+wBAgAdCnviPx6yFbhtFyN+x15uTl66UQCBAA6lHdECoZsjz8td/MHtomLIu8vXEuAAEB/8o5IfAs37ubV5nYWRV4HghIgANCddQpXFK5ONQqC+VNaWhR5GQhKgABAb+bTbtwXhavDZgqC66JI+OSdBAgA9GXdEVkKz3RspCCYF0XCP6wlQACgK3lHpHT7YiMFwXCXcR4SAEBn8o5Iieu4mYLg9f81ZAAA7h+QLt6+OG6lILjutJjoAQB8fEA63rpr/cTwMsqBAACRcuC98e1va+snhuVAAIB7wyE/i9dWQTD3FZt6OAYAoC/zKfQT9nqa0urhBUE5EACgqss58Ff4+TYEtl4QlAMBAD6eFW4qBNYpCOYcqB8IABC8J7d/OqeKhrUgaE8EAKCey1AQuJbjnOqaXwuC3pEGAKhjLeL9Mk0dplTdpUZBcHwyDQQAKFwSeT6nPzE9uiC4SwAAFI4Dl90lfanNgqAECADw+Tjw2tQg8K4gKAECAFQy/GhZeAk0AgP7K8dxLwECAFQxfX9Rbr+bUkDk0/61dwcpDMJQFEVj0pYmiu5/tyV0VBAKBjGSc1bg8CJPf0yrAgQAOKD9fFtTA7a/qlw2BQgAcEBTBz5f4VpziTkpQACAU9QO3Bnh5dCBvYFgn08KAHBDU4n5Z4XXz4+W/wwEUwAAoKm2pnes+ru3Vr9l3hQgAMBw5hKXpAABAIbzHS8qQACA0Tzqzbs1fQCSNUxdIwQCDwAAAABJRU5ErkJggg=="/>
                    </div>
                    <div class="content-wrapper contentWrapper-2xjF3">
                        <div class="sectionContent-3NYlN">
                            <div class="copyWrapper-2R7us">
                                <div class="detailLine-1Lbvr undefined halfLeft-1HqDN">
                                    <div class="dashDetailWrapper-ppq1R dark-2kOE_ "><span
                                            class="borderLine-3aHhe"></span><span class="dashDetail-33_7u"></span></div>
                                    <div>
                                        <h2 class="heading-01 sectionTitle-2fIox undefined dark-1RI-y">
                                            <div>WELCOME TO</div>
                                            <div>WILD RIFT</div>
                                        </h2>
                                    </div>
                                </div>
                                <div class="accordionWrapper-jYMQQ">
                                    <div class="accordion-1Wkgq accordion-2x0uj ">
                                        <div class="item-2ZxmI">
                                            <div class="header-3tbc7" data-index="0">
                                                <div class="titleWrapper-3FHW4 active-1Ri9D"><img class="icon-2kTCb"
                                                                                                  alt="bold competition icon"
                                                                                                  src="https://images.contentstack.io/v3/assets/blt370612131b6e0756/bltd49a1f62dd145698/5f597a732917074cd81a1e6d/ic-competition.png"/>
                                                    <h4 class="heading-04 font-normal title-3gre_">BOLD COMPETITION</h4>
                                                </div>
                                                <span class="indicator-1FTvI active-1Ri9D"></span></div>
                                            <div class="copy-01 description-kqOZ2">
                                                <div class="decriptionCopy-AUOe0">
                                                    <div class="richTextContent-2_e96 ">Team up with friends and test
                                                        your skills in 5v5 MOBA combat. All the high-skill competition
                                                        you crave, designed especially for mobile and console with
                                                        revamped controls and streamlined matches.
                                                    </div>
                                                    <span class="dashDetail-3YdzR"></span></div>
                                            </div>
                                        </div>
                                        <div class="item-2ZxmI">
                                            <div class="header-3tbc7" data-index="1">
                                                <div class="titleWrapper-3FHW4 false"><img class="icon-2kTCb"
                                                                                           alt="real league champs icon"
                                                                                           src="https://images.contentstack.io/v3/assets/blt370612131b6e0756/blt68c90ed19df72f7e/5f597a7465c9c14aec2faebf/ic-champions.png"/>
                                                    <h4 class="heading-04 font-normal title-3gre_">REAL LEAGUE
                                                        CHAMPS</h4></div>
                                                <span class="indicator-1FTvI false"></span></div>
                                            <div class="copy-01 description-kqOZ2">
                                                <div class="decriptionCopy-AUOe0">
                                                    <div class="richTextContent-2_e96 ">Banish the shadows with Lux’s
                                                        Final Spark or follow the wind with Yasuo’s Last Breath. Play
                                                        the League champions you love wherever you want.
                                                    </div>
                                                    <span class="dashDetail-3YdzR"></span></div>
                                            </div>
                                        </div>
                                        <div class="item-2ZxmI">
                                            <div class="header-3tbc7" data-index="2">
                                                <div class="titleWrapper-3FHW4 false"><img class="icon-2kTCb"
                                                                                           alt="vibrant universe icon"
                                                                                           src="https://images.contentstack.io/v3/assets/blt370612131b6e0756/bltc3f5e9f65db6c6e0/5f597a74d1e5eb4414692ee8/ic-universe.png"/>
                                                    <h4 class="heading-04 font-normal title-3gre_">VIBRANT UNIVERSE</h4>
                                                </div>
                                                <span class="indicator-1FTvI false"></span></div>
                                            <div class="copy-01 description-kqOZ2">
                                                <div class="decriptionCopy-AUOe0">
                                                    <div class="richTextContent-2_e96 ">Explore the living universe of
                                                        Runeterra through lore, comics, games, and more. Then dive into
                                                        the community of gamers, cosplayers, musicians, and content
                                                        creators who are waiting for you to join them.
                                                    </div>
                                                    <span class="dashDetail-3YdzR"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="videoWrapper-jHOQl">
                                <div class="videoContainer-1qUhF youtubeVideo-2LLmZ ">
                                    <div class="posterContainer-3PMhr" data-element="poster">
                                        <div class="icon playButton-1HE4d icon-2tHD8 currentColor-LyOgN">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 86.88 86.88">
                                                <g data-name="Layer 2">
                                                    <g data-name="Layer 1">
                                                        <path d="M54.63 44L38.22 33.06v21.88z" fill="#fff"
                                                              fill-rule="evenodd"></path>
                                                        <circle cx="43.44" cy="43.44" r="42.44" fill="none"
                                                                stroke="#fff" stroke-miterlimit="10"
                                                                stroke-width="2"></circle>
                                                    </g>
                                                </g>
                                            </svg>
                                        </div>
                                        <img src="https://i.ytimg.com/vi/pNjWjwae-us/maxresdefault.jpg"
                                             alt="youtube poster" class="posterImage-3av-B"/><span
                                            class="overlay-2VKk6"></span></div>
                                </div>
                                <span class="border-ojuPz bottomLeft-XSvX5 light-3sJ1f videoBorder-wUX0n"><span
                                        class="borderLine-24GpQ firstSide-2PgwT"></span><span
                                        class="dashDetail-26mBt"></span><span
                                        class="borderLine-24GpQ secondSide-2g7B7"></span></span></div>
                        </div>
                        <div class="button learnMoreCTA-3VCMk
    "><a class="dark-3gXr0  primaryButton-2ec0w" href="/"><span
                                    class="animatedBorder-1iWpQ top-cn3E4"></span><span class="background-u8Ka7"></span>
                                <div class="label-01 label-3gc3A "><span>LEARN MORE</span></div>
                                <span class="animatedBorder-1iWpQ bottom-B_Gla"></span></a></div>
                    </div>
                    <div class="volt-v0O2t bottomVolt-K6psi bottom-b1FgS"><img class="voltImage-UYrS0"
                                                                               src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAACgEAAAKKCAMAAAApjJkNAAAAM1BMVEUAAAD///////////////////////////////////////////////////////////////+3leKCAAAAEHRSTlMAQIDAYLBwMPDgkCAQ0FCgdHZfMAAAInNJREFUeNrs3V2SojAYBVCTyI+ikv2vdgraKXpaW4XqroF4zg4CL7dy8yW7FWtiHeMlhNMOAIC30YVYHXLOKVUx7kPYAQDwFkJ9TPmqTekYa1kQAOANdJeYDvmTQ6qGkrjbAQBQsFOo+5S/aFMaKuJmBwBAqZrrduCdKGhXEACgXKfpdOCNdiiIDREDABSpuwzDwt86p14/DABQorA/pjYPHm0KbniA+BRHddjwGgAAft4pxD7lJ1I6xssG6+F9lSdpnIK2tQkA8KH5XAs/qYe3NTMSYroTaHuXIwIAs21vO2xGLfzcYVtJsNn35/uJVhQEAF6Xc6ripcjo8PfywNKSYKj786OaezMLAQD+l2lW4hiLPFnWhTgEptKS4BADn8y+2BQEAL5x0ybGegsBaMnxwHPOhSXB5oWyO6W4ydkXAOAX5btSFfcFpoZmPz4mMi8Jrj0/dS/Ovmx0ChoA+AWPm8S+xCOCYU4OnPLTuj/F9YUUSRAAWJgAb4NggUcEhxzY5rlSiiv+Fl0Yu25JEAB4bEYhWuCsyPi4cJtna1O13vOS165bEgQAniTAtw6C/+TAQo4JfqyppJ4bAPjxBCgILsqBU3xa57PD3fhS3ixppUsBAFaQAEsNgsty4FQOr/EOmfEqnCU9d1F/FgC4SYCC4NccGNNh+edYY6PaXJYs6ZD6Ii8GAoD3lud6p+tjpntjllnhvMjSJZ3TUTkMwB/27mg5bRiIAugYkWA3QPX/X9vBLlHimWZKY4p2OecP7Kc7K90VadQ7GHMtlL6GpkQjwX//JIfDAJBBvaOS6Ym5Yb9cpks0EhzeH5VzOAwAz6Xe35ToAPFaqsgzErxWX3KVoAGAL9X/ZswzNjqclyUreUaC1xyYqwQNADw8AV6dun5Y7ZvL9mJ3LJYcmONbAICuEmAbCaYYG70Nux9lzDQiXXJgmvEmANBRAsw1Nmqp6btK6aNsu3xRphuPAEA/CbC35LPVIukcdZElB2a68QgA9JQAE3UK5m17mVYrHofXZDceAeDJ1U5N4a8Jthy4gVMHqxXn5ouQDwAZ1M5FT4JzDsy0WnHOgUrhABBbDSJ2EmyLpLd7f/m2s+Fuc2BHJWgAeB41mKn8DNsYafPAjTzuOLXlwJMqEADEU4May0vQ98haDkxySfB9wJmpBA0AudXoyiUxxDtEHPZty8qG2emGIVrXOVBdBAA+kwC/OB4ONhSct+3d4UfcMkTrOgeqiwDAbxLg3wwFI02P3oaWmnK0RQ7nXZnyxFoAyKemNZayC3Q+fNjvyli3d9NK6c4vPD6++wIASdT8prk0EiIKHv9Yrg37MFvLgc6GAaAb9YksUbD/EdJh1RJJUBsePk449YYB4GYS4FYHxH0Pkd7OnzJThiB4HF5X0TbT48kAEEl9buXSIO73hPg47F6muhJ8f8zHHJgh1wJARJU2Ftx3uU3mEgNXiSl+EGyLcFJ8DgAEU1mZusyCl0PhuhI/ObXnhbc3WiAD/GLvDnMbhYEAjC44LYGGxPc/7XapInUrg0hUEg957wb8+2R7BkAB3t2CFd0Rd4fipHD0ECx24A7W4QBAvTLr7ogvbdvUcLd4auYOA4OHYGExtkeCAHClAJ+rT2l4egx27/MvA2OH4I/fyoX/HgCoU+Zu/XQyeF59TRy1AleEU7AO9EgQgFeX+RVjSsPja7Ab+rwodAjO/E7EJkEAUIAV+jobfG8eMk/cXfIKgUNwvgN38M88AFhLAcbSp5Tatj1s14MfY14v6nBFaY30Jh/kkSAALyHzSMeU0uUrCH+tnU5v+UZRQ3D+dyJxjzgBYBUFuCvH9GloP52be6Lwo/k3D1KFbZ7UlTtwYX2gaREAUIAhjWkytFfn5rtDO0kp9bk+Uwj+mRVkjfT/IWhaBIDdyTAJeX62vEY65hknABQoQOJYelEXaX3gN8m/RQDYgwwl8WZFuoW1MUE/CQCuFCBRzd2jhlsfaGwYgPAybKn8RLAQTfHWBwpBAOLK8Ax9Gra+Rp3WB+ZlIedfAEABEtrc8piAa2OEIACBZHi6YxrawoFgzLUxFgkCEECGWoylA8Goa2MsEgSgZhnqciy8EAw8LiwEAahRhiptPTLcHFaNCwtBAHYpQ8XGt0078HTj2hgbpQHYiQzVK3Vg6HFhIQjAjRQgr2q8tD/vT4OPCwtB/rJ3Z8mNwlAARTWAMQSw9r/azljtTBUn5UEW52yBn1vSewLgVAqQTdvn1A3hYp7XhcdyIiEIwL0qcH/yYRfDew2sCwtBAL6lAOHF2H9xK9zCmogQBOATBQhXzMAw/3ZNRAgCcAcK3L2nDAwX9fDrv4kIQQBqVqANy9RdupCG9bfjgUIQgDoVaMd4WOfwlQb+JiIEAVCAtGCfc59S2sUXR+02xFdrejLlR0s5TR/DFcxx95fxQCEIQB0KXF+ednH4U3j9z8I+P9l/7sop/KCJV6SFIAAKkDuypCGc3xyfndxBbY0HCkEAFCA1W2JoVAXjgc9yn7o4BwBQgNRibDxN5phuPx4oBAFQgFTlELZgWGsYDxSCAChAKtGFzYhdquJa+NE+H9La7P07AAqQ6qWwKVW8GiMEAVCA3Ni4oWPAan4q9yEEpxSHAMCWFbi2cbfNwbRatoVfjUIQYMMK3EDfbTMCX7aF6zkO9IwgwFYVuI2+23B2VPKI9JElWxoG2JICN7NM9a0lPMSU0re/F2l3S+To9Zj6PgoAIShAmpJTXcGx7t8djOVDSrsYY3in7eNAS8MAG1Dg5vK01nMjPHf9/pt70jyltL6dDjZ/HGhXBKBlBaow9qmeMbQ4LT+1UT6k1J25Boe1mjekj412RQDaU6AeFWXgvKZcTjK+XRUPZzoOrOntwH/s3Vlu40AMRVGR1Dxy/6ttt4QYQbeDSImtYsn3bMDW3wOn+mTigTkAuBAHgmktzDKC1FV38L9b9ZTBwZBd4RUjggBwCQ6E1EXJgcPcm/+I/XZwsCn7oDGQEUEAyJ0DgUUZQft1FuvMqu3MzHj8p0M2he/npOkMA0COHIjPQoygPa0k15mZqs77RwdFoxYDV1bRGQaAzDiQjclURYpjosbAu2nLg/V3V6jnJeCe8L+d4QgFWwAACRCXZKZph9CGF55tae1G/5LVeM+eop4D3pcDgBw4kKvWKq3TJcFRdIk7oZdaawurIgAQmAO5a23RMlX7sSlDXnGOomNVBABicuAqumRJcBQNd77PPkR4dZiCIABE48DVTNbrnKDw1MxxXvNY/vv8QW5qVe3txndjQhAALsmBq9qSYHGyptQIVTc3/e7TRWRW1eXkQNiyMgwAAThwdZbgiIyEyIGm87C/ky1SbxXC1k9glc40hgFgPxIgkMsRGSnVJk+rtZ8krUFEVLWyF///KcrjfwDwbhx4MycfkdnmA83Tsv43K7kiUj9qF1/uEWgAeB8OvKktCQ7FSQbR/mh/NWTQakTKrVsc8u8BAEiAQLgjMlIeXRiOmQNfFgc7Yz4QAB4hAQKbjJPgIHVvnR8X+lRzsy0XP2OXxJaaciAAfEYCBL6W2TlBST0h2Nqy8zJLil2SqSIGAsAdCRB4LNckOEqd+HTMVL16Ak/WbrH+4IWSqS85HggANyRA4FST9aecE2xmXdIXBJvi5e55sNo7Ptj1TAYCAAkQOCKzw9Jj8huCna0r0ieSG90S4ZefvhQAABIgsF+WSbARXaz1hKYtCCYxyko/8JwwABQFCRCI4PFh6asVBNcgyCu+AHA9Dvxh7253GoeBKIASu00/UNp5/6dd2mRBaIFtQSJj95wnaJU/V9eeMcmTYIJl0oIgQGcCaCMJrr9Mep6KMZUB0IEAWlosnaAQvE4Nn1zNA2hZAFdtJcEEheB1KGZUCQI0KYB3WkqCGQrBiINKEKA5AXygqZh0KQRrrG6qpQzecQNoQgA3aOCxuedhPNZDrK9uHQ4DZBfATVpJgsMpRSEYsZcEAfIK4C5tJME0haAkCJBSABnMSfDpqdNCUBIEyCWATOZnh7stBC9qQxMjY5ltBtkV6EsAGc0xqdtC8LX2TP7WXNm/rzHruZQh+W8GkACheXMS7LcQfHG4HA+nLQVP5/2HR9r1WDZ6QaBdAbRgToL9FoIXNWkpuCv1yw9jHzbQoABaMifBfgvBvzMjZciVqoZS/7vy26AL0JAAWnRNgruOC8ElVWU6Hx7G7d7IM9CJABq25I2OC8GrKU0UfN4cJ8tvgA4E0IElb/RcCCaKgkPZHqxBBNoWQEeWvNF1IfgaBVe9K/h8KnV/54n2JtfdRuChBdChJQn2XQguyaqW1SaId5tjTfH4C4AECPyTBDsvBN+WyYwr1IK7zXmKu10+TMLNN8DDCOABLEmw90LwrRb87XXNw3ievjnubJ0gcAsJEPhxEuy/EJxNdVvK6fP/myQGvqjVvAjwJQkQyJAEWykEfzkLLoPC33VwSxD4hAT4h707ylITBsAwepKIIgyS/a+2reVBLVhmFA147wp8/M4fE4BySnBNg+D1GfGiJ6/tqUn5EamKO/8SBC4pQOC3skpwZYPgoE6pj/G01POCIVb1w8tlE50NA2cKELhQUgmucBC8GAabJWLwsK/q/LDadRFAAQJjCirBdQ6CN8vgLoT2aVtgl8d4QQb4BgUI3FVKCZ4HwTqvW5dS84wabPfdU39VdDYMKEBgRCklGHax6vIW1ClVMcbwwxxs+zzOvWFgDgUIfEcZJdiGuP5B8Eo6nxXHUwizG+yQF1H74DCgAIEJRZTgdgbBW91QhHEX/mj/TeBTc8zj/EkQmEMBAuUYSvDDB8GSdD4uAihAYEIBJbjdQbAERy8JAgoQmPL2EjQILuqYerdFAAUIvMhQggbBMnhSGlCAwCulFOPs67KHEHuD4IJS5dowoACBaW8swa+wi8kgOMK1YUABAqv0twQNgiXokhCED5YBXm4oQYNgAXxkDj5TBpjvTSV4OMU+ZW55PwZQgMCKzSrBr7D3dMyihhD0fgx8gAxQji41w7sldzvQHriw2kOCsHUZoDz/LcH2FKtj5poQBBQgsAFDCU7Pgb1rIuM8JAgoQGDtziU48XRJ2PfWwFFCEFCAwBYcUz/6ubmvEP03cIoQBBQgsA0pNXF3k4KHvQq8RwjCL/buMLdRGAjAKPYAgaQE7n/alSrazSballYhMvDeCfj5aewxKEBgJ4Zocr6mjxS5isBvCEFAAQJ70kWEH4gsJATh4CYAEIJwMBMAPJd3BKF4EwB88qA0HMMEAKuZ/zV8qoCiTACwvj5ymy4VUIYJAF5mCLsiUIIJAF4tmly7IgjLKUAA9uJ9V8QVQVhAAQIcV1OnlHLOY0Q37UcfZ1cE4WsKEODI+rH+SKVTqvM5dvQbPFcE4f8UIMDRdZGv7xm4zxSMxiuC8EABAnCfgbNLuuYc0U870HlFEG4pQAA+xblN1aO3lHIeY5g2b4jcOhmGqlKAAPxraO7GgXcnxHn7eyMejwEFCPA63dim0/1R68c+blkztjj/HZbtdCzYxehkGBQgwOou1TcuKbUFhVUfN7OyvY4FnQyDAgRYVV8vLo2Uci5kIbdvbt5Z3u1Y0M4wKECAtXTjHIFLpWseixiwDXMHLnDa7hKxnWFQgADrGM713Bg/C8ESJoIx5nSplnpLqd3k24JekwYFCLCCLvJNBi4vqjaXMFpb2IH3bws2WzsijtHJMChAgGcbfnf3LNW5KaCl5g78obS5zRH/GQYFCPB8/e/unp2uxXTg4o9/3BzZzhHxH/budclNGAbD8MoyhMNifP9X2x3qZjLQHEhrkJ33uYEdZv98kSxpYokgQAIEgP+u09l5Gd/IgQb6wnfmRHZMjjQW2tssEQRIgABwjv6NJCjexPvAdQ58q0U8lNAizrxEkCEUFCcCAE5Kgt/SDhZ6qtNqj3TFLeJcoyISgvdtS7ER5YgAgFOT4GjjeWDsVwtVKm4RZxsVkeBm1zgXGESBeREAkEFKgi9XhcQPJnqp3f37wtVNEWueUZG0DHLSxjlhFAVWRQBAXp02rn0pCow2pkRi7PYujnm+aNpsi7jTYc+37j8PM+ngPP1hWBMBAAdRdS48L7BJa+KayA99ZVCkkkXT+a6KiLhB+xi75f/P5RIYEQEAB5t0floUunhnpX2aZ6/eRbzBW8QZd8dcUhCMsdfGtfSHsRcJEAAqsRSF5EFRaBQjXeEffZZ1KqlFbG2jzGp3TJ4gGOkPYxcSIADU5mFR8FvcbKZrmh7N5TEuG2UMtYh1XnXB8wTB9FOA/jAeIwECQLVUl00iph8HPn8gWNlGmVX1M1cQTOPj9IdxDwkQAKrXafO3otDFm9gg/dom6do2ymjjQq6vHaV1Ot3+MfrD2CABAsDn6FMUtBsDY5dhvfLd54Knb5RZLZPOGwSZH8YtEiAAfKA/UdDeqHCShmgPMIqcvlFmVRDMGwSZH8aCBAgAn6zX69WxMViLgWlzzFEuEvZulCmnILgJgswPgwQIAPjdIPQSjDWFr8no0M6liN+zUaagguBtEKQ/DBIgAOBq0snG1Oz7jeEKDtDlW5STPsy7RuNWz/1hkAABAObsbQwX/lxw87n5g2Ay6Ux/GCRAAIAl+5/MFf1ccFMQPCQIJqrD8lIUIAECAEx448pGydsFNwXBI4Ng7LTh1AhIgAAAK/5llXSB2wW3BcFjgiCnRkACBACY0+ngzu9VPjpGXFZB8OtLgpu3BU5OjYAECAAwRxsXTGSSe8eIi8u9KQg+2STEU8FPFwEAv9i7o+UEgRgKw7ABXKBo3v9pO+riaIFaxikk7P89AZdnQk4Wu2slNGY21tK6YKsvnA0EJ0FwTsuqYL4UAAAjolyCpZ+U47pg1CfOGtIpCL4xsCqYHQUAwJbrgMxYGEnrgqIjP4+KPAXBVu9YFQQJEABgVJSuMbiq1pfnx6VpdwPB4lSG7hEEWRXMmgIAYJiZpsjEKa0L+hsIvgZBVgXzpAAAmGeqKTJbHelEorOB4GsQZFUwMwoAgBPWmiKL1RFfb6ikIPhXA6uCh6AAAPhisCmyeGna0QD0FgQHncEDxEekAAB4ZLQpsnRpenAyECz6sgmrvja1Ruz+pAcJEABwOHabIv9zabqVUM2Grd2DIK0RXxQAAPdsN0UmxkvT9jcifwRBWiPHoQAAHIT5pshsdeSDS9PD8kDQQhCkNWKZAgDgVxSROlxV5Y3Xn5AfVEeidGGLjcgUBEVXS2+NeFjbzIcCAODOICGcDzpbGqsjdivSX2UVatEJ3hrxRAEAcCTW4ZxLfhirI0Yr0pMguDIK0hpZhwQIAMhVXeUS/haqI7Yel1sIghSIXVAAAFyQipTwqI7Yq0hPg+D6KEiB+DckQABAhlqiwWx1ROzckn4KglGfcEvGJAUAwL6a+d/b6oiRW9J35TlcJKoSBa1SAADMazdLLn3X1aFxWjM+va+ORLm8OR1jLgiqCmcFr0iAAIDchGJDfTPcRmYXx4/dphpx3P90zEsQJAraoQAAmNcVG0ohMBFxOxK8+prUiNedjjEZBImCJEAAQBbi1rO4FAIT9yPBSXVkr9MxYxDspFUlCq5DAgQAZKcudtCHdiZxhMb7kxZ9qo7sdTomOZX3IEgU/CMSIIBv9u4tSU4YBqCokTBt89T+V5uPkKqZSqcHKuAX92yBn1vGsoDn2VwWry3YO1FHWeufUt3HiOP3p2N+7KiCQ5AUpAABAE1ZJpdH7z/v3uhrPxJ8t4Fu0Z8nRcoOQVKQAgQANCEMLpNJon22qEgrrfFldCToKD/2bekhSApSgACAui2dy2VVOyBUPy7ybow49aTIHoKiixkp+AUFCAB4Ks3XgNMW7Cj19Y+L/DVGLNINqeNp6oZ/hCDbRihAAMCTaO9ymSWe7YyxmX/DJ9URgo9OQQMAoCpRJpdLH+275/0bPqOaEDSL6kW6J30lAwCgNn51xxXQgG3+Gz6sohA0i6oi6wO+kwEAUJ+wZYvAPthnD5obzmYPQa92m9D43IgBAFClbBE4i10g6tbCm9LZzV0vXu1O2uRlQQMAoFZh62eXwaR2GfXSP+n62R/1haBZVN/OH2IDAKBmKi+X3hDsWksD+4YLkCQE//whrvtY0AAAqFzwffJ2ei12h6ij1B0WRdhDMIFFvUhXY7sbAAANiFv/cinN3g7iAZlc5m6VTYMloTrKUNF7MgYAQCOCHzqXzma3Uy99jedLhdlDMJGgKhX8IjYAAFqiY6rDwHmxRBaVod1nST6oOQT3twWLPRc0AACakyYDX3YAlwRLs4dgUkF1E1lL+noGAECbdBxuPn1RO4xLgqVJEIIfYnDoTtcgBQgAwGFRpe/cTUY7j5cEy9J1Mmq0LCZ3BgUIAMBJQcc7NrFFKwAvCV7hlSME1Z1BAQIAUMRFus0KEnVk3XBtITi44yhAAAD+k3oZuq6g1XDXCawbrikEe3cYBQgAwEV+7/Z6ufPm1VvJApcE6wjB4wlIAQIAsLt2t1d/cLnXtEqRx3/vKJcErwjBQXSxe4g7iALEL/buLUlRIAigKFWJtigI+1/tPOTD6RhpbLWF4pwNGPB1g6wsAXihQ0T0+bdz+myfcx/dsEJd5PccEqwL6s/Ta0Jw11bzKEAAYAGbMHO3pbuillSeHoLNzFejAAGAVWyLpE8/W9DZxEsIrngSPAAAm/TybZG2WdRI+mKJIdjNeR0KEABYwbZI2331JbIu5ZTgKdV5F6v6DDgAADz/01xqZv7uMadCbjBsvx+CTaqmKUAA4B9L3BY59XffYFjMxTXfC8Hd9MMrQADgf5azLdLW8UiBnsvYF7k7BPPUYytAAOBnjF/m7gqy0/nYPel0Yhn7IneE4GFf3aQAAYAfFxHjNdm3cjClOvdxGB5W6BUy6Tzj9TR1dYMCBADerYlrD3XfxvZFLiH4/gYcAABWInaFDIfHEJzfgAoQANi4Jvoy9kXGEJzRgAoQAOCvLnIRw+ExBKcaUAECAJR4meBHysdoxgbct9U1BQgAUPBweAzBQ75uWgUIADAlIuf174t8pHwVswoQAGDmcLguYDj8hwIEANjoP80pQACADQ6HFSAAgOGwAgQAMBxWgAAAhsMKEABg88PhX+zdQQrCQBAEQN1RCUJw/v9aD+YgHtRgDMlO1SsaGroTAIBi5XACAFCsHE4AADr/HJYAAQCKfg5LgAAAs43R2vF42L0EAKBYOZwAABQrhxMAgN+Mcd1XOZwAACwjzu2yiw2ZBABg8XL4tOlyOAEAmFQphxMAgL+K2Fo5nAAArGGIWzttY0MmAQBY0xjXmRsyEiAAQB8iWvuuHJYAAQC6MsT5UzksAQIAdOnNwYgECADQtTHa64aMBAgAUMHwdDAiAQIAVDIdjEiAAADlPA5GJEAAgHoeByMSIABAOdPBiAQIANzZu2MbgGEAhmHoViD//5vB+ULkFZpsct7BiAIEAMjZwYgCBADI2YbMUYAAADnvYEQBAgDkbENGAQIA9OxgRAECAOT8OxhRgAAAMZ8CBACIUYAAADUKEACgRgECANQoQACAGgUIAFCjAAEAahQgAECNAgQAqFGAAAA1ChAAoEYBAgDUKEAAgBoFCABQowABAGoUIABAjQIE4LJbBwIAAAAAgvytB7koAm4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgCB2K0DAQAAAABB/taDXBQBcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3Bkjs1oEAAAAAgCB/60EuigCAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALiJ3ToQAAAAABDkbz3IRZEBAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAELt1IAAAAAAgyN96kIsibgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAGI3ToQAAAAABDkbz3IRRFwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCMRuHQgAAAAACPK3HuSiCIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAYrcOBAAAAAAE+VsPclEEANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADATezWgQAAAACAIH/rQS6KDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAYDYrQMBAAAAAEH+1oNcFHFjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIQOzWgQAAAACAIH/rQS6KgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAgdutAAAAAAECQv/UgF0UA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNARK7dSAAAAAAIMjfepCLIgDgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG5qt44JAABAAAbpb/+8BhmkwAABAGoMEACgxgABAGoMEACgxgABAGoMEACgZmcBAEi5Bw7gESKOz01SAAAAAElFTkSuQmCC"/>
                    </div>
                </section>
                <section class="container gameImageData-zkvSz">
                    <div class="volt-v0O2t topVolt-pNMQz top-1myq9"><img class="voltImage-UYrS0"
                                                                         src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAACgEAAAKLCAMAAADi0EqoAAAAM1BMVEUAAAD///////////////////////////////////////////////////////////////+3leKCAAAAEHRSTlMAgEDAYDCw8HCgIBBQ4NCQi6e6iQAAH59JREFUeNrs1gEBADAMw6Dewf2rnZCACvYAAEjZPgAAKQYIAFBjgAAANQYIAFBjgAAANQYIAFBjgAAANQYIAFBjgAAANQYIAFBjgAAANQYIAFBjgAAANQYIAFBjgAAANQYIAFBjgAAANQYIAFBjgAAANQYIAFBjgAAANQYIAFBjgADAsVsHAgAAAACC/K0HuSjixgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEIDYrQMBAAAAAEH+1oNcFAE3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAQOzWgQAAAACAIH/rQS6KALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMkdutAAAAAAECQv/UgF0UAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcxG4dCAAAAAAI8rce5KLIAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAODGAAEAbgwQAIjdOhAAAAAAEORvPchFETcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAADcGCABwY4AAxG4d0wAAwEAQSrrWv98XcqACoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoOYNEAAg5gwQACDGAAEAagwQAKDGAAEAagwQAKDGAAEAagwQAKDGAAEAagwQGLt1IAAAAAAgyN96kIsiAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgCJ3ToQAAAAABDkbz3IRREAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3sVsHAgAAAACC/K0HuSgyQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEAGK3DgQAAAAABPlbD3JRxI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAsXdvOW4CURRFuXUNGJvX/EebIDtyt9Ry5A5OomKtCVCfWxwewNEoQACAo1GAAAAHk60CBAA4hjkjSml+UoAAAHWbcoixXJsbBQgAUK8+uziXS3OnAAEA6vWYezcKEACgXlMuH+ZeBQgAUK9t7j095l4FCABQr8yI0jZ3ChAAoF7zp7lXAQIA1KvP4ePcqwABAOqVGefH3KsAAQDqdZt7mzsFCABQr+nz3KsAAQD20+dmWv8X2X019ypAAIBvx94SEWXTfKmUMkZ0/6AI51ye/MZDAQIAvCBziCgv31Zrx8h+fa/H3HttnlOAAAC/1+fw53NqG/P6Jv1Lc68CBAB4YsqIHcuqHdZ9zRn7zb2XcjqP40UBAgBHlUOMbbO70u829457zb1tOUfXDcu5XN0DBACOae7ijV9OKTvMvfuc7lpKROba59Z+VmAA4KByOZXmzebvnWy/OfpSTrHkdoxb+3kOEAA4qnk4tc3fML049y7b3Lvb2Jvb5X+1nzdBAH6wdzc6isJgGEZLKwr4g/d/tRtnErvZmB0naeBDzrmKJ7y0BXbrVLqcljK8P/ce8tRw7P1W289ZYABgr07HoU8LOiw299axt55rfm7cChAA2Ksy9GlZw3+f8bi0CLT8GHvr/dOv2k8BAgD7dJ0vaWlTeXnZdJO595wvdez9qf0UIACwP7euT4s7j//OvUPum4y9Yx1732o/BQgA7M1tmNLypvFU596cG469v28/BQgA7Mpp7tMK8vF+/Zp7z+3H3ur2bvspQABgP8ohrWLKfaux97Xb8Tdv2ClAAGA3xpw25zn2xmw/BQgAhHac0nbUsTd2+ylAACCw6za+/z3H3o20nwIEAOIazym059i7sfZTgABAWIcU02PsnUu5v6GMEdtPAQIAUY0pmP5r7L3e31LGLsf+h/EOABBOmH7Kufseez+l/RQgABBVWlkdez+t/RQgABDVkFZRx96PbT8FCACEtWwC1rH349tPAQIAcZVLaqDp2FudyjxstP0UIAAQ2XXOqVp97P2r/YJfVagAAYBNK/Mhrz72flT7KUAAILhTKXM3TO3GXu2nAAGAmEoZuy7nqdnYq/0UIAAQ0q0cu+7S5IDF1HXlMfZqPwUIf9i7o+U2YSCAoqzWsTE2WP//tc24D0o6mTZxgcHmnB/g9c4KrQDYoFNmxKGUbmZDKeUaEbd8V39gzDiU7sVVAIBVtYFfWXPKVt71EXHMzHG/7acAAYC1Zd7iUDaRWZ+GhPtpPwUIAKygnfS+dWxEBQBoZg+/61O/nfGqKgCA8NuZCgAwi8ybo94nUQEA/seY5+i3cbkDBQgALOr3We+lW1m7xBvOmRUgALCKUx7XH/ld7s13+7DbeTr3xo4KEABY1pjnlf/yeyvlXn3THwV6Dj8bKkAAYCmt/IZuFS37TrVp6Xc191OAAMCC8hj9KsO2ln1j/cqUYeqnAAGAJU15i1K6xQ33Z3ozx79PHy8dXacAAYBm9qHf0C3qUsohInP6110TC6U/UYAAwOzpt9ysrWXfMfMbt4wd935JAQIAc5ky+pZ+y2Wf8tuaCgDsUFupslj2Kb8NqwDAruR57n/shg/Zp/yeQwUAdmI6RhlmXeASmdNmNwuiAAFg36ZzX+Ya9p0zTz/7uPLbnAoAvLRTRpnjRd7pocWCV/v8NqkCAK9rirfHTnj7+6xvrI/IjCieb9u0CgC8qLEfuu8q9+i7ZWZ9QLvgofyeQ+UXe3eY0zAMA2BUqbeVTrTz/U8LbH+gTINKZaj1e6f4FMcJAOxT+yn5unZrvjGXmy94WO3dlgQA9inaofvk0D7EuylXEa75bVcCACwb9rrmt3kJAGDYW0wCADwQ0Qx7dycBAL4bbfbuWQIAzPY7fOCxdwkA4MivmAQACnPkV1MCAPU48isuAYA6IpojPxQgAFQwecsPBQgANdy+7/CWHwoQAPbPrBcFCABFTHEx60UBAkAFYwz2elGAAFDB6JIfChAAaugjhB8KEACeqY8+/0NvuwMFCADP0EfEpbXWdbNDt+Ml7xN+bEICALPmGlo7dI+ra8gvhB/bkgBA5jX7Xn+7UHs+5Z8Y4yT8uEMBAsB6rjPebmlxHYdc12Srl8cUIACs8EnurPsWOL9MuZLwjh9v7N1bktowEEDRaTXgB8bW/lebCqRSmVQywxvbnLMCf95St+QzKUAAuH617tY/ZmxjnMV3gAIEgGes1vWbXXPzpNeKH/NQAWCN7vvDjG3bHa498JtiY9LL3FQAWJHTKyr3vFOxL5HNFRt+rnYwZxUAViBzesArKqXtxksHvYMNPxagAsByjdlFe0quB8Sf7mO1KgAsTpP5uCHrvsQZ8Ze6jyWrALAYY3YPfUelHyIP39zrcKGXNagAMHsXX++4XGmn/Oo/wYP7vKxJBYC5anJ33PJ7rL7E7iD7eC8VAObmOOx9xqi1Hz699JKZEVEs97F+FQDmYszpOOx9kr7Ln6aItjjs471UAHi1zAgJBv+nAAFYjSZ3yg/OoQABWLzjBQ/LdnA+BQjAYik/mIsKAJ8pP1i9CgAnyg/eRgUA5QdvpgLArMpvW0q0w/AB/KYAAZidO5RfX0obU+Zh3MXG+zDwFwUIwHxkRtxQfvtSNhG7HE8ZOUXpP4B/UIAAvN6x/MoNU97oMpv6Szr2g28oQABe5/Tf3uuP+zLH+odDRuvY7wd7d5McJwwEYNRSj8HzA6P7nzZVkM2kUpMmCxvBewdg/ZVaLSBBAQLw/aal/O7/c7uvlGfE2F6NcS12hSFPAQLwfaa4lku9bR7zPpYxb/uLGMrDxBd+XAOAxHJvrvuGmNurl8t+9w9gFxoAZJZ78933anbZD/anAcDWFY/bss67dJ/0gx41AE5siuuG4Wytl997HdIP+tYAOKExhnSl3WtdB73/FIP0gz40AM5kw0W/NfymTE9a84DONADOYE6/6Herj/KMOftOoMddoEcNgCMb0y/63ZfyW478zHvh6BoAhxSRfdGv1lJizh8lVod+0L0GwKHk33W510sZYsx/1U0/OIwGwCFseNflM3/oF1GUHxxQA6BvG37gtt70S+bkl2kvHFcDoE/pK3nrksc1xkT4DaVUR35wfA2AvryMe1NX/YQfoAAB+pT8jUc+/cKoF06rAbBzEaWkQ+1Wv95veUzrcsfnB3BiDYCdSrzAkk+/iGf5MukFFCDAPk3Z33is7m/Sb45rKdXPOwAFCLBTifLL3fWbYzDoBRQgwK7N8WbFI51+ofsABQjQgTHKZUuy1cef6RfmvIACBOjGL/buLidCGAzDaEsZYHSAb/+rNdFE44UJ/kxth3NW8YaHtts+Ht9tn690XkvJznUAFiBAV9b9dnC9XYb3h9xKucq8gAUI0KVteTp2u8te1re/+0af+wALEKBf835g/l3yYvYBHyxAgJ6tYwL4fwFALbP9B7QhAKhkEnWBRgQAdSwJoBEBQBUKMNCOAKCGKQE0IwCowbNtQEMCgAq2BNCOAKCCkgDaEQBUMCeAdgQAX3AUGHhUAcBdreWal+GSANoRANzFVvZ8GxwBBloUAPyluZSchyEBNCwAEHuBkwkAfqO8xt7nBNCRAOAH5jKJvUC3AoBvxt5R7AU6FwCIvcDJBABiL3AyAfDCzr3rxg0DYRj1cGRptasL3/9pE2yaIE3gNWUDnnNKVSo/8AeJsRcopgNg7AWK6QAYe4FiOkBhxl6gpg5QkLEXqK0DFGLsBVCAQBVHKj8ABQjUsOdq7QVQgEAJS85xa/c3ABQg8OOZewEUIFDF7nIvgAIEilhyjsncC6AAgQoyIzzlDKAAgQoOcy+AAgSKWHI19wIoQGCwfJrjL88ve/9WmXEz9wIoQGCMPXONuLX2/6O11qZYs3+l59zrXReA3xQg8FlLZsT22qnafYvsF9vNvQD/UIDAd7+b8pjmfomcB869rooAKEAobcl18Jx6X0fPvW3U/723KSK2yXwMoAChqCXP2zUPp0yj5t5Rv9daxJxHhvYDUIBQVq7P9rvM2V+2DJx7H22LM7P3Y3ZtBEABQmF5Tu9vV2svzb0RrQ0be9fce//Tfh6LAVCAUNexTl8UQ+cH596tPcaNvUvv2g9AAcIv9u4AV00gCsPowAiIit79r7ZVEvrS9BGbzENGz1nFl/yZuUTk5nJOW+mGp+feU8mxN7QfgAIEZrlpUwGlAjDf596u5Nir/QAUIPDF9XZJGzsO3829t3nuLTb2aj+AOwUIfNUfT2lrXV75bLrg2LvIY+NGCIACBO6G8XJOmzuN5efeZezVfgD/pACBWX9IL9COyxmPS1tu7NV+AKsUIPDb9XhOL3DIeSww954eY+81IrQfwHMUIDBWevyia4/z2Kv9AP6PAoSPN1YXSfexN+chvjVoP4BVChA+XEX9tzr2Lu13u98vBmCVAoSPNtWw/y5jr/YD2KkAKtLvupiWsVf7AexcAPUY0x79GXu1H0AlAqjGtK92WsZe7QdQmwCq0aRdWMbe50y50X4AOxNANW6psPJj79/td6jh5QrA5wmgGkOXtrWMvX2eIrQfwNsIoB7DMW3mPI+9EaH9AN5OADWZDuf0s07tYR57tR/A+wqgMv2xSw+vH3u1H0ClAqhQHpv2hWPv4to3F+0HUJ8A6nLNfdNc2m6TsXe9/boEQJ0CqMG0dF+psVf7AXywAPZryPnWHNo2lfH4yFn7AaAAf7F3h7upAkEARp0dK6CAvP/T3nrbkCZGqymrtJ7zBPz8MsPuwgqN2UWUeu9ovJVSthHRZeao/QBe0ASsxOfA7wmhdSil9PEu32k/gBcwAc81ZEb0Zb9ZkdOQsI2IdX0VAAoQfrkhdx+bXgD4jgKE3034AbACE3BO+AHwp01APc0K//EDAAUINYzZxbY4QgvASk3AkiO/p+x6D6WUeJcn7QYAFCBUN+Qu2geP/PaltBG7zOFLgB7bsgEABQgVDf+XvfvNw7yVEnGcq2+Wu9g6ZgKAAoSKmuvlVz37Zo30A0ABQmWZj9r27i9n38mYxyg2vgAoQKhnyGP0lYNr/rcvc1zP3hmAP2kCrhgzovLQ71DKNmKXeX3vvIvW9TIAKECoachoz2ZtdbJP+QHwHQUIdTXZVZv6ldJHdJnNNCk/AO6gAKGWIedztTWmfafsU34ArMIEfLTfZkH7ecl7o8EzcgBcpgBhWWPXLjX3K58XuIxrvlMaABQgL+0Ufwvc1txe3/Fevs+vV34A3EYBwiIyyuHH1TdMd8tT+XnEA4D7KED4sabrD/f/19dHRM6zvjs1meENDwBWY4IXM25v/qtvexZ99xoc7QXgH3v3lts2DEQB1NTIkuVY0ux/tYUTVyiaBkFiNpDNc1bAz4vhPPYooS1x+MDxFvmm+zLf1uZnwAOA/UpozBxD2cSrMWuJKNr8ANi/BO6y7XHW5gfAw0jAZy8AjUnge5O9PnsBeFgJON0GQGMSMN8BQGMS+LjkV2mZ37HrVQ4B2JEE/lfJ76XrSlnL6aR+CMC+JPBmqdbld+z6MkXEWjQNArBLCY0bY6g02PvSXUqJyIypXCwHBGDHEhoV1Xb5HbtTmWLOzCVK78cXgP1LaMscU6XzHedbye9qjnKyGRqAh5HQhHp/vYeuO5Uh5nw1a/YD4AElPLW3ud5zpZLfGpE3o2Y/AB5XwjOKWLe53kolP9EPgOeR8ESWak1+h3PXlzWW/EMM5jwAeA4Jj28LfhV0XSlDjCn6AfDEEu4wx9VartaI/Fnz23THoYbj75Kf6AdAAxK+ZImYyrXg9s/piiV/wLyN9Va62xYxpugHQEsSPjW+pr7+89DV5992+tV7u9u2ZIp+ALQo4QNLDKX0X4hc5yGv9hz8tpKf6AdA0xLe577LN/aoXCJrili3+FnxbpvoBwASIDdjRLlnf95lGmu9Y8ufFbz8cbftnVH0A/jF3t3mqA1DUQDFdkIgHyT7X20lOnVRMyjD1AMhOWcBiJ9X7/nlslsTe3YpcVPRVsdmDf/jRvxs5OeTzgAgAe5Zuad1XT8O03c1s01v0d62uUH0AwAJcG9y8Cujrc7Dd/9GyB+TKdnbdteQxhDrAwAgAe7FkI45+BXSxfDo5nfIL/yeMvLLaTOcRD8AkAB3o0kpv60rKZ7Ol4dy32zRW7i37f6Bb3sAACTAXbikMYefV4S/nPtKp8921tt278rDgS8ASID70Mw6cktqYzgOS9Ezz/uK6pZHfvmpnysPAJAAd2E28iutrkJqFgrj4uytXdneNk/9AEACJL/yO/ykLoY7W9+0UBhXtrfNB50BQALctyGdnzHyavuQhtnjvnMI/Sz2Fe1tuwZO+14A+JcEuE+XNIb+KcetXf9369tch32nhSVvmd62ZSmFk/teAJAAty+l8OTb1iqEa+j72axV5962ZYP7XgCQAHchpbDFVWd7O/Jz5AEAXyQBbluTjpsMPbPetqWP+jnyAIAbEuA2bTT5zXrblief+nsBYE4C3JaNJr8Yw5+Rn5d+ALA+E8+WI0+1ueQ3621blLz0A4AvkADf2/WF2+a+aJJ72x77uM32Dl0A4E1MfM62d1n9e+QnAQPAu5m4tc7kV8cYwjieqmoV0elvb5t1LwC8p4mr9SW/LsZTCCldpiaNYQ1jszpWH71t1r0A8N4mCij3PZMY+xCOKX382DFUL09PH71tzfSQwSf9AGC9Jl7f4dFet7y3IWtI48sbcHNv26PDT+W9ALB2E8uWt5zdf4z7ZpcSfX14mTzymx61xVMX4Bd7d7TbKAxEAVTgJCSkUP7/a1diibPbpK0JiFj4nA/ggaermfEMsFcDCVbbYRyn+7onCeq9XdP6b8nPAWMAKMDABgv92ro+T23eR314Y9s03m2T/ACgIAMLHvcmjPddHtJVHsN+dX2+3W3zuBcASjOwXq0r9nlj7vs2+s17L7z23bbw2sCj070AsBMD6ySeuj5Mfd4so1+82zZbkPwAYHcGni6ya2c0ek/jluQso197u9tmzg8AkACXDfq1yUuS+7ccQ7tOJT/JDwCQAJ/qY+RZ8zrGgsXIW95tixr7/ACgHEO5uuQBtzYGq9X2BK5f8guhebHpLfkBQGmG8jTj7uU2seb3mboxJWzc74132/qXH7ocXG8DgDINBQmhSnyOcRxrarnux5ua0Qt+gqe9AFC2oQAxoyVlq0toMt2Pd5zuthnzAwAkwBUOuLXjhYykj25cRIsPj1/ebaPZCwCUkABnjOQd68R9eWGcm/v1ozncbYsVSskPANh/Apwzkpf4yKMfn8omfHPVkt90t83tNgBgJAEubXVOo34JFb8Ng1+829ZJfgDAAwnwX/F5wzrRrwuhqj6SPpjD3bYp/Ep+AEARCTCe8UhxrQ8/R78wdno3jlHT3bYFSw298AAAikiAsdeZXPU7he+/dUou+GVxt+2+z89WFwCggAQYB/2WNHxjozd1wi+Tu23avQBAUQmwi6fLZkW/XHLfkpJf1Cv6AQCFJMBwibFnRvTLJPfd7rY1wxIm/QCAyf4TYBOqj3ZOle3LXr8wd74vk7tt/0c//V4A4G7fCbC7HNr0NSrne/RrQvisqvpNJbPr7W6b6Ad/2LvbHDdhKAqgYDN8TAJ4/6utmIyQUqktVEywyTk78L+r+2w/AEqQ8tE2n1ujXx2nte7bPebNZG/bb3f9etEPAPiTqybA4b7tY782fse+/YEpk71tT7o4eOYBAPzLJRPg2N825L8+7I59mVZ+izGa+AIAG10wAU73KmufoVkrv8OyXwUAsNnlEmDXVJla97bJfgDAtaSTtdkNQh+V35SOFQdPPQCAXKRzDVUu1r1th4tz460HAJCTtM/1AuDT3jbhDwB4B+lM463arpTK71vX9q78AQB5Smfqq9da97ZN6UeNQ2OfLwCQr3SmUP1FlnvbNmh76Q8AyFvao7wE+Kj8xvQi45z534YAAGcnwLl6kuXetu2m3qsPAKAI6URdWy1y3du27yiN3/4AgFKkE4xxqJvwUXjlt+pa5R8AUJT0QjHO9T3cjt7bNqbTdHH26hcAKE76eV2MdR1C7nvbdme/Ogh/AECR0h77h719+Di68uvSWdZT3f31DACULB1v+hr2fpaxt23HqYa60foBAFeQjvMY9t6K2du2xbQcqgk6PwDgSo4Yi7bLsPdClV9cYl8fFH4AwEVlMxbdubft+K5vWFKftg8AeAP/N+y9h1uhe9tW41fTVzdCHwDwdtJWXWzrOoRyK78l8s31cobgA2d+sXdvSY0DMRRAkeQQO4kf+1/thFTwDCkGCPw4zTmruKWW+gLAr/bVAo/H623bV72E1uhFPgCANz750+X4OL1tdZ3xdemGAwDgI/8p8Nj8yG+sszjrjPgAAO5zU+Cx0d626W/ec7kBAPBDVXN0edhSb9v4+p4bab4HALBR3x/57et6rxF92t8DANi8O3vbxjob4qw33AMAeCyf97aN62gv8sXxCQCAB/T+yK/Wrb1e2AMAaMS1t238N+tZ2gMAaNjBxh4AAAAAAAAA98vsTh6cAQDat8suouYheqclAAAtO+Qp4rmmZSrRDwCgYYfMiKFqWRbRDwCgYcfMPubLV9KiHwBAy/Ky4Ffj8qqeoxP9AACas7ss+K25b41+2uIAANpyXfCbljf2NcQpnwAAaMZlwW+oWm6NNUeKfgAAzcjs1sOOG+XKAwCgHbv1sOMdU83ee/+0cwcpCANBEEXpKcEImnj/04ouAsLkAOm8d4q/6GoAgB72z81z+ZafkQcAwPntn5sPrHnU4tAPAKCJUXnP3ZOtbsIPAKCTWf2tSek+AICO/uovSdUyzHoBALr61d8r2aqew7ADAKA/1QcAcCEfg7ywnVjEoXMAAAAASUVORK5CYII="/>
                    </div>
                    <div class="volt-v0O2t mobileTopVolt-3R9X8 top-1myq9"><img class="voltImage-UYrS0"
                                                                               src="../static/volt-top-03-ebd351ee575baa2ee90a0a9c469a8277.png"/>
                    </div>
                    <div class="content-wrapper light contentWrapper-1SCaE">
                        <div class="titleCopyWrapper-3UxOk">
                            <div class="titleCopyWrapper-1zptg copyWrapper-Vs74R"><span
                                    class="leftLine-1Z4OO dark-bP-w5"></span>
                                <div class="detailLine-1Lbvr undefined left-3ehf0">
                                    <div class="dashDetailWrapper-ppq1R dark-2kOE_ "><span
                                            class="borderLine-3aHhe"></span><span class="dashDetail-33_7u"></span></div>
                                    <div>
                                        <h2 class="heading-01 sectionTitle-2fIox title-U1XU8 dark-1RI-y">
                                            <div>CHOOSE</div>
                                            <div>YOUR CHAMP</div>
                                        </h2>
                                    </div>
                                </div>
                                <p class="copy-01 description-eFk_k">Dunk on the competition with a giant sword, freeze
                                    enemies in their tracks with a cross-the-map ice arrow, or lure opponents to their
                                    doom with graceful charm. Whatever your style, there’s a champ for you.</p></div>
                            <div class="ctasWrapper-2gHih">
                                <div class="button findChampButton-33_qx
    "><a href="https://findyourchampion.wildrift.leagueoflegends.com/en-us/" class="dark-3gXr0  primaryButton-2ec0w"
         target="_blank" rel="noopener noreferrer"><span class="animatedBorder-1iWpQ top-cn3E4"></span><span
                                            class="background-u8Ka7"></span>
                                        <div class="label-01 label-3gc3A "><span>FIND YOUR CHAMP</span></div>
                                        <span class="animatedBorder-1iWpQ bottom-B_Gla"></span></a></div>
                                <a class="button  linkButton-1Ll1h white-3RHqi " href="/">
                                    <div class="label-01 undefined"><span>VIEW ALL CHAMPIONS</span></div>
                                    <div class="icon icon-1djbk icon-2tHD8 currentColor-LyOgN">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24.72 42.18">
                                            <path data-name="2" d="M3.63 1.49l19.6 19.6-19.6 19.6" fill="none"
                                                  stroke="#fff" stroke-width="5"></path>
                                        </svg>
                                    </div>
                                </a></div>
                        </div>
                        <ul class="championWrapper-6JBVW">
                            <li class="championImage-1iMem"><img
                                    src="https://images.contentstack.io/v3/assets/blt370612131b6e0756/blt804eabffbf15dc51/5f4defe95acde4265bb2da77/Champion_garen_HP.png"
                                    alt="Champion_garen_HP.png"/></li>
                            <li class="championImage-1iMem"><img
                                    src="https://images.contentstack.io/v3/assets/blt370612131b6e0756/blt3efee145a07dc9a7/5f4defe82931d5251031459e/Champion_ashe_HP.png"
                                    alt="Champion_ashe_HP.png"/></li>
                            <li class="championImage-1iMem"><img
                                    src="https://images.contentstack.io/v3/assets/blt370612131b6e0756/blt49d455537d60c8ea/5f57beb458d02047f3edc787/Champion_Ahri_HP.png"
                                    alt="Champion_Ahri_HP.png"/></li>
                        </ul>
                    </div>
                    <div class="volt-v0O2t bottomVolt-2lxc1 bottom-b1FgS"><img class="voltImage-UYrS0"
                                                                               src="../static/volt-bottom-03-cf0abe4ea8d9841ce571f6379349bac9.png"/>
                    </div>
                    <div class="volt-v0O2t mobileBottomVolt-3pvK7 bottom-b1FgS"><img class="voltImage-UYrS0"
                                                                                     src="../static/volt-bottom-04-7092c5e75e861b248d53c98ac37a6e31.png"/>
                    </div>
                </section>
                <section class="container gameVideoData-2MiX3">
                    <div class="content-wrapper light contentWrapper-2GeoW">
                        <div class="copyWrapper-3tTRW">
                            <div class="detailLine-1Lbvr detailLine-1Pn0w top-1jJmO">
                                <div class="dashDetailWrapper-ppq1R dark-2kOE_ "><span
                                        class="borderLine-3aHhe"></span><span class="dashDetail-33_7u"></span></div>
                                <div>
                                    <h2 class="heading-01 sectionTitle-2fIox heading-08&quot; title-3a44c dark-1RI-y">
                                        <div>COMPETE</div>
                                        <div>WITH FRIENDS</div>
                                    </h2>
                                </div>
                            </div>
                            <p class="copy-01 description-1LzKr">Queue up with your full squad, or see how high you can
                                climb the ranked ladder solo. Every game is a chance to land the perfect skill shot,
                                turn the tides in a crazy teamfight, or pull off that sweet, sweet pentakill.</p></div>
                        <div class="videoWrapper-3u_lJ">
                            <div class="videoTestWrapper-3_xxs">
                                <div class="gameVideo-2DQCg video-CB3lJ">
                                    <video preload="auto" muted="" loop="" playsinline="">
                                        <source
                                            src="https://assets.contentstack.io/v3/assets/blt370612131b6e0756/blt610efefd79fc386c/5f4defead2205827bd4f7557/FunCompetition_01.mp4"
                                            type="video/mp4"/>
                                    </video>
                                    <div class="poster-3GL6m"
                                         style="pointer-events:none;background-image:url(./&quot%3Bhttps:/images.contentstack.io/v3/assets/blt370612131b6e0756/blt5c7e20ba1bcc0a56/5f4df068b4ebcc25c4c57a4b/FunCompetition_Poster.jpg&quot%3B.html)"></div>
                                </div>
                            </div>
                            <svg class="whiteDiamond-IhRVM" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 93.21 93.21">
                                <g data-name="Layer 2">
                                    <g data-name="Layer 1">
                                        <path class="cls2-3nB18"
                                              d="M46.6.004l46.605 46.605L46.6 93.215-.005 46.609z"></path>
                                    </g>
                                </g>
                            </svg>
                            <svg class="borderDiamond-2ipXY" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 60.33 60.33">
                                <g data-name="Layer 2">
                                    <g data-name="Layer 1">
                                        <path class="cls1-fb8tv"
                                              d="M30.17.002l30.165 30.165L30.17 60.332.005 30.167z"></path>
                                        <path class="cls2-3nB18"
                                              d="M30.17 22.233l7.934 7.934L30.17 38.1l-7.934-7.934z"></path>
                                    </g>
                                </g>
                            </svg>
                            <svg class="borderOuterCircle-3en-3" xmlns="http://www.w3.org/2000/svg"
                                 viewBox="0 0 586.21 586.21">
                                <defs>
                                    <mask id="mask">
                                        <circle cx="295.11" cy="295.11" r="290.11" stroke="#fff" stroke-width="10"
                                                fill="none" stroke-miterlimit="10"></circle>
                                    </mask>
                                </defs>
                                <g id="Layer_2" data-name="Layer 2">
                                    <g id="Layer_1-2" data-name="Layer 1">
                                        <path id="maskReveal" mask="url(#mask)"
                                              class="cls1-fb8tv borderCircleDetails-1sftD"
                                              d="M86.11 499.08a292 292 0 01-2-409.95h4.24a289 289 0 00.36 408.37l1.62 1.61zM495.84 499.09l1.63-1.62a289 289 0 00.37-408.36h4.23a292 292 0 01-2 410z"></path>
                                        <circle cx="293.11" cy="293.11" r="287.52" stroke="#32c8ff" stroke-width="3"
                                                fill="none" stroke-miterlimit="10"></circle>
                                    </g>
                                </g>
                            </svg>
                        </div>
                    </div>
                </section>
                <section class="container imageSlider-LOpcF">
                    <div class="volt-v0O2t topVolt-126E0 top-1myq9"><img class="voltImage-UYrS0"
                                                                         src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAACgEAAAKLCAMAAADi0EqoAAAAM1BMVEUAAAD///////////////////////////////////////////////////////////////+3leKCAAAAEHRSTlMAQIDAEPBwkGCgUDCwIODQNDy4KAAAJJxJREFUeNrs1jENwDAABLFv90jhjzZAzkbhnQ8AgJBt/wUAIMQAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAoMYAAQBqDBAAeOzWgQAAAACAIH/rQS6KuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQAuDFAAIAbAwQgdutAAAAAAECQv/UgF0XAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIADAjQECANwYIBC7dSAAAAAAIMjfepCLIgBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAAQBuDBAA4MYAid06EAAAAAAQ5G89yEURAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAANwYIAHBjgAAAN7FbBwIAAAAAgvytB7koMkAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBAC4MUAAgBsDBABitw4EAAAAAAT5Ww9yUcSNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggAMCNAQIA3BggALFbBwIAAAAAgvytB7koAm4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgABAG4MEADgxgCB2K0DAQAAAABB/taDXBQBcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3BggAcGOAAAA3Bkjs1oEAAAAAgCB/60EuigCAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALgxQACAGwMEALiJ3ToQAAAAABDkbz3IRZEBAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAwI0BAgDcGCAAEHv3kqIwFEVR9JjUz9Jo5j9a4SnYsGGECL7ctUaxe5tqFCAAQDUKEACgGgUIAFBNkmnYj98zAABF5OZr9/OrAwEAKkijAwEA6kijAwEA6sjdQwf+DeNpBgBgY/KMDgQA2Jg0OhAAoI5cLe7A43kGAKBred3hXwcCAHQsjQ4EAKgjjQ4EAKgja5h2w36cAQDoQhodCABQRxodCABQR95jMpYDAPhUWYvBMABAJ/JABwIAbFve796BxnIAAB8gazEYBgDoRJbQgQAAG5LldCAAF/buJbdBGAACqD+QkIAb7n/aNqISkVDooqAg/N4F2I7w2AOcQvg4w3IAAKtOmADlQACAVadNgHIgAMAbJ0+Ak96wHADArIoEaFgOAOBFRQlw0rgwDABQWQJUEAQAqDQBToqCIABAZQlw0igIAgBUlgBfFkUcDAMA1JQA54Kgg2EAgJoS4FwQdDAMAFBTApwLgl6OAQCoKQH+ip2XYwAAjpUA+/QjxtiHPfV3L8cAABwlATaXoR0nbc5DSl2MTdhHicnLMQAAH0+AT+WeXoPZFAZvKcVYwvaaeDEtBwBwhB5gE9PtMS49s+Blh/+C0bQcAMAhboKsTP62OafUbVwY7E3LAQCVCwcx5cC3Hlv/Fiym5QCAaoVD+Xvh4ysPKd1j2eoculMQBACqEw5osfCx9wlxryAIANQkbKwMi1y298JHztdNPlkUBAGAOoTN9d01j2Obh9TFsv/Cx7IsGP6lURCEb/buKLdtGAgCKJdUFEeK3PvftkhdlIATGAVMWxT93gH0PVjtcgAYXaqax8BLKstzbK0bPh4fBeOUix/DAMCY0gNty78zi3N5z7E+s+Gj3o1ETBYEAQCq9GhTzHV6d64TwWc2fNQoaEEQAODxCbAO0y6hrf4a3qXh41xKzktsKoYBgNeVnunqyOLPsci0U8PH2yUKrg5FAICXk57h9j/V8pGX2Knho74mE3IgAPAq0i6ubyzqrcjjGz5aVM/VHGg/EAA4nrSrb5HtsiI47drwUavn3IkAAENK+/vpl2r5zEt00PBRynueI/4vdioYBgCOIfVi+p7YLn+Gty4aPv6OBdd0k9/CAMARpL5s8cMf3F8ln2LqpeHja1swR2y3HsJ2JQIA9Cz16MdNvrevm+H13s+2HNGVG3PBKcRAAKBXqV/r9ZlIrRWZ7lwQbB3OzqW85xzXYXA7fdgNBAD6k3oXNQc2ekSwLgg+Ip69lVJyzkvEJadus1EgANCZdAz1oOP6VGSOrZMFQQCAg0gHMtUc2PbtmHXxpB8A8ELS4dQmjsYDwS2yqjcA4BWkg6oPPjceCE4x5+J+AwAYWTq0mgNbDwTjlD8tCAIAY0oDqDmw9UBw1fkLAAwoDaM+I916ILjFbEEQABhIGkzNga0HglOcLAgCAENIQ1qXGtYal4rEYkEQADi4NLCbQ7v7WoY3C4K/2buD3AZhIAqghhCVhtL0/qet6GaiSnEolhNT3jsA6xH/zxgA2K/073VjpsT3eerH7T8EBwVBAGCP0jHkZ7Xz6b0v2BRREAQA9iUdyZDPbq8f/dgV7aAIhgGAPUjHs3T4Mrsc5zgds/HjgmEAoG3pqOJszP3TMVNJ6CwYBgBalY5tigpf5nRMSUHQ5RgAoDmJWBeusykyXVyOAQCakrhNbr8WVTZF5q53OQYAaENi/anneGR42D5muhwDAPyRCbBE+ZpIWILhaXvs7Gk5AGA1E+CTxJpIvWB48rQcALCGCfCpYk2kXjA8e1oOAMgzAT5Z9Pfid12NYHjwtBwAcI8J8JXmLup7dYLh7qIgCAD8ZgJ8vWVNJP7W1TglPSsIAgA3TICt6MY+2ntVTkkPCoIAwA8TYFNur0jXemNYQRAAMAG2ZwltV5T3rqd+7EqiZ8EwAGACbEvUA7POp7eygqBgGAAwAbYl6oF5pQVBwTAAYAJsStQDH7iWFgRdjgEATIAtiXrg48sxYzdsDp9djgEATIBtiXrgmoLgvH3adDkGADABfrN3B7kJw1AQQEPSQJpQuP9tq4rFZ0GF408kQ967AGI3iseepkQ9cNtpuaOXYwAACbAlUQ8sKgge6uOmaTkAQAJsSVk9MD8tt5iWAwAkwKZEPbCkINjXFwRNywEAEmBToh5YVBCcqwuCpuUAAAmwJVEPLCsITvUFwVlBEACQANsR9cACl9S0nIIgAOxdR0uiHlhgTE3LKQgCwH51tCfqgSWGzLScgiAA7FFHo1Z+pLv+TctlPj06GAaA/eho2srS3pgtCDoYBoA96HgDy8pLvNmCoINhAPhsHW9jWdnZu2YLgl6OAYAP1fFmoh5YPC031U/LzVu/HHMZhkEFEQCekAC5HdVGMistCCam5fqNXo453yXbw6HvT9IgADwiAVL1inQUBJf61Pn6l2P6r0d/a+pPjqABIEiAZK6JREHwkCgIxs/ljf3P/5FzlgQB4I8ESPqaSBQEj/UFwRdOy53nZ586J88VAoAESHpN5L4g2CcKgq+ZlhtPS8mfm71XCAASIOk1kfuC4FxfEJzyBcHLLQQKggDwgATIFteFoyA41RcEo4+4bQiMIOhoGAAkQDKrcuGSnparPw7+nlfmXC9XA4AESPLZmDCmpuUSBcHz9NXQi4UA8JE6dmOpOaYdMtNytQXB63GpPIh2aRiAX/buNsdNGIoCqD8ICZiE2f9qq6pR6UiZCWCiMuGcBUTh35V93zMSIBvlwDFXFATLmlx2O/dhd0+YAMC7CBzSqhx4qi4InhbeB19DCDtZXQ0A7yRwYFMOXKK2ILjkYng8x/DHDlZXA8DbCBzeuhw41hYEx9nzwZdmg9U4LoYBQAKkpq03udUVBOeez42phErTZAoAEKBmnd9UELy+dnDj1PVN2EQxMQzA4QWon6adCoJl/T3t8/O53F43/EaPywFwXAFqQtnjgmCsGuBdtCWmviDoYhiA4wnwvbi+INi8ZID3NFyasC1vywFwMAFmHwiu2SCYKgqC0wDvjPvgeqW3OQaAgwiwLJWlnFcUBPvVBcGvBnjHvyFwFyVIAPhJAizXxD51+ba0ILj15pixbcKLNLG1OQaA9xVgvWvs0znnhQXBNjYVB4KfS4ldDE95Wg4AJEBeoMQ+pWFuUXCI9UeQ53y/qr2EGfbwtNwt37ljBuC/C7CpJsY+/U6DOY+PQlBq46bZrIQKFZMps41dusTy6K/HS0pdNnsCwHMSID9PiTFew5uYJlNmGObF3RLbNDgZBOBrEiDsQeyfFgTHxWsLo3U0ADwmAcJ+fPOM8hgrxpCtowHgMwkQ9uZRZDvX/2jqTCEDcCcBwk79WxBswzbK5exaGICPDwkQdq30acg5bKm0TgMBkADheJqYHAYCIAHC8ZR2MCECgAQI20i3nIfUxhJ2r7TDB/CLvTvLcRsGoihagyROks39rzZAZ2CAtjtumxqs3LMCfT7UY5UAkACBDpaYPrY4Rk8WdZBD08C7QAAACRDoYlD707L6fOwguERGgQAAEiDQyZLNx1rfIQjmxKtAAAAJEOgbAxtPllUOKAdCIACABAj0LYUv9S+jB9NFDoZJIACABAj0Neinc8xXP1oznHkTCAAgAQK96Y2fs3myfJjrMcN0qQAAkACB3kp7HNhc3KZjDAQ11efEQWRQ1Wg2Oz8eAYD/hAB43KCWrrUeciA42Fif4qbSLKpmyTk5CACnJgCebIWPNxCMl/okt89frjrZ7PTLAHBGAuApS7b5VjoaPZkW2Un2+rRriuX24DNboCAGgFMRAC/QKXn95fPKsOxAvb7CQ17kDlUzkiAAnIIA6LEk0saBu98Q1Et9zej2EQNJggBwWgKg2+lAr7f5bFllO3Gsr/MQi3xJ1QIrIwDwlgTAGjdj9t0UGULtw8Ok/86+2RIbIwDwVgRA0/tmzL1NkUXWVq61m+tsjyTXopPNlMMA8BYEwDpKDD7eT1WrF8NWuxrdYpFHaLbASBAAjk0ArGjRdjNm62JYx9qdp4e/t+jEvggAHJUAWJ1OwWuzVTE8eF3FxS2XxyvxaMnHCgA4EgGwhbYksmkxXLIlr6vw9Pt1IN0wALwdAbChQW2+1mabYnjRKbT0ucPrwKaoGRdkAGB3AqCDnq1w/1PSrY9dZwrnIZZvZ1KCIAB8AwkQOIkSv2qF2ynpIn1p/jWG3D0GfoRSDsgAwCNIgMCZ3NkVXv8fw0V/PhA8QAwkCP5g745y3YSBKIAOGEICIdn/bluhqn5SK9XJw5UD52wg+bwyd2YA/k0ChMMZusvtWVC5m3YvCA5VCoLP26WLEAQBGhZAG7rrVJJ+amyO2QqCjcRAQRDgLyRAOLQ/Vsb8380x92crMVAQBPhKAoTjKykHbh6pv3bD7j9+S2MrMXALgqaGASRAOIlcDiwqCK67//qUHs3EQOtjACRAOJG8ObCoILjEvrb3t3ZiYMTauSwCIAHCKeTNgUUFwWuNgmBqKAZGLLNbwwASIJzAOhfM69Y9LddPaWwoBkaYFQGQAOEEhoIZkdqn5S59erQUA2NQEQSQAOH48oxIaUFwrlAQvD9bioHblZPJl2EACRAOrWCBdM3TcrmO11QM9GUYQAKEw8szIoUeaapQELxMadwlBi47/iczwwASIBxYnhEpNVY4LTfsc1ouTdcldtPN/c2DIIAECEeVZ0TKpXuFguDc359txcBtvbVJEQAJEI4qz4iUe9QoCHb9LbUVA2PxHgggAcJx5RmRF4wVCoLDtwuCY+rnNaLVrYYAnyuAI1rm/p2gU6EgGFtBsKkYuK0QNCgCnFoAh7XOL5YD82m5efcPw98rCD7u/a8nytZKiwAfKYBjG7occ14uCFY4LXdL34qB8UtbpUWATxPAGXSXnHJeLwiuEdFKQfDr9ugGtxoCfIYATmO55pDzRkFwiZ/a2CD4/D0o3M5/AvgkAZzLOvfvv7/df22OaaKMl6Z5jayd0iJA+wI4oVwOfK8geOmGNsp4Y+rnIb5oprQI0LQATusHe3e0oygQRAG0EFCQBf//b3ed3Uwnxow2S0s7c84XGJ9uqi7VqRy4riD4cTmmgjLenS9EKiktAtQqgJ8tlQPXFgSn+GP3Mt5lPE5RzvU3WQwD30cApMuBqyynKZI9C4LtaZijiFQQtBgGvoMA+Nx3jpeqQuCq53z79vnj0esLghbDwHsLgE22wpfjHE8q/5zvknM1cH1B0OUY4F0FwFZb4XboopD0nG89O+EPzeByDPCGAmDDb4XHc2Qon7n69KBwSdPZ5RjgrQTAg2yTt+vsT3NkKZ+5LuMwRUlpWe1yDPAeAuChLm8ceMnfBpfPXPm3o9f/WZ6WA2oXANuPA/tximzlP8pY0tHA4hpPywEVC4As17T13CAwXiddba5oGHg1eVoOqFIA5JufeVi4P3SxSvmrzUtqBr7C7Gk5oDIBUGwt3I9zZCt/tTl9Jhyv1DWDgiBQiQD4P1/nmg3mgCULgpfTuYvXmhQEgf0FQNEc2J9jAyULgss4zPFqs4IgkEECBGp290rf/hHw8eyt/3Wc4vU6BUHgKRIg8Aaa8+HQtn01Q8Db2VtVKfDfkNJiGPiCBAiw1eytqhT4d0hpMQzcJQEClCovphQ4xw7SkNJiGLghAQIkxQqCy3juYhdpSOlyDPBJAgRIihYE2+McO2sGl2OAKwkQ4EbBguBy2j0EfgwpXY4BJECAbOtf72ibqMPcHFyOASRAgMKa68e5fRs16Zqjp+UACRB+s3eHuQnDMBhA44SV0pKy+5922iZRaZoqBAEKfe8CVf99sh0btiiclgMkQIBNGp2WAyRAgE2qTssBEiDAmnQ/M3tTKSXnHDGme9lFb0AQkAAB1qH2+/I5+06D+9xF7NI9jAYEAQkQYB3G4Z8C3cfdsmA1IAhIgACrMK+b/icL5txHNP6eAUFAAgRYheX63Kkcc25bFow+F41hQAIEeLql+txcFhxibDkgqDEMSIAAT3fRhY9S9rmPaFaA1BgGJECA69WIRn3aix7wnsohdxFtCpA2x8C2JQCutOsPp59cNsTu7hc+2g8LRm9zDGxWAuAW0R1Ov7Esd1Efe+FjahAFx8HmGNigBMDN4rz2eSr7HOOjL3xMv+9G6i0DgjbHwJYkANqoQz5Ony2DYCxc+Fh8QlwNCAISIMDjfIeouTY3t4YfeeFjXjI9pis4MQzvLwHQXI1z8e48I7h7woWPeZvM6KEIIAECPMD4p5ZWjrmPNGtfn2u/TWYc5EB4RwmAZY1rdx/lkIfxThc+mm+Tmb+nLwzvJAFwgeazfFPZd1GfeeFjPj1nPhA2JwHQwLUt1dN3Z3j39Asfpewv7RBHZ28MvL4EwMXajwfOneEYbx4QbDCwN5XjJWXBGsYD4aUlAJ4h+nMp7c/ymJVc+CjlkHNEXf4H50TgNSUAGmi54qUc8xAppbVc+FjOgnXIusLwchJf7N1Lkp0wDAVQ2fA+DU2S/a82mWmQbia4q7B9zgIohipZugK4pv2aSJ4VOa6Vlw03N3KL+P3FGvHxfnkUhp4EALeQdeD/qyKR7pLsvCxLrbWU/Ld1e9kUhl4EADeS68JtV0X2rC/beyz/vGqtH3+APgQAt/PdJY7fy7XsmEOUC6ACBLi1fcs5vpYNwVW0M0wvALi1LNcaNwSLk78wrwCgA1kHNmsI5gKKUD+YTgDQjYyRbtwQXA0IwlwCgL5ktda8IWhAEGYRAPQo68CGDcHcRPYwDGMLAPq1nkzxPa4dFTlK/fAwDKMKAHp3ZB3Y7qhItho9DMN4AoAxHGfpLo/l9SzHpQFByTEwkABgJPtpyt/yWbdy4duSY2AQAcB4zpd6fy31WdYLb86SY6B3AcCoyruelWqXomPW8pQcA/0KAIaWsTEn0TEXikyn5aBHAcAEMjbmJ6JjdqfloDMBwDSOkh279psih9Ny0I0AYDL7Vj9zgq/1psjqtBx0IACY0kmh1uLIcNkMCMKNBQDzOl8TyZsiBgRhMAHA7HJN5ORh+HXhYdiAINxNAMDJmki7CEEDgvCXvTtIbRiIoQDqiY1Lkzrp/U9b6EaFkngyqkE0713A24/1R6pjAoBwOUdM++PBcLxDMRiGJ0mAACTlr4nkB8NbO70bDEM3CRCA40V97zPcfzG8Jb5gMAwdJEAAxiT+1kU98MFg+JwqCNocA49IgAAkHLFFOlznVEHQ5hi4RwIEIOHQLdJxY3ht2/gvR5tj4BcJEICEY7dI5zfHxDdsjoEfJEAAKthii/RuQbCN/3N0Wg6+SYAA1HGJ67/7BcFFQRDKmADg4HpgFARP4wVBp+VAAgSgmP7jv/P7+OaYxWk5kAABKKarHpg/LdfOCoIgAQJQStQDOwqC63hBsCkIggQIQClxXPjY03IKgiABAlBKHBfeN2dOyykIggQIQClxXHjfLXla7sNgGCRAAMqI48L7rvPb2pZE4DQYBgkQgDKiHtghWxA0GAYJEIAqnnu+ccsWBG2OAQkQgCJie2CPq4IgSIAA/BOd2wPjtNyaOC13sjkGJEAAqohtLt0Fwcv4CNrmGF7dBABVRD2wvyDYxjOn03K8rgkAalmiHthfEFyGZ9BOy32xd2e5scJAFEAZDDTz/nf7pJcPkvQQcNFRJM7ZAOLvyr7l4ooKAPiL5q0euLcgWOUXBK2W42IKAPi7xuFgZa/u81+OSe9cLTcN5X9DVVVT7diRgyRAAC5ne0V6p3UKvBxTDu8oCPbp7tyxrKrJySP7SIAAXNM2JnKgINjmFwTLkwuCTZWeXUHfqt5wMi9JgABcWjoczJrwarnmrAzYj68zZ+vZap6QAAGgmMvDF7V1ZLXcaQXBtZ1/Dp1D1bsc5isJEACObhPZrNHVcvGCYH1LxR6CIJ9IgADwxXj8fG6pu/yC4FyGk9n0cRIoCLKbBAgAJ93ThguCS+g6+OD/6QgiAQLAIzlzG2u0IJh7Mbx0hz87W2mMBAgAp83vLtGCYJ03HTwkq+zYQQIEgLflwKbu2sBquWr7YqAUuFdyIIgECACBHHhfEByL38tla1cWHxwI8pgECACZOTCjIFgGCoLTeug++JZiLyS2nZFhJEAAiObArSCYfuV8bq3GYhOYTAEJEADiObCpu6qc338+t3waDXExzEYCBICA2HN+dZ//ckza+2Rh3Y5nrc5zMYwECADn7Ptdp8DLMeWe1XJLNxR3TAwjAQLAnVgoyygItvkFwfLHe9rpNp+bdScFQSRAALgPZRkFwfBquebAaEj8HxUEkQAB4LuUd1hWR1fLLc9HQ4pHvByDBAgA54ekjGdV1uhquTVjdVwg6yoIXlgBALw6EaybjNVy6dwB3mko3qQcFAQvqQCAf+zdWVbiQBgG0BoCqQwF7H+1LZzTrVFBILGJcu8Cktfv/FPxhSFub06CcwYEwyl5vqsEDuH7VAOCzyYAAFeqsU8p39I7zcPMEmT3+rfchO/VGBB8HgEAuFk8ZsE253y5Hdw3ywTPmI6a8F/MGBDc5zalPr5VUkpd9lbd2gQAYJ54lN4qMQ7hZxtuHBDct19tQw+nCqpm8zoEAIAz6nVXEsfS3LZ90uk230YCBABmWP4Z5U2q3in5cQIAwDVi+exIYq4zu83OEl5BAgQAHmnoJ5dquoWKjJ1y4CUSIADweH9PV+ewHDHwPAkQAFiLJtbFv5gsiXxCAgQAfrlh2xkNnJIAAYD7pZRirGH9aunUAl9JgADA/ZpymrXb5S71sQnrNmzHAycSIACw2KxdHlNZdxDsWw3hFxIgALCA2rfHmy4/IQjWrRVhCRAAWMpQjiddpkEwrFFT9IMlQABgObWfHmDZ5C71Q1gbIVACBAAWj4HjdOBul9N2ZVvDjXawBAgALKyJ6cOrHHlM/Yo6w7V1I0YCBAAWF0ubD+9scpvWUhAs+XCv3Kb8QiVRAgQAzgwH7g7v7dexKnJ3IXDXDv/KnbFPaXzeMBgAAC51hVdYEGzK7nCfXVdqmBiOWTA/22vEAQDgclf403i0f+yEYLm/fLcZtzF8VGOf2pwPTyEAAHylxtdd4Yldbh+1MhxnhbXcluHch+M2jfl3v0USAP6wd7fJbcJQFEAlvgLG2N7/ajtu2o6VZlJcCQzknA0Y/7sjvXcFME/sHpZEEq/pEIyZKa2tq9P0Veztq+GgSTAAAGRUByYHgmu/LpfMA+bFwO+VBAMAQMZx4Gs3RZrqlip/KZwmwYOsjAQAgP8zJp0xL9oUmepbKfXQxTl/+z4nuPONkQAAkKGJ3ZDEob8fl2vCorr2VtDlPPcE8747vNdGwQAAkG3sq7r9YtZuWPJieCoew9q6Sm6F/3U5fN7b5XAAACjXGZOEsdUuhofbEq5D9cT5ZYw7OhIMAACh/JLIuhfDsUt6qwsfB8Yw37SLKcEAAFDceEpuhVfaGI59tVBry/X87NfG05YXhwMAwDKa5Fb4U/W5fJX0eEp+tqD67enTyzFWbxvsEgwAABkyboXTKulQ1hQXOoNr7znw+a/pqi0NCQYAgAzZu8JptJpCUU3sMo7giofWJvbbGBIMAABLSneFLzN6mas4hrJiv8wJ3OXnvfAOg2AAAFhc2iB9nbN6Uf5ieDwtk7vaJAfO18zfFpEAAYD9i30yHLhmc8wUq/fmmI3kwFlrwxIgAHAQSWXMys0xTeyWaY55f/5k60EwAABkWHg4MM1Wp/IDgpnNMeW/9dMZQQkQADicJlbDzCB2rRcZEByWCF3XP22HW1sWCQAA23BvbslYws01PT4t9/LimHfNry1mCRAAOLKxnx/D2nooPyD48LTcBopjHgqlLxIgAHBk04cdkfWflosZT8sVX2pJn5iTAAGA42o+7ois/7TcGDMGBEu3XqdLwxIgAHBYSYF0RitfjiYZECy7KBKzrqvPVwkQADiqewTLbOXL9zAguJVFkd8F1xcJkB/s3Vlu4zAQBFDKkpdYsXz/2w40zGIHQRa2GVDEeyfQZ6FVzQaAPuUdkdiQLW5+Lwi2cVHk5s+wBAgAdCnviPx6yFbhtFyN+x15uTl66UQCBAA6lHdECoZsjz8td/MHtomLIu8vXEuAAEB/8o5IfAs37ubV5nYWRV4HghIgANCddQpXFK5ONQqC+VNaWhR5GQhKgABAb+bTbtwXhavDZgqC66JI+OSdBAgA9GXdEVkKz3RspCCYF0XCP6wlQACgK3lHpHT7YiMFwXCXcR4SAEBn8o5Iieu4mYLg9f81ZAAA7h+QLt6+OG6lILjutJjoAQB8fEA63rpr/cTwMsqBAACRcuC98e1va+snhuVAAIB7wyE/i9dWQTD3FZt6OAYAoC/zKfQT9nqa0urhBUE5EACgqss58Ff4+TYEtl4QlAMBAD6eFW4qBNYpCOYcqB8IABC8J7d/OqeKhrUgaE8EAKCey1AQuJbjnOqaXwuC3pEGAKhjLeL9Mk0dplTdpUZBcHwyDQQAKFwSeT6nPzE9uiC4SwAAFI4Dl90lfanNgqAECADw+Tjw2tQg8K4gKAECAFQy/GhZeAk0AgP7K8dxLwECAFQxfX9Rbr+bUkDk0/61dwcpDMJQFEVj0pYmiu5/tyV0VBAKBjGSc1bg8CJPf0yrAgQAOKD9fFtTA7a/qlw2BQgAcEBTBz5f4VpziTkpQACAU9QO3Bnh5dCBvYFgn08KAHBDU4n5Z4XXz4+W/wwEUwAAoKm2pnes+ru3Vr9l3hQgAMBw5hKXpAABAIbzHS8qQACA0Tzqzbs1fQCSNUxdIwQCDwAAAABJRU5ErkJggg=="/>
                    </div>
                    <div class="">
                        <div class="content-wrapper light undefined">
                            <div class="detailLine-1Lbvr undefined halfLeft-1HqDN">
                                <div class="dashDetailWrapper-ppq1R light-UQWdL "><span class="borderLine-3aHhe"></span><span
                                        class="dashDetail-33_7u"></span></div>
                                <div>
                                    <h2 class="heading-01 sectionTitle-2fIox title-xGveS light-1Cd0M">
                                        <div>THE WORLD</div>
                                        <div>OF WILD RIFT</div>
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-wrapper">
                        <ul class="slider-3XhNX">
                            <li class="sliderElementWrapper-2llA6"><img
                                    src="https://images.contentstack.io/v3/assets/blt370612131b6e0756/blt02dd32b665c25036/5f4defe8b553152466d1b21a/Homepage_World_1.jpg"
                                    alt="image"/>
                                <div class="copyWrapper-31w81">
                                    <div class="icon svgFrame-3QEuk icon-2tHD8 currentColor-LyOgN">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 358 525">
                                            <path data-element="frame-misc-bottom" d="M264.7 4l4.1-4H358v4z"
                                                  fill="#fff"></path>
                                            <path data-element="frame-misc-top" d="M83.3 516l-4 4H0v-4z"
                                                  fill="#fff"></path>
                                            <path data-element="frame-path" fill="none" stroke-width="1.5" stroke="#fff"
                                                  d="M1 4.1V516l339.1-.1 16.9-15.7V4.1z"></path>
                                        </svg>
                                    </div>
                                    <p class="copy-01 sliderCopy-15Ah9">Explore the universe of League of Legends, the
                                        world of Runeterra, and a global community of incredible fans.</p></div>
                            </li>
                            <li class="sliderElementWrapper-2llA6"><img
                                    src="https://images.contentstack.io/v3/assets/blt370612131b6e0756/blta987e61269c6a8ff/5f4defe80ab106265a187750/Homepage_World_2.jpg"
                                    alt="image"/>
                                <div class="copyWrapper-31w81">
                                    <div class="icon svgFrame-3QEuk icon-2tHD8 currentColor-LyOgN">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 358 525">
                                            <path data-element="frame-misc-bottom" d="M264.7 4l4.1-4H358v4z"
                                                  fill="#fff"></path>
                                            <path data-element="frame-misc-top" d="M83.3 516l-4 4H0v-4z"
                                                  fill="#fff"></path>
                                            <path data-element="frame-path" fill="none" stroke-width="1.5" stroke="#fff"
                                                  d="M1 4.1V516l339.1-.1 16.9-15.7V4.1z"></path>
                                        </svg>
                                    </div>
                                    <p class="copy-01 sliderCopy-15Ah9">Dive into massive events, top-tier League
                                        esports, and unforgettable in-game experiences.</p></div>
                            </li>
                            <li class="sliderElementWrapper-2llA6"><img
                                    src="https://images.contentstack.io/v3/assets/blt370612131b6e0756/bltd5cdaa742790e0a2/5f4defe827123625cbaec51a/Homepage_World_3.jpg"
                                    alt="image"/>
                                <div class="copyWrapper-31w81">
                                    <div class="icon svgFrame-3QEuk icon-2tHD8 currentColor-LyOgN">
                                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 358 525">
                                            <path data-element="frame-misc-bottom" d="M264.7 4l4.1-4H358v4z"
                                                  fill="#fff"></path>
                                            <path data-element="frame-misc-top" d="M83.3 516l-4 4H0v-4z"
                                                  fill="#fff"></path>
                                            <path data-element="frame-path" fill="none" stroke-width="1.5" stroke="#fff"
                                                  d="M1 4.1V516l339.1-.1 16.9-15.7V4.1z"></path>
                                        </svg>
                                    </div>
                                    <p class="copy-01 sliderCopy-15Ah9">With our focus on player experience, you can
                                        count on us to support and improve the game for years to come.</p></div>
                            </li>
                        </ul>
                    </div>
                </section>
            </main>
            <footer id="riotbar-footer"></footer>
        </div>
    </div>
    <div id="gatsby-announcer"
         style="position:absolute;top:0;width:1px;height:1px;padding:0;overflow:hidden;clip:rect(0, 0, 0, 0);white-space:nowrap;border:0"
         aria-live="assertive" aria-atomic="true"></div>
</div>
<!--<script src="../webpack-runtime-9b24a6416f60e357fb63.js" async=""></script>-->
</body>
</html>
