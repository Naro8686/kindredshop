<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{config('google.ga_id')}}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', '{{config('google.ga_id')}}');
    </script>
    <title>{{ config('app.name', 'Kindredshop') }} @yield('title')</title>
    <link rel="icon" href="{{asset('favicons/cropped-Kindred_The_Lamb_2-32x32.png')}}" sizes="32x32">
    <link rel="icon" href="{{asset('favicons/cropped-Kindred_The_Lamb_2-192x192.png')}}" sizes="192x192">
    <link rel="apple-touch-icon" href="{{asset('favicons/cropped-Kindred_The_Lamb_2-180x180.png')}}">
    {{--    <meta name="viewport" content="initial-scale=1.0, width=device-width">--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('meta')
    <link href="{{asset('fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
    @stack('styles')
</head>
<body>
@if ($errors->any())
    <div id="notify" class="alert border-b-4 border-danger">
        @foreach ($errors->all() as $error)
            <p class="notify">{{ $error }}</p>
        @endforeach
    </div>
@endif
@foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(session()->has($msg))
        <div id="notify" class="alert border-b-4 border-{{ $msg }}">
            <p class="notify">{!!  session()->get($msg)  !!}</p>
        </div>
    @endif
@endforeach

<div id="app" class="flex flex-col h-full">
    <div class="modal-cont">
        <div class="modal" id="modal" tabindex="-1" role="dialog" aria-hidden="true"></div>
    </div>
    <header
        class="relative text-white @if(Route::currentRouteName() !== "home") dark @else gamer-bg @endif bg text-center">
        <div class="">
            <div class="header flex justify-between container py-4 relative"
                 :class="{'blacked-nav':mobileNavVisibility}">
                <a href="{{route('home')}}">
                    <img src="{{asset('img/logos/logo.png')}}" class="logo-nav" alt="logo">
                </a>
                <div id="nav-icon3" @click="toggleMobileNav" class="lg:hidden" :class="{open:mobileNavVisibility}">
                    <span></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div class="hidden lg:flex items-center">
                    <ul class="nav-li font-bold flex">
                        @foreach($games as $game)
                            <li><a href="{{route('payment.index',[$game->slug])}}">{{$game->shortName('SMURFS','BUY')}}</a></li>
                        @endforeach
                        <li><a href="{{route('reviews.index')}}">Reviews</a></li>
                        <li class="relative cursor-pointer" @click="toggleDropdown">More<i
                                class="fas fa-chevron-down ml-2"></i>
                            <div class="dd" data-dd>
                                <ul>
                                    <li><a href="{{route('home')}}">Homepage</a></li>
                                    <li><a href="{{route('blog')}}">Blog</a></li>
                                    <li><a href="{{route('terms')}}#tos">Terms and Conditions</a></li>
                                    <li><a href="{{route('terms')}}#privacy-policy">Privacy Policy</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
                <mobile-nav :lol="[['Buy Smurf','{{route('payment.index',['lol-smurfs'])}}']]"
                            :website="[['Reviews','{{route('reviews.index')}}'],['Homepage','{{route('home')}}'],['Blog','{{route('blog')}}'],['Terms and Conditions','{{route('terms')}}#tos'],['Privacy Policy','{{route('terms')}}#privacy-policy']]"
                            v-cloak></mobile-nav>
            </div>
            @yield('head-bg')
        </div>
    </header>
    <main class="flex-grow relative">
        <div class="fixed cookie-notice text-center rounded bg-white z-20">
            <div class="p-2 pr-0 xl:pl-4"><p class="font-medium">By browsing, you agree to our use of cookies 🍪</p>
                <p class="mt-1 text-xs lg:text-sm"><a href="/terms-and-conditions#privacy-policy" target="_blank"
                                                      class="underline">Learn more</a></p></div>
            <div class="px-2 xl:px-4 flex items-center cursor-pointer">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13 13" stroke-linecap="round" class="h-6 xl:h-8">
                    <polyline stroke-width="1" fill="none" stroke="#4a5568" points="1 1,6.5 6.5,12 1"></polyline>
                    <polyline stroke-width="1" fill="none" stroke="#4a5568" points="1 12,6.5 6.5,12 12"></polyline>
                </svg>
            </div>
        </div>
        @yield('content')
    </main>
    <footer class="text-white z-10 pb-3">
        <div class="container">
            <div class="row pt-4">
                <div class="col-md-4">
                    <div class="footer__one_block">
                        <a href="{{route('home')}}"><img alt="logo" src="{{asset('img/logos/logo.png')}}"
                                                         class="w-75 mb-3 m-auto"></a>
                        <p class="mt-5">{{ucfirst($_SERVER['SERVER_NAME'])}} isn’t endorsed by Riot
                            Games and doesn’t reflect the views or opinions of Riot Games or anyone officially involved
                            in
                            producing or managing League of Legends. League of Legends and Riot Games are trademarks or
                            registered trademarks of Riot Games, Inc. League of Legends © Riot Games, Inc.</p>
                        <div class="mt-5">
                            <p>© {{date('Y').' '.config('app.name','Kindredshop')}} | All Rights
                                Reserved</p></div>
                    </div>
                </div>
                <div class="col-md-4 d-flex justify-content-center">
                    <div class="footer__two_block">
                        <h4 class="mb-4">Quick links</h4>
                        <ul>
                            <li><a href="{{route('reviews.index')}}">Reviews</a></li>
                            <li><a href="{{route('home')}}">Homepage</a></li>
                            <li><a href="{{route('blog')}}">Blog</a></li>
                            <li><a href="{{route('terms')}}#tos">Terms and Conditions</a></li>
                            <li><a href="{{route('terms')}}#privacy-policy">Privacy Policy</a></li>
                            <li><a href="{{route('terms')}}#contact">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 d-flex justify-content-center">
                    <div class="footer__tree_block">
                        <h4 class="mb-4">Shop</h4>
                        <ul>
                            @foreach($games as $game)
                                <li><a class="text-capitalize"
                                       href="{{route('payment.index',[$game->slug])}}">{{$game->shortName('accounts')}}</a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="container mt-5">
                            <div class="row">
                                <div class="col-md-6 mt-3"><img src="{{asset('img/logos/payop-footer.png')}}"
                                                                alt="payop"></div>
                                <div class="col-md-6 mt-3"><img src="{{asset('img/logos/coinbase-footer.png')}}"
                                                                alt="coinbase"></div>
                                <div class="col-md-6 mt-3"><img src="{{asset('img/logos/skrill-footer.png')}}"
                                                                alt="skrill"></div>
                                <div class="col-md-6 mt-3"><img src="{{asset('img/logos/stripe-footer.png')}}"
                                                                alt="stripe"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </footer>
</div>
{{--Plugin start--}}
<script src="//code.tidio.co/jpzdbclwomqymbfld4mkrwdvnkdjjhfs.js" async></script>
{{--Plugin end--}}
<script src="{{asset('js/manifest.js')}}"></script>
<script src="{{asset('js/vendor.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>

@stack('scripts')
</body>
</html>
