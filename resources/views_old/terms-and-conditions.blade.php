@extends('layouts.app')
@section('title')
    | {{$seo->title ?? 'Terms and Conditions'}}
@endsection
@section('meta')
    <meta name="description" content="{{$seo->description}}">
    <meta name="keywords" content="{{$seo->keywords}}">
@endsection
@section('head-bg')
    <div class="px-12 pb-12 pt-8 container">
        <h1 class="uppercase text-4xl font-bold">LEGAL</h1>
    </div>
@stop
@section('content')
    <section class="tos-section text-wrap"><p class="text-center text-sm font-bold">Kindredshop.net is a
            customer-oriented website, in case of any issues simply get in touch with us and we will sort things out
            as soon as possible.</p>
        <div class="font-bold text-center my-8"><p><a href="#refund-policy" class="text-blue-600">— Refund and
                    cancellation policy —</a></p>
            <p><a href="{{route('terms')}}#privacy-policy" class="text-blue-600">— Privacy policy —</a></p>
            <p><a href="{{route('terms')}}#tos" class="text-blue-600">— Terms and Conditions —</a></p>
            <p><a href="{{route('terms')}}#contact" class="text-blue-600">— Contact details —</a></p></div>
        <h3 id="refund-policy">Refund and cancellation policy<span>\\</span></h3>
        <p>Last Updated 1 May 2021</p>
        <p><b>CANCELLATION</b></p>
        <p><b></b>You can cancel your order and receive a refund if you haven't yet used the product.</p>
        <p><b>REFUNDS &amp; REPLACEMENTS</b></p>
        <p><b></b>In the occurrence of the delivered product being faulty, not meeting its description or becoming
            faulty within its warranty period, we will (1) replace it, (2) refund you or (3) partially refund you
            and allow to keep the product. You can choose the exact form of compensation.</p>
        <p><b></b>Warranties on our products only cover defects that the customer did not negatively contribute to
            with his activity.</p>
        <p><b></b>Product warranties might have different binding periods with "Lifetime warranty" referring to
            warranty for as long as our website remains in operation but not less than 6 months.</p>
        <h3 id="privacy-policy">Privacy policy<span>\\</span></h3>
        <p>Last Updated 1 May 2021</p>
        <p><b>COOKIES</b></p>
        <p><b></b>Cookies created by <b>{{$_SERVER['SERVER_NAME']}}</b> serve functionality purposes (such as currency
            management
            or
            form pre-filling). We also use cookies from 3rd party vendors, namely Bing, Facebook, Google, Tidio,
            Hotjar, and Stripe. Those cookies serve marketing, analytical and functionality purposes.</p>
        <p>You can disable cookies in every browser. To do so, please follow the instructions from your
            browser provider. Note that after doing so, you won't be able to use websites whose functionality relies
            on cookies.</p>
        <p><b>PERSONAL INFORMATION</b></p>
        <p><b></b>We store your e-mail address, IP address, and country code of the IP address to maintain order
            history and remain tax compliant.</p>
        <p>You may withdraw your e-mail address from our database at any time by sending an inquiry to
            sales@kindredshop.net.</p>
        <p><b>{{$_SERVER['SERVER_NAME']}}</b> does not store any of your payment information. Storing such information
            is the
            duty of our payment processors.</p>
        <h3 id="tos">Terms and Conditions<span>\\</span></h3>
        <p>Last Updated 1 May 2021</p>
        <p><b>GENERAL</b></p>
        <p>The Site provides the following services: Sale of digital goods in online games (Services), which are
            obtained in form of rewards for progressing in them.</p>
        <p>All Services are delivered digitally, via e-mail.</p>
        <p>The Site collects instant, one-time payments only.</p>
        <p>You agree that by accessing the Site and/or Services, you have read, understood, and agree to be bound by
            all of these Terms and Conditions. If you do not agree with all of these Terms and Conditions, then you
            are prohibited from using the Site and Services and you must discontinue use immediately. We recommend
            that you print a copy of these Terms and Conditions for future reference.</p>
        <p>We may make changes to these Terms and Conditions at any time. The updated version of these Terms and
            Conditions will be indicated by an updated “Revised” date and the updated version will be effective as
            soon as it is accessible. You are responsible for reviewing these Terms and Conditions to stay informed
            of updates. Your continued use of the Site represents that you have accepted such changes.</p>
        <p>We may update or change the Site from time to time to reflect changes to our products, our users' needs
            and/or our business priorities.</p>
        <p>The Site is intended for users who are at least 18 years old. If you are under the age of 18, you are not
            permitted to use the Services without parental permission.</p>
        <p>Additional policies which also apply to your use of the Site include:</p>
        <p>● Our privacy policy (https://kindredshop.net/terms-and-conditions#privacy-policy), which sets out the
            terms on which we process any personal data we collect from you, or that you provide to us. By using the
            Site, you consent to such processing and you warrant that all data provided by you is accurate.</p>
        <p>● Our refund and cancellation policy (https://kindredshop.net/terms-and-conditions#refund-policy), which
            describe our warranty and refund policies for the goods sold on our website.</p>
        <p><b>OUR CONTENT</b></p>
        <p>Unless otherwise indicated, the Site and Services including source code, databases, functionality,
            software, website designs, audio, video, text, photographs, and graphics on the Site (Our Content) are
            owned or licensed to us, and are protected by copyright and trade mark laws.</p>
        <p>Except as expressly provided in these Terms and Conditions, no part of the Site, Services or Our Content
            may be copied, reproduced, aggregated, republished, uploaded, posted, publicly displayed, encoded,
            translated, transmitted, distributed, sold, licensed, or otherwise exploited for any commercial purpose
            whatsoever, without our express prior written permission.</p>
        <p>Provided that you are eligible to use the Site, you are granted a limited licence to access and use the
            Site and Our Content and to download or print a copy of any portion of the Content to which you have
            properly gained access solely for your personal, non-commercial use.</p>
        <p>You shall not (a) try to gain unauthorised access to the Site or any networks, servers or computer
            systems connected to the Site; and/or (b) make for any purpose including error correction, any
            modifications, adaptions, additions or enhancements to the Site or Our Content, including the
            modification of the paper or digital copies you may have downloaded.</p>
        <p>Although we make reasonable efforts to update the information on our site, we make no representations,
            warranties or guarantees, whether express or implied, that Our Content on the Site is accurate, complete
            or up to date.</p>
        <p><b>SITE MANAGEMENT</b></p>
        <p>We do not guarantee that the Site will be secure or free from bugs or viruses. You are responsible for
            configuring your information technology, computer programs and platform to access the Site and you
            should use your own virus protection software.</p>
        <p><b>MODIFICATIONS TO AND AVAILABILITY OF THE SITE</b></p>
        <p>We reserve the right to change, modify, or remove the contents of the Site at any time or for any reason
            at our sole discretion without notice. We also reserve the right to modify or discontinue all or part of
            the Services without notice at any time.</p>
        <p>We cannot guarantee the Site and Services will be available at all times. We may experience hardware,
            software, or other problems or need to perform maintenance related to the Site, resulting in
            interruptions, delays, or errors. You agree that we have no liability whatsoever for any loss, damage,
            or inconvenience caused by your inability to access or use the Site or Services during any downtime or
            discontinuance of the Site or Services.</p>
        <p>We are not obliged to maintain and support the Site or Services or to supply any corrections, updates, or
            releases.</p>
        <p>There may be information on the Site that contains typographical errors, inaccuracies, or omissions that
            may relate to the Services, including descriptions, pricing, availability, and various other
            information. We reserve the right to correct any errors, inaccuracies, or omissions and to change or
            update the information at any time, without prior notice.</p>
        <p><b>COMPLAINTS</b></p>
        <p>In order to resolve a complaint regarding the Services, please contact us by email at
            sales@kindredshop.net.</p>
        <p><b>DISCLAIMER/LIMITATION OF LIABILITY</b></p>
        <p>The Site and Services are provided on an as-is and as-available basis. You agree that your use of the
            Site and/or Services will be at your sole risk except as expressly set out in these Terms and
            Conditions. All warranties, terms, conditions and undertakings, express or implied (including by
            statute, custom or usage, a course of dealing, or common law) in connection with the Site and Services
            and your use thereof including, without limitation, the implied warranties of satisfactory quality,
            fitness for a particular purpose and non-infringement are excluded to the fullest extent permitted by
            applicable law.</p>
        <p>We make no warranties or representations about the accuracy or completeness of the Site’s content and are
            not liable for any (1) errors or omissions in content; (2) any unauthorized access to or use of our
            servers and/or any and all personal information stored on our server; (3) any interruption or cessation
            of transmission to or from the site or services; and/or (4) any bugs, viruses, trojan horses, or the
            like which may be transmitted to or through the site by any third party. We will not be responsible for
            any delay or failure to comply with our obligations under these Terms and Conditions if such delay or
            failure is caused by an event beyond our reasonable control.</p>
        <p><b>TERM AND TERMINATION</b></p>
        <p>These Terms and Conditions shall remain in full force and effect while you use the Site or Services or
            are otherwise a user of the Site, as applicable. You may terminate your use or participation at any
            time, for any reason, by following the instructions for terminating user accounts in your account
            settings, if available, or by contacting us at sales@kindredshop.net.</p>
        <p>Without limiting any other provision of these Terms and Conditions, we reserve the right to, in our sole
            discretion and without notice or liability, deny access to and use of the Site and the Services
            (including blocking certain IP addresses), to any person for any reason including without limitation for
            breach of any representation, warranty or covenant contained in these Terms and Conditions or of any
            applicable law or regulation.</p>
        <h3 id="contact">Contact details<span>\\</span></h3>
        <p><b>E-mail</b>: <a rel="nofollow" href="mailto:sales@kindredshop.net"
                             class="underline">sales@kindredshop.net</a></p>
        <p><b>Live Chat</b>: Accessible in the bottom right corner of the website</p>
        <p><b>Site information</b>: Digisale & Webdesign Individual entrepreneur Sharov A.A.</p>
@endsection


