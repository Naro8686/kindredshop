@extends('layouts.admin')

@section('content')

    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Edit Server</h6>
            </div>
            <div class="card-body">
                <form action="{{route('admin.servers.update',$server->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Games</label>
                        <select class="form-control" id="exampleFormControlSelect1" name="game_id">
                            @foreach($games as $game)
                                <option @if($server->game->id === $game->id || (int)old('game_id') === $game->id) selected @endif value="{{$game->id}}">{{$game->name}}</option>
                            @endforeach
                        </select>
                        @error('game_id')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="serverName">Name</label>
                        <input type="text" class="form-control" id="serverName" aria-describedby="nameHelp"
                               value="{{old('name')??$server->name}}" name="name">
                        @error('name')
                        <small id="nameHelp" class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection
