@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Meta Tags</h6>
            </div>
            <div class="card-body">
                <form action="{{route('admin.meta.update',[$seo->id])}}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label for="seo_title">Title</label>
                        <input type="text" class="form-control" id="seo_title" maxlength="255"
                               placeholder="Meta Title" required name="seo[title]" value="{{ old('seo.title') ?? $seo->title }}">
                        @error('seo.title')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="seo_description">Description</label>
                        <input type="text" class="form-control" id="seo"
                               placeholder="Meta Description" name="seo[description]" value="{{ old('seo.description') ?? $seo->description }}">
                        @error('seo.description')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="seo_keywords">Keyword</label>
                        <input type="text" class="form-control" id="seo_keywords"
                               placeholder="Meta Keyword" name="seo[keywords]" value="{{ old('seo.keywords') ?? $seo->keywords }}">
                        @error('seo.keywords')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success btn-block"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Edit Game</h6>
            </div>
            <div class="card-body">
                <form action="{{route('admin.games.update',$game->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="gameName">Name</label>
                        <input type="text" class="form-control" id="gameName" aria-describedby="nameHelp"
                               value="{{old('name')??$game->name}}" name="name">
                        @error('name')
                        <small id="nameHelp" class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection
