@extends('layouts.admin')

@section('content')

    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Add Game</h6>
            </div>
            <div class="card-body">
                <form action="{{route('admin.games.store')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="gameName">Name</label>
                        <input type="text" class="form-control" id="gameName" aria-describedby="nameHelp"
                               value="{{old('name')}}" name="name">
                        @error('name')
                        <small id="nameHelp" class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection
