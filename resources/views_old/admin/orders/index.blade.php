@extends('layouts.admin')

@section('content')
    @push('styles')
        <link href="{{asset('sAdmin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
        <style>
            button.order-status {
                cursor: default !important;
            }
            .table td, .table th{
                vertical-align: middle;
            }
        </style>
    @endpush
    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Orders</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive ">
                    <table class="table table-bordered table-sm text-center text-nowrap" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Reg</th>
                            <th>Acc</th>
                            <th>Info</th>
                            <th>Buyer</th>
                            <th>IP address</th>
                            <th>Status</th>
                            <th>Payment</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td><strong>{{$order->id}}</strong></td>
                                <td>{{$order->product &&  $order->product->server ? $order->product->server->name : 'unknown'}}</td>
                                <td>{{$order->product ? $order->product->title : 'unknown'}}</td>
                                <td>
                                    <pre class="mb-0 pl-2 pr-2 text-gray-700">{{$order->accountsToString()}}</pre>
                                </td>
                                <td>{{$order->email}}</td>
                                <td>
                                    @if(!is_null($order->ip))
                                        <div class="d-flex align-items-center justify-content-between flex-row">
                                            <span>{{$order->ip}}</span>
                                            <div
                                                style="width: 25px;margin-left: 10px">{!! $order->flag('img-fluid img-thumbnail w-100') !!}</div>
                                        </div>
                                    @endif
                                </td>
                                <td>{!! $order->statusHtml() !!}</td>
                                <td>{{$order->type ? ucfirst($order->type) : 'undefined' }}</td>
                                <td><p class="mb-0 d-flex align-items-center justify-content-center">{{$order->amount}}$ @if(!is_null($order->coupon))<span
                                            class="badge badge-info float-right ml-2"><i class="fa fa-ticket-alt"></i> {{$order->coupon->percent}}% </span>@endif</p></td>
                                <td><p class="h6 text-nowrap mb-0">{{$order->dataTimeMsc()}}</p></td>
                                <td>
                                    <div class="d-flex justify-content-between" role="group">
                                        <button type="button" class="btn btn-sm btn-danger mr-1" data-toggle="modal"
                                                data-target="#confirmModal"
                                                data-url="{{route('admin.orders.destroy',$order->id)}}"><i class="fa fa-trash"></i>
                                        </button>
                                        <a title="Check status" href="{{route('admin.orders.show',$order->id)}}" type="button" class="@if($order->status !== \App\Models\Order::PENDING) disabled @endif btn btn-sm btn-info mr-1">
                                            <i class="fa fa-info-circle"></i>
                                        </a>
                                        @if(!is_null($order->email) || !is_null($order->ip))
                                            @if($ban = $order->isBanned())
                                                <form action="{{route('admin.black-list.destroy',$ban->id)}}"
                                                      method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button title="lock" class="btn btn-sm btn-danger"><i
                                                            class="fa fa-ban"></i></button>
                                                </form>
                                            @else
                                                <form action="{{route('admin.black-list.store')}}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="email" value="{{$order->email}}">
                                                    <input type="hidden" name="ip" value="{{$order->ip}}">
                                                    <button title="unlock" class="btn btn-sm btn-success"><i
                                                            class="fa fa-check-circle"></i></button>
                                                </form>
                                            @endif
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#dataTable').DataTable({
                    "lengthMenu": [[25, 50, 100], [25, 50, 100]],
                    "order": [[ 0, "desc" ]]
                });
            });
        </script>
    @endpush
@endsection
