@extends('layouts.admin')

@section('content')

    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Edit Product</h6>
            </div>
            <div class="card-body">
                <form action="{{route('admin.products.update',$product->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-row">
                        <div class="col-md-8 mb-3">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title"
                                   value="{{old('title')??$product->title}}" name="title" required>
                            @error('name')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>

                        <div class="col-md-2 mb-3">
                            <label for="validationDefaultUsername">Price</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend2">$</span>
                                </div>
                                <input type="number" name="price" value="{{old('price')??(int)$product->price}}"
                                       class="form-control"
                                       id="validationDefaultUsername" aria-describedby="inputGroupPrepend2" required>
                            </div>
                            @error('price')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="col-md-2 mb-3">
                            <label for="validationDefaultLifetime">Lifetime percent</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend22">%</span>
                                </div>
                                <input type="number" name="lifetime" value="{{old('lifetime')??$product->lifetime}}"
                                       class="form-control"
                                       id="validationDefaultLifetime" aria-describedby="inputGroupPrepend22" required>
                            </div>
                            @error('lifetime')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label for="validationDefault03">Description</label>
                            <textarea rows="5" class="form-control" id="validationDefault03"
                                      name="description">{{old('description')??$product->description}}</textarea>
                            @error('description')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="selectGame">Game</label>
                            <select id="selectGame" class="form-control" name="game_id" disabled>
                                <option selected
                                        value="{{$product->server->game->id}}">{{$product->server->game->name}}</option>
                            </select>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="selectServer">Server</label>

                            <select id="selectServer" class="form-control" name="server_id" disabled>
                                <option selected value="{{$product->server->id}}">{{$product->server->name}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label for="validationDefault039">Accounts</label>
                            <textarea rows="{{is_null($product->accounts)?5:count($product->accounts)}}" class="form-control" id="validationDefault03"
                                      name="accounts">{{old('accounts')??$product->accountsToString()}}</textarea>
                            @error('accounts')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>
                    <button class="btn btn-primary" type="submit">Submit form</button>
                </form>
            </div>
        </div>
    </div>
@endsection
