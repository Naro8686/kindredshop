@extends('layouts.admin')

@section('content')
    @push('styles')
        <link href="{{asset('sAdmin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    @endpush
    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Products</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive ">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th>Game</th>
                            <th>Server</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <td colspan="6">
                                <a href="{{route('admin.products.create')}}" class="btn btn-success float-right">Add</a>
                            </td>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td><strong>{{$product->id}}</strong></td>
                                <td>{{$product->title}}</td>
                                <td>{{$product->server->game->name}}</td>
                                <td>{{$product->server->name}}</td>
                                <td>{{$product->allAccCount()}}</td>
                                <td>
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <a href="{{route('admin.products.edit',$product->id)}}"
                                           class="btn btn-primary">Edit</a>
                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#confirmModal"
                                                data-url="{{route('admin.products.destroy',$product->id)}}"><i class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#dataTable').DataTable();
            });
        </script>
    @endpush
@endsection
