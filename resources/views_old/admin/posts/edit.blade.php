@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Meta Tags</h6>
            </div>
            <div class="card-body">
                <form action="{{route('admin.meta.update',[$seo->id])}}" method="POST">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label for="seo_title">Title</label>
                        <input type="text" class="form-control" id="seo_title" maxlength="255"
                               placeholder="Meta Title" required name="seo[title]" value="{{ old('seo.title') ?? $seo->title }}">
                        @error('seo.title')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="seo_description">Description</label>
                        <input type="text" class="form-control" id="seo"
                               placeholder="Meta Description" name="seo[description]" value="{{ old('seo.description') ?? $seo->description }}">
                        @error('seo.description')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="seo_keywords">Keyword</label>
                        <input type="text" class="form-control" id="seo_keywords"
                               placeholder="Meta Keyword" name="seo[keywords]" value="{{ old('seo.keywords') ?? $seo->keywords }}">
                        @error('seo.keywords')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-success btn-block"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Post edit</h6>
            </div>
            <div class="card-body">
                <form action="{{route('admin.posts.update',[$post->id])}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="post_id" value="{{$post->id}}">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" maxlength="255"
                               placeholder="Enter title here" required name="title"
                               value="{{ old('title') ?? $post->title}}">
                        @error('title')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="image">
                            @if(!is_null($post->image))
                                <img src="{{asset($post->image)}}" alt="" class="col-md-3 col-sm-12 img-thumbnail">
                            @else
                                Image
                            @endif
                        </label>
                        <input type="file" class="form-control-file" id="image" name="image">
                        @error('image')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="body">Body</label>
                        <textarea class="form-control" id="body" rows="10" cols="10"
                                  name="body">{{ old('body') ?? $post->body}}</textarea>
                        @error('body')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="submit" name='update' class="btn btn-outline-success" value="Update"/>
                        @if($post->isActive())
                            <input type="submit" name='save' class="btn btn-outline-secondary" value="Save Draft"/>
                        @else
                            <input type="submit" name='publish' class="btn btn-success" value="Publish"/>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        ClassicEditor
            .create(document.querySelector('#body'), {
                ckfinder: {
                    uploadUrl: "{{route('admin.ckeditor.upload', ['_token' => csrf_token() ])}}",
                },
            })
            .then(editor => {
                console.log(editor);
                window.editor = editor;
            })
            .catch(error => {
                console.error('There was a problem initializing the editor.', error);
            });
    </script>
@endpush
