@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        @if(count([config('setting_fields', [])]) )
            <form method="post" action="{{ route('admin.settings.store') }}" class="form-horizontal" role="form">
                @csrf
                @foreach(config('setting_fields') as $section => $fields)
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">
                                <i class="{{ data_get($fields, 'icon', 'fa fa-tools') }}"></i>
                                {{ $fields['title'] }}
                            </h6>
                        </div>
                        <div class="card-body">
                            <p class="text-muted">{{ $fields['desc'] }}</p>
                            @foreach($fields['elements'] as $field)
                                @includeIf('admin.setting.fields.' . $field['type'] )
                            @endforeach
                        </div>
                    </div>
                @endforeach
                <div class="row m-b-md">
                    <div class="col-md-12">
                        <button class="btn-primary btn">
                            Save Settings
                        </button>
                    </div>
                </div>
            </form>
        @endif
    </div>
@endsection
