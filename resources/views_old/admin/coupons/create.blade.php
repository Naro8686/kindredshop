@extends('layouts.admin')

@section('content')

    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Add Coupon</h6>
            </div>
            <div class="card-body">
                <form action="{{route('admin.coupons.store')}}" method="POST">
                    @csrf
                    <div class="form-row">
                        <div class="col-md-8 mb-3">
                            <label for="code">Code</label>
                            <input type="text" class="form-control" id="code" value="{{old('code')}}" name="code"
                                   required="">
                            @error('code')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationDefaultPercent">Percent</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">%</span>
                                </div>
                                <input type="number" name="percent" min="1" max="100" value="{{old('percent')}}"
                                       class="form-control" id="validationDefaultPercent" required="">

                            </div>
                            @error('percent')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox"
                                       value="1" id="unlimited"
                                       name="unlimited" @if(old('unlimited')) checked @endif>
                                <label class="form-check-label" for="unlimited">
                                    Unlimited
                                </label>
                            </div>
                            @error('unlimited')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection
