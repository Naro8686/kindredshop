<?php echo $version; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{url('/')}}</loc>
        <priority>1.00</priority>
    </url>
    @foreach($games as $game)
        <url>
            <loc>{{url("/$game->slug")}}</loc>
            <priority>0.80</priority>
        </url>
    @endforeach
    <url>
        <loc>{{url('/reviews')}}</loc>
        <priority>0.80</priority>
    </url>
    <url>
        <loc>{{url('/blog')}}</loc>
        <priority>0.80</priority>
    </url>
    @foreach($posts as $post)
        <url>
            <loc>{{url("/post/$post->slug")}}</loc>
            <priority>0.80</priority>
        </url>
    @endforeach
    <url>
        <loc>{{url('/terms-and-conditions')}}</loc>
        <priority>0.80</priority>
    </url>
</urlset>
