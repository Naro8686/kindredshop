@extends('layouts.app')
@section('title')
    | {{$seo->title ?? 'Home'}}
@endsection
@section('meta')
    <meta name="description" content="{{$seo->description}}">
    <meta name="keywords" content="{{$seo->keywords}}">
@endsection
@section('head-bg')
    @push('styles')
        <link rel="stylesheet" type="text/css" href="{{asset('css/head_bg.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/new.css')}}">
    @endpush
    <section class="registrationHero">
        <span class="dashDetail-2"></span>
        <div class="container">
            <div class="content-wrapper contentWrapper-3LPGz bg-text">
                <div>
                    <h1 class="heading-01 font-bold">KINDRED</h1>
                    <h3 class="heading-03 tagline noCTA">
                        TRUSTED SOURCE FOR BUYING<br>
                        LEAGUE OF LEGENDS ACCOUNTS SINCE 2015
                    </h3>
                    <div class="appStoreWrapper">
                        <div class="storesWrapper">
                            @foreach($games as $game)
                                <a class="appButton uppercase"
                                   href="{{route('payment.index',[$game->slug])}}">{{$game->shortName('SMURFS','BUY')}}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('content')
    <div class="section-head-btn">
        <div class="container">
            <div class="row">
                <div class="col-md-4 d-flex">
                    <div class="p-1 mr-3">
                        <img src="{{asset('img/items/instant_408.png')}}" alt="item">
                    </div>
                    <div>
                        <h1 class="font-bold">INSTANT DELIVERY</h1>
                        <p class="mt-4">Your League/Valorand account will be delivered instantly 24/7 through e-mail
                            after the
                            payment has reached us.</p>
                    </div>
                </div>
                <div class="col-md-4 d-flex">
                    <div class="p-1 mr-4">
                        <img src="{{asset('img/items/warranty_409.png')}}" alt="item">
                    </div>
                    <div>
                        <h1 class="font-bold">LIFETIME WARRANTY</h1>
                        <p class="mt-4">You can select up to lifetime warranty and 20 days are free. Our extended
                            warranty covers 100% of
                            account issue.</p>
                    </div>
                </div>
                <div class="col-md-4 d-flex">
                    <div class="p-1 mr-4">
                        <img src="{{asset('img/items/secure_410.png')}}" alt="item">
                    </div>
                    <div>
                        <h1 class="font-bold">SECURE PAYMENTS</h1>
                        <p class="mt-4">Our site is secured using SSL technology and uses popular and secure payment
                            services.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section class="gameOverview">
        <div class="volt topVolt top-1m">
            <img alt="" class="voltImage"
                 src="{{asset('img/bg/Unknown.png')}}"/>
        </div>
        <div class="content-wrapper contentWrapper-2">
            <div class="sectionContent-3N">
                <div class="copyWrapper-2R">
                    <div class="accordionWrapper-jY">
                        <div class="accordion-1W accordion-2x ">
                            <div class="item-2Z">
                                <div class="header-3t" data-index="0">
                                    <div class="titleWrapper-3F">
                                        <span
                                                class="border-oj bottomLeft-XS light-3s videoBorder-w border___top">
                                            <span class="borderLine-24 firstSide-2P"></span>
                                            <span class="dashDetail-26 top-detail"></span>
                                        </span>
                                        <h1 class="heading-01 font-bold">
                                            WELCOME TO<br>
                                            KINDREDSHOP.NET
                                        </h1>
                                    </div>
                                </div>
                                <div class="copy-01 description-kq">
                                    <div class="decriptionCopy-AU">
                                        <div class="richTextContent-2" style="max-width: 360px">
                                            We are one of the most trustworthy shops on the market and selling League of
                                            Legends accounts since 2013. To prove that, we have a huge amount of
                                            positive feedback, long lifespan, and we approach our work with a serious
                                            attitude.
                                            Here you can find League Smurfs, Valorant Smurfs, LoL accounts with rare
                                            skins, gifting service and more. If you buy from us, you can be rest assured
                                            in the quality of our service and the speed of delivery!
                                        </div>
                                        {{--                                        <span class="dashDetail-3YdzR"></span>--}}
                                    </div>

                                </div>
                                <div class="d-flex mb-5">
                                    @foreach($games as $game)
                                        <a class="appButton uppercase" style="margin-right: 15px"
                                           href="{{route('payment.index',[$game->slug])}}">{{$game->shortName('SMURFS','BUY')}}</a>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="videoWrapper-jH">
                    <div class="videoContainer-1q youtubeVideo-2LL ">
                        <div class="posterContainer-3PM" data-element="poster">
                            <img src="{{asset('img/bg/akali.png')}}"
                                 alt="akali" class="posterImage-3av"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="volt bottomVolt-K6psi bottom-b1">
            <img class="voltImage"
                 src="{{asset('img/bg/Unknown-2.png')}}"
                 alt=""/>
        </div>
    </section>
    <div class="container iMac">
        <div class="row">
            <div class="col-md-6 d-flex align-items-center">
                <img class="w-75" src="{{asset('img/items/iMac.png')}}" alt="tv">
            </div>
            <div class="col-md-6">
                <div class="detailLine-1 detailLine-1P top-1j">
                    <div class="dashDetailWrapper-pp dark-2k" style="opacity: 1;">
                        <span class="borderLine-3"></span>
                        <span class="dashDetail-33"></span>
                    </div>
                </div>
                <h1 class="heading-01 font-bold">HOW TO<br>BUY ACCOUNT</h1>
                <div class="mw-450px">
                    <p class="mt-5">Only a 4 step process that can have you gaming on your new account within 5 minutes.
                        Here’s all that you need to do:</p>
                    <ul class="mt-5 mb-5">
                        <li>1. Choose server.</li>
                        <li>2. Select the account variant.</li>
                        <li>3. Review your smurf.</li>
                        <li>4. Proceed to Checkout.</li>
                    </ul>
                    <p class="mt-5">Once your order is placed you will instantly receive the account details via email.
                        Try
                        it! :)</p>
                </div>
            </div>
        </div>
    </div>
    <section class="gameOverview bg-gradient-orange">
        <div class="volt topVolt top-1m bg-rotate">
            <img alt="" class="voltImage"
                 src="{{asset('img/bg/Unknown-3.png')}}"/>
        </div>
        <div class="content-wrapper contentWrapper-2">
            <div class="sectionContent-3N mb-5">
                <div class="titleWrapper-3F">
                    <span class="border-oj bottomLeft-XS light-3s videoBorder-w border___left"><span
                                class="dashDetail-26 top-detail"></span> <span
                                class="borderLine-24 firstSide-2P"></span></span>
                    <h1 class="heading-01 font-bold">
                        WHY YOU<br>
                        SHOULD CHOOSE US
                    </h1>
                </div>

            </div>
            <div class="container mt-5 mb-5 choose_us">
                <div class="row">
                    <div class="col-md-4 d-flex">
                        <h1 class="font-bold">1.</h1>
                        <div class="p-3">
                            <h2 class="font-bold">Make you happy since 2013</h2>
                            <p class="mt-4">
                                Kindredshop one of the most trustworthy shops and selling League of Legends accounts
                                since 2013. We've been in the business for years and know how to keep you satisfied.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex">
                        <h1 class="font-bold">2.</h1>
                        <div class="p-3">
                            <h2 class="font-bold">Everyone will like it</h2>
                            <p class="mt-4">
                                At Kindredshop.net we have the best prices, many different accounts, rarest skins and
                                more!
                                You will definitely find an excellent account for you and your friends!
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex">
                        <h1 class="font-bold">3.</h1>
                        <div class="p-3">
                            <h2 class="font-bold">High rating</h2>
                            <p class="mt-4">
                                We have a huge amount of positive feedback, long lifespan, and we approach our work with
                                a serious attitude. You can check it on
                                reviews.io and truspilot.com
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex mt-5 pt-1">
                        <h1 class="font-bold">4.</h1>
                        <div class="p-3">
                            <h2 class="font-bold">Prices</h2>
                            <p class="mt-4">
                                We understand that an important parts of the purchase is the price. We are constantly
                                comparing our prices to offer you the best and cheapest service.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex mt-5 pt-1">
                        <h1 class="font-bold">5.</h1>
                        <div class="p-3">
                            <h2 class="font-bold">Always Unranked</h2>
                            <p class="mt-4">
                                The accounts have completely clear game history in every season, ranked games have never
                                been played. Literally a perfect Smurf!
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex mt-5 pt-1">
                        <h1 class="font-bold">6.</h1>
                        <div class="p-3">
                            <h2 class="font-bold">Full access</h2>
                            <p class="mt-4">
                                You will be able to verify your own e-mail and change the password whenever you want.
                                Only you are the owner of the account!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="volt bottomVolt-K6psi bottom-b1 bg-rotate">
            <img class="voltImage"
                 src="{{asset('img/bg/Unknown-4.png')}}"
                 alt=""/>
        </div>
    </section>
    <div class="container review__block">
        <div class="row">
            <div class="col-md-6 d-flex flex-column justify-content-center align-items-center">
                <img class="w-50" src="{{asset('img/items/reviewsio.png')}}" alt="reviewsio">
                <img class="w-50 mt-5" src="{{asset('img/items/trustpilot.png')}}" alt="trustpilot">
            </div>
            <div class="col-md-6">
                <div class="detailLine-1 detailLine-1P top-1j">
                    <div class="dashDetailWrapper-pp dark-2k " style="opacity: 1;">
                        <span class="borderLine-3"></span>
                        <span class="dashDetail-33"></span>
                    </div>
                </div>
                <h1 class="heading-01 font-bold">REVIEWS</h1>
                <p class="mt-5 font-spiegel-reg">
                    Kindredshop.net has an average rating of <b>{{round($reviews->avg('rating'), 1)}}</b> based on
                    over {{$reviews->count()}} reviews. You can view them all or submit one yourself on our <a
                            href="{{route('reviews.index')}}"><b>reviews page.</b></a>
                </p>
                <p class="mt-5 font-spiegel-reg">
                    Rated an average of <b>4.8 stars</b> across hundreds reviews by third party review collector <b>Trustilot</b>
                    and <b>4.7 stars</b> by <a href="https://www.reviews.io/"><b>reviews.io</b></a>
                </p>
            </div>
        </div>
    </div>
    <section class="gameOverview gameOverview-bottom">
        <div class="volt topVolt top-1m">
            <img alt="" class="voltImage"
                 src="{{asset('img/bg/Unknown.png')}}"/>
        </div>
        <div class="content-wrapper contentWrapper-2">
            <div class="sectionContent-3N">
                <div class="copyWrapper-3R">
                    <div class="accordionWrapper-jY">
                        <div class="accordion-1W accordion-2x ">
                            <div class="item-2Z">
                                <div class="header-3t" data-index="0">
                                    <div class="titleWrapper-3F">
                                        <span class="border-oj bottomLeft-XS light-3s videoBorder-w border___top">
                                            <span class="borderLine-24 firstSide-2P"></span>
                                            <span class="dashDetail-26 top-detail"></span>
                                        </span>
                                        <h1 class="heading-01 font-bold">
                                            ABOUT OUR<br>
                                            SMURF ACCOUNTS
                                        </h1>
                                    </div>
                                </div>
                                <div class="copy-01 description-kq">
                                    <div class="decriptionCopy-AU mt-5 mb-5">
                                        <div class="mb-5">
                                            If you need a LoL Smurf Account at a great price and you want to be sure of
                                            its quality, there is no better place than Kindredshop.net but for those who
                                            are not very well versed in LoL Accounts and want to understand what we
                                            offer, here is a short excursion – if you start playing League of Legends
                                            from the beginning, your account will be assigned level 1 during
                                            registration, you will have very few champions, runes and available game
                                            modes, this is not so much fun!
                                        </div>
                                        <div class="mb-5">
                                            But we have great news for you, you no longer need to spend hundreds of
                                            hours slowly raising your account level to enjoy the game of League of
                                            Legends completely, pumping takes a lot of time, and the fun begins in
                                            ranked matches, you can play with friends and participate in this
                                            competition, as well as you can get in the same game with professional
                                            players!
                                        </div>
                                        <div class="mb-5">
                                            An order to gain access to Summmoner’s Rift, you must have a level 30
                                            account, and it takes a huge amount of time to reach this level, but you can
                                            buy a League of Legends account level 30 account and go straight to the best
                                            that the greatest MOBA of all time has to offer. . In other words,
                                            immediately after the purchase, you can enjoy the game without spending
                                            hundreds of hours and a huge amount of energy on the meaningless work that
                                            has already been done for you, and instead of months of boring games,
                                            immediately enjoy the greatness of the League of Legends!
                                        </div>
                                        {{--                                        <span class="dashDetail-3YdzR"></span>--}}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
