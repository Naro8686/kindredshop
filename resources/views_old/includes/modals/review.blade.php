<div class="flex justify-between text-gray-900 pb-4 border-b border-gray-100"><h2
        class="font-bold sm:text-xl">Add review</h2><i
        class="text-2xl fas fa-times cursor-pointer modal-close"></i>
</div>
<div class="text-center">
    <form class="review" action="{{route('reviews.store')}}" method="POST">
        @csrf
        <label for="name">Name</label>
        <input id="name" required="required" type="text" class="box-gray w-100" name="name">
        <label for="rating">Rating</label>
        <select id="rating" required="required" class="box-gray arrow" name="rating">
            <option value="5">5 Stars</option>
            <option value="4">4 Stars</option>
            <option value="3">3 Stars</option>
            <option value="2">2 Stars</option>
            <option value="1">1 Stars</option>
        </select>
        <label for="game">Games</label>
        <select id="game" required="required" class="box-gray arrow" name="game_id">
            @foreach($games as $game)
                <option @if($loop->iteration === 1) selected @endif value="{{$game->id}}">{{$game->name}}</option>
            @endforeach
        </select>
        <label for="comment">Comment</label>
        <textarea id="comment" required="required" class="box-gray" name="comment"></textarea>
        <button class="text-center btn btn-orange mt-4" type="submit">
            <div class="relative inline-block"><p>Submit</p>
                <div class="icon-con">
                    <i class="fas fa-cog fa-spin" style="display: none;"></i>
                </div>
            </div>
        </button>
    </form>
</div>
<div class="hidden pt-4 text-center">
    <button class="inline-block font-bold">Close</button>
</div>
