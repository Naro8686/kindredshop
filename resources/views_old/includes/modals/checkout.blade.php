<div class="flex justify-between text-gray-900 pb-4 border-b border-gray-100 modal-close"><h2
        class="font-bold">Checkout</h2><i class="text-2xl fas fa-times cursor-pointer"></i>
</div>
@if($order['min'])
    <form id="checkout" data-product-id="{{$order['product_id']}}" data-order-id="{{$order['id']}}"
          action="{{route('payment.store')}}" method="POST"
          onsubmit="return false">
        @csrf
        <div class="flex text-center items-center justify-between pt-2">
            <div class="flex flex-col">
                <div class="flex mb-2 align-items-center mb-2"><i
                        class="fas fa-box-open text-blue-600 mr-2 text-xl align-items-center"></i>
                    <p class="font-bold text-xs sm:text-lg">{{$order['info']}}</p></div>
                <div class="d-flex items-center mb-3 align-items-center">
                    <label for="acc_count" class="text-sm mr-1 sm:text-base m-0">Quantity:</label>
                    <input type="number" min="{{$order['min']}}" max="{{$order['max']}}"
                           value="{{$order['min']}}"
                           name="count"
                           id="acc_count"
                           class="font-bold text-xl w-12 text-center mr-2 rounded border border-gray-300">

                    <button id="apple" class="text-orange-600 font-bold text-lg uppercase sm:text-xl"
                            style="display: none;" type="button">Apply
                    </button>
                </div>
                {{--            <div><p class="text-xs">Buy 2 or more and save <b--}}
                {{--                        class="text-blue-600">5%</b></p></div>--}}
            </div>
            <div>
                <div class="font-bold text-right"><p id="amount"
                                                     class="text-xl sm:text-2xl md:text-3xl">{!! $order['amountHtml'] !!}</p>
                </div>
                <button type="button"
                        class="add_coupon border-b-2 text-xs sm:text-base focus:outline-none rounded border-b border-gray-300">
                    <span>Add Coupon</span>
                </button>
            </div>
        </div>
        <div
            class="py-3 sm:py-6 border-b border-t border-gray-100 mt-3 d-flex justify-content-center align-items-center flex-column">
            <div class="coupon_cont mb-3" style="display: none;">
                <div>
                    <div class="d-flex">
                        <input spellcheck="false"
                               type="text"
                               class="font-bold p-2 mr-2 bg-gray-100 uppercase w-32 text-sm lg:text-lg text-center border-gray-300 border-2 rounded-lg focus:outline-none text-gray-700">
                        <button type="button" id="coupon"
                                class="text-center btn btn-orange text-sm lg:text-lg ">
                            <div class="relative inline-block"><p>Add</p>
                                <div class="icon-con">
                                    <i class="fas fa-cog fa-spin" style="display: none;"></i>
                                </div>
                            </div>
                        </button>
                    </div>
                </div>
            </div>
            <div class="text-center email_content w-75"><p
                    class="text-sm sm:text-base font-medium mb-4">
                    Please, enter your e-mail to proceed.</p>
                <input type="email" name="email" required
                       class="border w-full rounded text-base sm:text-lg shadow px-4 py-2 border-blue-600">
            </div>
        </div>
        <div class="pm-buttons">
            <button data-type="stripe" name="type"
                    class="w1 rounded border border-gray-300 @if((int)setting('stripe',0) === 0) disabled @endif"
                    @if((int)setting('stripe',0) === 0) disabled @endif type="submit"><span class="load load-sm"
                                                                                            style="display: none;"></span>
                @if((int)setting('stripe',0) === 0)
                    <p class="text-gray-700 z-10 position-absolute">Not available now</p>
                @endif
                <img src="{{asset('img/logos/stripe.jpg')}}" class="h-full ob-fit" alt="">

            </button>
            <button data-type="payop" name="type"
                    class="w1 rounded border border-gray-300 @if((int)setting('payop',0) === 0) disabled @endif"
                    @if((int)setting('payop',0) === 0) disabled @endif type="submit"><span class="load load-sm"
                                                                                           style="display: none;"></span>
                <p class="text-gray-700 z-10 position-absolute">Not available now</p>
                <img src="{{asset('img/logos/payop.jpg')}}" class="h-full ob-fit" alt="">
            </button>
            <button data-type="coinbase" name="type"
                    class="w1 rounded border border-gray-300 @if((int)setting('coinbase',0) === 0) disabled @endif"
                    @if((int)setting('coinbase',0) === 0) disabled @endif type="submit"><span class="load load-sm"
                                                                                              style="display: none;"></span>

                <p class="text-gray-700 z-10 position-absolute">Not available now</p>
                <img src="{{asset('img/logos/coinbase.jpg')}}" class="h-full ob-fit" alt="">

            </button>
            <button data-type="paypal" name="type"
                    class="w1 rounded border border-gray-300 @if((int)setting('paypal',0) === 0) disabled @endif"
                    @if((int)setting('paypal',0) === 0) disabled @endif type="submit"><span class="load load-sm"
                                                                                            style="display: none;"></span>
                <p class="text-gray-700 z-10 position-absolute">Not available now</p>
                <img src="{{asset('img/logos/paypal.jpg')}}" class="h-full ob-fit" alt="">
            </button>
        </div>
        <p class="mt-4 text-center tos-text">By clicking the button, you agree to our <a
                href="{{route('terms')}}" target="_blank" class="underline font-medium">Terms
                &amp; Conditions</a>.</p>
    </form>
@endif
<div class="hidden pt-4 text-center">
    <button class="inline-block font-bold">Close</button>
</div>
