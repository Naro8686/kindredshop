
    <div class="flex flex-wrap justify-center">
        @foreach($servers as $server)
            <button data-server-id="{{$server->id}}" data-game-id="{{$server->game_id}}"
                    class="text-center relative m-1 rounded px-2 py-4 @if($oldServerName === $server->name) selected @endif">
                <h3 class="font-bold text-lg md:text-2xl text-blue-100">
                    {{$server->name}}
                </h3>
            </button>
        @endforeach
    </div>

