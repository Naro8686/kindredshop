@extends('layouts.app')
@section('title')
    | {{$seo->title ?? 'Reviews'}}
@endsection
@section('meta')
  <meta name="description" content="{{$seo->description}}">
  <meta name="keywords" content="{{$seo->keywords}}">
@endsection
@section('head-bg')
    <div class="px-12 pb-12 pt-8 container">
        <h1 class="uppercase text-4xl font-bold">Reviews</h1>
    </div>
@stop
@section('content')
    <div class="container">
        <section class="mt-8">
            <h2 class="m-auto text-3xl text-center font-bold mb-4">On-site reviews</h2>
            <div class="vc-l">
                <div class="w-full mb-4 p-4">
                    <div class="flex justify-around flex-wrap sm:flex-row-reverse m-auto lg:w-1/2 items-end">
                        <div class="w-full sm:w-auto mb-4 sm:mb-0">
                            <button class="btn btn-orange mx-auto block"
                                    data-toggle="modal" data-type="review" data-target="#modal">Write a review
                            </button>
                        </div>
                        <div class="text-center"><label for="games"
                                                        class="block font-bold text-gray-700 mb-1">Product</label>
                            <select id="games" class="inline-block box" name="game_id">
                                <option value="">All</option>
                                @foreach($games as $game)
                                    <option value="{{$game->id}}">{{$game->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="text-center">
                            <label for="sort" class="block font-bold text-gray-700 mb-1">Sort by</label>
                            <select id="sort" class="inline-block box" name="sort">
                                <option value="0">Newest first</option>
                                <option value="1">Oldest first</option>
                                <option value="2">Best first</option>
                                <option value="3">Worst first</option>
                            </select></div>
                    </div>
                </div>
                <div class="flex flex-wrap justify-between reviews">
                    @include('includes.reviews',$reviews)
                </div>
            </div>
        </section>
    </div>
@endsection
@push('scripts')
    <script>
        let params = {
            sort: 1,
            game_id: null,
        };
        $(document).on('change', 'select#sort,select#games', function (e) {
            let name = $(this).attr('name');
            params[`${name}`] = this.value;
            $.get('/reviews/sort', params).done(function (data) {
                $('.reviews').html(data)
            })
        })
    </script>
@endpush


