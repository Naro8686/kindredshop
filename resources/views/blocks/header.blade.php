<section class="first">

    <div class="container">
			<input type="checkbox" class="menu-check" id="menu-btn">
			<label class="menu-btn" for="menu-btn"><span></span></label>
            <nav class="menu">
            <ul class="menu-wrapper"> 
                <li class="menu-item"><a href="{{ route('home') }}" class="menu-link">Home</a></li>
                <li class="menu-item"><a href="{{ route('home') }}#smurfs" class="menu-link">Smurfs</a></li>
                <li class="menu-item"><a href="{{ route('home') }}#faq" class="menu-link">FAQ</a></li>
                <li class="menu-item menu-item_h">
                    <a href="https://kindredshop.net/terms-and-conditions" class="menu-link">More</a>
                    <ul class="submenu">
                        <li class="submenu-item"><a href="{{ route('terms') }}#terms" class="submenu-item-link">Terms and Conditions</a></li>
                        <li class="submenu-item"><a href="{{ route('terms') }}#privacy-policy" class="submenu-item-link">Privacy Policy</a></li>
                        <li class="submenu-item"><a href="{{ route('terms') }}#contact-details" class="submenu-item-link">Contact</a></li>
                    </ul>
                </li>
            </ul>
            </nav>

        <div class="first-content">
            
            <h1 class="first-content-title">Your legend</h1>
            <h1 class="first-content-title">starts right here!</h1>

            <p class="first-content-text">
                Trusted source for buying <span>League of Legends</span>
            </p>
            <p class="first-content-text" style="margin-top: 0px;">
                unique accounts since 2015
            </p>
             
            <div class="first-content-trustpilot">
            
                <div class="first-content-stars">
                    <img src="{{asset('img/Logo.png')}}" alt="trustpilot">
                    <img src="{{asset('img/stars.png')}}" alt="stars" style="margin-top: 17px;">
                </div>
            
                <img src="{{asset('img/arrow-line.png')}}" style="width: 10px; height: 55px;" alt="arrow-line" class="arrow-line_img">
            
                <div class="first-content-rating">
                    <span class="rating-span">4.85</span>
                    <p class="rating-text">out of 5</p>
                </div>
            
                <img src="{{asset('img/arrow-line2.png')}}" style="width: 2px; height: 55px;" alt="arrow-line" class="arrow-line_img">
            
                <div class="first-content-reviews">
                    <span class="reviews-span">1.000+</span>
                    <p class="reviews-text">reviews</p>
                </div>

            </div>
            
            <a href="#smurfs" class="first-content-img_link" ><img src="{{asset('img/arrow-down1.png')}}" class="first-content-img" alt="arrow-down"></a>
        
        </div>

    </div>

</section>