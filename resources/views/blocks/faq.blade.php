<section class="fourd" id="faq">

    <div class="container">
        
        <div class="fourd-wrapper-texts">
            <h2 class="fourd-title">Frequently Asked Questions</h2>
            <p class="fourd-text">
                Have a quick question? It's most probably answered here.
            </p>
        </div>

        <div class="item">
            <div class="item__name">
                <span>What do you actually sell here? </span>
            </div>
            <div class="item__desc">
                <p>Level 30 League of Legends accounts with lots of Blue Essence and clean ranked history, they are literally perfect smurf accounts. You can pick between different amounts of Blue Essence, which will be enough to buy 25+ champions.</p>
            </div>
        </div>

        <div class="item">
            <div class="item__name">
                <span>Is it safe to buy accounts?</span>
            </div>
            <div class="item__desc">
                <p>Yes, as safe as it gets when it comes to buying LoL Smurfs. We don't believe customers should be risking their money when purchasing online so we offer Lifetime Warranty as an option for every account. Never worry again.</p>
            </div>
        </div>

        <div class="item">
            <div class="item__name">
                <span>Can I change e-mail and password?</span>
            </div>
            <div class="item__desc">
                <p>Yes! All of our League of Legends accounts come with login details, which means that you will be able to attach your email and change the password whenever you want.</p>
                <p>Login to <a href="https://account.riotgames.com">https://account.riotgames.com</a></p>  
                <p>Click on "Your Username" -> "Settings" located in top right corner. Change the Email address by clicking on the email address field. (You will see the old email address there, just click on it and it will work)</p>
            </div>
        </div>

        <div class="item">
            <div class="item__name">
                <span>What is Lifetime Warranty?</span>
            </div>
            <div class="item__desc">
                <p>We provide an extended lifetime guarantee and instant replacement on all our smurf accounts. If something happens to your League of Legends account, you'll receive a replacement. Your LoL Smurf is under our protection!</p>
            </div>
        </div>
        
        <div class="item">
            <div class="item__name">
                <span>I bought the account, where is the account information? </span>
            </div>
            <div class="item__desc">
                <p>Your League account will be delivered instantly 24/7 through e-mail after the payment has reached us. Sometimes our emails go to spam folder, check this too! </p>
            </div>
        </div>
        
        <div class="item">
            <div class="item__name">
                <span>How can I get in toch with you?</span>
            </div>
            <div class="item__desc">
                <p>We provide excellent support for our League of Legends accounts! Solve any of your questions through e-mail: sales@kindredshop.net or via Live Chat in the lower right corner.</p>
            </div>
        </div>

    </div>

</section>