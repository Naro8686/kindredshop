<section class="five">

    <div class="container container-five">

        <div class="five-wrapper-texts">   

            <h2 class="five-title">About Us</h2>

            <p class="five-text">
                We at Kindredshop.net have the best prices, many different accounts, the rarest skins and more! Frankly, we have everything you can wish for. You will definitely find an excellent account for you and your friends!
            </p>

        </div> 
        
        <div class="five-wrapper">

            <div class="five-card">
                <h3 class="five-card-title"><span>since</span> 2015</h3>
                <p class="five-card-text">
                    We are one of the most trustworthy shops on the market and selling League of Legends accounts since 2015. You can trust us! 
                </p>
            </div>

            <div class="five-card">
                <h3 class="five-card-title">100.000<span>+</span></h3>
                <p class="five-card-text">
                    Orders completed. Huge amount of positive feedback, long lifespan, and we approach our work with a serious attitude. 
                </p>
            </div>

            <div class="five-card">
                <h3 class="five-card-title">99<span>%</span></h3>
                <p class="five-card-text">
                    Customer satisfaction rate! If you buy from us, you can be rest assured in the quality of our service and the speed of delivery! 
                </p>
            </div>

        </div>

    </div>

</section>