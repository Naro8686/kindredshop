<div class="modal-wrap active" id="modal-wrap" data-prodid="{{ $product['id'] }}" data-lifetimeis="{{ $product['lifetimeis'] }}" data-lifetimepercent="{{ $product['lifetime'] }}" data-pricetotal="{{ $product['price_total'] }}" data-priceone="{{ $product['price'] }}" data-quantity="1" data-couponis="0" data-couponpercent="0">

    <div class="checkout">

        <h1 class="checkout-title">Checkout - Blue Essence Smurf</h1>

        <div class="checkout-wrapper-date">
            <div class="checkout-date-card">
                <p class="head-text">{{ $product['title'] }}</p>
                <p class="foot-text">{{ $product['description'] }}</p>
            </div>
            <div class="checkout-date-card">
                <p class="head-text">{{ $product['server']['name'] }}</p>
                <p class="foot-text">server</p>
            </div>
            <div class="checkout-date-card">
                <p class="head-text">Lifetime</p>
                <p class="foot-text">warranty</p>
            </div>
            <div class="checkout-date-card">
            @php
            $price = number_format((float)$product['price_total'],2);
            $price_int = explode('.',$price)[0];
            $price_flt = explode('.',$price)[1];
            @endphp
                <p class="head-text">$<span class="bign">{{ $price_int }}</span>.<span class="minn">{{ $price_flt }}</span></p>
                <p class="foot-text foot-text_n">total price</p>
            </div>
        </div>

        <div class="checkout-wrapper-inputs">

            <div class="checkout-card-input">
                <span>Quantity:</span>
                <input id="quantity_input" min="1" type="number" class="checkout-input" value="1">
            </div>

            <div class="checkout-card-input">
                <span>Apply coupon:</span>
                <input id="couponcode" type="text" class="checkout-input checkout-input_long" placeholder="coupon code">
            </div>

        </div>

        <div class="checkout-mail" style="display: none">
            <p class="checkout-mail-text">Please, enter your e-mail to proceed:</p>
            <input type="email" name="usermail" class="checkout-mail-input" placeholder="example@gmail.com">
            <div class="checkout-mail-error">Please enter correct Email</div>
        </div>

        <div class="checkout-payment-wrapper">
            @if ((int)($settings[\App\Models\Order::STRIPE_TYPE.'.enabled'] ?? false) && !is_null($product->getPayLink(\App\Models\Order::STRIPE_TYPE)->get($link_type)))
            <div class="checkout-payment-card" data-type="stripe">
            @else
            <div class="checkout-payment-card checkout-payment-card_op" data-type="stripe">
            @endif
                <img src="{{asset('img/stripe.svg')}}" alt="stripe" class="checkout-payment-img" width="150px" height="52px">
                <img src="{{asset('img/visa.svg')}}" alt="visa" class="checkout-payment-img" width="64px" height="58px">
                <img src="{{asset('img/mastercard.svg')}}" alt="mastercard" class="checkout-payment-img" width="64px" height="58px">
                <img src="{{asset('img/klarna.svg')}}" alt="klarna" class="checkout-payment-img" width="64px" height="58px">
                <img src="{{asset('img/applepay.svg')}}" alt="applepay" class="checkout-payment-img" width="64px" height="58px">
            </div>
            @if ($settings[\App\Models\Order::COINBASE_TYPE.'.enabled'] ?? false)
            <div class="checkout-payment-card" data-type="coinbase">
            @else
            <div class="checkout-payment-card checkout-payment-card_op" data-type="coinbase">
            @endif
                <img src="{{asset('img/coinbase.svg')}}" alt="coinbase" class="checkout-payment-img" width="150px" height="52px">
                <img src="{{asset('img/bitcoin.svg')}}" alt="bitcoin" class="checkout-payment-img" width="64px" height="58px">
                <img src="{{asset('img/ethereum.svg')}}" alt="ethereum" class="checkout-payment-img" width="64px" height="58px">
                <img src="{{asset('img/litecoin.svg')}}" alt="litecoin" class="checkout-payment-img" width="64px" height="58px">
                <img src="{{asset('img/dogecoin.svg')}}" alt="dogecoin" class="checkout-payment-img" width="64px" height="58px">
            </div>
            @if ((int)($settings[\App\Models\Order::PAYPAL_TYPE.'.enabled'] ?? false) && !is_null($product->getPayLink(\App\Models\Order::PAYPAL_TYPE)->get($link_type)))
            <div class="checkout-payment-card" data-type="paypal">
            @else
            <div class="checkout-payment-card checkout-payment-card_op" data-type="paypal">
            @endif
                <img src="{{asset('img/paypal.svg')}}" alt="paypal" class="checkout-payment-img" width="150px" height="52px">
                <img src="{{asset('img/visa.svg')}}" alt="visa" class="checkout-payment-img" width="64px" height="58px">
                <img src="{{asset('img/mastercard.svg')}}" alt="mastercard" class="checkout-payment-img" width="64px" height="58px">
                <img src="{{asset('img/discover.svg')}}" alt="discover" class="checkout-payment-img" width="64px" height="58px">
                <img src="{{asset('img/amex.svg')}}" alt="amex" class="checkout-payment-img" width="64px" height="58px">
            </div>
            @if ((int)($settings[\App\Models\Order::PAYOP_TYPE.'.enabled'] ?? false))
            <div class="checkout-payment-card" data-type="payop">
            @else
            <div class="checkout-payment-card checkout-payment-card_op" data-type="payop">
            @endif
                <img src="{{asset('img/payop.svg')}}" alt="payop" class="checkout-payment-img" width="150px" height="52px">
                <img src="{{asset('img/visa.svg')}}" alt="visa" class="checkout-payment-img" width="64px" height="58px">
                <img src="{{asset('img/mastercard.svg')}}" alt="mastercard" class="checkout-payment-img" width="64px" height="58px">
                <img src="{{asset('img/skrill.svg')}}" alt="skrill" class="checkout-payment-img" width="64px" height="58px">
                <img src="{{asset('img/150-plus.svg')}}" alt="150+" class="checkout-payment-img" width="64px" height="58px">
            </div>
        </div>

        <p class="checkout-footer">
            By clicking the payment button, you agree to our <a href="https://kindredshop.net/terms-and-conditions#terms">Terms & Conditions</a>.
        </p>

    </div>

    <div class="overlay"></div>

</div>
