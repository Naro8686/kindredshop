                <div id="cursummary" data-server="{{ $product->server->id }}" data-lifetime="{{ $product->lifetime }}" data-price="{{ $product->price }}" data-id="{{ $product->id }}" data-nowprice="{{ $product->price }}">

                    <div class="result-wrapper">
                        <div class="result-wrapper-text">
                            <span class="result-span">
                                {{ $product->title }}
                            </span>
                            <p class="result-subtext">
                                {{ $product->description }}
                            </p>
                            <span class="result-span">
                                {{ $product->server->name }}
                            </span>
                            <p class="result-subtext">
                                server
                            </p>
                            <p class="result-text">
                                Unranked all seasons Level 30+ & honor 2
                            </p>
                        </div>
                        <img src="{{asset('img/blueessence.webp')}}" alt="blueessence" class="blueessence-logo">
                    </div>

                    <div class="garanty-wrapper">
                        <div class="garanty-card">
                            <img src="{{asset('img/waiting-mail.png')}}" alt="mail">
                            <p class="garanty-text">Instant</p>
                            <p class="garanty-text">Delivery</p>
                        </div>
                        <div class="garanty-card">
                            <img src="{{asset('img/award-card.png')}}" alt="award">
                            <p class="garanty-text">Lifetime</p>
                            <p class="garanty-text">Warranty</p>
                        </div>
                        <div class="garanty-card">
                            <img src="{{asset('img/profile-lock.png')}}" alt="profile-lock">
                            <p class="garanty-text">Full</p>
                            <p class="garanty-text">Access</p>
                        </div>
                    </div>

                    <div class="second-right-wrapper-texts"></div>

                    <p class="second-right-wrapper-text">We provide an extended lifetime guarantee and instant replacement on all our smurf accounts. If something happens to your League of Legends account, you'll receive a replacement!</p>

{{--                    <div class="check-box">--}}
{{--                        <input type="checkbox" id="checkbox-id" />--}}
{{--                        <label for="checkbox-id">Lifetime Warranty (+{{ $product->lifetime }}%)</label>--}}
{{--                    </div>--}}

                    @php
                    $price_int = round($product->price);
                    $price_flt = $product->price - round($product->price);
                    @endphp

                    <button id="modal-btn" class="btn">
                    Checkout $<span class="big-btn">{{ $price_int }}</span>.<span class="small-btn">{{ $price_flt }}</span>
                    </button>

                </div>
