@if (count($products) > 0)

                <div class="variant">

                    <div class="variant-first-row">
                        
                        <h3 class="variant-title">SELECT VARIANT</h3>

                        <?php /*

                        <div class="variant-leng">
                            <span class="variant-leng-choos">
                                USD  <span class="variant-leng-eur">EUR</span>
                            </span>
                        </div>

                        */ ?>

                    </div>

                    <div class="variant-wrapper">
                        @php $iter = 0; @endphp
                        @foreach ($products as $product)
                            @php $accCount = $product->accCount($session_id); @endphp
                            @if($accCount > 0 && $iter === 0)
                                @php $iter++; @endphp
                                @php $card_yellow = true; @endphp
                            @else
                                @php $card_yellow = false; @endphp
                            @endif
                            <div @class(["variant-card","variant-card_yellow" => $card_yellow,"disabled" => !$accCount]) data-prodid="{{ $product->id }}">
                                <div class="card-left-wrapper">
                                <span class="variant-number">
                                 {{ $product->title }}
                                </span>
                                    <p class="variant-name">
                                        {{ $product->description }}
                                    </p>
                                </div>
                                @php
                                    $price_int = round($product->price);
                                    $price_flt = $product->price - round($product->price);
                                @endphp
                                @if($accCount)
                                    <div class="variant-price">
                                        $<span class="variant-price-big">{{ $price_int }}</span>.{{ $price_flt }}
                                    </div>
                                @else
                                    <div class="variant-out-of-stock">
                                        <span>OUT OF STOCK</span>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                    </div>

                </div>

@else

                <div class="variant">
                    <div class="variant-first-row">
                        <h3 class="variant-title">
                            PRODUCTS NOT FOUND
                        </h3>
                    </div>
                </div

@endif