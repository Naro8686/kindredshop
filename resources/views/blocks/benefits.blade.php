<section class="third">

    <div class="container">

        <h2 class="third-title">Enjoy your benefits</h2>

        <p class="third-text">You get all the extra features and benefits!</p>

        <div class="third-wrapper">

            <div class="third-card third-card_f">
                <img src="{{asset('img/waiting-mail-big.png')}}" alt="waiting-mail">
                <h3 class="third-card-title">Instant Delivery</h3>
                <p class="third-card-text">
                    Your League of Legends smurf account will be delivered instantly 24/7 through e-mail with the account information using our fully automated delivery system. No more waiting!
                </p>
            </div>

            <div class="third-card third-card_s">
                <img src="{{asset('img/verify-credit-big.png')}}" alt="verify-credit">
                <h3 class="third-card-title third-card-title_mt">Secure Payments</h3>
                <p class="third-card-text">
                    Safety is the main thing for us, we only use popular and secure payment methods. Your credit card information is never saved and is processed using the most secure SSL technologies.
                </p>
            </div>

            <div class="third-card third-card_t">
                <img src="{{asset('img/award-card-big.png')}}" alt="award-card">
                <h3 class="third-card-title">Lifetime Warranty</h3>
                <p class="third-card-text">
                    We provide an extended lifetime guarantee and instant replacement on all our smurf accounts. If something happens to your League of Legends account, you'll receive a replacement.
                </p>
            </div>

            <div class="third-card third-card_fo">
                <img src="{{asset('img/profile-lock-big.png')}}" alt="profile-lock">
                <h3 class="third-card-title">Full access</h3>
                <p class="third-card-text">
                    Once you receive your League of legends account, you will be able to verify your own e-mail and change the password whenever you want. Only you are the owner of the account!
                </p>
            </div>

        </div>

    </div>
    
</section>