@extends('layouts.admin')

@section('content')
    @push('styles')
        <link href="{{asset('sAdmin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
        <style>
            .table td, .table th{
                vertical-align: middle;
            }
        </style>
    @endpush
    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Coupons</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive ">
                    <table class="table table-bordered table-sm text-center text-nowrap" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Code</th>
                            <th>Percent</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <td colspan="2">
                                <form action="{{route('admin.coupons.generate')}}" method="POST">
                                    @csrf
                                    <div class="form-row">
                                        <input type="number" min="1" max="100" name="percent" required
                                               value="{{old('percent')}}"
                                               class="form-control col-4 text-center m-1" placeholder="%">
                                        <button class="btn  btn-sm btn-outline-info float-right col-7 m-1">Generate</button>
                                    </div>
                                </form>

                            </td>
                            <td colspan="2">
                                <a href="{{route('admin.coupons.create')}}" class="btn btn-sm btn-success float-right">Add</a>
                            </td>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($coupons as $coupon)
                            <tr>
                                <td><strong>{{$coupon->id}}</strong></td>
                                <td>
                                    <h6 class="d-flex align-items-center justify-content-center m-0">
                                        {{$coupon->code}}
                                        @if($coupon->used())
                                            <span class="badge badge-danger float-right ml-2">Used</span>
                                        @elseif($coupon->unlimited)
                                            <span class="badge badge-secondary float-right ml-2">Unlimited</span>
                                        @endif
                                    </h6>
                                </td>
                                <td>
                                    <h6 class=" m-0">{{$coupon->percent}}%</h6>
                                </td>
                                <td>
                                    <div class="btn-group-sm float-right" role="group" aria-label="Basic example">
                                        <a href="{{route('admin.coupons.edit',$coupon->id)}}"
                                           class="btn btn-sm btn-primary">Edit</a>
                                        <button type="button" class="btn btn-sm btn-danger" data-toggle="modal"
                                                data-target="#confirmModal"
                                                data-url="{{route('admin.coupons.destroy',$coupon->id)}}"><i
                                                class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#dataTable').DataTable();
            });
        </script>
    @endpush
@endsection
