@extends('layouts.admin')

@section('content')
    @push('styles')
        <link href="{{asset('sAdmin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
        <style>
            button.order-status {
                cursor: default !important;
            }

            .table td, .table th {
                vertical-align: middle;
            }
        </style>
    @endpush
    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Orders</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-sm text-center text-nowrap" id="orders_table" width="100%"
                           cellspacing="0">
                        <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Reg</th>
                            <th>Acc</th>
                            <th>Info</th>
                            <th>Buyer</th>
                            <th>IP address</th>
                            <th>Status</th>
                            <th>Payment</th>
                            <th>Amount</th>
                            <th>Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

    </div>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#orders_table').DataTable({
                    autoWidth: true,
                    processing: true,
                    serverSide: true,
                    lengthMenu: [[25, 50, 100], [25, 50, 100]],
                    order: [[0, 'desc']],
                    ajax: '{!! route('admin.orders.index') !!}',
                    columns: [
                        {data: 'id', name: 'id'},
                        {data: 'product.server.name', name: 'product.server.name'},
                        {data: 'product.title', name: 'product.title'},
                        {data: 'details', name: 'details', orderable: false},
                        {data: 'email', name: 'email'},
                        {data: 'ip', name: 'ip'},
                        {data: 'status', name: 'status'},
                        {data: 'type', name: 'type'},
                        {data: 'amount', name: 'amount'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ]
                })
            });
        </script>
    @endpush
@endsection
