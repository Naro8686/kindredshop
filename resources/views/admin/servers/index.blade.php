@extends('layouts.admin')

@section('content')
    @push('styles')
        <link href="{{asset('sAdmin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    @endpush
    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Servers</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive ">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Name</th>
                            <th>Game</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <td colspan="4">
                                <a href="{{route('admin.servers.create')}}" class="btn btn-success float-right">Add</a>
                            </td>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($servers as $server)
                            <tr>
                                <td><strong>{{$server->id}}</strong></td>
                                <td>{{$server->name}}</td>
                                <td><?php /* {{$server->game->name}} */ ?></td>
                                <td>
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <a href="{{route('admin.servers.edit',$server->id)}}"
                                           class="btn btn-primary">Edit</a>
                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#confirmModal"
                                                data-url="{{route('admin.servers.destroy',$server->id)}}"><i class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#dataTable').DataTable();
            });
        </script>
    @endpush
@endsection
