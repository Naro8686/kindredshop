@extends('layouts.admin')

@section('content')

    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Edit Server</h6>
            </div>
            <div class="card-body">
                <form action="{{route('admin.servers.update',$server->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group d-none">
                        <label for="exampleFormControlSelect1">Games</label>
                        <select class="form-control" id="exampleFormControlSelect1" name="game_id">
                            @foreach($games as $game)
                                @if ($loop->first)
                                    <option value="{{$game->id}}" selected>{{$game->name}}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('game_id')
                        <small class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="serverName">Name</label>
                        <input type="text" class="form-control" id="serverName" aria-describedby="nameHelp"
                               value="{{old('name')??$server->name}}" name="name">
                        @error('name')
                        <small id="nameHelp" class="form-text text-danger">{{$message}}</small>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="gameDescription">Description</label>
                        <textarea 
                            class="form-control" 
                            id="gameDescription" 
                            name="description"
                        >{{old('description')??$server->description}}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection
