@extends('layouts.admin')

@section('content')
    @push('styles')
        <link href="{{asset('sAdmin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    @endpush
    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Reviews</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive ">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Name</th>
                            <th>Game</th>
                            <th>Comment</th>
                            <th>Rating</th>
                            <th>Published</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($reviews as $review)
                            <tr>
                                <td><strong>{{$review->id}}</strong></td>
                                <td>{{$review->name}}</td>
                                <td>{{$review->game->name}}</td>
                                <td>{{$review->comment}}</td>
                                <td>{{$review->rating}}</td>
                                <td>{{$review->published ? "Yes":"No"}}</td>
                                <td>
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <a href="{{route('admin.reviews.edit',$review->id)}}" class="btn btn-primary">
                                            Edit
                                        </a>
                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#confirmModal"
                                                data-url="{{route('admin.reviews.destroy',$review->id)}}"><i
                                                    class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#dataTable').DataTable();
            });
        </script>
    @endpush
@endsection
