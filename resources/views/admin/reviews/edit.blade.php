@extends('layouts.admin')

@section('content')
    @push('styles')
        <link href="{{asset('sAdmin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    @endpush
    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Review</h6>
            </div>
            <div class="card-body">
                <form action="{{route('admin.reviews.update',$review->id)}}" method="POST">
                    @method("PUT")
                    @csrf
                    <div class="form-group">
                        <label for="exampleFormControlInput1">Name</label>
                        <input name="name" type="text" class="form-control" id="exampleFormControlInput1" value="{{$review->name}}">
                    </div>
                    <div class="form-group">
                        <label for="gamesFormControlSelect1">Games</label>
                        <select name="game_id" class="form-control" id="gamesFormControlSelect1">
                            @foreach($games as $game)
                                <option @if($review->game_id === $game->id) selected @endif value="{{$game->id}}">{{$game->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlSelect1">Rating</label>
                        <select name="rating" class="form-control" id="exampleFormControlSelect1">
                            @for($i = 1;$i<=5;$i++)
                                <option @if($review->rating === $i) selected @endif value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Comment</label>
                        <textarea name="comment" class="form-control" id="exampleFormControlTextarea1" rows="3">{{$review->comment}}</textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn @if(!$review->published) btn-dark @else btn-outline-dark @endif"
                                type="submit">Save
                        </button>
                        <button class="btn @if($review->published) btn-success @else btn-outline-success @endif"
                                name="published" value="1" type="submit">Publish
                        </button>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection
