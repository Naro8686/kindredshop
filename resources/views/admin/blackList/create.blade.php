@extends('layouts.admin')

@section('content')

    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Ban</h6>
            </div>
            <div class="card-body">
                <form action="{{route('admin.black-list.store')}}" method="POST">
                    @csrf
                    <div class="form-row">
                        <div class="col-md-7 mb-3">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" value="{{old('email')}}" name="email">
                            @error('email')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="col-md-1 d-flex align-items-center justify-content-center">
                            <p class="text-center mb-0">or</p>
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="ip">IP</label>
                            <input type="text" class="form-control" id="ip" value="{{old('ip')}}" name="ip">
                            @error('ip')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection
