@extends('layouts.admin')

@section('content')
    @push('styles')
        <link href="{{asset('sAdmin/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    @endpush
    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Black List</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive ">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>Email</th>
                            <th>IP</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <td colspan="4">
                                <a href="{{route('admin.black-list.create')}}"
                                   class="btn btn-success float-right">Add</a>
                            </td>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($bans as $ban)
                            <tr>
                                <td><strong>{{$ban->id}}</strong></td>
                                <td><h6>{{$ban->email}}</h6></td>
                                <td>
                                    @if(!is_null($ban->ip))
                                        <div class="d-flex align-items-center">
                                            <h6 class="mb-0">{{$ban->ip}}</h6>
                                            <div
                                                style="width: 25px;margin-left: 10px">{!! $ban->flag('img-fluid img-thumbnail w-100') !!}</div>
                                        </div>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group float-right" role="group" aria-label="Basic example">
                                        <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#confirmModal"
                                                data-url="{{route('admin.black-list.destroy',$ban->id)}}"><i class="fa fa-trash"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    @push('scripts')
        <script>
            $(document).ready(function () {
                $('#dataTable').DataTable();
            });
        </script>
    @endpush
@endsection
