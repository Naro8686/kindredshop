@extends('layouts.admin')

@section('content')

    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Black List</h6>
            </div>
            <div class="card-body">
                <form action="{{route('admin.coupons.update',$ban->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-row">
                        <div class="col-md-8 mb-3">
                            <label for="code">Code</label>
                            <input type="text" class="form-control" id="code" value="{{old('code')?? $ban->code}}"
                                   name="code"
                                   required="">
                            @error('code')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="validationDefaultPercent">Percent</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">%</span>
                                </div>
                                <input type="number" name="percent" min="1" max="100"
                                       value="{{old('percent')??$ban->percent}}"
                                       class="form-control" id="validationDefaultPercent" required="">

                            </div>
                            @error('percent')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection
