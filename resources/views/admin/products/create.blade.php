@extends('layouts.admin')

@section('content')

    <div class="container-fluid">
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Add Product</h6>
            </div>
            <div class="card-body">
                <form action="{{route('admin.products.store')}}" method="POST">
                    @csrf
                    <div class="form-row">
                        <div class="col-md-8 mb-3">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title"
                                   value="{{old('title')}}" name="title" required>
                            @error('name')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>

                        <div class="col-md-4 mb-3">
                            <label for="validationDefaultUsername">Price</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend2">$</span>
                                </div>
                                <input type="number" name="price" value="{{old('price')}}" class="form-control"
                                       id="validationDefaultUsername" aria-describedby="inputGroupPrepend2" required>
                            </div>
                            @error('price')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
{{--                        <div class="col-md-2 mb-3">--}}
{{--                            <label for="validationDefaultLifetime">Lifetime percent</label>--}}
{{--                            <div class="input-group">--}}
{{--                                <div class="input-group-prepend">--}}
{{--                                    <span class="input-group-text" id="inputGroupPrepend22">%</span>--}}
{{--                                </div>--}}
{{--                                <input type="number" min="1" max="100" name="lifetime" value="{{old('lifetime')}}" class="form-control"--}}
{{--                                       id="validationDefaultLifetime" aria-describedby="inputGroupPrepend22" required>--}}
{{--                            </div>--}}
{{--                            @error('lifetime')--}}
{{--                            <small class="form-text text-danger">{{$message}}</small>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label for="validationDefault03">Description</label>
                            <textarea rows="5" class="form-control" id="validationDefault03"
                                      name="description">{{old('description')}}</textarea>
                            @error('description')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6 mb-3">
                            <label for="selectGame">Game</label>
                            <select id="selectGame" class="form-control" name="game_id">
                                @foreach($games as $game)
                                    <option @if((int)old('game_id') === $game->id) selected
                                            @endif value="{{$game->id}}">{{$game->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="selectServer">Server</label>

                            <select id="selectServer" class="form-control" name="server_id">
                                @foreach($servers as $server)
                                    <option @if((int)old('server_id') === $server->id) selected
                                            @endif value="{{$server->id}}">{{$server->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            <label for="validationDefault039">Accounts</label>
                            <textarea rows="5" class="form-control" id="validationDefault03"
                                      name="accounts">{{old('accounts')}}</textarea>
                            @error('accounts')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">
                        <p class="col-md-12 mb-3">PayPal links</p>
                        <div class="col-md-12 mb-3">
                            <label for="link_basic">Basic</label>
                            <input class="form-control" id="link_basic"
                                   name="links[paypal][basic]"
                                   value="{{old('links.paypal.basic')}}">
                            @error('links.paypal.basic')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
{{--                        <div class="col-md-6 mb-3">--}}
{{--                            <label for="link_lifetime">Lifetime</label>--}}
{{--                            <input class="form-control" id="link_lifetime"--}}
{{--                                   name="links[paypal][lifetime]"--}}
{{--                                   value="{{old('links.paypal.lifetime')}}">--}}
{{--                            @error('links.paypal.lifetime')--}}
{{--                            <small class="form-text text-danger">{{$message}}</small>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
                    </div>
                    <div class="form-row">
                        <p class="col-md-12 mb-3">Stripe links</p>
                        <div class="col-md-12 mb-3">
                            <label for="stripe_basic">Basic</label>
                            <input class="form-control" id="stripe_basic"
                                   name="links[stripe][basic]"
                                   value="{{old('links.stripe.basic')}}">
                            @error('links.stripe.basic')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                        {{--                        <div class="col-md-6 mb-3">--}}
                        {{--                            <label for="link_lifetime">Lifetime</label>--}}
                        {{--                            <input class="form-control" id="link_lifetime"--}}
                        {{--                                   name="links[paypal][lifetime]"--}}
                        {{--                                   value="{{old('links.paypal.lifetime')}}">--}}
                        {{--                            @error('links.paypal.lifetime')--}}
                        {{--                            <small class="form-text text-danger">{{$message}}</small>--}}
                        {{--                            @enderror--}}
                        {{--                        </div>--}}
                    </div>
                    <button class="btn btn-primary" type="submit">Submit form</button>
                </form>
            </div>
        </div>
    </div>
    @push('scripts')
        <script>
            $(document).on('change', 'select#selectGame', function (e) {
                let serverList = $('select#selectServer');
                let game_id = $(this).val();
                serverList.attr('disabled', 'disabled');
                $.get("{{route('admin.products.selectServers')}}", {game_id: game_id}).done(function (data) {
                    serverList.html('');
                    data.servers.forEach(function (server) {
                        serverList.append(`<option value="${server.id}">${server.name}</option>`);
                    })
                    serverList.removeAttr('disabled');
                })

            })
        </script>
    @endpush
@endsection
