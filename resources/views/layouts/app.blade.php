<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-58924591-5"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-58924591-5');
    </script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @yield('meta')
    <meta name="msapplication-TileColor" content="#0b1424">
    <meta name="theme-color" content="#0b1424">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="icon" href="{{asset('favicons/cropped-Kindred_The_Lamb_2-32x32.png')}}" sizes="32x32">
    <link rel="icon" href="{{asset('favicons/cropped-Kindred_The_Lamb_2-192x192.png')}}" sizes="192x192">
    <link rel="apple-touch-icon" href="{{asset('favicons/cropped-Kindred_The_Lamb_2-180x180.png')}}">
    <title>{{ config('app.name', 'Kindredshop') }} @yield('title')</title>
</head>
<body>

@yield('content')

@yield('cookiebeware')

    <footer class="footer">
        <div class="footer-line"></div>
        <div class="container">
            <p class="footer-text">
                Kindredshop.net isn’t endorsed by Riot Games and doesn’t reflect the views or opinions of Riot Games or anyone officially involved in producing or managing League of Legends. League of Legends and Riot Games are trademarks or registered trademarks of Riot Games, Inc. League of Legends © Riot Games, Inc.
            </p>
            <div class="footer-pay-logo">
                <img alt="payop" src="{{asset('img/payop.svg')}}">
                <img alt="stripe" src="{{asset('img/stripe.svg')}}">
                <img alt="paypal" src="{{asset('img/paypal.svg')}}">
                <img alt="coinbase" src="{{asset('img/coinbase.svg')}}">
            </div>
            <p class="footer-copy">
                © 2022 Kindredshop.net - All Rights Reserved
            </p>
        </div>
    </footer>


    @if(session()->has('success'))
    <div id="messagebox" style="display:block">
        <div id="mbmessage">
            <h3>Success!</h3>
            <p>{!! session()->get('success') !!}</p>
            <div class="close">+</div>
        </div>
    </div>
    @elseif(session()->has('danger'))
    <div id="messagebox" style="display:block">
        <div id="mbmessage">
            <h3>Error!</h3>
            <p>{!! session()->get('danger') !!}</p>
            <div class="close">+</div>
        </div>
    </div>
    @else
    <div id="messagebox">
        <div id="mbmessage">
            <h3></h3>
            <p></p>
            <div class="close">+</div>
        </div>
    </div>
    @endif
    <script src="{{asset('js/jquery-3.6.0.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/newsite.js')}}"></script>
    <script src="//code.tidio.co/jpzdbclwomqymbfld4mkrwdvnkdjjhfs.js" async></script>
</body>
</html>