@forelse($products as $product)
    <div data-id="{{$product->id}}" data-price="{{$product->price}}" data-percent="{{$product->lifetime}}"
         class="_variant @if($loop->iteration === 1) _highlighted @endif">
        <div>
            <p class="_amount">{{$product->title}}</p>
            <p class="_type">{{$product->description}}</p>
        </div>
        @if($product->accCount(request()->session()->getId()))
            <div class="font-bold">
                <p class="text-xl _price">{!! $product->priceHtml() !!}</p>
            </div>
        @else
            <div class="uppercase">
                <p class="text-sm product-empty-text">OUT OF STOCK</p>
            </div>
        @endif
    </div>
@empty
    <h1 class="text-center text-gray-500">Empty(</h1>
@endforelse
