<div class="rounded sh2 p-5 flex flex-col bg-white mb-4 w-full sm:w-1/2s lg:w-1/3s review w-100">
    <div class="flex justify-between top">
        <div>
            {!! $review->flag("h-full inline-block mr-1") !!}
            <p class="inline-block text-sm  text-gray-700 font-medium">{{$review->name}}</p>
        </div>
        <div class="flex items-center text-sm">
            @for($i = 1; $i <= $review->rating; $i++)
                <i class="fas fa-star text-orange-600"></i>
            @endfor
        </div>
    </div>
    <div
         class="text-center flex-grow flex justify-center items-center text-sm my-6 px-4">{{$review->comment}}
    </div>
    <div class="flex justify-between text-gray-700">
        <p class="text-sm">{{$review->game->name}}</p>
        <p class="text-sm">{{$review->created_at->diffForHumans()}}</p>
    </div>
</div>
