@extends('layouts.app')
@section('title')
    | {{$seo->title ?? 'Blog'}}
@endsection
@section('meta')
    <meta name="description" content="{{$seo->description}}">
    <meta name="keywords" content="{{$seo->keywords}}">
@endsection
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
@endpush
@section('head-bg')
    <div class="px-12 pb-12 pt-8 container">
        <h1 class="uppercase text-4xl font-bold">BLOG</h1>
    </div>
@stop
@section('content')
    <div class="container">
        <section class="flex flex-wrap justify-between">
            @forelse($posts as $post)
                <a href="{{route('posts.show',[$post->slug])}}"
                   class="blog-post-widget border-2 border-transparent hover:border-blue-400 cursor-pointer mb-6 sm:w-1/2s lg:w-1/3s flex flex-wrap flex-col bg-white rounded sh2 p-5">
                    @if(!is_null($post->image))
                        <div class="flex-grow flex items-center">
                            <img src="{{asset($post->image)}}" class="rounded">
                        </div>
                    @endif
                    <div class="mt-2">
                        <h3 class="text-center font-bold mb-2 text-xl">{{$post->title}}</h3>
                        <p class="mb-2 text-gray-600 text-xs font-bold">
                            <i class="far fa-calendar-alt mr-1"></i>{{$post->created_at->diffForHumans()}}
                        </p>
                        <p class="text-sm">{!! Str::limit(strip_tags($post->body), 165); !!}</p></div>
                </a>
            @empty
                <label class="text-left display-1 text-gray-500 mt-12 pt-8 mb-8 pb-12 h-100">empty(</label>
            @endforelse
        </section>
    </div>
@endsection



