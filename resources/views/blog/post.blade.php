@extends('layouts.app')
@section('title')
    | {{$seo->title ?? $post->title}}
@endsection
@section('meta')
    <meta name="description" content="{{$seo->description}}">
    <meta name="keywords" content="{{$seo->keywords}}">
@endsection
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
@endpush
@section('content')
    <div class="container">
        <article class="text-wrap blog-post">
            @if(!is_null($post->image))
                <img src="{{asset($post->image)}}" class="header rounded" alt="">
            @endif
            <h1 class="text-4xl font-bold text-center my-4">{{$post->title}}</h1>
            <p class="text-gray-600 font-bold mb-2">
                {{$post->created_at->diffForHumans()}}
                <i class="far fa-calendar-alt ml-1"></i></p>
            <div class="content mb-4 center-h">
                {!! $post->body !!}
            </div>
            <a href="{{route('blog')}}" class="btn btn-gray a-cen">Back to Blog</a>
        </article>
    </div>
@endsection



