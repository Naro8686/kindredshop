<!doctype html>
<html class="i-amphtml-singledoc i-amphtml-standalone" lang="en">
<head>
    <meta charset="utf-8">
    <title>Kindredshop</title>
    <style>
        html {
            overflow-x: auto !important
        }

        html {
            height: 100% !important;
            width: 100% !important
        }

        body {
            -webkit-text-size-adjust: 100%;
            -moz-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        table {
            overflow: scroll;
        }

        ul li, ol li {
            margin-bottom: 15px;
        }

        body {
            width: 100%;
            font-family: arial, "helvetica neue", helvetica, sans-serif;
        }

        table {
            border-collapse: collapse;
            table-layout: fixed;
        }

        table td, body, .es-wrapper {
            padding: 0;
            Margin: 0;
        }

        p, hr {
            Margin: 0;
        }

        h1, h2, h3, h4, h5 {
            Margin: 0;
            line-height: 120%;
            font-family: arial, "helvetica neue", helvetica, sans-serif;
        }

        .es-left {
            float: left;
        }

        .es-right {
            float: right;
        }

        .es-p5t {
            padding-top: 5px;
        }

        .es-p5b {
            padding-bottom: 5px;
        }

        .es-p10t {
            padding-top: 10px;
        }

        .es-p10b {
            padding-bottom: 10px;
        }

        .es-p10l {
            padding-left: 10px;
        }

        .es-p10r {
            padding-right: 10px;
        }

        .es-p20 {
            padding: 20px;
        }

        .es-p20l {
            padding-left: 20px;
        }

        .es-p20r {
            padding-right: 20px;
        }

        .es-p25t {
            padding-top: 25px;
        }

        .es-p30t {
            padding-top: 30px;
        }

        .es-p30b {
            padding-bottom: 30px;
        }

        .es-p30l {
            padding-left: 30px;
        }

        .es-p35t {
            padding-top: 35px;
        }

        .es-menu td {
            border: 0;
        }

        s {
            text-decoration: line-through;
        }

        p, ul li, ol li {
            font-family: arial, "helvetica neue", helvetica, sans-serif;
            line-height: 150%;
        }

        a {
            text-decoration: underline;
        }

        .es-menu td a {
            text-decoration: none;
            display: block;
        }

        .es-menu amp-img, .es-button amp-img {
            vertical-align: middle;
        }

        .es-wrapper {
            width: 100%;
            height: 100%;
            background-color: #F4F3F2;
        }

        .es-wrapper-color {
            background-color: #F4F3F2;
        }

        .es-header-body p, .es-header-body ul li, .es-header-body ol li {
            color: #333333;
            font-size: 14px;
        }

        .es-header-body a {
            color: #1376C8;
            font-size: 14px;
        }

        .es-content-body {
            background-color: #FFFFFF;
        }

        .es-content-body p, .es-content-body ul li, .es-content-body ol li {
            color: #333333;
            font-size: 14px;
        }

        .es-content-body a {
            color: #2CB543;
            /*font-size: 14px;*/
        }

        .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li {
            color: #333333;
            font-size: 14px;
        }

        .es-footer-body a {
            color: #FFFFFF;
            font-size: 14px;
        }

        .es-infoblock a {
            font-size: 12px;
            color: #CCCCCC;
        }

        h1 {
            font-size: 30px;
            font-style: normal;
            font-weight: normal;
            color: #333333;
        }

        h2 {
            font-size: 24px;
            font-style: normal;
            font-weight: normal;
            color: #333333;
        }

        h3 {
            font-size: 20px;
            font-style: normal;
            font-weight: normal;
            color: #333333;
        }

        .es-header-body h1 a, .es-content-body h1 a, .es-footer-body h1 a {
            font-size: 30px;
        }

        .es-header-body h2 a, .es-content-body h2 a, .es-footer-body h2 a {
            font-size: 24px;
        }

        .es-header-body h3 a, .es-content-body h3 a, .es-footer-body h3 a {
            font-size: 20px;
        }
    </style>
</head>
<body>
<div class="es-wrapper-color"> <!--[if gte mso 9]>
    <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
        <v:fill type="tile" color="#f4f3f2"></v:fill>
    </v:background><![endif]-->
    <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td valign="top">
                <table cellpadding="0" cellspacing="0" class="es-content" align="center">
                    <tbody>
                    <tr>
                        <td align="center">
                            <table bgcolor="#ffffff" class="es-content-body" align="center" cellpadding="0"
                                   cellspacing="0" width="600" style="background-color: #ffffff">
                                <tbody>
                                <tr>
                                    <td class="es-p10t es-p10b es-p10r es-p10l" style="background-color: #333333"
                                        bgcolor="#333333" align="left">
                                        <table width="100%" cellspacing="0" cellpadding="0">
                                            <tbody>
                                            <tr>
                                                <td width="580" valign="top" align="center">
                                                    <table width="100%" cellspacing="0" cellpadding="0"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="center" style="font-size: 0px">
                                                                <a href="https://kindredshop.net" target="_blank">
                                                                    <img
                                                                            style="object-fit: contain; width: 100%; height: 39px;max-width: 110px"
                                                                            alt="Logo"
                                                                            src="{{asset('img/logos/logo.png',true)}}">
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p30t es-p5b es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" align="center" valign="top">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="font-size: 16px;font-family: 'merriweather sans', 'helvetica neue', helvetica, arial, sans-serif">
                                                                    <strong><em>PURCHASE INFO:</em></strong></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p25t es-p20r es-p20l" align="left"> <!--[if mso]>
                                        <table width="560" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="194" valign="top"><![endif]-->
                                        <table cellpadding="0" cellspacing="0" class="es-left" align="left">
                                            <tbody>
                                            <tr>
                                                <td width="174" class="es-m-p0r es-m-p20b" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="font-family: roboto, 'helvetica neue', helvetica, arial, sans-serif;color: #696969;font-size: 11px">
                                                                    <strong>AMOUNT</strong></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="es-hidden" width="20"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td>
                                        <td width="150" valign="top"><![endif]-->
                                        <table cellpadding="0" cellspacing="0" class="es-left" align="left">
                                            <tbody>
                                            <tr>
                                                <td width="150" class="es-m-p20b" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left"><p
                                                                        style="font-family: roboto, 'helvetica neue', helvetica, arial, sans-serif;color: #696969;font-size: 11px">
                                                                    <strong>GATEWAY</strong></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td>
                                        <td width="20"></td>
                                        <td width="196" valign="top"><![endif]-->
                                        <table cellpadding="0" cellspacing="0" class="es-right" align="right">
                                            <tbody>
                                            <tr>
                                                <td width="196" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left"><p
                                                                        style="font-family: roboto, 'helvetica neue', helvetica, arial, sans-serif;color: #696969;font-size: 11px">
                                                                    <strong>DATE PURCHASED</strong></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td></tr></table><![endif]--></td>
                                </tr>
                                <tr>
                                    <td class="es-p5t es-p20r es-p20l" align="left"> <!--[if mso]>
                                        <table width="560" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="194" valign="top"><![endif]-->
                                        <table cellpadding="0" cellspacing="0" class="es-left" align="left">
                                            <tbody>
                                            <tr>
                                                <td width="174" class="es-m-p0r es-m-p20b" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l">
                                                                <p style="line-height: 23px;font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size: 15px;white-space: nowrap">
                                                                    $ {{$order->amount}}
                                                                    @if(!is_null($order->coupon))
                                                                        <span>(coupon {{$order->coupon->percent}}%)</span>
                                                                    @endif
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="es-hidden" width="20"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td>
                                        <td width="150" valign="top"><![endif]-->
                                        <table cellpadding="0" cellspacing="0" class="es-left" align="left">
                                            <tbody>
                                            <tr>
                                                <td width="150" class="es-m-p20b" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left">
                                                                <p style="font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size: 15px">
                                                                    {{$order->type ? ucfirst($order->type) : 'undefined' }}
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td>
                                        <td width="20"></td>
                                        <td width="196" valign="top"><![endif]-->
                                        <table cellpadding="0" cellspacing="0" class="es-right" align="right">
                                            <tbody>
                                            <tr>
                                                <td width="196" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left">
                                                                <p style="font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size: 15px">
                                                                    {{$order->created_at->format('d.m.y H:i')}}
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td></tr></table><![endif]--></td>
                                </tr>
                                <tr>
                                    <td class="es-p35t es-p20r es-p20l" align="left"> <!--[if mso]>
                                        <table width="560" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="194" valign="top"><![endif]-->
                                        <table cellpadding="0" cellspacing="0" class="es-left" align="left">
                                            <tbody>
                                            <tr>
                                                <td width="174" class="es-m-p20b" align="left">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="font-family: roboto, 'helvetica neue', helvetica, arial, sans-serif;color: #696969;font-size: 11px">
                                                                    <strong>PRODUCT</strong></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="es-hidden" width="20"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td>
                                        <td width="150" valign="top"><![endif]-->
                                        <table cellpadding="0" cellspacing="0" class="es-left" align="left">
                                            <tbody>
                                            <tr>
                                                <td width="150" align="left" class="es-m-p20b">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left"><p
                                                                        style="font-family: roboto, 'helvetica neue', helvetica, arial, sans-serif;color: #696969;font-size: 11px">
                                                                    <strong>GAME</strong></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td>
                                        <td width="20"></td>
                                        <td width="196" valign="top"><![endif]-->
                                        <table cellpadding="0" cellspacing="0" class="es-right" align="right">
                                            <tbody>
                                            <tr>
                                                <td width="196" align="left">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left"><p
                                                                        style="font-family: roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size: 11px;color: #696969">
                                                                    <strong>WARRANTY</strong></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td></tr></table><![endif]--></td>
                                </tr>
                                <tr>
                                    <td class="es-p5t es-p20r es-p20l" align="left"> <!--[if mso]>
                                        <table width="560" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="194" valign="top"><![endif]-->
                                        <table cellpadding="0" cellspacing="0" class="es-left" align="left">
                                            <tbody>
                                            <tr>
                                                <td width="174" class="es-m-p20b" align="left">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l">
                                                                <p style="font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size: 15px">
                                                                    {{$order->product ? $order->product->title : ''}} {{$order->product &&  $order->product->server ? $order->product->server->name : ''}}
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="es-hidden" width="20"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td>
                                        <td width="150" valign="top"><![endif]-->
                                        <table cellpadding="0" cellspacing="0" class="es-left" align="left">
                                            <tbody>
                                            <tr>
                                                <td width="150" align="left" class="es-m-p20b">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left">
                                                                <p style="font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size: 15px">
                                                                    {{$order->product && $order->product->server && $order->product->server->game? ucfirst($order->product->server->game->nameMatch()) : ''}}
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td>
                                        <td width="20"></td>
                                        <td width="196" valign="top"><![endif]-->
                                        <table cellpadding="0" cellspacing="0" class="es-right" align="right">
                                            <tbody>
                                            <tr>
                                                <td width="196" align="left">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left">
                                                                <p style="font-size: 15px;font-family: roboto, 'helvetica neue', helvetica, arial, sans-serif">
                                                                    @if($order->lifetime) Lifetime @else Limited @endif
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <!--[if mso]></td></tr></table><![endif]--></td>
                                </tr>
                                <tr>
                                    <td class="es-p35t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" align="center" valign="top">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="font-family: roboto, 'helvetica neue', helvetica, arial, sans-serif;color: #696969;font-size: 11px">
                                                                    <strong>ORDER ID</strong></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p5t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" align="center" valign="top">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size: 15px">
                                                                    {{$order->id}}
                                                                </p></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" class="es-p20" style="font-size:0">
                                                                <table border="0" width="100%" cellpadding="0"
                                                                       cellspacing="0" role="presentation">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td style="border-bottom: 1px solid #cccccc;background:none;height:1px;width:100%;margin:0px 0px 0px 0px"></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p10t es-p5b es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" align="center" valign="top">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="font-size: 16px;font-family: 'merriweather sans', 'helvetica neue', helvetica, arial, sans-serif">
                                                                    <strong><em>PRODUCT:</em></strong></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p25t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" class="es-m-p0r" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="font-family: roboto, 'helvetica neue', helvetica, arial, sans-serif;color: #696969;font-size: 11px">
                                                                    <b>LOGIN:PASSWORD&nbsp;</b></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p5t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" class="es-m-p0r" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l">
                                                                <p style="line-height: 23px;font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size: 15px;white-space: pre-line">{!! $order->accountsToString() !!}</p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p25t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" class="es-m-p0r" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="font-family: roboto, 'helvetica neue', helvetica, arial, sans-serif;color: #696969;font-size: 11px">
                                                                    <b>CHANGE PASSWORD AND E-MAIL</b></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p5t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" class="es-m-p0r" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="line-height: 23px;font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size: 15px">
                                                                    <a href="https://account.riotgames.com"
                                                                       style="color: #3d85c6" target="_blank">https://account.riotgames.com</a>
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p25t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" class="es-m-p0r" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="font-family: roboto, 'helvetica neue', helvetica, arial, sans-serif;color: #696969;font-size: 11px">
                                                                    <b>ADDITIONAL INFO</b></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p5t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" class="es-m-p0r" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="font-size: 15px;line-height: 23px;font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif">
                                                                    Copy and paste creditionals&nbsp;without ":" and
                                                                    "space"&nbsp;if
                                                                    you have a problem logging into the game.<br><br>To
                                                                    protect your account&nbsp;we recommend:<br>1.&nbsp;Don’t
                                                                    buy RP about two weeks.<br>2.&nbsp;Don’t play ranked
                                                                    games immediately after purchase, wait about one
                                                                    month.<br>3.&nbsp;Don’t try to contact official LoL
                                                                    support.<br>4.&nbsp;Don’t tell anyone that you
                                                                    bought
                                                                    this account.<br><br>If you have any problems&nbsp;or
                                                                    want buy more with discount, just contact us.</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" class="es-p20" style="font-size:0">
                                                                <table border="0" width="100%" cellpadding="0"
                                                                       cellspacing="0" role="presentation">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td style="border-bottom: 1px solid #cccccc;background:none;height:1px;width:100%;margin:0px 0px 0px 0px"></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p10t es-p5b es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" align="center" valign="top">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="font-size: 16px;font-family: 'merriweather sans', 'helvetica neue', helvetica, arial, sans-serif">
                                                                    <strong><em>CONTACT US:</em></strong></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p25t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" class="es-m-p0r" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="font-family: roboto, 'helvetica neue', helvetica, arial, sans-serif;color: #696969;font-size: 11px">
                                                                    <b>E-MAIL</b></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p5t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" class="es-m-p0r" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="font-size: 15px;line-height: 23px;font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif">
                                                                    sales@kindredshop.net <em><span
                                                                                style="font-size: 11px;color: #000000">or reply to this message</span></em>
                                                                </p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p25t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" class="es-m-p0r" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="font-family: roboto, 'helvetica neue', helvetica, arial, sans-serif;color: #696969;font-size: 11px">
                                                                    <b>LIVECHAT</b></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p5t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" class="es-m-p0r" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="font-size: 15px;line-height: 23px;font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif">
                                                                    Accessible in the bottom right corner of the
                                                                    website</p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p25t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" class="es-m-p0r" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="font-family: roboto, 'helvetica neue', helvetica, arial, sans-serif;color: #696969;font-size: 11px">
                                                                    <b>TELEGRAM</b></p></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="es-p5t es-p20r es-p20l" align="left">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tbody>
                                            <tr>
                                                <td width="560" class="es-m-p0r" align="center">
                                                    <table cellpadding="0" cellspacing="0" width="100%"
                                                           role="presentation">
                                                        <tbody>
                                                        <tr>
                                                            <td align="left" class="es-p30l"><p
                                                                        style="line-height: 23px;font-family: helvetica, 'helvetica neue', arial, verdana, sans-serif;font-size: 15px">
                                                                    <a href="http://telegram.me/kindredsupp"
                                                                       style="color: #3d85c6"
                                                                       target="_blank">@kindredsupp</a></p></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" class="es-p20" style="font-size:0">
                                                                <table border="0" width="100%" cellpadding="0"
                                                                       cellspacing="0" role="presentation">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td style="border-bottom: 1px solid #cccccc;background:none;height:1px;width:100%;margin:0px 0px 0px 0px"></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                @if(!is_null($randomCoupon))
                                    <tr>
                                        <td class="es-p10t es-p30b es-p20r es-p20l" align="left">
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                <tr>
                                                    <td width="560" align="center" valign="top">
                                                        <table cellpadding="0" cellspacing="0" width="100%"
                                                               role="presentation">
                                                            <tbody>
                                                            <tr>
                                                                <td align="left" class="es-p30l">
                                                                    <p style="font-size: 16px;font-family: 'merriweather sans', 'helvetica neue', helvetica, arial, sans-serif">
                                                                        <strong><em><span style="color:#FF0000">{{$randomCoupon->percent}}% </span>COUPON:{{$randomCoupon->code}}
                                                                            </em></strong>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
