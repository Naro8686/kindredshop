<?php echo $version; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc>{{url('/')}}</loc>
        <priority>1.00</priority>
    </url>
    <url>
        <loc>{{url('/terms-and-conditions')}}</loc>
        <priority>0.80</priority>
    </url>
</urlset>
