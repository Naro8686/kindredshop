@extends('layouts.app')
@section('title')
    | {{$seo->title ?? 'Home'}}
@endsection
@section('meta')
    <meta name="description" content="{{$seo->description}}">
    <meta name="keywords" content="{{$seo->keywords}}">
@endsection
@section('content')

    <section class="first-policy">
        
        <div class="container">
            <input type="checkbox" class="menu-check" id="menu-btn">
			<label class="menu-btn" for="menu-btn"><span></span></label>
            <nav class="menu">
            <ul class="menu-wrapper"> 
                <li class="menu-item"><a href="{{ route('home') }}" class="menu-link">Home</a></li>
                <li class="menu-item"><a href="{{ route('home') }}#smurfs" class="menu-link">Smurfs</a></li>
                <li class="menu-item"><a href="{{ route('home') }}#faq" class="menu-link">FAQ</a></li>
                <li class="menu-item menu-item_h">
                    <a href="https://kindredshop.net/terms-and-conditions" class="menu-link">More</a>
                    <ul class="submenu">
                        <li class="submenu-item"><a href="{{ route('terms') }}#terms" class="submenu-item-link">Terms and Conditions</a></li>
                        <li class="submenu-item"><a href="{{ route('terms') }}#privacy-policy" class="submenu-item-link">Privacy Policy</a></li>
                        <li class="submenu-item"><a href="{{ route('terms') }}#contact-details" class="submenu-item-link">Contact</a></li>
                    </ul>
                </li>
            </ul>
            </nav>

            <div class="policy-content">
                <h1 class="policy-content-title">
                    KINDREDSHOP LEGAL
                </h1>
                <p class="policy-content-text">
                    Kindredshop.net is a customer-oriented website, in case of any issues simply get in touch with us and we will sort things out as soon as possible.
                </p>
                <div class="policy-wrapper-links">
                    <a href="#refund" class="policy-link">
                        — <span>Refund and cancellation policy</span>
                    </a>
                    <a href="#privacy-policy" class="policy-link">
                        — <span>Privacy policy</span>
                    </a>
                    <a href="#terms" class="policy-link">
                        — <span>Terms and Conditions</span>
                    </a>
                    <a href="#contact-details" class="policy-link">
                        — <span>Contact details</span>
                    </a>
                </div>
            </div>
    </section>
    <section class="main-content-policy">
        <div class="refund" id="refund">
            <div class="container">
                <h2 class="refund-title">
                    REFUND AND CANCELLATION POLICY
                </h2>
                <p class="updated">
                    Last Updated 1 May 2021
                </p>
                <h3 class="refund-subtitle">
                    CANCELLATION
                </h3>
                <p class="refund-text">
                    You can cancel your order and receive a refund if you haven't yet used the product.
                </p>
                <h3 class="refund-subtitle">
                    REFUNDS & REPLACEMENTS
                </h3>
                <p class="refund-text">
                    In the occurrence of the delivered product being faulty, not meeting its description or becoming
                    faulty within its warranty period, we will (1) replace it, (2) refund you or (3) partially refund
                    you and allow to keep the product. You can choose the exact form of compensation.
                </p>
                <p class="refund-text">
                    Warranties on our products only cover defects that the customer did not negatively contribute to
                    with his activity.
                </p>
                <p class="refund-text">
                    Product warranties might have different binding periods with "Lifetime warranty" referring to
                    warranty for as long as our website remains in operation but not less than 6 months.
                </p>
            </div>
        </div>
        <div class="privacy-policy" id="privacy-policy">
            <div class="container">
                <h2 class="refund-title">
                    PRIVACY POLICY
                </h2>
                <p class="updated">
                    Last Updated 1 May 2021
                </p>
                <h3 class="refund-subtitle">
                    COOKIES
                </h3>
                <p class="refund-text">
                    Cookies created by kindredshop.net serve functionality purposes (such as currency management or form pre-filling). We also use cookies from 3rd party vendors, namely Bing, Facebook, Google, Tidio, Hotjar, and Stripe. Those cookies serve marketing, analytical and functionality purposes.
                </p>
                <p class="refund-text">
                    You can disable cookies in every browser. To do so, please follow the instructions from your browser provider. Note that after doing so, you won't be able to use websites whose functionality relies on cookies.
                </p>
                <h3 class="refund-subtitle">
                    PERSONAL INFORMATION
                </h3>
                <p class="refund-text">
                    We store your e-mail address, IP address, and country code of the IP address to maintain order history.
                </p>
                <p class="refund-text">
                    You may withdraw your e-mail address from our database at any time by sending an inquiry to sales@kindredshop.net.
                </p>
                <p class="refund-text">
                    kindredshop.net does not store any of your payment information. Storing such information is the duty of our payment processors.
                </p>
            </div>
        </div>
        <div class="terms" id="terms">
            <div class="container">
                <h2 class="refund-title">
                    TERMS AND CONDITIONS
                </h2>
                <p class="updated">
                    Last Updated 1 May 2021
                </p>
                <h3 class="refund-subtitle">
                    GENERAL
                </h3>
                <p class="refund-text">
                    The site provides the following services: sale of digital goods in online games (services), which are obtained in form of rewards for progressing in them.
                </p>
                <p class="refund-text">
                    All services are delivered digitally, via e-mail.
                </p>
                <p class="refund-text">
                    The site collects instant, one-time payments only.
                </p>
                <p class="refund-text">
                    You agree that by accessing the site and/or services, you have read, understood, and agree to be bound by all of these Terms and Conditions. If you do not agree with all of these Terms and Conditions, then you are prohibited from using the site and services and you must discontinue use immediately. We recommend that you print a copy of these Terms and Conditions for future reference.
                </p>
                <p class="refund-text">
                    We may make changes to these Terms and Conditions at any time. The updated version of these Terms and Conditions will be indicated by an updated “Revised” date and the updated version will be effective as soon as it is accessible. You are responsible for reviewing these Terms and Conditions to stay informed of updates. Your continued use of the site represents that you have accepted such changes.
                </p>
                <p class="refund-text">
                    We may update or change the site from time to time to reflect changes to our products, our users' needs and/or our business priorities.
                </p>
                <p class="refund-text">
                    The site is intended for users who are at least 18 years old. If you are under the age of 18, you are not permitted to use the services without parental permission.
                </p>
                <p class="refund-text">
                    Additional policies which also apply to your use of the site include:
                </p>
                <p class="refund-text">
                    Our privacy policy (https://kindredshop.net/terms-and-conditions#privacy-policy), which sets out the terms on which we process any personal data we collect from you, or that you provide to us. By using the site, you consent to such processing and you warrant that all data provided by you is accurate.
                </p>
                <p class="refund-text">
                    Our refund and cancellation policy (https://kindredshop.net/terms-and-conditions#refund-policy), which describe our warranty and refund policies for the goods sold on our website.
                </p>
                <h3 class="refund-subtitle">
                    OUR CONTENT
                </h3>
                <p class="refund-text">
                    Unless otherwise indicated, the site and services including source code, databases, functionality, software, website designs, audio, video, text, photographs, and graphics on the site (Our Content) are owned or licensed to us, and are protected by copyright and trade mark laws.
                </p>
                <p class="refund-text">
                    Except as expressly provided in these Terms and Conditions, no part of the site, services or our content may be copied, reproduced, aggregated, republished, uploaded, posted, publicly displayed, encoded, translated, transmitted, distributed, sold, licensed, or otherwise exploited for any commercial purpose whatsoever, without our express prior written permission.
                </p>
                <p class="refund-text">
                    Provided that you are eligible to use the site, you are granted a limited licence to access and use the site and our content and to download or print a copy of any portion of the content to which you have properly gained access solely for your personal, non-commercial use.
                </p>
                <p class="refund-text">
                    You shall not (a) try to gain unauthorised access to the site or any networks, servers or computer systems connected to the site; and/or (b) make for any purpose including error correction, any modifications, adaptions, additions or enhancements to the site or our content, including the modification of the paper or digital copies you may have downloaded.
                </p>
                <p class="refund-text">
                    Although we make reasonable efforts to update the information on our site, we make no representations, warranties or guarantees, whether express or implied, that our content on the site is accurate, complete or up to date.
                </p>
                <h3 class="refund-subtitle">
                    SITE MANAGEMENT
                </h3>
                <p class="refund-text">
                    We do not guarantee that the site will be secure or free from bugs or viruses. You are responsible for configuring your information technology, computer programs and platform to access the site and you should use your own virus protection software.
                </p>
                <h3 class="refund-subtitle">
                    MODIFICATIONS TO AND AVAILABILITY OF THE SITE
                </h3>
                <p class="refund-text">
                    We reserve the right to change, modify, or remove the contents of the site at any time or for any reason at our sole discretion without notice. We also reserve the right to modify or discontinue all or part of the services without notice at any time.
                </p>
                <p class="refund-text">
                    We cannot guarantee the site and services will be available at all times. We may experience hardware, software, or other problems or need to perform maintenance related to the site, resulting in interruptions, delays, or errors. You agree that we have no liability whatsoever for any loss, damage, or inconvenience caused by your inability to access or use the site or services during any downtime or discontinuance of the site or services.
                </p>
                <p class="refund-text">
                    We are not obliged to maintain and support the site or services or to supply any corrections, updates, or releases.
                </p>
                <p class="refund-text">
                    There may be information on the site that contains typographical errors, inaccuracies, or omissions that may relate to the services, including descriptions, pricing, availability, and various other information. We reserve the right to correct any errors, inaccuracies, or omissions and to change or update the information at any time, without prior notice.
                </p>
                <h3 class="refund-subtitle">
                    COMPLAINTS
                </h3>
                <p class="refund-text">
                    In order to resolve a complaint regarding the Services, please contact us by email at sales@kindredshop.net.
                </p>
                <h3 class="refund-subtitle">
                    DISCLAIMER/LIMITATION OF LIABILITY
                </h3>
                <p class="refund-text">
                    The site and services are provided on an as-is and as-available basis. You agree that your use of the site and/or services will be at your sole risk except as expressly set out in these Terms and Conditions. All warranties, terms, conditions and undertakings, express or implied (including by statute, custom or usage, a course of dealing, or common law) in connection with the site and services and your use thereof including, without limitation, the implied warranties of satisfactory quality, fitness for a particular purpose and non-infringement are excluded to the fullest extent permitted by applicable law.
                </p>
                <p class="refund-text">
                    We make no warranties or representations about the accuracy or completeness of the site’s content and are not liable for any (1) errors or omissions in content; (2) any unauthorized access to or use of our servers and/or any and all personal information stored on our server; (3) any interruption or cessation of transmission to or from the site or services; and/or (4) any bugs, viruses, trojan horses, or the like which may be transmitted to or through the site by any third party. We will not be responsible for any delay or failure to comply with our obligations under these Terms and Conditions if such delay or failure is caused by an event beyond our reasonable control.
                </p>
                <h3 class="refund-subtitle">
                    TERM AND TERMINATION
                </h3>
                <p class="refund-text">
                    These Terms and Conditions shall remain in full force and effect while you use the site or services or are otherwise a user of the site, as applicable. You may terminate your use or participation at any time, for any reason, by following the instructions for terminating user accounts in your account settings, if available, or by contacting us at sales@kindredshop.net.
                </p>
                <p class="refund-text">
                    Without limiting any other provision of these Terms and Conditions, we reserve the right to, in our sole discretion and without notice or liability, deny access to and use of the site and the services (including blocking certain IP addresses), to any person for any reason including without limitation for breach of any representation, warranty or covenant contained in these Terms and Conditions or of any applicable law or regulation.
                </p>
            </div>
        </div>
        <div class="contact-details" id="contact-details">
            <div class="container">
                <h2 class="refund-title">
                    CONTACT DETAILS
                </h2>
                <p class="contact-details-text">
                    E-mail: <a href="mailto:sales@kindredshop.net">sales@kindredshop.net</a>
                </p>
                <p class="contact-details-text">
                    Live Chat: <span>Accessible in the bottom right corner of the website</span>
                </p>
                <p class="contact-details-text">
                    Site information: <span>Digisale & Webdesign Individual entrepreneur Sharov A.A.</span>
                </p>
            </div>
        </div>
    </section>

@endsection

@section('cookiebeware')
@if ($closedbeware != 'true')
<div class="message-cookies">
    <p class="message-cookies-text">
        By browsing, you agree to our use of cookies 🍪
    </p>
    <div class="after">+</div>
</div>
@endif
@endsection