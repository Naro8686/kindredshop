@extends('layouts.app')
@section('title')
| {{$seo->title ?? 'Home'}}
@endsection
@section('meta')
    <meta name="description" content="{{$seo->description}}">
    <meta name="keywords" content="{{$seo->keywords}}">
@endsection
@section('content')

    @include('blocks.header')

    <section class="second" id="smurfs">
        
        <div class="container">

            <div class="second-wrapper">

                <div class="second-left-wrapper">

                    <h2 class="second-title">Buy an Unranked Smurf now!</h2>

                    <p class="second-text">Accounts with high amounts of Blue Essence.</p>

                    <div id="servlist">

                        <div class="server">
                            
                            <h3 class="server-title">CHOOSE SERVER</h3>

                            <div class="server-wrapper">
                            @foreach ($servers as $server)
                                @if ($loop->first)
                                <div class="server-card server-card_yellow" data-servid="{{ $server->id }}">
                                @else
                                <div class="server-card" data-servid="{{ $server->id }}">
                                @endif
                                    <span class="card-name">
                                    {{ $server->name }}
                                    </span>
                                    <p class="card-country">
                                    {{ $server->description }}
                                    </p>
                                </div>
                            @endforeach
                            </div>

                        </div>

                    </div>

                    <div id="prodlist"></div>

                </div> 
                
                <div id="summary" class="second-right-wrapper"></div>

            </div>  

        </div>

    </section>

    @include('blocks.benefits')

    @include('blocks.faq')

    @include('blocks.about_us')

    <div id="befmodal"></div>

@endsection

@section('cookiebeware')
@if ($closedbeware != 'true')
<div class="message-cookies">
    <p class="message-cookies-text">
        By browsing, you agree to our use of cookies 🍪
    </p>
    <div class="after">+</div>
</div>
@endif
@endsection